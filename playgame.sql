-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2018 at 04:16 PM
-- Server version: 5.7.19-log
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `playgame`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `image_file_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_file_size` int(11) DEFAULT NULL,
  `image_content_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `role_id`, `image_file_name`, `image_file_size`, `image_content_type`, `image_updated_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'mostafa', 'mostafa.zeinivand@gmail.com', '$2y$10$vRuvGJHYJiiX5Jt/YSZxKecKSsOpI7isGoPs.PKt0Wcf143bkQfLC', 1, NULL, NULL, NULL, NULL, 'rRZGPrJQZhU9FoJrwFraWTEbu8759LQ3QY2J7AA7pNiTviyj1q83qqPHKMkV', '2018-05-08 06:07:46', '2018-05-08 06:07:46'),
(2, 'ramin', 'rkhatibi@gmail.com', '$2y$10$anUFhEM8a4SdoHKAwXjnZe5rai4w00NS/ZaHtxhaGisqo2RNfEGMa', 1, NULL, NULL, NULL, NULL, NULL, '2018-05-08 06:07:46', '2018-05-08 06:07:46');

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `published` tinyint(1) NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `updater_id` int(10) UNSIGNED DEFAULT NULL,
  `deleter_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updater_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleter_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bookmarks`
--

CREATE TABLE `bookmarks` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `bookmarkable_id` int(10) UNSIGNED NOT NULL,
  `bookmarkable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bookmarkable_sub_type` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text NOT NULL,
  `description` text DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `hex` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `views` int(11) NOT NULL,
  `games` int(10) UNSIGNED NOT NULL,
  `bookmarks` int(11) NOT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `updater_id` int(10) UNSIGNED DEFAULT NULL,
  `deleter_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updater_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleter_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `slug`, `name`, `description`, `type`, `hex`, `views`, `games`, `bookmarks`, `_lft`, `_rgt`, `parent_id`, `creator_id`, `updater_id`, `deleter_id`, `creator_ip`, `updater_ip`, `deleter_ip`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'cards', '{\"fa\": \"cards\"}', NULL, 1, '#661866', 0, 1, 0, 1, 2, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 06:50:26', '2018-05-08 14:02:08', NULL),
(2, 'puzzle', '{\"fa\": \"Puzzle\"}', NULL, 1, '#8c248c', 0, 2, 0, 3, 4, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:21:26', '2018-05-08 14:02:08', NULL),
(3, 'casual', '{\"fa\": \"Casual\"}', NULL, 1, '#5e185e', 0, 1, 0, 5, 6, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:21:42', '2018-05-08 14:02:08', NULL),
(4, 'arcade', '{\"fa\": \"Arcade\"}', NULL, 1, '#471747', 0, 0, 0, 7, 8, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:21:50', '2018-05-08 11:21:50', NULL),
(5, 'sport', '{\"fa\": \"Sport\"}', NULL, 1, '#851c85', 0, 0, 0, 9, 10, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:26:04', '2018-05-08 11:26:04', NULL),
(6, 'match', '{\"fa\": \"Match\"}', NULL, 1, '#de74de', 0, 0, 0, 11, 12, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:26:19', '2018-05-08 11:26:19', NULL),
(7, 'racing', '{\"fa\": \"Racing\"}', NULL, 1, '#ad29ad', 0, 0, 0, 13, 14, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:26:26', '2018-05-08 11:26:26', NULL),
(8, 'casino', '{\"fa\": \"Casino\"}', NULL, 1, '#6fd4a3', 0, 0, 0, 15, 16, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:26:39', '2018-05-08 11:26:39', NULL),
(9, 'adventure', '{\"fa\": \"Adventure\"}', NULL, 1, '#381038', 0, 1, 0, 17, 18, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:26:46', '2018-05-08 11:32:23', NULL),
(10, 'action', '{\"fa\": \"Action\"}', NULL, 1, '#12b37d', 0, 0, 0, 19, 20, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:26:57', '2018-05-08 11:26:57', NULL),
(11, 'memory', '{\"fa\": \"Memory\"}', NULL, 1, '#5b9137', 0, 1, 0, 21, 22, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:27:06', '2018-05-08 14:02:08', NULL),
(12, 'educational', '{\"fa\": \"Educational\"}', NULL, 1, '#e00d37', 0, 1, 0, 23, 24, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:27:18', '2018-05-08 11:32:23', NULL),
(13, 'pool-casual', '{\"fa\": \"Pool Casual\"}', NULL, 1, '#689e21', 0, 1, 0, 25, 26, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:27:31', '2018-05-08 11:32:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categorizables`
--

CREATE TABLE `categorizables` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `categorizable_id` int(10) UNSIGNED NOT NULL,
  `categorizable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorizables`
--

INSERT INTO `categorizables` (`category_id`, `categorizable_id`, `categorizable_type`, `created_at`, `updated_at`) VALUES
(1, 8, 'App\\Game', '2018-05-08 14:02:08', '2018-05-08 14:02:08'),
(2, 6, 'App\\Game', '2018-05-08 11:30:57', '2018-05-08 11:30:57'),
(2, 8, 'App\\Game', '2018-05-08 14:02:08', '2018-05-08 14:02:08'),
(3, 8, 'App\\Game', '2018-05-08 11:38:44', '2018-05-08 11:38:44'),
(9, 7, 'App\\Game', '2018-05-08 11:32:23', '2018-05-08 11:32:23'),
(11, 8, 'App\\Game', '2018-05-08 11:38:44', '2018-05-08 11:38:44'),
(12, 7, 'App\\Game', '2018-05-08 11:32:23', '2018-05-08 11:32:23'),
(13, 7, 'App\\Game', '2018-05-08 11:32:23', '2018-05-08 11:32:23');

-- --------------------------------------------------------

--
-- Table structure for table `device_token`
--

CREATE TABLE `device_token` (
  `id` int(10) UNSIGNED NOT NULL,
  `token_id` int(10) UNSIGNED DEFAULT NULL,
  `reg_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `force_update`
--

CREATE TABLE `force_update` (
  `id` int(10) UNSIGNED NOT NULL,
  `latest` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `minimum` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device` tinyint(4) NOT NULL,
  `updater_id` int(10) UNSIGNED DEFAULT NULL,
  `updater_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `force_update`
--

INSERT INTO `force_update` (`id`, `latest`, `minimum`, `device`, `updater_id`, `updater_ip`, `created_at`, `updated_at`) VALUES
(1, '1.0', '1.0', 1, NULL, NULL, '2018-05-08 06:07:45', '2018-05-08 06:07:45'),
(2, '1.0', '1.0', 2, NULL, NULL, '2018-05-08 06:07:45', '2018-05-08 06:07:45');

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE `games` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `supported_browsers` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `editor_choice` tinyint(1) NOT NULL,
  `bookmarks` int(10) UNSIGNED NOT NULL,
  `likes` int(10) UNSIGNED NOT NULL,
  `views` int(10) UNSIGNED NOT NULL,
  `published` tinyint(1) NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `updater_id` int(10) UNSIGNED DEFAULT NULL,
  `deleter_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updater_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleter_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `games`
--

INSERT INTO `games` (`id`, `title`, `link`, `description`, `supported_browsers`, `editor_choice`, `bookmarks`, `likes`, `views`, `published`, `creator_id`, `updater_id`, `deleter_id`, `creator_ip`, `updater_ip`, `deleter_ip`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'تلگرام', 'http://google.com', 'اپلیکیشن چت، تماس صوتی و خبررسانی', '[\"1\",\"3\",\"4\"]', 1, 0, 0, 0, 1, 1, 1, 1, '127.0.0.1', '127.0.0.1', '127.0.0.1', '2018-05-08 07:34:03', '2018-05-08 11:45:57', '2018-05-08 11:45:57'),
(2, 'test', '', NULL, NULL, 0, 0, 0, 0, 1, 1, 1, 1, '127.0.0.1', '127.0.0.1', '127.0.0.1', '2018-05-08 07:48:19', '2018-05-08 07:50:58', '2018-05-08 07:50:58'),
(3, 'mgame', '', NULL, '[]', 0, 0, 0, 0, 0, 1, 1, 1, '127.0.0.1', '127.0.0.1', '127.0.0.1', '2018-05-08 10:37:40', '2018-05-08 10:55:51', '2018-05-08 10:55:51'),
(4, 'mgame', '', NULL, '[\"1\",\"3\",\"4\",\"5\",\"7\"]', 0, 0, 0, 0, 1, 1, 1, 1, '127.0.0.1', '127.0.0.1', '127.0.0.1', '2018-05-08 10:42:32', '2018-05-08 10:55:46', '2018-05-08 10:55:46'),
(5, 'mgame', 'http://google.com', NULL, '[\"2\"]', 0, 0, 0, 0, 0, 1, 1, 1, '127.0.0.1', '127.0.0.1', '127.0.0.1', '2018-05-08 11:13:31', '2018-05-08 11:45:54', '2018-05-08 11:45:54'),
(6, '3+3', 'http://google.com', 'Stack the Ancient Greek Architectural columns one over the other and try to build highest tower', '[\"1\",\"4\",\"6\"]', 0, 0, 0, 0, 1, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:30:57', '2018-05-08 11:30:57', NULL),
(7, 'Animal Crush', 'http://google.com', 'Stack the Ancient Greek Architectural columns one over the other and try to build highest tower', '[\"3\",\"5\",\"6\"]', 0, 0, 0, 0, 1, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:32:23', '2018-05-08 11:32:23', NULL),
(8, 'Animals', 'http://google.com', 'Stack the Ancient Greek Architectural columns one over the other and try to build highest tower', '[]', 0, 0, 0, 0, 1, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:38:44', '2018-05-08 11:38:44', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `game_views`
--

CREATE TABLE `game_views` (
  `id` int(10) UNSIGNED NOT NULL,
  `game_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `likable_id` int(10) UNSIGNED NOT NULL,
  `likable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `likable_sub_type` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `file_file_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_file_size` int(11) DEFAULT NULL,
  `file_content_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_updated_at` timestamp NULL DEFAULT NULL,
  `thumbnail_file_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail_file_size` int(11) DEFAULT NULL,
  `thumbnail_content_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail_updated_at` timestamp NULL DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `updater_id` int(10) UNSIGNED DEFAULT NULL,
  `deleter_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updater_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleter_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `file_file_name`, `file_file_size`, `file_content_type`, `file_updated_at`, `thumbnail_file_name`, `thumbnail_file_size`, `thumbnail_content_type`, `thumbnail_updated_at`, `type`, `width`, `height`, `duration`, `creator_id`, `updater_id`, `deleter_id`, `creator_ip`, `updater_ip`, `deleter_ip`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'blue.png', 176215, 'image/png', '2018-05-08 07:23:25', NULL, NULL, NULL, NULL, 1, 2560, 1600, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 07:23:25', '2018-05-08 07:23:25', NULL),
(2, 'Energy_sunset.png', 52668, 'image/png', '2018-05-08 07:39:11', NULL, NULL, NULL, NULL, 1, 2560, 1600, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 07:39:11', '2018-05-08 07:39:11', NULL),
(3, 'landing-1.png', 3967, 'image/png', '2018-05-08 11:30:37', NULL, NULL, NULL, NULL, 1, 80, 80, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:30:37', '2018-05-08 11:30:37', NULL),
(4, 'landing-3.png', 14626, 'image/png', '2018-05-08 11:32:07', NULL, NULL, NULL, NULL, 1, 80, 80, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:32:07', '2018-05-08 11:32:07', NULL),
(5, 'landing-2.png', 11053, 'image/png', '2018-05-08 11:32:11', NULL, NULL, NULL, NULL, 1, 80, 80, NULL, 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:32:11', '2018-05-08 11:32:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mediables`
--

CREATE TABLE `mediables` (
  `media_id` int(10) UNSIGNED NOT NULL,
  `mediable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mediable_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(75) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `updater_id` int(10) UNSIGNED DEFAULT NULL,
  `deleter_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updater_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleter_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mediables`
--

INSERT INTO `mediables` (`media_id`, `mediable_type`, `mediable_id`, `field`, `creator_id`, `updater_id`, `deleter_id`, `creator_ip`, `updater_ip`, `deleter_ip`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'App\\Game', 6, 'thumbnail', 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:30:57', '2018-05-08 11:30:57', NULL),
(4, 'App\\Game', 8, 'thumbnail', 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:38:44', '2018-05-08 11:38:44', NULL),
(5, 'App\\Game', 7, 'thumbnail', 1, NULL, NULL, '127.0.0.1', NULL, NULL, '2018-05-08 11:32:23', '2018-05-08 11:32:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_01_07_073615_create_tagged_table', 1),
(2, '2014_01_07_073615_create_tags_table', 1),
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2016_06_29_073615_create_tag_groups_table', 1),
(6, '2016_06_29_073615_update_tags_table', 1),
(7, '2016_12_17_114551_create_categories_table', 1),
(8, '2016_12_17_119816_create_categorizables_table', 1),
(9, '2017_01_04_103131_user_token', 1),
(10, '2017_01_24_133752_DeviceToken', 1),
(11, '2017_03_04_122541_ForceUpdate', 1),
(12, '2017_06_13_053146_settings', 1),
(13, '2017_07_31_180003_admins', 1),
(14, '2017_08_01_172647_rules', 1),
(15, '2017_09_17_064345_bookmarks', 1),
(16, '2017_10_10_135451_user_payload', 1),
(17, '2017_10_16_072615_user_purchases', 1),
(18, '2017_11_09_074400_categories', 1),
(19, '2018_01_27_095813_notifications', 1),
(20, '2018_02_28_061149_mobile_terminated', 1),
(21, '2018_02_28_135502_user_purchase_mci', 1),
(22, '2018_03_12_094804_pages', 1),
(23, '2018_03_13_082331_media', 1),
(24, '2018_03_17_062310_mediables', 1),
(25, '2018_04_09_100119_ads', 1),
(26, '2018_04_17_083037_likes', 1),
(27, '2018_04_25_060029_games', 1),
(28, '2018_05_08_061614_game_views', 2);

-- --------------------------------------------------------

--
-- Table structure for table `mobile_terminated`
--

CREATE TABLE `mobile_terminated` (
  `id` int(10) UNSIGNED NOT NULL,
  `msisdn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `message_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_status` tinyint(4) DEFAULT NULL,
  `subscription_status` tinyint(1) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `notificable_id` int(10) UNSIGNED DEFAULT NULL,
  `device_type` tinyint(4) NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `title`, `message`, `url`, `image`, `type`, `notificable_id`, `device_type`, `creator_id`, `creator_ip`, `created_at`, `updated_at`) VALUES
(1, 'test', NULL, 'http://playgame.site/posts/2', 'http://playgame.site/uploads/App/Media/files/000/000/001/original/blue.png', 2, 2, 3, 1, '127.0.0.1', '2018-05-08 07:50:47', '2018-05-08 07:50:47');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `updater_id` int(10) UNSIGNED DEFAULT NULL,
  `deleter_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updater_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleter_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'su', '{\"sections\":{\"UserController\":{\"read\":\"1\",\"write\":\"1\",\"delete\":\"1\",\"update\":\"1\",\"publish\":\"0\"},\"CategoryController\":{\"read\":\"1\",\"write\":\"1\",\"delete\":\"1\",\"update\":\"1\",\"publish\":\"0\"},\"TagController\":{\"read\":\"1\",\"write\":\"1\",\"delete\":\"1\",\"update\":\"1\",\"publish\":\"0\"},\"GameController\":{\"read\":\"1\",\"write\":\"1\",\"delete\":\"1\",\"update\":\"1\",\"publish\":\"1\"},\"AdminController\":{\"read\":\"1\",\"write\":\"1\",\"delete\":\"1\",\"update\":\"1\",\"publish\":\"0\"},\"RoleController\":{\"read\":\"1\",\"write\":\"1\",\"delete\":\"1\",\"update\":\"1\",\"publish\":\"0\"},\"MediaController\":{\"read\":\"1\",\"write\":\"1\",\"delete\":\"1\",\"update\":\"1\",\"publish\":\"0\"},\"PageController\":{\"read\":\"1\",\"write\":\"1\",\"delete\":\"1\",\"update\":\"1\",\"publish\":\"1\"},\"ForceUpdateController\":{\"read\":\"1\",\"write\":\"1\",\"delete\":\"1\",\"update\":\"1\",\"publish\":\"0\"},\"SettingsController\":{\"read\":\"1\",\"write\":\"1\",\"delete\":\"1\",\"update\":\"1\",\"publish\":\"0\"}},\"dynamic\":{\"toggle\":{\"GameCategory\":[\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\"]}}}', '2018-05-08 00:00:00', '2018-05-08 11:27:31');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_keywords` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon_file_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon_file_size` int(11) DEFAULT NULL,
  `favicon_content_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon_updated_at` timestamp NULL DEFAULT NULL,
  `image_file_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_file_size` int(11) DEFAULT NULL,
  `image_content_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_updated_at` timestamp NULL DEFAULT NULL,
  `updater_id` int(10) UNSIGNED DEFAULT NULL,
  `updater_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `site_description`, `site_keywords`, `favicon_file_name`, `favicon_file_size`, `favicon_content_type`, `favicon_updated_at`, `image_file_name`, `image_file_size`, `image_content_type`, `image_updated_at`, `updater_id`, `updater_ip`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-08 06:07:45', '2018-05-08 06:07:45');

-- --------------------------------------------------------

--
-- Table structure for table `tagging_tagged`
--

CREATE TABLE `tagging_tagged` (
  `id` int(10) UNSIGNED NOT NULL,
  `taggable_id` int(10) UNSIGNED NOT NULL,
  `taggable_type` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_slug` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tagging_tagged`
--

INSERT INTO `tagging_tagged` (`id`, `taggable_id`, `taggable_type`, `tag_name`, `tag_slug`, `created_at`) VALUES
(2, 6, 'App\\Game', '3+3', '3-3', '2018-05-08 11:30:57'),
(3, 6, 'App\\Game', 'Puzzle', 'puzzle', '2018-05-08 11:30:57'),
(4, 7, 'App\\Game', 'Animals', 'animals', '2018-05-08 11:32:23'),
(5, 7, 'App\\Game', 'Farm', 'farm', '2018-05-08 11:32:23'),
(6, 8, 'App\\Game', 'Animal', 'animal', '2018-05-08 11:38:44');

-- --------------------------------------------------------

--
-- Table structure for table `tagging_tags`
--

CREATE TABLE `tagging_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag_group_id` int(10) UNSIGNED DEFAULT NULL,
  `slug` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suggest` tinyint(1) NOT NULL DEFAULT '0',
  `count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `views` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tagging_tags`
--

INSERT INTO `tagging_tags` (`id`, `tag_group_id`, `slug`, `name`, `suggest`, `count`, `views`) VALUES
(2, NULL, '3-3', '3+3', 0, 1, 0),
(3, NULL, 'puzzle', 'Puzzle', 0, 1, 0),
(4, NULL, 'animals', 'Animals', 0, 1, 0),
(5, NULL, 'farm', 'Farm', 0, 1, 0),
(6, NULL, 'animal', 'Animal', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tagging_tag_groups`
--

CREATE TABLE `tagging_tag_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `msisdn` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_file_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_file_size` int(11) DEFAULT NULL,
  `image_content_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_updated_at` timestamp NULL DEFAULT NULL,
  `unsubscribed_sms_at` datetime DEFAULT NULL,
  `verification_code` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verification_expire` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `notification_status` tinyint(1) NOT NULL,
  `mobile_service_provider` tinyint(4) NOT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleter_id` int(10) UNSIGNED DEFAULT NULL,
  `deleter_ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_payload`
--

CREATE TABLE `user_payload` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_purchases`
--

CREATE TABLE `user_purchases` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_purchase_mci`
--

CREATE TABLE `user_purchase_mci` (
  `id` int(10) UNSIGNED NOT NULL,
  `msisdn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `channel` tinyint(4) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `event_id` int(11) DEFAULT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `method` tinyint(4) NOT NULL DEFAULT '3',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_token`
--

CREATE TABLE `user_token` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `device_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` tinyint(4) NOT NULL,
  `ip` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_ip` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookmarks`
--
ALTER TABLE `bookmarks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookmarks_user_id_foreign` (`user_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`);

--
-- Indexes for table `categorizables`
--
ALTER TABLE `categorizables`
  ADD UNIQUE KEY `categorizables_ids_type_unique` (`category_id`,`categorizable_id`,`categorizable_type`),
  ADD KEY `categorizables_categorizable_id_categorizable_type_index` (`categorizable_id`,`categorizable_type`);

--
-- Indexes for table `device_token`
--
ALTER TABLE `device_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `device_token_token_id_foreign` (`token_id`);

--
-- Indexes for table `force_update`
--
ALTER TABLE `force_update`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_views`
--
ALTER TABLE `game_views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `game_views_game_id_foreign` (`game_id`),
  ADD KEY `game_views_user_id_foreign` (`user_id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `likes_user_id_foreign` (`user_id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mediables`
--
ALTER TABLE `mediables`
  ADD PRIMARY KEY (`media_id`,`mediable_type`,`mediable_id`,`field`),
  ADD KEY `mediables_mediable_id_mediable_type_index` (`mediable_id`,`mediable_type`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile_terminated`
--
ALTER TABLE `mobile_terminated`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tagging_tagged`
--
ALTER TABLE `tagging_tagged`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tagging_tagged_taggable_id_index` (`taggable_id`),
  ADD KEY `tagging_tagged_taggable_type_index` (`taggable_type`),
  ADD KEY `tagging_tagged_tag_slug_index` (`tag_slug`);

--
-- Indexes for table `tagging_tags`
--
ALTER TABLE `tagging_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tagging_tags_slug_index` (`slug`),
  ADD KEY `tagging_tags_tag_group_id_foreign` (`tag_group_id`);

--
-- Indexes for table `tagging_tag_groups`
--
ALTER TABLE `tagging_tag_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tagging_tag_groups_slug_index` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_payload`
--
ALTER TABLE `user_payload`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_payload_user_id_foreign` (`user_id`);

--
-- Indexes for table `user_purchases`
--
ALTER TABLE `user_purchases`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_purchases_token_type_unique` (`token`,`type`),
  ADD KEY `user_purchases_user_id_foreign` (`user_id`);

--
-- Indexes for table `user_purchase_mci`
--
ALTER TABLE `user_purchase_mci`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_token_user_id_device_id_unique` (`user_id`,`device_id`),
  ADD UNIQUE KEY `user_token_token_unique` (`token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bookmarks`
--
ALTER TABLE `bookmarks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `device_token`
--
ALTER TABLE `device_token`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `force_update`
--
ALTER TABLE `force_update`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `games`
--
ALTER TABLE `games`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `game_views`
--
ALTER TABLE `game_views`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `mobile_terminated`
--
ALTER TABLE `mobile_terminated`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tagging_tagged`
--
ALTER TABLE `tagging_tagged`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tagging_tags`
--
ALTER TABLE `tagging_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tagging_tag_groups`
--
ALTER TABLE `tagging_tag_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_payload`
--
ALTER TABLE `user_payload`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_purchases`
--
ALTER TABLE `user_purchases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_purchase_mci`
--
ALTER TABLE `user_purchase_mci`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookmarks`
--
ALTER TABLE `bookmarks`
  ADD CONSTRAINT `bookmarks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categorizables`
--
ALTER TABLE `categorizables`
  ADD CONSTRAINT `categorizables_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `device_token`
--
ALTER TABLE `device_token`
  ADD CONSTRAINT `device_token_token_id_foreign` FOREIGN KEY (`token_id`) REFERENCES `user_token` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `game_views`
--
ALTER TABLE `game_views`
  ADD CONSTRAINT `game_views_game_id_foreign` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`),
  ADD CONSTRAINT `game_views_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `likes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mediables`
--
ALTER TABLE `mediables`
  ADD CONSTRAINT `mediables_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tagging_tags`
--
ALTER TABLE `tagging_tags`
  ADD CONSTRAINT `tagging_tags_tag_group_id_foreign` FOREIGN KEY (`tag_group_id`) REFERENCES `tagging_tag_groups` (`id`);

--
-- Constraints for table `user_payload`
--
ALTER TABLE `user_payload`
  ADD CONSTRAINT `user_payload_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_purchases`
--
ALTER TABLE `user_purchases`
  ADD CONSTRAINT `user_purchases_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_token`
--
ALTER TABLE `user_token`
  ADD CONSTRAINT `user_token_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
