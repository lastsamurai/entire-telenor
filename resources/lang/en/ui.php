<?php

return [
    'text' => [
        'dashboard'                 => 'Dashboard',
        'user-profile'              => 'User Profile',
        'not-defined'               => 'not defined',
        'row'                       => 'Row',
        'name'                      => 'Name',
        'name_ur'                      => 'Urdu Name',
        'title'                     => 'Title',
        'email'                     => 'Email',
        'phone'                     => 'Phone',
        'status'                    => 'Status',
        'mtn'                       => 'Irancell',
        'mci'                       => 'MCI',
        'operator'                  => 'Operator',
        'add'                       => 'Add',
        'list'                      => 'List',
        'example'                   => 'exp',
        'color'                     => 'Color',
        'file'                      => 'File',
        'image'                     => 'Image',
        'banner'                    => 'Banner',
        'video'                     => 'Video',
        'audio'                     => 'Audio',
        'all'                       => 'All',
        'parent'                    => 'Parent',
        'category'                  => 'Category',
        'category-parent'           => 'Parent category',
        'edit'                      => 'Edit',
        'delete'                    => 'Remove',
        'generate-password'         => 'Generate email and send via email',
        'password'                  => 'Password',
        'current-password'          => 'Current password',
        'new-password'              => 'New password',
        'retype-new-password'       => 'Repeat new password',
        'role'                      => 'Role',
        'created-at'                => 'Create date',
        'updated-at'                => 'Last modified',
        'ios'                       => 'IOS',
        'android'                   => 'Android',
        'description'               => 'Description',
        'description_ur'               => 'Urdu Description',
        'link'                      => 'Link',
        'publish'                   => 'Publish',
        'not-published'             => 'Not published',
        'unpublish'                 => 'Unpublished (draft)',
        'editor-choice'             => 'Select editor',
        'push'                      => 'Push',
        'site-description'          => 'Site description',
        'site-keywords'             => 'Site keywords',
        'favicon'                   => 'Favicon',
        'site-image'                => 'Site image',
        'tag'                       => 'Tag',
        'id'                        => 'Id',
        'used-number'               => 'Used number',
        'views'                     => 'Views',
        'url'                       => 'URL',
        'supported-browsers'        => 'Supported browsers',
        'price'                     => 'Price',
        'day'                       => 'Day',
        'daily'                     => 'Daily',
        'week'                      => 'Week',
        'weekly'                    => 'Weekly',
        'month'                     => 'Month',
        'monthly'                   => 'Monthly',
        'unit'                      => 'Unit',
        'value'                     => 'Value',
        'home'                      =>'Home',
        'leaderboard'               => 'Leaderboard',
        'users' => [
            'id'                    => 'Id',
            'list'                  => 'User\'s list',
            'created-at'            => 'Created at',
            'active'                => 'Active',
            'deactive'              => 'Deactive',
        ],
        'tags' => [
            'list'                  => 'Tag\'s List',
        ],
        'roles' => [
            'read'                  => 'Read',
            'write'                 => 'Write',
            'update'                => 'Update',
            'delete'                => 'Remove',
            'publish'               => 'Publish',
            'permission'            => 'Permissions',
        ],
        'force-update' => [
            'device-type'           => 'Device type',
            'minimum-version'       => 'Minimum version',
            'latest-version'        => 'Last version',
        ],
    ],

    'modal' => [
        'confirm'                   => 'Are you sure to continue?',
        'confirm-info'              => 'Please be inform that this operation may not be reversable',
        'force-update'              => 'Change application version',
        'force-update-info'         => 'Please note that change application version may prevent some users to use app',
    ],

    'btn' => [
        'close'                     => 'Close',
        'confirm'                   => 'Confirm',
        'delete'                    => 'Remove',
        'activate'                  => 'Activate',
        'deactivate'                => 'Deactivate',
        'submit'                    => 'Submit',
        'cancel'                    => 'Cancel',
        'add'                       => 'Add',
        'edit'                      => 'Edit',
    ],

    'sidebar' => [
        'dashboard'                 => 'Dashboard',

        'users' => [
            'name'                  => 'Users'
        ],

        'categories' => [
            'name'                  => 'Category',
            'label'                 => 'Categories',
            'add'                   => 'Add category',
            'list'                  => 'List categories',
        ],

        'tags' => [
            'name'                  => 'Tag',
            'label'                 => 'Tags',
            'add'                   => 'Add tag',
            'list'                  => 'List tags',
        ],

        'games' => [
            'name'                  => 'Game',
            'label'                 => 'Games',
            'add'                   => 'Add game',
            'list'                  => 'List games',
        ],

        'admins' => [
            'name'                  => 'Admin',
            'label'                 => 'Admins',
            'add'                   => 'Add admin',
            'list'                  => 'List admins',
        ],

        'roles' => [
            'name'                  => 'Role',
            'label'                 => 'Roles',
            'add'                   => 'Add role',
            'list'                  => 'List roles',
        ],

        'force-update' => [
            'name'                  => 'Force update',
        ],

        'pages' => [
            'name'                  => 'Page',
            'label'                 => 'Pages',
            'add'                   => 'Add page',
            'list'                  => 'List pages',
        ],

        'subscriptions' => [
            'name'                  => 'Subscription',
            'label'                 => 'Subscription',
        ],

        'settings' => [
            'name'                  => 'Setting',
            'label'                 => 'Settings',
        ],

        'media' => [
            'name'                  => 'Media',
            'label'                 => 'Media',
            'add'                   => 'Add media',
            'list'                  => 'List media',
        ],

        'ads' => [
            'name'                  => 'Ads',
            'label'                 => 'Ads',
            'add'                   => 'Add ads',
            'list'                  => 'List ads',
        ],


        'post-category' => 'Post category',
    ],

    'placeholder' => [
        'search'                    => 'Search',
        'minimum-version'           => 'Minimum version',
        'latest-version'            => 'Latest version',
    ],

    'example' => [
        'category-name'             => 'Football',

        'game-name'                 => 'Bable',
        'game-description'          => 'Plot all the balls on pool table',
        'game-supported-browsers'   => 'Chrome, Firefox',

        'page-name'                 => 'About us',

        'name'                      => 'Haifa',
        'role-name'                 => 'Super user',

        'site-description'          => 'Play games',
        'site-keywords'             => 'Games
        ',

        'tag'                       => 'ball',

        'url'                       => 'http://google.com',
        'price'                     => '3 pound',
        'unit'                      => '(Pound)',
    ]
];
