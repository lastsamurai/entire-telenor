<?php

return [
    'subscription-text'     => 'by :price you can play!',
    'purchase_error'        => 'you may not have purchased',
    'success'               => 'success',
    'wrong_msisdn'          => 'msisdn is wrong! we can\'t convert it to phone number',
    'unknown_error'         => 'unknown error',
    'user_not_found'        => 'user not found',
    'today_game'            =>'Game of the Day',
    'play'                  =>'Play',
    'supported_browsers'   =>'Supported browsers',
    'alcohol'               =>'Alcohol',
    'trending_game'         =>'Hot Games',
    'favorite'            =>'Favorite',
    'subscribe'                 =>'Subscribe',
    'login'                 =>'Login',
    'logout'                 =>'Logout',
    'related'               =>'Related Games',
    'copy-right'            =>'All rights reserved for Samssson | 2019',
    'enter_phone_number'      =>'Please enter your phone number',
    'code'                  =>'Code',
    'unsubscribe'           =>'Unsubscribe',
    'back'                  =>'Back',
    'confirm_unsubscribe'   =>'Are you sure you want to unsubscribe?',
    'enter_last_code'       =>'Please enter the last code received',
    'add_to_fav'            =>'Add to favorite',
    'invalid_phone'         =>'The number you entered does not belong to a Sudanese company. Please enter a number starting with 01',
    'unsuccessful_payment'  =>'Sorry, your balance is insufficient, to deduct the service fee, please recharge your balance',
    'logout_successfully'   =>'You successfully logout',
    'reach_to_max_login'    =>'You reach to the max number of devices to login, please logout from one to continue',


];
