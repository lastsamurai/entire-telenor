<?php

return [
    'text' => [
        'dashboard'                 => 'پنل مدیریت',
        'user-profile'              => 'حساب کاربری',
        'not-defined'               => 'تعریف نشده',
        'row'                       => 'ردیف',
        'name'                      => 'نام',
        'title'                     => 'عنوان',
        'email'                     => 'ایمیل',
        'phone'                     => 'شماره همراه',
        'status'                    => 'وضعیت',
        'mtn'                       => 'ایرانسل',
        'mci'                       => 'همراه اول',
        'operator'                  => 'اپراتور',
        'add'                       => 'افزودن',
        'list'                      => 'لیست',
        'example'                   => 'مثال',
        'color'                     => 'رنگ',
        'file'                      => 'فایل',
        'image'                     => 'عکس',
        'banner'                    => 'بنر',
        'video'                     => 'ویدیو',
        'audio'                     => 'صدا',
        'all'                       => 'همه',
        'parent'                    => 'پدر',
        'category'                  => 'دسته بندی',
        'category-parent'           => 'پدر دسته',
        'edit'                      => 'ویرایش',
        'delete'                    => 'حذف کردن',
        'generate-password'         => 'تولید و ارسال کلمه عبور توسط ایمیل',
        'password'                  => 'کلمه عبور',
        'current-password'          => 'کلمه عبور فعلی',
        'new-password'              => 'کلمه عبور جدید',
        'retype-new-password'       => 'تکرار کلمه عبور جدید',
        'role'                      => 'نقش',
        'created-at'                => 'تاریخ ایجاد',
        'updated-at'                => 'آخرین تغییر',
        'ios'                       => 'آی‌او‌اس',
        'android'                   => 'اندروید',
        'description'               => 'توضیحات',
        'link'                      => 'لینک',
        'publish'                   => 'منتشر کردن',
        'not-published'             => 'منتشر نشده',
        'unpublish'                 => 'عدم انتشار (حالت پیش نویس)',
        'editor-choice'             => 'انتخاب سردبیر',
        'push'                      => 'ارسال نوتیفیکیشن',
        'site-description'          => 'توضیحات وبسایت',
        'site-keywords'             => 'کلیدواژه های وبسایت',
        'favicon'                   => 'فاوآیکن',
        'site-image'                => 'عکس نمایه سایت',
        'tag'                       => 'برچسب',
        'id'                        => 'شناسه',
        'used-number'               => 'تعداد استفاده',
        'views'                     => 'بازدیدها',
        'url'                       => 'آدرس صفحه',
        'supported-browsers'        => 'مرورگرهای پشتیبانی شده',
        'price'                     => 'قیمت',
        'day'                       => 'روز',
        'daily'                     => 'روزانه',
        'week'                      => 'هفته',
        'weekly'                    => 'هفتگی',
        'month'                     => 'ماه',
        'monthly'                   => 'ماهانه',
        'unit'                      => 'واحد',
        'value'                     => 'مقدار',
        'users' => [
            'id'                    => 'شماره کاربری',
            'list'                  => 'لیست کاربران',
            'created-at'            => 'تاریخ عضویت',
            'active'                => 'فعال',
            'deactive'              => 'غیر فعال',
        ],
        'tags' => [
            'list'                  => 'لیست برچسب‌ها',
        ],
        'roles' => [
            'read'                  => 'خواندن',
            'write'                 => 'نوشتن',
            'update'                => 'بروز رسانی / بررسی',
            'delete'                => 'حذف',
            'publish'               => 'انتشار',
            'permission'            => 'دسترسی',
        ],
        'force-update' => [
            'device-type'           => 'نوع سیستم',
            'minimum-version'       => 'نسخه حداقل',
            'latest-version'        => 'آخرین نسخه',
        ],
    ],

    'modal' => [
        'confirm'                   => 'آیا مطمئنید که میخواهید این عملیات را انجام دهید؟',
        'confirm-info'              => 'در صورتی که این عملیات را انجام دهید، ممکن است امکان بازگشت وجود نداشته باشد',
        'force-update'              => 'تغییر نسخه اپلیکیشن',
        'force-update-info'         => 'تغییر در حداقل نسخه اپلیکیشن ممکن است باعث جلوگیری از ورود دسته ای از کاربران فعلی اپلیکیشن شود',
    ],

    'btn' => [
        'close'                     => 'بستن',
        'confirm'                   => 'تایید',
        'delete'                    => 'حذف کردن',
        'activate'                  => 'فعال کردن',
        'deactivate'                => 'غیرفعال کردن',
        'submit'                    => 'ارسال',
        'cancel'                    => 'انصراف',
        'add'                       => 'افزودن',
        'edit'                      => 'ویرایش',
    ],

    'sidebar' => [
        'dashboard'                 => 'داشبورد',

        'users' => [
            'name'                  => 'کاربران'
        ],

        'categories' => [
            'name'                  => 'دسته',
            'label'                 => 'دسته بندی‌ها',
            'add'                   => 'افزودن دسته',
            'list'                  => 'لیست دسته بندی‌ها',
        ],

        'tags' => [
            'name'                  => 'برچسب',
            'label'                 => 'برچسب‌ها',
            'add'                   => 'افزودن برچسب',
            'list'                  => 'لیست برچسب‌ها',
        ],

        'games' => [
            'name'                  => 'بازی',
            'label'                 => 'بازی‌ها',
            'add'                   => 'افزودن بازی',
            'list'                  => 'لیست بازی‌ها',
        ],

        'admins' => [
            'name'                  => 'مدیر',
            'label'                 => 'مدیران',
            'add'                   => 'افزدون مدیر',
            'list'                  => 'لیست مدیران',
        ],

        'roles' => [
            'name'                  => 'نقش‌',
            'label'                 => 'نقش‌ها',
            'add'                   => 'افزودن نقش‌',
            'list'                  => 'لیست نقش‌ها',
        ],

        'force-update' => [
            'name'                  => 'نسخه اپلیکیشن',
        ],

        'pages' => [
            'name'                  => 'صفحه',
            'label'                 => 'صفحه‌ها',
            'add'                   => 'افزودن صفحه',
            'list'                  => 'لیست صفحه‌ها',
        ],

        'subscriptions' => [
            'name'                  => 'اشتراک',
            'label'                 => 'اشتراک‌ها',
        ],

        'settings' => [
            'name'                  => 'تنظیم',
            'label'                 => 'تنظیمات',
        ],

        'media' => [
            'name'                  => 'چند رسانه‌ای',
            'label'                 => 'چند رسانه‌ای',
            'add'                   => 'افزودن چند رسانه‌ای',
            'list'                  => 'لیست چند رسانه‌ای',
        ],

        'ads' => [
            'name'                  => 'تبلیغ',
            'label'                 => 'تبلیغ‌ها',
            'add'                   => 'افزودن تبلیغ',
            'list'                  => 'لیست تبلیغ‌ها',
        ],

        'post-category' => 'دسته‌بندی مطالب',
    ],

    'placeholder' => [
        'search'                    => 'جستجو کنید',
        'minimum-version'           => 'نسخه حداقل',
        'latest-version'            => 'آخرین نسخه',
    ],

    'example' => [
        'category-name'             => 'فوتبال',

        'game-name'                 => 'تلگرام',
        'game-description'          => 'اپلیکیشن چت، تماس صوتی و خبررسانی',
        'game-supported-browsers'   => 'کروم، فایرفاکس',

        'page-name'                 => 'درباره ما',

        'name'                      => 'سینا',
        'role-name'                 => 'سوپر یوزر',

        'site-description'          => 'طعمی متفاوت از دیالوگ‌ها و لحظه‌های خاطره‌انگیز فیلم‌ها',
        'site-keywords'             => 'ویدیو, فیلم, سینما',

        'tag'                       => 'توپ',

        'url'                       => 'http://google.com',
        'price'                     => '۳ هزارتومان',
        'unit'                      => 'دلار آمریکا (USD)',
    ]
];
