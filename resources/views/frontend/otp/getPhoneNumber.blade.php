@extends('frontend.otp.base')
@section('title')
Enter Your Phone Number
@endsection
@section('content')

<div class="row RobotoLight">
    Send your Phone Number to [number] and start your journey
</div>
<form action="/users/confirm-pin" method="get">
    <div class="row"><input name="msisdn" type="text" class="pretty RobotoRegular" placeholder="09xx xxx xxxx"/></div>
    <div class="row"><button type="submit" class="pretty RobotoRegular" >SEND</button></div>
</form>

@endsection