<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>@yield('title')</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500&display=swap' rel='stylesheet'>
        <style>
            body{
                font-family: Roboto;
                font-size: 18px;
                color: #FFFFFF;
            }
            .Roboto{font-family:'Roboto';font-weight:400;} /* 400 is industry standard for normal */

            .RobotoLight{font-family:'Roboto';font-weight:300;} /* 300 is industry standard for normal */

            .RobotoMedium{font-family:'Roboto';font-weight:500;} /* 500 is industry standard for normal */

            .medium{
                font-family: Roboto-Medium;
                letter-spacing: -0.64px;
            }
            .light{
                font-family: Roboto-Light;
                letter-spacing: -0.64px;
            }
            .wrapper{
                position:relative;
                width:360px;
                height:640px;
                margin-left:auto;
                margin-right:auto;
                background-color:#0F162F;
                background-image:url( {{asset('images/telenor_landing_banner.png')}} );
                background-repeat: no-repeat;
                background-size: 100% 56.3%;
                padding:0;
            }
            .content{
                position:absolute;
                width:auto;
                height:43.7%;
                bottom:0;
                margin-left:36px;
                margin-right:36px;
                text-align:center;
            }
            .content .row{
                margin-top:15px;
                margin-bottom:15px;
            }
            .pretty{
                background: #0268B5;
                box-shadow: 0 2px 6px 0 rgba(0,0,0,0.40);
                color:#FFFFFF;
                height:48px;
                width:100%;
                -webkit-border-radius: 30px;
                -moz-border-radius: 30px;
                border-radius: 30px;
                border:none;
                margin-top:5px;
                margin-bottom:5px;
                font-size: 18px;
                padding:10px;
            }
            input.pretty{
                text-align:center;
                background: #CCDDFF;
                color:black;
                //opacity: 0.7;
                font-size: 16px;
                color: #000000;
            }
            input.pretty:focus{
                outline:none;
            }
            button.pretty{
                background: #0268B5;
            }
            button.pretty:focus{
                outline:none;
            }
            a.link{
                display:block;
                text-decoration: none;
                font-size: 18px;
                color: #66CCFF;
                letter-spacing: -0.64px;
                //margin-top:40px;
                margin-bottom:5px;
            }
            img.icon{
                width:36px;
                height:36px;
            }
        </style>
    </head>
    <body>
        <div class="wrapper">
            <div class="content">
                @yield('content')
            </div>
        </div>

        @yield('jquery')
    </body>
</html>