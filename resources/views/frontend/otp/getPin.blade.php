@extends('frontend.otp.base')
@section('title')
Enter OTP
@endsection
@section('content')

<div class="row RobotoLight">
    A code was sent to you via SMS. It can take up to two minutes.
</div>
<form action="/users/confirm-pin" method="POST">
    <input type="hidden" name="_method" value="POST"/>
    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
    <input type="hidden" name="msisdn" value="{{$msisdn}}" />
    
    <div class="row RobotoRegular">
        <input name="code" type="text" class="pretty" placeholder="____"/>
    </div>
    <div class="row RobotoRegular">
        <button type="submit" class="pretty ">CONFIRM</button>
    </div>
</form>
<div id="wait" class="row">
    <span class="RobotoLight" style='float:left;'>Please wait to recieve code</span>
    <span id="timer" class="RobotoRegular" style="float:right;" data-start="120000" data-value="">1:59</span>
    <span class=clear:both;></span>
</div>
<div class="row">
    <a class="link RobotoRegular" href="#" >Change Phone Number</a>
</div>

@endsection

@section('jquery')
<script type='text/javascript'>

        var i=setInterval(function(){
            var timer=document.getElementById('timer');
            if(!timer.getAttribute('data-value'))timer.setAttribute('data-value', timer.getAttribute('data-start'));

            if(timer.getAttribute('data-value')<=0)callback();


            timer.setAttribute('data-value',timer.getAttribute('data-value')-1000);
            timer.innerHTML=Math.floor( timer.getAttribute('data-value')/60000 )+":"+((timer.getAttribute('data-value')%60000)/1000).toFixed();
        },1000);
        
        function callback(){
            console.log('time over for '+i);
            clearInterval(i);
            document.getElementById('wait').innerHTML="<div class='RobotoLight' style='float:left;'>Didn't receive code?</div><div class='RobotoRegular link' style='float:right;'><a style='color:#FFC400;' href='#'>Send Again</a></div><div style='clear:both;'></div>";
            var timer=document.getElementById('timer');
            timer.innerHTML="0:0";
        }

</script>
@endsection