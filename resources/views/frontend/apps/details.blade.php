@extends('layouts.default')
@section('content')
    <main class="row">
        <div class="col-12">
            <section class="row justify-content-center mb-5 mt-5" id="app-details">
                <div class="col-12 col-md-6 col-lg-4 pt-3">
                    <div class="row">
                        <div class="col-12 col-md-5 mb-4 text-center">
                            <img src="{{ $game->media[\App\Mediable::THUMBNAIL_FIELD]->file->url() }}" class="col-9 col-md-12 p-0" id="app-icon">
                        </div>

                        <div class="col-12 col-md-7 mb-4">
                            <h1>{{ $game->title }}</h1>
                            <h4>{{ ($category = $game->categories->first()) ? $category->name : '' }}</h4>
                            @if(Auth::user())
                                <a href="{{ url("apps/play/$game->id") }}" class="w-100 mt-2 button bg-green color-white" target="_blank">{{trans('messages.play')}}</a>
                            @else
                                <button type="button" class="w-100 mt-2 button bg-green color-white" data-toggle="modal" data-target="#login-modal">{{trans('messages.subscribe')}}</button>
                            @endif
                            <div class="col-9 justify-content-center text-center mt-2 favorite-icon @if($favorite) selected @endif">
                                <a href="javascript:void(0)" class="color-blue pr-3" id="add-favorite" data-user="{{$user_id}}" data-game ={{$game->id}}>{{trans('messages.add_to_fav')}}</a>
                            </div>
                        </div>
                    </div>

                    <div class="row m-0 mt-4" id="app-description">
                        <div class="col-12 pb-4 pt-4">
                            <div class="row mb-5">
                                <div class="col-12">
                                    <strong>{{ $game->description }}</strong>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 mb-3">
                                    <p class="details-title">{{trans('messages.supported_browsers')}}</p>
                                    <b>{{ $game->browsers }}</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @if(count($related)>0)
                <section class="row justify-content-center" id="related-apps">
                    <div class="col-12 col-md-9 col-xl-7 pb-3">
                        <b id="related-title">{{trans('messages.related')}}</b>
                        <div class="slick-related-apps-carousel">
                            @foreach($related as $value)
                                <div>
                                    <a href="{{ url("apps/details/$value->id") }}"> <img src="{{ $value->media[\App\Mediable::THUMBNAIL_FIELD]->file->url() }}" alt="{{ $value->title }}"></a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </section>
            @endif

        </div>
    </main>
@stop