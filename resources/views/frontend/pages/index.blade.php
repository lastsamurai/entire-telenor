<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="theme-color" content="#050e1d">
    <meta name="msapplication-navbutton-color" content="#050e1d">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <title>@if(isset($page->title)) {{ $page->title }} - @endif {{ config('app.name') }} </title>
    <link rel="icon" type="image/png" href="{{ URL("/img/rooma-favicon.png") }}">

    <!-- STYLESHEET -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/admin/bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/iran-yekan.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/iran-roboto.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/pages.css') }}" />

    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->

    <link rel="canonical" href="{{ Request::url() }}">

    <!-- Google Plus -->
    <meta itemprop="name" content="{{ $page->title }}">
    <meta itemprop="description" content="{{ str_limit(strip_tags($page->value), 150) }}">
    <meta itemprop="image" content="{{ URL("img/rooma-appicon.png") }}">
    <meta itemprop="url" content="{{ Request::url() }}"/>

    <!-- Twitter -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@rooma">
    <meta name="twitter:title" content="{{ $page->title }}">
    <meta name="twitter:description" content="{{ str_limit(strip_tags($page->value), 150) }}">
    <meta name="twitter:image:src" content="{{ URL("img/rooma-appicon.png") }}">

    <!-- Open Graph General (Facebook & Pinterest) -->
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="og:title" content="{{ $page->title }}">
    <meta property="og:description" content="{{ str_limit(strip_tags($page->value), 150) }}">
    <meta property="og:site_name" content="{{ config('app.name') }}">
    <meta property="og:image" content="{{ URL("img/rooma-appicon.png") }}">
    <meta property="og:type" content="website">
    <meta property="og:locale" content="fa_ir">

</head>

<body>

<article class="container-fluid">
    <div class="row" id="details">
        <div class="col-xs-12" id="article">
            <h1>{{ $page->title }}</h1>

            <p>{!! $page->value !!}</p>
        </div>
    </div>
</article>

</body>
</html>
