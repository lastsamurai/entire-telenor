@extends('layouts.default')
@section('content')
    <section class="row" id="title-section">
        <div class="col-12">
            <div class="container">
                <div class="row">
                    <div class="col-12 p-0">
                        <div class="row m-0">
                            <div class="col-12 text-center">
                                {{--<h1>PLAY GAME</h1>
                                <h4>Let`s enjoy the game by signing up just for first time</h4>--}}
                            </div>
                        </div>

                        <div class="image" id="ground"></div>
                        <div class="image parallax-item p-plus" data-speed="1.0" id="cloud-1"></div>
                        <div class="image parallax-item p-plus" data-speed="2.0" id="cloud-2"></div>
                        <div class="image parallax-item p-plus" data-speed="1.0" id="cloud-3"></div>
                        <div class="image parallax-item p-plus" data-speed="4.0" id="cloud-4"></div>
                        <div class="image parallax-item p-minus" data-speed="1.0" id="sun"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <main class="row">
        <div class="col-12">
            <section class="row justify-content-center" id="landing-recent">
                <div class="col-12">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 pt-3">
                                <div class="row">
                                    <div class="col-12 col-md-6 mb-4">
                                        <header class="row">
                                            <div class="col-12">
                                                <h2>{{trans('messages.today_game')}}</h2>
                                            </div>
                                        </header>

                                        <div class="row" id="recent-container">
                                            <div class="col-12 pt-3 pb-3">
                                                <div class="row">
                                                    @if(!empty($top_app))
                                                    <div id="game-box">
                                                        <a href="{{url("apps/details/$top_app->id?lang=$lang") }}">
                                                            <img src="{{ $top_app->media[\App\Mediable::THUMBNAIL_FIELD]->file->url() }}" class="w-100 d-block">
                                                        </a>

                                                        <a href="{{ url("apps/details/$top_app->id?lang=$lang") }}" class="button bg-yellow-dark color-white font-black-han-sans d-block w-100 mt-5">{{trans('messages.play')}}</a>
                                                    </div>

                                                    <div class="col">
                                                        <a href="{{ url("apps/details/$top_app->id?lang=$lang") }}"><h3>{{ $top_app->title }}</h3></a>

                                                        <h5>{{ ($category = $top_app->categories->first()) ? $category->name : '' }}</h5>

                                                        <p class="font-weight-normal">{{ $top_app->description }}</p>

                                                        <b class="d-block">{{trans('messages.supported_browsers')}}</b>
                                                        <small>{{ $top_app->browsers }}</small>
                                                    </div>
                                                        @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6 mb-4">
                                        <header class="row">
                                            <div class="col-12">
                                                <h2>{{trans('messages.trending_game')}}</h2>
                                            </div>
                                        </header>

                                        <div class="row" id="recent-container">
                                            <div class="col-12 pt-3 pb-3">
                                                @foreach($most_viewed as $key => $value)
                                                    <div class="row recent-apps mb-3">
                                                        <div class="col-2 p-0 text-center">
                                                            <a href="{{ url("apps/details/$value->id/?lang=$lang") }}">
                                                                <span class="number">{{ PersianNumber($key + 1) }}</span>
                                                            </a>
                                                        </div>

                                                        <div class="col-10">
                                                            <div class="row">
                                                                <div class="col-12 pr-0">
                                                                    <div class="media">
                                                                        <a href="{{ url("apps/details/$value->id/?lang=$lang") }}">
                                                                            <img class="align-self-start ml-3" src="{{ $value->media[\App\Mediable::THUMBNAIL_FIELD]->file->url() }}" alt="{{ $value->title }}">
                                                                        </a>

                                                                        <div class="media-body">
                                                                            <a href="{{ url("apps/details/$value->id/?lang=$lang") }}">
                                                                                <p>{{ $value->title }}</p>
                                                                                <p>{{ $value->description }}</p>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="row justify-content-center" id="category-navigation">
                <div class="col-12">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="slick-carousel">
                                    <div class="@if($user_id) active @endif">
                                        <a href="{{ url("/?likes=true&lang=$lang") }}" class="category-item @if(!$user_id) show-login-modal @endif "  data-user="{{$user_id}}">{{ trans('messages.favorite') }}</a>
                                    </div>
                                    <div class="@if(!Request::query('category') && (!$user_id)) active @endif">
                                        <a href="{{ url('/') }}" class="category-item">{{trans('messages.alcohol')}}</a>
                                    </div>

                                    @foreach($categories as $category)
                                        <div class="@if(Request::query('category') == $category->id) active @endif">
                                            <a href="{{ url("/?lang=$lang&category=$category->id") }}" class="category-item">{{ $category->name }}</a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="row justify-content-center" id="recent-apps-section">
                <div class="col-12">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 mt-4 pt-4">
                                <div class="row">
                                    @foreach($games as $game)
                                        <div class="col-6 col-sm-3 col-md-2 pb-3">
                                            <a href="{{ url("apps/details/$game->id/?lang=$lang") }}" title="{{ $game->title }}">
                                                <img src="{{ $game->media[\App\Mediable::THUMBNAIL_FIELD]->file->url() }}" class="w-100" alt="{{ $game->title }}">
                                            </a>
                                            <div class="text-center">{{ $game->title }}</div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@stop