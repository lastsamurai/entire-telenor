@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item disabled">
                <span class="page-link">
                    <span class=""><i class="fa fa-chevron-left"></i></span>
                </span>
            </li>
        @else
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev">
                    <span class=""><i class="fa fa-chevron-left"></i></span>
                </a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item disabled"><span class="page-link">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active"><span class="page-link">{{ numbers($page) }}</span></li>
                    @else
                        <li class="page-item"><a class="page-link" href="{{ $url }}">{{ numbers($page) }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">
                    <span class=""><i class="fa fa-chevron-right"></i></span>
                </a>
            </li>
        @else
            <li class="page-item disabled">
                <span class="page-link">
                    <span class=""><i class="fa fa-chevron-right"></i></span>
                </span>
            </li>
        @endif
    </ul>
@endif