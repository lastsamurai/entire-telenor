<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if(isset($title)) {{ $title }} - @endif {{ config('app.name') }} </title>
    <link rel="icon" type="image/png" href="{{ URL("/img/favicon.ico") }}">

    <!-- STYLESHEET -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/droid-arabic-kufi.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/slick.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/slick-theme.css') }}" />
    @if(app('request')->input('lang')== 'en')
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/layout-left.css') }}?v1.0.7" />
    @else
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/layout-right.css') }}?v1.0.7" />
    @endif
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/iStyle.css') }}?v1.0.7" />
    @yield('css', '')


    <!-- JS -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/iScript.js') }}?v1.0.10"></script>
    @yield('js', '')
    <?php $invalid_phone_message = trans('messages.invalid_phone')?>

    <script type="text/javascript">
        var baseurl = '{{ URL::to('/') }}';
        var token = '{{ csrf_token() }}';
        var invalidPhoneMessage = "{{$invalid_phone_message}}"
    </script>



    @if(app('request')->input('lang')== 'en')
        <script>
            var rtlDirection = false;
        </script>
    @else
        <script>
            var rtlDirection = true;
        </script>
    @endif

    <!--[if lt IE 9]>
    <script src="{{ url('js/html5shiv.min.js') }}"></script>
    <![endif]-->

    <link rel="canonical" href="{{ Request::url() }}">

</head>

<body @if(isset($pageId))id="{{ $pageId }}"@endif>

<div class="container-fluid">
    @include('layouts.header')

    @yield('content')
    @include('layouts.footer')
</div>


</body>
</html>
