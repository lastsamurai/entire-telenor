<div class="modal fade" id="{{ $modalId }}" tabindex="-1" role="dialog" aria-labelledby="{{ $modalId }}-label" aria-hidden="true">
    <div class="modal-dialog">
        <form action="#" method="post" class="d-inline-block needs-validation validate">
            @if(isset($modalMethod) and $modalMethod == 'PUT')
                <input name="_method" type="hidden" value="PUT">
            @endif
            {!! csrf_field() !!}
            <input type="hidden" name="id" class="modal-id" value="">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <br>

                    @if($type == 'confirm')
                        <i class="fa fa-question-circle fa-4x"></i>

                        <h4 id="{{ $modalId }}-label" class="semi-bold">{{ trans('ui.modal.confirm') }}</h4>
                        <p class="no-margin">{{ trans('ui.modal.confirm-info') }}</p>
                    @elseif($type == 'force-update')
                        <i class="fa fa-refresh fa-3x"></i>

                        <h4 id="{{ $modalId }}-label" class="semi-bold">{{ trans('ui.modal.force-update') }}</h4>
                        <p class="no-margin">{{ trans('ui.modal.force-update-info') }}</p>
                    @endif
                    <br>
                </div>

                <div class="modal-body">
                    @if($type == 'force-update')
                        <div class="row form-row">
                            <div class="col-md-12">
                                <label class="mb-2" id="force-update-label"></label>
                            </div>
                        </div>

                        <div class="row form-row">
                            <div class="col-md-8">
                                <input type="text" name="minimum" id="minimum" class="form-control" placeholder="{{ trans('ui.placeholder.minimum-version') }}" required>
                            </div>

                            <div class="col-md-4">
                                <input type="text" name="latest" id="latest" class="form-control" placeholder="{{ trans('ui.placeholder.latest-version') }}" required>
                            </div>
                        </div>
                    @elseif($type == 'media')
                        <div id="media-src"></div>
                        <div id="media-link"><code></code></div>
                    @endif
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('ui.btn.close') }}</button>

                    @if(isset($edit) and $edit)
                        <a href="javascript:;" class="btn btn-dark modal-edit-btn" target="_blank">{{ trans('ui.btn.edit') }}</a>
                    @endif

                    @if($type == 'media')
                        <button type="submit" class="btn btn-danger">{{ trans('ui.btn.delete') }}</button>
                    @else
                        <button type="submit" class="btn btn-primary">{{ trans('ui.btn.confirm') }}</button>
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>