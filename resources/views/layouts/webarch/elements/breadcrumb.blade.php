@if(isset($items) and count($items))
<ul class="breadcrumb">
    <li><a href="{{ route('dashboard.index') }}" class="active">{{ trans('ui.text.dashboard') }}</a></li>

    @foreach($items as $item)
        @if($loop->last)
            <li><p>{{ trans($item['label']) }}</p></li>
        @else
            <li><a href="{{ route($item['route']) }}" class="active">{{ trans($item['label']) }}</a></li>
        @endif
    @endforeach
</ul>
@endif