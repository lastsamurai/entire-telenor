<div class="form-group">
    <label class="form-label" for="{{ $picker_id }}">{{ $picker_label }}</label>

    <div class="controls mediapicker-handler" id="{{ $picker_id }}" data-pickertype="{{ $picker_type }}" data-toggle="modal" data-target="#mediapicker">
        {!! Form::hidden($picker_name, '', ['class' => 'handler-id']) !!}
        @if($picker_required)
            {!! Form::text("{$picker_name}_preview", null, ['class' => 'form-control handler-preview ltr', 'readonly', 'required']) !!}
        @else
            {!! Form::text("{$picker_name}_preview", null, ['class' => 'form-control handler-preview ltr', 'readonly']) !!}
        @endif
    </div>
</div>

<div class="form-group" id="{{ $picker_id }}-picker-preview">
    @if($picker_type == \App\Media::TYPE_IMAGE and $picker_preview)
        <img src="{{ $picker_preview }}" width="250px">
    @elseif($picker_type == \App\Media::TYPE_VIDEO and $picker_preview)
        <video controls>
            <source src='{{ $picker_preview }}' type='video/mp4'>
        </video>
    @elseif($picker_type == \App\Media::TYPE_AUDIO and $picker_preview)
        <audio controls>
            <source src='{{ $picker_preview }}' type='audio/mp3'>
        </audio>
    @endif
</div>