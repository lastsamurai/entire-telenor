<div class="modal fade" id="mediapicker" data-handler="" tabindex="-1" role="dialog" aria-labelledby="mediapicker-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <br>

                {!! Form::open(['route' => 'dashboard.media.store', 'method' => 'POST', 'class' => 'dropzone picker']) !!}


                <div class="fallback">
                    <input name="file" type="file" multiple />
                </div>
                {!! Form::close() !!}

                <br>

                <div class="progress progress-striped mb-0 active" id="mediapicker-loading" style="display: none">
                    <div data-percentage="45%" style="width: 100%;" class="progress-bar progress-bar-danger progress-animated"></div>
                </div>
            </div>

            <div class="modal-body">

                <ul class="nav nav-tabs text-center" role="tablist">
                    @if($image and $video and $audio)
                        <li class="mediapicker-list{{ ($active == 'all') ? ' active' : '' }}">
                            <a href="#tabPickerAll" role="tab" data-toggle="tab" data-url="{{ route('dashboard.media.index', ['response' => 'html']) }}" data-content="false">{{ trans('ui.text.all') }}</a>
                        </li>
                    @endif

                    @if($image)
                        <li class="mediapicker-list{{ ($active == 'image') ? ' active' : '' }}">
                            <a href="#tabPickerImage" role="tab" data-toggle="tab" data-url="{{ route('dashboard.media.index', ['type' => \App\Media::TYPE_IMAGE, 'response' => 'html']) }}" data-content="false">{{ trans('ui.text.image') }}</a>
                        </li>
                    @endif

                    @if($video)
                        <li class="mediapicker-list{{ ($active == 'video') ? ' active' : '' }}">
                            <a href="#tabPickerVideo" role="tab" data-toggle="tab" data-url="{{ route('dashboard.media.index', ['type' => \App\Media::TYPE_VIDEO, 'response' => 'html']) }}" data-content="false">{{ trans('ui.text.video') }}</a>
                        </li>
                    @endif

                    @if($audio)
                        <li class="mediapicker-list{{ ($active == 'audio') ?  ' active' : '' }}">
                            <a href="#tabPickerAudio" role="tab" data-toggle="tab" data-url="{{ route('dashboard.media.index', ['type' => \App\Media::TYPE_AUDIO, 'response' => 'html']) }}" data-content="false">{{ trans('ui.text.audio') }}</a>
                        </li>
                    @endif
                </ul>

                <div class="tab-content">
                    <div class="tab-pane{{ ($active == 'all') ? ' active' : '' }}" id="tabPickerAll">
                        <div class="row column-seperation">
                            <div class="col-xs-12 mediapicker-content"></div>
                        </div>
                    </div>

                    <div class="tab-pane{{ ($active == 'image') ? ' active' : '' }}" id="tabPickerImage">
                        <div class="row">
                            <div class="col-xs-12 mediapicker-content"></div>
                        </div>
                    </div>

                    <div class="tab-pane{{ ($active == 'video') ? ' active' : '' }}" id="tabPickerVideo">
                        <div class="row">
                            <div class="col-xs-12 mediapicker-content"></div>
                        </div>
                    </div>

                    <div class="tab-pane{{ ($active == 'audio') ? ' active' : '' }}" id="tabPickerAudio">
                        <div class="row">
                            <div class="col-xs-12 mediapicker-content"></div>
                        </div>
                    </div>
                </div>
            </div>



        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" id="picker-cancel-btn" data-dismiss="modal">{{ trans('ui.btn.close') }}</button>
            <button type="button" class="btn btn-primary" id="picker-pick-btn" data-dismiss="modal">{{ trans('ui.btn.confirm') }}</button>
        </div>
    </div>
</div>
</div>