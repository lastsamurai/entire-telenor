
@if($errors->any())
    <script type="text/javascript">
        @foreach($errors->all() as $error)
        showErrorMessage('{{ $error }}');
        @endforeach
    </script>
@endif

@if(session()->has('error_flash'))
    <script type="text/javascript">
        showErrorMessage("{{ session()->get('error_flash') }}");
    </script>
@endif

@if(session()->has('success_flash'))
    <script type="text/javascript">
        showSuccess("{{ session()->get('success_flash') }}");
    </script>
@endif
