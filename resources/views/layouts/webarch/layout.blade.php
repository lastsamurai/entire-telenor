<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>پنل مدیریت</title>

    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/boostrap-slider/slider.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/jquery-metrojs/MetroJs.min.css') }}"/>
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/shape-hover/demo.min.css') }}"/>
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/shape-hover/component.min.css') }}"/>
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/owl-carousel/owl.carousel.min.css') }}"/>
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/owl-carousel/owl.theme.min.css') }}"/>
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/pace/pace-theme-flash.min.css') }}" media="screen"/>
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/jquery-slider/jquery.sidr.light.css') }}" media="screen"/>
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/bootstrap-tag/bootstrap-tagsinput.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/dropzone/dropzone.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/bootstrap-select2/select2.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/persian-datepicker/persian-datepicker-0.4.5.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/froala/froala_editor.pkgd.css') }}" media="screen" />
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/froala/froala_editor.css') }}" media="screen" />
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/jquery-superbox/style.min.css') }}" media="screen" />
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/bootstrapv3/bootstrap.min.css') }}"/>
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/bootstrapv3/bootstrap-theme.min.css') }}"/>
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/animate.min.css') }}"/>
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/jquery-scrollbar/jquery.scrollbar.css') }}"/>
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/jquery-notifications/messenger.min.css') }}"/>
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/jquery-notifications/messenger-theme-flat.css') }}"/>
    <link type="text/css" rel="stylesheet" href="{{ url('css/plyr.css') }}"/>
    @yield('css', '')
    @if(app()->getLocale() == 'en')
        <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/webarch.css') }}" />
    @else
        <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/webarch.rtl.css') }}" />
    @endif

    <link type="text/css" rel="stylesheet" href="{{ url('css/iran-roboto.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ url('css/iran-yekan.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ url('css/font-awesome.css') }}"/>

    <script type="text/javascript">
        var baseurl = '{{ URL::to('/dashboard') }}/';
        var siteurl = '{{ URL::to('/') }}/';
        var token = '{{ csrf_token() }}';

        var image_type = parseInt('{{ \App\Media::TYPE_IMAGE }}');
        var video_type = parseInt('{{ \App\Media::TYPE_VIDEO }}');
        var audio_type = parseInt('{{ \App\Media::TYPE_AUDIO }}');
    </script>
</head>

<body class="">
@include('layouts.webarch.header')

<div class="page-container row-fluid">
    @include('layouts.webarch.sidebar')

    <div class="page-content">
        <div class="content sm-gutter">
            @yield('content')
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ url('js/webarch/plugins/pace/pace.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/jquery/jquery-1.11.3.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/bootstrapv3/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/jquery-block-ui/jqueryblockui.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/jquery-unveil/jquery.unveil.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/jquery-numberAnimate/jquery.animateNumbers.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/jquery-validation/localization/messages_fa.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/bootstrap-tag/bootstrap-tagsinput.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/bootstrap-select2/select2.min.js') }}"></script>

<script type="text/javascript" src="{{ url('js/plyr.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/dropzone/dropzone.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/webarch.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/owl-carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/jquery-metrojs/MetroJs.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/jquery-notifications/messenger.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/webarch/plugins/jquery-notifications/messenger-theme-flat.min.js') }}"></script>

@yield('js', '')

<script src="{{ url('js/webarch/plugins/dashboard_v2.js') }}"></script>
<script src="{{ url('js/webarch/iScript.js') }}"></script>


@include('layouts.webarch.elements.flash-messages')
</body>
</html>