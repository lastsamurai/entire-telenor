<div class="page-sidebar " id="main-menu">
    <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
        <div class="user-info-wrapper sm">
            <div class="profile-wrapper sm">
                @if($currentAdmin->image->url())
                    <img src="{{ $currentAdmin->image->url() }}" alt="" data-src="{{ $currentAdmin->image->url() }}" data-src-retina="{{ $currentAdmin->image->url() }}" width="69" height="69"/>
                @endif

                <div class="availability-bubble online"></div>
            </div>
            <div class="user-info sm">
                <div class="username">{{ $currentAdmin->name }}</div>
                <div class="status">{{ $currentAdmin->role_name }}</div>
            </div>
        </div>

        <p class="menu-title sm">
            <a href="">به‌روز کردن <span class="pull-right"><i class="material-icons">refresh</i></span></a>
        </p>

        <ul>
            <li class="{{ (Request::is('dashboard')) ? 'active': '' }}">
                <a href="{{ route('dashboard.index') }}">
                    <i class="material-icons">home</i>
                    <span class="title">{{ trans('ui.sidebar.dashboard') }}</span>
                    <span class="label label-important pull-right"></span>
                </a>
            </li>

            @foreach(config('acl.sections') as $value)
                @if(CheckAccess($permissions, $value['controller'], $value['access']))
                    @if(is_array($value['url']))
                        <li class="{{ (Request::is($value['url']['prefix']) || Request::is("{$value['url']['prefix']}/*")) ? $current = 'start open active': $current = '' }}">
                            <a href="javascript:;">
                                <i class="material-icons">{{ $value['icon'] }}</i>
                                <span class="title">{{ trans("ui.sidebar.{$value['id']}") }}</span>

                                <span class="{{ ($current) ? 'selected' : '' }}"></span>
                                <span class="arrow {{ ($current) ? 'open' : '' }}"></span>
                            </a>
                            <ul class="sub-menu">
                                @foreach($value['url']['group'] as $url)
                                    <li class="{{ (Request::is($url['link'])) ? 'active': '' }}"><a href="{{ route($url['link']) }}">{{ trans("ui.sidebar.{$url['id']}") }}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    @else
                        <li class="{{ (Request::is($value['url']) || Request::is("{$value['url']}/*")) ? 'active': '' }}">
                            <a href="{{ route($value['url']) }}">
                                <i class="material-icons">{{ $value['icon'] }}</i>
                                <span class="title">{{ trans("ui.sidebar.{$value['id']}") }}</span>
                                <span class="label label-important pull-right"></span>
                            </a>
                        </li>
                    @endif
                @endif

            @endforeach
        </ul>
        <div class="clearfix"></div>
    </div>
</div>
<a href="#" class="scrollup">Scroll</a>