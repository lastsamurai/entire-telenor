<div class="header navbar navbar-inverse">
    <div class="navbar-inner">
        <div class="header-seperation">
            <ul class="nav pull-left notifcation-center visible-xs visible-sm">
                <li class="dropdown">
                    <a href="#main-menu" data-webarch="toggle-right-side">
                        <i class="material-icons">menu</i>
                    </a>
                </li>
            </ul>

            <a href="{{ url('dashboard') }}">
                <img src="{{ url('img/dashboard/logo.png') }}" class="logo" alt="" data-src="{{ url('img/dashboard/logo.png') }}" data-src-retina="{{ url('img/dashboard/logo2x.png') }}" width="106" height="21"/>
            </a>

            <ul class="nav pull-right notifcation-center">
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="{{ url('dashboard') }}" class="dropdown-toggle active" data-toggle="">
                        <i class="material-icons">home</i>
                    </a>
                </li>
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="mailbox.php" class="dropdown-toggle">
                        <i class="material-icons">email</i><span class="badge bubble-only"></span>
                    </a>
                </li>
                <li class="dropdown visible-xs visible-sm">
                    <a href="#" data-webarch="toggle-right-side">
                        <i class="material-icons">chat</i>
                    </a>
                </li>
            </ul>
        </div>

        <div class="header-quick-nav">
            <div class="pull-left">
                <ul class="nav quick-section">
                    <li class="quicklinks">
                        <a href="#" class="" id="layout-condensed-toggle">
                            <i class="material-icons">menu</i>
                        </a>
                    </li>
                </ul>
                <ul class="nav quick-section">
                    <li class="quicklinks  m-r-10">
                        <a href=""><i class="material-icons">refresh</i></a>
                    </li>

                    <li class="quicklinks"><span class="h-seperate"></span></li>

                    <li class="quicklinks">
                        <a href="#" class="" id="my-task-list" data-placement="bottom" data-content='' data-toggle="dropdown" data-original-title="پیام ها">
                            <i class="material-icons">notifications_none</i>
                            <span class="badge badge-important bubble-only"></span>
                        </a>
                    </li>

                    @if(isset($searchUrl) and $searchUrl)
                        <li class="m-r-10 input-prepend inside search-form no-boarder">
                            {!! Form::open(['url' => url($searchUrl), 'method' => 'get']) !!}
                            <span class="add-on"> <i class="material-icons">search</i></span>
                            <input name="keyword" type="text" class="no-boarder " placeholder="{{ trans('ui.placeholder.search') }}" style="width:250px;">
                            {!! Form::close() !!}
                        </li>
                    @endif
                </ul>
            </div>

            <div id="notification-list" style="display:none">
                <div style="width:300px">
                    <div class="notification-messages info has-image">
                        <div class="user-profile">
                            <img src="{{ url('image/dashboard/d.jpg') }}" alt="" data-src="{{ url('image/dashboard/d.jpg') }}" data-src-retina="{{ url('image/dashboard/d2x.jpg') }}" width="35" height="35">
                        </div>
                        <div class="message-wrapper">
                            <div class="heading">
                                <span>عارف احمدی شما برای شما یک نظر ارسال کرد</span>
                            </div>
                            <div class="description">
                                <span>خیلی کولاکه :)</span>
                            </div>
                            <div class="date pull-left">
                                <span>یک دقیقه قبل</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="notification-messages danger">
                        <div class="iconholder">
                            <i class="icon-warning-sign"></i>
                        </div>
                        <div class="message-wrapper">
                            <div class="heading">
                                <span>فضای سرور در حال پر شدن است</span>
                            </div>
                            <div class="description">
                                <span>حجم دیتابیس و فایل سرور به 90 درصد رسیده است</span>
                            </div>
                            <div class="date pull-left">
                                <span>43 دقیقه قبل</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="notification-messages success has-image">
                        <div class="user-profile">
                            <img src="{{ url('image/dashboard/h.jpg') }}" alt="" data-src="{{ url('image/dashboard/h.jpg') }}" data-src-retina="{{ url('image/dashboard/h2x.jpg') }}" width="35" height="35">
                        </div>
                        <div class="message-wrapper">
                            <div class="heading">
                                <span>شما 150 پیام در صندوق پیام خود دارید</span>
                            </div>
                            <div class="description">
                                <span>به آنها رسیدگی کنید</span>
                            </div>
                            <div class="date pull-left">
                                <span>دیروز</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="pull-right">
                @if($currentAdmin->image->url())
                    <div class="chat-toggler sm">
                        <div class="profile-pic">
                            <img src="{{ $currentAdmin->image->url() }}" alt="" data-src="{{ $currentAdmin->image->url() }}" data-src-retina="{{ $currentAdmin->image->url() }}" width="35" height="35"/>
                            <div class="availability-bubble online"></div>
                        </div>
                    </div>
                @endif

                <ul class="nav quick-section ">
                    <li class="quicklinks">
                        <a data-toggle="dropdown" class="dropdown-toggle  pull-right " href="#" id="user-options">
                            <i class="material-icons">tune</i>
                        </a>
                        <ul class="dropdown-menu  pull-right" role="menu" aria-labelledby="user-options">
                            <li>
                                <a href="{{ route('dashboard.admins.self.edit') }}">{{ trans('ui.text.user-profile') }}</a>
                            </li>
                            <li>
                                <a href="calender.html">تقویم</a>
                            </li>
                            <li>
                                <a href="email.html"> صندوق پیام‌ها&nbsp;&nbsp;
                                    <span class="badge badge-important animated bounceIn">۲</span>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="login.html"><i class="material-icons">power_settings_new</i>&nbsp;&nbsp;خروج</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>