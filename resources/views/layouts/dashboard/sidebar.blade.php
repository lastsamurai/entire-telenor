<header class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <a href="{{ URL("/dashboard") }}">
        {{--<img src="{{ url("img/app-logo.png") }}" alt="{{ config("app.name") }}" style="width: 50%">--}}
        <span>{{ config("app.name") }}</span>
    </a>
</header>

<ul class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <li class="{{ (Request::is('dashboard')) ? 'active': '' }}">
        <a href="{{ URL("dashboard") }}">
            <i class="icon material-icons dashboard-icon"></i>
            <span>داشبورد</span>
        </a>
    </li>

    @foreach(config('acl.sections') as $value)
        @if(CheckAccess($permissions, $value['controller'], $value['access']))
            @if(is_array($value['url']))
                @foreach($value['url'] as $url => $name)
                    <li class="{{ (Request::is($url) || Request::is("$url/*")) ? 'active': '' }}">
                        <a href="{{ URL($url) }}">
                            <i class="icon {{ $value['icon'] }}"></i>
                            {{ $name }}
                        </a>
                    </li>
                @endforeach
            @else
                <li class="{{ (Request::is($value['url']) || Request::is("{$value['url']}/*")) ? 'active': '' }}">
                    <a href="{{ URL($value['url']) }}">
                        <i class="icon {{ $value['icon'] }}"></i>
                        {{ $value['name'] }}
                        @if($value['controller'] == "PendingRequestController")
                            <span class="sidebar-badge">{{ PersianNumber($pending_requests_count) }}</span>
                        @endif
                    </a>
                </li>
            @endif
        @endif

    @endforeach
</ul>
