@if($errors->any())
    <div class="modal fade flash-modal" id="error-modal" tabindex="-1" role="dialog" aria-labelledby="error-modal" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body text-xs-center">
                    <i class="icon error-icon"></i>
                    <b>خطایی رخ داد</b>
                    @foreach($errors->all() as $error)
                        <span>{{ $error }}</span>
                    @endforeach
                </div>

                <div class="modal-footer text-xs-center">
                    <button type="button" class="btn btn-submit" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#error-modal').modal('show');
    </script>
@endif

@if(Session::has('success_flash'))
    <div class="modal fade flash-modal" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="success-modal" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body text-xs-center">
                    <i class="icon success-icon"></i>
                    <b>موفق</b>
                    <span>{{ Session::get('success_flash') }}</span>
                </div>

                <div class="modal-footer text-xs-center">
                    <button type="button" class="btn btn-submit" data-dismiss="modal">باشه</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#success-modal').modal('show');
    </script>
@endif

@if(Session::has('error_flash'))
    <div class="modal fade flash-modal" id="error-modal" tabindex="-1" role="dialog" aria-labelledby="error-modal" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body text-xs-center">
                    <i class="icon error-icon"></i>
                    <b>خطا</b>
                    <span>{{ Session::get('error_flash') }}</span>
                </div>

                <div class="modal-footer text-xs-center">
                    <button type="button" class="btn btn-submit" data-dismiss="modal">باشه</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#error-modal').modal('show');
    </script>
@endif

<div class="modal fade reject-modal" id="logout-modal" tabindex="-1" role="dialog" aria-labelledby="logout-modal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            {!! Form::open(['url' => '/dashboard/auth/logout', 'method' => 'post']) !!}
            <div class="modal-body">
                <header class="row col-xs-12 flex-items-xs-center reject-order-header">
                    <i class="icon alert-icon"></i>
                    <strong>آیا مطمئنید که می خواهید خارج شوید؟</strong>
                </header>
                <br/><br/>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-close" data-dismiss="modal">خیر</button>
                <button type="submit" class="btn btn-remove">بله</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>