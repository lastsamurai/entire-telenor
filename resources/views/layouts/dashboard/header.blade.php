<div class="col-xs-8">
    <a href="{{ url('dashboard/admins/self/edit') }}"><strong>سلام {{ Auth::guard('admin')->user()->name }}</strong></a>
</div>

<div class="col-xs-4 no-padding text-xs-left">
    <a href="javascript:void(0);" data-toggle="modal" data-target="#logout-modal">خروج</a>
</div>