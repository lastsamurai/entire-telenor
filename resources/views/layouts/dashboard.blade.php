<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if(isset($title)) {{ $title }} - @endif {{ config('app.name') }} </title>
    <link rel="icon" type="image/png" href="{{ URL("/img/cinemalog-favicon.png") }}">

    <!-- STYLESHEET -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/admin/bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/iran-roboto.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/shabnam.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/admin/froala_editor.pkgd.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/admin/froala_editor.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/font-awesome.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/material-icons.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/admin/bootstrap-tagsinput.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/chosen.css') }}" />
    @if(app()->getLocale() == 'en')
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/admin/layout-left.css') }}" />
    @else
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/admin/layout-right.css') }}" />
    @endif
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/admin/iStyle.css') }}" />
    @yield('css', '')

    <!-- JS -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/admin/tether.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/admin/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/admin/froala_editor.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/admin/froala_fa.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/admin/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/admin/Sortable.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/admin/jquery.binding.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/chosen.proto.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/chosen.jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/typeahead.jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bloodhound.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/admin/iScript.js') }}"></script>
    @yield('js', '')

    <script type="text/javascript">
        var baseurl = '{{ URL::to('/dashboard') }}/';
        var siteurl = '{{ URL::to('/') }}/';
        var token = '{{ csrf_token() }}';
    </script>

    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container row col-lg-12 col-md-12 col-sm-12 col-xs-12 no-margin no-padding">
    <aside class="col-lg-2 col-md-2 col-sm-12 col-xs-12" id="sidebar">
        @include('layouts.dashboard.sidebar')
    </aside>

    <main class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
        <header class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" id="header">
            @include('layouts.dashboard.header')
        </header>
        @yield('content')
    </main>
</div>


<footer>
    @include('layouts.dashboard.footer')
</footer>

</body>
</html>
