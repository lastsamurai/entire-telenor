<footer class="row justify-content-center mt-5" id="footer-style-1">
    <div class="container">
        <div class="row mb-4">
            <div class="col-12">
                <img id="site-footer-logo" src="{{url('img/landing/item-logo-telenor-white.png')}}">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="widget">
                    <h3>Useful Tools</h3>
                    <ul class="quickLinksFooter">
                        <li class="item-563"><a href="/find-a-store">
                                Store Locator</a>
                        </li>

                        <li class="item-542"><a href="/coverage-map">Coverage Map</a></li>

                        <li class="item-557"><a href="/internet-usage-calculator">Internet Usage Calculator</a></li>

                        <li class="item-556"><a href="/roaming">Roaming Rates</a></li>

                        <li class="item-656"><a href="/frequently-asked-questions">Frequently Asked Questions</a></li>

                        <li class="item-663"><a href="/sitemap">Sitemap</a></li>

                        <li class="item-734"><a href="/mic-tanzania-ltd-subscriber-customer-terms-and-conditions">MIC Tanzania Ltd Subscriber/Customer Terms and Conditions</a></li>

                        <li class="item-895"><a href="http://cdn.tigocloud.net/tz/files/tigo_terms_and_conditions_of_purchase.pdf">Standard Terms and Conditions</a></li>
                    </ul>

                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="widget">
                    <h3>
                        Engage With Us						</h3>
                    <ul class="quickLinksFooter">
                        <li class="item-638"><a href="/contact-us">Contact Us</a></li><li class="item-639"><a href="/tigo-newsletter">Newsletter</a></li><li class="item-764"><a href="/careers">Careers</a></li></ul>

                </div><!-- end widget -->
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="widget">
                    <h3>
                        Engage With Us						</h3>
                    <ul class="quickLinksFooter">
                        <li class="item-638"><a href="/contact-us">Contact Us</a></li><li class="item-639"><a href="/tigo-newsletter">Newsletter</a></li><li class="item-764"><a href="/careers">Careers</a></li></ul>

                </div><!-- end widget -->
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="widget">
                    <h3>
                        Engage With Us						</h3>
                    <ul class="quickLinksFooter">
                        <li class="item-638"><a href="/contact-us">Contact Us</a></li><li class="item-639"><a href="/tigo-newsletter">Newsletter</a></li><li class="item-764"><a href="/careers">Careers</a></li></ul>

                </div><!-- end widget -->
            </div>
            <div class="noShow">
            </div>
        </div>
    </div>
</footer>

<div class="row" id="copyrights">
    <div class="container">
        <div class="col-12 col-sm-12 col-md-12 border-top-white">
            <div class="row">
                <div class="col-10 col-md-10 col-sm-10 copyright-text pt-2">
                    <span><a href="#">Contact Us</a>  |  <a href="#">Integrity Hotline </a>|  <a href="#">About his webstie </a>|  <a href="#">Privacy statement</a></span>
                </div>
                <div class="col-2 col-md-2 col-sm-2 copyright ltr">
                    <span>© Telenor Group</span>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="modal fade registration-modal" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <b class="modal-title align-self-center">{{trans('messages.subscribe')}}</b>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <p class="error"></p>
                            <p class="success"></p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 justify-content-center text-center mb-3">
                            <span for="phone">{{trans('messages.enter_phone_number')}}</span>
                        </div>
                        <div class="col-12">
                            <input type="text" name="phone" id="modal-phone" class="input-text placeholder" placeholder="01XXXXXXXX" value="@if (Request::header('msisdn')) {{ Request::header('msisdn') }} @endif">
                        </div>
                    </div>

                    {{--<div class="row mt-3">
                        <div class="col-12">
                            <input type="text" name="code" id="modal-code" class="input-text" placeholder="Verification Code">
                        </div>
                    </div>--}}
                </div>
            </div>
            <div class="modal-footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 pb-2">
                            <button type="button" class="button bg-green color-white w-100" id="login-btn">{{trans('messages.subscribe')}}</button>
                        </div>
                    </div>
                    <div class="row justify-content-center text-center">
                        <div class="col-12 pb-2">
                            <span>{{trans('messages.subscription-text',['price'=>$subscription->price])}}</span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<div class="modal fade registration-modal" id="verification-modal" tabindex="-1" role="dialog" aria-labelledby="verificationModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <b class="modal-title align-self-center">{{trans('messages.subscribe')}}</b>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <p class="error"></p>
                            <p class="success"></p>
                        </div>
                    </div>

                    <input type="hidden"  name="phone" id="verification-modal-phone" value="">

                    <div class="row mt-3">
                        <div class="col-12 justify-content-center text-center mb-3">
                            <span for="phone">{{trans('messages.enter_last_code')}}</span>
                        </div>
                        <div class="col-12">
                            <input type="text" name="code" id="modal-code" class="input-text" placeholder="{{trans('messages.code')}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 pb-2">
                            <button type="button" class="button bg-green color-white w-100" id="verification-btn">{{trans('messages.subscribe')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade registration-modal" id="logout-modal" tabindex="-1" role="dialog" aria-labelledby="logoutModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <b class="modal-title align-self-center">{{trans('messages.unsubscribe')}}</b>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <p class="error"></p>
                            <p class="success"></p>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-12  text-center">
                            <b>{{trans('messages.confirm_unsubscribe')}}</b>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-12">

                            <input type="hidden" name="logout_user_id" id="logout_user_id" value="@if(Auth::user()){{Auth::user()->id}} @endif">
                            <button type="button" class="button border-blue color-blue w-100" id="logout-btn">{{trans('messages.unsubscribe')}}</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 pb-2">
                            <button type="button" class="button bg-green color-white w-100" id="return-btn">{{trans('messages.back')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

