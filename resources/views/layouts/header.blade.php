<header class="row @if(isset($pageId) and $pageId == 'landing-page') fixed-top @else row sticky-top @endif" id="site-header">
    <div class="col-12">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-md-6" id="site-title">
                            <div class="row">
                                <div class="col-3">
                                    <a href="{{ url("/?lang=$lang") }}">
                                        <img src="{{ url('img/landing/item-logo-telenor.png') }}">
                                    </a>
                                </div>
                                <div class="col-7" id="language-container-big">
                                    <select class="change-language" id="change-lang-big">
                                        @foreach(config('settings.languages') as $key=>$value):
                                        <option value="{{$key}}" @if($key == app()->getLocale()) selected @endif>{{$value}}</option>
                                        @endforeach
                                    </select>
                                    {{--<a href="{{url("/?lang=$lang")}}" id="home-lg" >{{trans('ui.text.home')}}</a>--}}
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-md-6 text-center" id="login-button">
                            <div class="row">
                                <div class="col-4 col-md-7 p-0 m-0">
                                </div>
                                <div class="col-4 col-md-3 p-0 m-0">
                                    <div class="row">
                                        <div class="col-6 col-md-8">
                                            <a href="{{url('/leaderboard')}}" id="site-title">{{trans('ui.text.leaderboard')}}</a>
                                        </div>
                                    {{--    <div class="col-6 col-md-3">
                                            --}}{{--                                            <a href="{{url("/?lang=$lang")}}" id="home-xs" >{{trans('ui.text.home')}}</a>--}}{{--

                                        </div>--}}
                                    </div>
                                </div>
                                <div class="col-4 col-md-2 p-0 m-0">
                                    @if(!Auth::user())
                                        <a href="javascript:;" class="button color-white  color-pink-dark color-white-hover ml-2" data-toggle="modal" data-target="#login-modal">{{trans('messages.login')}}</a>
                                    @else
                                        <a href="javascript:;" class="button color-white  color-pink-dark color-white-hover ml-2" id="logout" data-toggle="modal" data-target="#logout-modal">{{trans('messages.logout')}}</a>
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<section class="row justify-content-center" id="language-container" >
    <div class="col-12">
        <select class="change-xs-language justify-content-center" id="change-xs-lang">
            @foreach(config('settings.languages') as $key=>$value):
            <option value="{{$key}}" @if($key == app()->getLocale()) selected @endif>{{$value}}</option>
            @endforeach
        </select>
    </div>
</section>


