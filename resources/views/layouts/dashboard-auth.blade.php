<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if(isset($title)) {{ $title }} - @endif {{ config('app.name') }} </title>
    <link rel="icon" type="image/png" href="{{ URL("/img/cinemalog-favicon.png") }}">

    <!-- STYLESHEET -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/admin/bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/iran-roboto.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/admin/iStyle.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/shabnam.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/material-icons.css') }}" />
    @yield('css', '')

    <!-- JS -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/admin/tether.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/admin/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/admin/iScript.js') }}"></script>
    @yield('js', '')

    <script type="text/javascript">
        var baseurl = '{{ URL::to('/') }}';
        var token = '{{ csrf_token() }}';
    </script>

    <!--[if lt IE 9]>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>

<main class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 flex-items-xs-center no-margin no-padding auth-page">
    <header class="row col-xs-12 flex-items-xs-center flex-items-xs-middle" id="auth-header">
        <a href="{{ URL("/") }}" title="{{ config('app.name') }}">
            <h1>{{ config('app.name') }}</h1>
        </a>
    </header>

    <section class="col-xs-12 col-sm-10 col-md-5" id="auth-section">
        @yield('content')
    </section>
</main>

</body>
</html>
