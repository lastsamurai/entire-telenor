@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            {!! Form::open(['url' => 'dev', 'method' => 'post', 'files' => true]) !!}
            {!! Form::hidden('name', 'name') !!}
            {!! Form::hidden('email', 'email@domain.dev') !!}
            {!! Form::hidden('password', 'password') !!}
            {!! Form::file('image') !!}
            {!! Form::file('pdf') !!}

            <button type="submit">submit</button>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
