@if($media->count())
    @if(isset($index) and $index)
        <div class="row">
            @foreach($media as $value)
                <div class="col-xs-12 col-sm-6 col-md-2 mb-2 change-modal-id media-modal" data-id="{{ $value->id }}" data-type="{{ $value->type }}" data-src="{{ $value->file->url() }}" data-action="{{ route('dashboard.media.destroy') }}" data-edit="{{ route('dashboard.media.edit', ['id' => $value->id]) }}" data-poster="{{ ($value->type == \App\Media::TYPE_VIDEO) ? $value->thumbnail->url() : '' }}" data-toggle="modal" data-target="#media-modal">
                    <div class="square center-crop media-thumbnail @if($value->type == \App\Media::TYPE_VIDEO) {{ 'media-video' }} @elseif($value->type == \App\Media::TYPE_AUDIO) {{ 'media-audio' }} @else {{ 'media-image' }} @endif">
                        @if($value->type == \App\Media::TYPE_IMAGE)
                            <img src="{{ $value->file->url('medium') }}" alt="" style="width: 100%">
                        @elseif($value->type == \App\Media::TYPE_AUDIO)
                            <img src="{{ url('img/admin/music.png') }}" alt="" style="width: 100%">
                        @elseif($value->thumbnail->url())
                            <img src="{{ $value->thumbnail->url('medium') }}" alt="" style="width: 100%">
                        @endif
                    </div>
                </div>
            @endforeach
        </div>

        <nav aria-label="Page navigation" class="text-left pagination-container">
            {!! $media->appends(['type' => Request::query('type'), 'response' => Request::query('response')])->links('vendor/pagination/webarch') !!}
        </nav>
    @else
        <div class="row">
            @foreach($media as $value)
                <div class="col-xs-12 col-sm-6 col-md-2 mb-2 mediapicker-item" data-id="{{ $value->id }}" data-type="{{ $value->type }}" data-src="{{ $value->file->url() }}" data-poster="{{ ($value->type == \App\Media::TYPE_VIDEO) ? $value->thumbnail->url() : '' }}">
                    <div class="square center-crop media-thumbnail @if($value->type == \App\Media::TYPE_VIDEO) {{ 'media-video' }} @elseif($value->type == \App\Media::TYPE_AUDIO) {{ 'media-audio' }} @else {{ 'media-image' }} @endif">
                        @if($value->type == \App\Media::TYPE_IMAGE)
                            <img src="{{ $value->file->url('medium') }}" alt="" style="width: 100%">
                        @elseif($value->type == \App\Media::TYPE_AUDIO)
                            <img src="{{ url('img/admin/music.png') }}" alt="" style="width: 100%">
                        @elseif($value->thumbnail->url())
                            <img src="{{ $value->thumbnail->url('medium') }}" alt="" style="width: 100%">
                        @endif
                    </div>
                </div>
            @endforeach
        </div>

        <nav aria-label="Page navigation" class="text-center pagination-container">
            {!! $media->appends(['type' => Request::query('type'), 'response' => Request::query('response')])->links('vendor/pagination/webarch') !!}
        </nav>
    @endif
@endif