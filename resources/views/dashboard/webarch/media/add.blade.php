@extends('layouts.webarch.layout')

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => 'dashboard.media.index', 'label' => 'ui.sidebar.media.label'], ['route' => 'dashboard.media.add', 'label' => 'ui.text.add'])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title no-border">
                            <h3 class="d-inline-block"><span class="semi-bold">{{ trans('ui.text.add') }}</span> {{ trans('ui.sidebar.media.name') }}</h3>
                        </div>

                        <div class="grid-body no-border" style="display: block;">
                            <div class="row-fluid">
                                {!! Form::open(['route' => 'dashboard.media.store', 'method' => 'POST', 'class' => 'dropzone']) !!}


                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop