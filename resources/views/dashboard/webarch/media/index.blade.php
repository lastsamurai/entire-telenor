@extends('layouts.webarch.layout')

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => 'dashboard.media.index', 'label' => 'ui.sidebar.media.list'])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title">
                            <h4><span class="semi-bold">{{ trans('ui.sidebar.media.label') }}</span></h4>

                            <div class="tools">
                                <a href="{{ route('dashboard.media.index') }}" class="btn {{ (!Request::query('type')) ? 'btn-default' : 'btn-dark' }} no-transition">
                                    <span>{{ trans('ui.text.all') }}</span>
                                </a>

                                <a href="{{ route('dashboard.media.index', ['type' => \App\Media::TYPE_AUDIO]) }}" class="btn {{ (Request::query('type') == \App\Media::TYPE_AUDIO) ? 'btn-default' : 'btn-dark' }} no-transition">
                                    <span>{{ trans('ui.text.audio') }}</span>
                                </a>

                                <a href="{{ route('dashboard.media.index', ['type' => \App\Media::TYPE_VIDEO]) }}" class="btn {{ (Request::query('type') == \App\Media::TYPE_VIDEO) ? 'btn-default' : 'btn-dark' }} no-transition">
                                    <span>{{ trans('ui.text.video') }}</span>
                                </a>

                                <a href="{{ route('dashboard.media.index', ['type' => \App\Media::TYPE_IMAGE]) }}" class="btn {{ (Request::query('type') == \App\Media::TYPE_IMAGE) ? 'btn-default' : 'btn-dark' }} no-transition">
                                    <span>{{ trans('ui.text.image') }}</span>
                                </a>

                                <a href="{{ route('dashboard.media.add') }}" class="btn btn-primary no-transition">
                                    <i class="fa fa-plus-square-o"></i>
                                    <span>{{ trans('ui.btn.add') }}</span>
                                </a>
                            </div>
                        </div>

                        <div class="grid-body">
                            @include('dashboard.webarch.media.content', ['media' => $media, 'index' => true])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.webarch.elements.modal', ['type' => 'media', 'modalId' => 'media-modal', 'edit' => true])
@stop