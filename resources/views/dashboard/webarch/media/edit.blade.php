@extends('layouts.webarch.layout')

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => 'dashboard.media.index', 'label' => 'ui.sidebar.media.label'], ['route' => 'dashboard.media.edit', 'label' => 'ui.text.edit'])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title no-border">
                            <h3 class="d-inline-block"><span class="semi-bold">{{ trans('ui.text.edit') }}</span> {{ trans('ui.sidebar.media.name') }}</h3>
                        </div>

                        <div class="grid-body no-border" style="display: block;">
                            <div class="row-fluid">
                                {!! Form::model($media, ['route' => 'dashboard.media.update', 'method' => 'PUT', 'files' => true, 'class' => 'needs-validation validate']) !!}
                                {!! Form::hidden('id', $media->id) !!}

                                <div class="row">
                                    <br>

                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="form-label" for="file-uploader">{{ trans('ui.text.file') }}</label>

                                            <div class="controls">
                                                <input type="file" name="file" id="file-uploader" class="form-control" accept="image/*,video/*,audio/*" required/>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            @if($media->type == \App\Media::TYPE_VIDEO)
                                                <video poster="{{ $media->thumbnail->url() }}" controls>
                                                    <source src="{{ $media->file->url() }}" type="video/mp4">
                                                </video>
                                            @elseif($media->type == \App\Media::TYPE_AUDIO)
                                                <audio controls>
                                                    <source src="{{ $media->file->url() }}" type="audio/mp3">
                                                </audio>
                                            @else
                                                <img src="{{ $media->file->url() }}" width="100%">
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 text-left margin-top-20">
                                        <a href="{{ route('dashboard.media.index') }}" class="btn btn-white btn-cons">{{ trans('ui.btn.cancel') }}</a>
                                        <button type="submit" class="btn btn-success btn-cons">{{ trans('ui.btn.submit') }}</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop