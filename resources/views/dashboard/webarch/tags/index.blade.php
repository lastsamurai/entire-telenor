@extends('layouts.webarch.layout')

@section('css')
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/jquery-datatable/jquery.dataTables.min.css') }}"/>
@stop
@section('js')
    <script type="text/javascript" src="{{ url('js/webarch/plugins/jquery-datatable/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/webarch/plugins/jquery-datatable/dataTables.tableTools.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/webarch/plugins/datatables-responsive/datatables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/webarch/plugins/datatables-responsive/lodash.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/webarch/plugins/datatables.js') }}"></script>
@stop

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => 'dashboard.tags.index', 'label' => 'ui.sidebar.tags.name'], ['route' => 'dashboard.tags.index', 'label' => 'ui.text.list'])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title">
                            <h4><span class="semi-bold">{{ trans('ui.text.tags.list') }}</span></h4>
                        </div>

                        <div class="grid-body">
                            <table class="table table-hover table-condensed data-table">
                                <thead>
                                <tr>
                                    <th style="width: 100px;" class="text-center" data-hide="phone,tablet">{{ trans('ui.text.id') }}</th>
                                    <th>{{ trans('ui.text.name') }}</th>
                                    <th data-hide="phone">{{ trans('ui.text.used-number') }}</th>
                                    <th data-hide="phone">{{ trans('ui.text.views') }}</th>
                                    <th style="width: 100px" data-hide="phone"></th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($tags as $tag)
                                    <tr class="change-modal-id show-on-hover-container">
                                        <td class="v-align-middle text-center">
                                            <span>{{ numbers($tag->id) }}</span>
                                        </td>

                                        <td class="v-align-middle">{{ $tag->name }}</td>

                                        <td class="v-align-middle">
                                            <span class="">{{ numbers($tag->count) }}</span>
                                        </td>

                                        <td class="v-align-middle">
                                            <span class="">{{ numbers($tag->views) }}</span>
                                        </td>

                                        <td class="v-align-middle text-center list-tools">

                                            @if($update)
                                                <a href="{{ route('dashboard.tags.edit', ['id' => $tag->id]) }}" class="show-on-hover-visible" data-toggle="tooltip" data-placement="top" title="{{ trans('ui.text.edit') }}">
                                                    <i class="fa fa-pencil color-dark-gray"></i>
                                                </a>
                                            @endif

                                            @if($delete)
                                                <a href="javascript:;" class="show-on-hover-visible change-modal-id" data-id="{{ $tag->id }}" data-action="{{ route('dashboard.tags.destroy') }}" data-toggle="modal" data-target="#delete-modal">
                                                    <i class="fa fa-trash-o color-red"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.webarch.elements.modal', ['type' => 'confirm', 'modalId' => 'delete-modal'])

@stop