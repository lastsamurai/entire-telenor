@extends('layouts.webarch.layout')

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => "dashboard.ads.index", 'label' => "ui.sidebar.ads.label"], ['route' => "dashboard.ads.edit", 'label' => 'ui.text.edit'])])
    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title no-border">
                            <h3 class="d-inline-block"><span class="semi-bold">{{ trans('ui.text.edit') }}</span> {{ trans("ui.sidebar.ads.name") }}</h3>
                        </div>

                        <div class="grid-body no-border" style="display: block;">
                            <div class="row-fluid">
                                {!! Form::model($ads, ['route' => "dashboard.ads.update", 'method' => 'PUT', 'files' => true, 'class' => 'needs-validation validate']) !!}
                                {!! Form::hidden('id', $ads->id) !!}

                                <div class="row">
                                    <br>

                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="form-label" for="name">{{ trans('ui.text.title') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.post-name') }}»</span>

                                            <div class="controls">
                                                {!! Form::text("title", null, ['class' => 'form-control', 'id' => 'name', 'required']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="">{{ trans('ui.text.description') }}</label>

                                            <div class="controls">
                                                {!! Form::textarea("description", null, ['id' => 'description', 'class' => 'form-control', 'required']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="url">{{ trans('ui.text.url') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.url') }}»</span>

                                            <div class="controls">
                                                {!! Form::url("url", $ads->link, ['class' => 'form-control ltr', 'id' => 'url', 'required']) !!}
                                            </div>
                                        </div>

                                        @include('layouts.webarch.elements.mediapicker.handler', ['picker_id' => 'post-image', 'picker_name' => 'image', 'picker_type' => \App\Media::TYPE_IMAGE, 'picker_required' => false, 'picker_label' => trans('ui.text.image'), 'picker_preview' => (isset($ads->media['thumbnail'])) ? $ads->media['thumbnail']->file->url() : null])

                                        @include('layouts.webarch.elements.mediapicker.handler', ['picker_id' => 'post-banner', 'picker_name' => 'banner', 'picker_type' => \App\Media::TYPE_IMAGE, 'picker_required' => false, 'picker_label' => trans('ui.text.banner'), 'picker_preview' => (isset($ads->media['banner'])) ? $ads->media['banner']->file->url() : null])

                                        @include('layouts.webarch.elements.mediapicker.handler', ['picker_id' => 'post-video', 'picker_name' => 'video', 'picker_type' => \App\Media::TYPE_VIDEO, 'picker_required' => false, 'picker_label' => trans('ui.text.video'), 'picker_preview' => (isset($ads->media['video'])) ? $ads->media['video']->file->url() : null])

                                        @if($publish)
                                            <div class="form-group mt-4 mb-0">
                                                <div class="checkbox check-info">
                                                    {{ Form::checkbox('published', 1, null, ['id' => 'publish-flag']) }}
                                                    <label for="publish-flag">{{ trans('ui.text.publish') }}</label>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 text-left margin-top-20">
                                        <a href="{{ route('dashboard.categories.index') }}" class="btn btn-white btn-cons">{{ trans('ui.btn.cancel') }}</a>
                                        <button type="submit" class="btn btn-success btn-cons">{{ trans('ui.btn.submit') }}</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.webarch.elements.mediapicker.modal', ['image' => true, 'video' => true, 'audio' => false, 'active' => 'image'])
@stop