@extends('layouts.webarch.layout')

@section('js')
    <script type="text/javascript" src="{{ url('js/webarch/plugins/rubaxa/sortable.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/webarch/plugins/rubaxa/jquery.binding.js') }}"></script>
@stop

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => "dashboard.ads.index", 'label' => "ui.sidebar.ads.list"])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title">
                            <h4><span class="semi-bold">{{ trans("ui.sidebar.ads.label") }}</span></h4>

                            <div class="tools">
                                <a href="{{ route("dashboard.ads.add") }}" class="btn btn-primary">
                                    <i class="fa fa-plus-square-o"></i>
                                    <span>{{ trans('ui.btn.add') }}</span>
                                </a>
                            </div>

                        </div>

                        <div class="grid-body">
                            @if(isset($ads) and $ads->count())
                                <table class="table table-bordered no-more-tables">
                                    <thead>
                                    <tr>
                                        <th class="text-right">
                                            <span>{{ trans('ui.text.title') }}</span>
                                        </th>
                                        <th class="text-center">
                                            <span>{{ trans('ui.text.description') }}</span>
                                        </th>
                                        <th class="text-center" width="95px" data-hide="phone">
                                            <span>{{ trans('ui.text.image') }}</span>
                                        </th>
                                        <th class="text-center">
                                            <span>{{ trans('ui.text.url') }}</span>
                                        </th>
                                        <th class="text-center" width="100px"></th>
                                    </tr>
                                    </thead>

                                    <tbody id="sortable-container" data-target="{{ route('dashboard.ads.sort') }}">
                                    @foreach($ads as $value)
                                        <tr class="show-on-hover-container sortable-rows" data-id="{{ $value->id }}">
                                            <td class="text-right">
                                                <span>{{ $value->title }}</span>

                                                @if($value->published == \App\Ads::NOT_PUBLISHED)
                                                    <span class="badge badge-warning">{{ trans('ui.text.not-published') }}</span>
                                                @endif
                                            </td>

                                            <td class="text-center">
                                                <span>{{ str_limit(strip_tags($value->description), 85) }}</span>
                                            </td>

                                            <td class="text-center">
                                                @if(isset($value->media['thumbnail']))
                                                    <img src="{{ url($value->media['thumbnail']->file->url()) }}" width="85px">
                                                @endif
                                            </td>

                                            <td class="text-center">
                                                <code>{{ $value->link }}</code>
                                            </td>

                                            <td class="text-center list-tools">
                                                @if($update)
                                                    <a href="{{ route("dashboard.ads.edit", ['id' => $value->id]) }}" class="show-on-hover-visible" data-toggle="tooltip" data-placement="top" title="{{ trans('ui.text.edit') }}">
                                                        <i class="fa fa-pencil color-dark-gray"></i>
                                                    </a>
                                                @endif


                                                @if($publish)
                                                    @if($value->published)
                                                        <a href="{{ route("dashboard.ads.unpublish", ['id' => $value->id]) }}" class="show-on-hover-visible" data-toggle="tooltip" data-placement="top" title="{{ trans('ui.text.unpublish') }}">
                                                            <i class="fa fa-times color-dark-gray"></i>
                                                        </a>
                                                    @else
                                                        <a href="{{ route("dashboard.ads.publish", ['id' => $value->id]) }}" class="show-on-hover-visible" data-toggle="tooltip" data-placement="top" title="{{ trans('ui.text.publish') }}">
                                                            <i class="fa fa-check color-dark-gray"></i>
                                                        </a>
                                                    @endif
                                                @endif

                                                @if($delete)
                                                    <a href="javascript:;" class="show-on-hover-visible change-modal-id" data-id="{{ $value->id }}" data-action="{{ route("dashboard.ads.destroy") }}" data-toggle="modal" data-target="#activate-modal">
                                                        <i class="fa fa-trash-o color-red"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.webarch.elements.modal', ['type' => 'confirm', 'modalId' => 'activate-modal'])
@stop