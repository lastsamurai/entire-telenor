@extends('layouts.webarch.layout')

@section('css')
    <link type="text/css" rel="stylesheet" href="{{ url('css/webarch/plugins/jquery-datatable/jquery.dataTables.min.css') }}"/>
@stop
@section('js')
    <script type="text/javascript" src="{{ url('js/webarch/plugins/jquery-datatable/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/webarch/plugins/jquery-datatable/dataTables.tableTools.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/webarch/plugins/datatables-responsive/datatables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/webarch/plugins/datatables-responsive/lodash.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/webarch/plugins/datatables.js') }}"></script>
@stop

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => 'dashboard.users.index', 'label' => 'ui.sidebar.users.name'], ['route' => 'users.index', 'label' => 'ui.text.list'])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title">
                            <h4><span class="semi-bold">{{ trans('ui.text.users.list') }}</span></h4>
                        </div>

                        <div class="grid-body">
                            <table class="table table-hover table-condensed data-table">
                                <thead>
                                <tr>
                                    <th style="width: 100px;" data-hide="phone,tablet">{{ trans('ui.text.users.id') }}</th>
                                    <th>{{ trans('ui.text.name') }}</th>
                                    <th style="width: 200px">{{ trans('ui.text.phone') }}</th>
                                    <th style="width: 85px" data-hide="phone,tablet">{{ trans('ui.text.operator') }}</th>
                                    <th style="width: 200px" data-hide="phone">{{ trans('ui.text.email') }}</th>
                                    <th style="width: 75px" data-hide="phone">{{ trans('ui.text.status') }}</th>
                                    <th style="width: 150px">{{ trans('ui.text.users.created-at') }}</th>
                                    <th style="width: 100px" data-hide="phone"></th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($users as $user)
                                    <tr class="change-modal-id">
                                        <td class="v-align-middle text-center">
                                            <span>{{ numbers($user->id) }}</span>
                                        </td>

                                        <td class="v-align-middle">{{ ($user->name) ? $user->name : trans('ui.text.not-defined') }}</td>

                                        <td class="v-align-middle">
                                            <span class="">{{ numbers($user->msisdn) }}</span>
                                        </td>

                                        <td class="v-align-middle">
                                            <span class="">{{ ($user->mobile_service_provider == \App\User::MTN_TYPE) ? trans('ui.text.mtn') : trans('ui.text.mci') }}</span>
                                        </td>

                                        <td>
                                            <span class="">{{ ($user->email) ? $user->email : trans('ui.text.not-defined') }}</span>
                                        </td>

                                        <td class="v-align-middle">
                                            @if($user->active)
                                                <span class="label">{{ trans('ui.text.users.active') }}</span>
                                            @else
                                                <span class="label label-inverse">{{ trans('ui.text.users.deactive') }}</span>
                                            @endif
                                        </td>

                                        <td class="v-align-middle">
                                            <span class="ltr d-block text-right">{{ datetime($user->created_at) }}</span>
                                        </td>

                                        <td class="v-align-middle text-center">
                                            @if($user->active)
                                                <button type="button" class="btn btn-danger btn-mini change-modal-id" data-id="{{ $user->id }}" data-action="{{ route('dashboard.users.update.deactivate') }}" data-toggle="modal" data-target="#delete-modal">{{ trans('ui.btn.deactivate') }}</button>
                                            @else
                                                <button type="button" class="btn btn-primary btn-mini change-modal-id" data-id="{{ $user->id }}" data-action="{{ route('dashboard.users.update.activate') }}" data-toggle="modal" data-target="#delete-modal">{{ trans('ui.btn.activate') }}</button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.webarch.elements.modal', ['type' => 'confirm', 'modalId' => 'delete-modal'])
@stop