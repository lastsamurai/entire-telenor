@extends('layouts.webarch.layout')

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => 'dashboard.force-update.index', 'label' => 'ui.sidebar.force-update.name'])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title">
                            <h4><span class="semi-bold">{{ trans('ui.sidebar.force-update.name') }}</span></h4>
                        </div>

                        <div class="grid-body">
                            @if(isset($forceUpdate) and $forceUpdate->count())
                                <table class="table table-bordered no-more-tables">
                                    <thead>
                                    <tr>
                                        <th class="text-center">{{ trans('ui.text.force-update.device-type') }}</th>
                                        <th class="text-center">{{ trans('ui.text.force-update.minimum-version') }}</th>
                                        <th class="text-center">{{ trans('ui.text.force-update.latest-version') }}</th>
                                        <th class="text-center">{{ trans('ui.text.updated-at') }}</th>
                                        <th class="text-center" width="75px"></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($forceUpdate as $value)
                                        <tr class="show-on-hover-container">
                                            <td class="text-center">
                                                @if($value->device == \App\ForceUpdate::ANDROID_DEVICE)
                                                    <span class="name">{{ trans('ui.text.android') }}</span>
                                                @else
                                                    <span class="name">{{ trans('ui.text.ios') }}</span>
                                                @endif
                                            </td>

                                            <td class="text-center">
                                                <code class="minimum">{{ $value->minimum }}</code>
                                            </td>

                                            <td class="text-center">
                                                <code class="latest">{{ $value->latest }}</code>
                                            </td>

                                            <td class="text-center">
                                                <span class="ltr d-block text-center">{{ datetime($value->updated_at, 'date') }}</span>
                                            </td>

                                            <td class="text-center list-tools">
                                                @if($update)
                                                    <a href="javascript:;" class="show-on-hover-visible change-modal-id edit-force-update" data-id="{{ $value->id }}" data-action="{{ route('dashboard.force-update.update') }}" data-toggle="modal" data-target="#force-update-modal">
                                                        <i class="fa fa-pencil color-dark-gray"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.webarch.elements.modal', ['type' => 'force-update', 'modalId' => 'force-update-modal', 'modalMethod' => 'PUT'])
@stop