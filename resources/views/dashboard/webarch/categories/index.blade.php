@extends('layouts.webarch.layout')

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => 'dashboard.categories.index', 'label' => 'ui.sidebar.categories.list'])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title">
                            <h4><span class="semi-bold">{{ trans('ui.sidebar.categories.label') }}</span></h4>

                            <div class="tools">
                                <a href="{{ route('dashboard.categories.add') }}" class="btn btn-primary">
                                    <i class="fa fa-plus-square-o"></i>
                                    <span>{{ trans('ui.btn.add') }}</span>
                                </a>
                            </div>

                        </div>

                        <div class="grid-body">
                            @if(isset($categories) and $categories->count())
                                <table class="table table-bordered no-more-tables">
                                    <thead>
                                    <tr>
                                        <th class="text-right">{{ trans('ui.text.name') }}</th>
{{--                                        <th class="text-right">{{ trans('ui.text.parent') }}</th>--}}
                                        <th class="text-center" width="95px" data-hide="phone">{{ trans('ui.text.image') }}</th>
                                        <th class="text-center">{{ trans('ui.text.color') }}</th>
                                        <th class="text-center" width="100px"></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($categories as $category)
                                        <tr class="show-on-hover-container">
                                            <td class="text-right">{{ $category->name }}</td>
{{--                                            <td class="text-right">{{ (!empty($category->getAncestors()->toArray())) ? implode(',', $category->getAncestors()->pluck('name')->toArray()) : trans('ui.text.not-defined') }}</td>--}}
                                            <td class="text-center">
                                                @if(isset($category->media['thumbnail']))
                                                    <img src="{{ url($category->media['thumbnail']->file->url()) }}" width="85px">
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <code class="color-hex">{{ $category->hex }}</code>
                                                <code class="color-preview" style="background: {{ $category->hex }}"></code>
                                            </td>

                                            <td class="text-center list-tools">
                                                @if($update)
                                                    <a href="{{ route('dashboard.categories.edit', ['id' => $category->id]) }}" class="show-on-hover-visible" data-toggle="tooltip" data-placement="top" title="{{ trans('ui.text.edit') }}">
                                                        <i class="fa fa-pencil color-dark-gray"></i>
                                                    </a>
                                                @endif

                                                @if($delete)
                                                    <a href="javascript:;" class="show-on-hover-visible change-modal-id" data-id="{{ $category->id }}" data-action="{{ route('dashboard.categories.destroy') }}" data-toggle="modal" data-target="#activate-modal">
                                                        <i class="fa fa-trash-o color-red"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <nav aria-label="Page navigation" class="text-left pagination-container">
                                    {!! $categories->links('vendor/pagination/webarch') !!}
                                </nav>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.webarch.elements.modal', ['type' => 'confirm', 'modalId' => 'activate-modal'])
@stop