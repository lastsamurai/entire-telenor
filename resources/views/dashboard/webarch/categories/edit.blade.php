@extends('layouts.webarch.layout')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/colorpicker.css') }}" />
@stop
@section('js')
    <script type="text/javascript" src="{{ URL::asset('js/colorpicker.js') }}"></script>
@stop

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => 'dashboard.categories.index', 'label' => 'ui.sidebar.categories.label'], ['route' => 'dashboard.categories.edit', 'label' => 'ui.text.edit'])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title no-border">
                            <h3 class="d-inline-block"><span class="semi-bold">{{ trans('ui.text.edit') }}</span> {{ trans('ui.sidebar.categories.name') }}</h3>
                        </div>

                        <div class="grid-body no-border" style="display: block;">
                            <div class="row-fluid">
                                {!! Form::model($category, ['route' => 'dashboard.categories.update', 'method' => 'PUT', 'files' => true, 'class' => 'needs-validation validate']) !!}
                                {!! Form::hidden('id', $category->id) !!}
                                {!! Form::hidden('type', $type) !!}
                                {!! Form::hidden('parent_id', 0) !!}

                                <div class="row">
                                    <br>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="form-label" for="name">{{ trans('ui.text.name') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.category-name') }}»</span>

                                            <div class="controls">
                                                {!! Form::text("name_en", (isset($category->name->en))?$category->name->en:null, ['class' => 'form-control', 'id' => 'name', 'required']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="name">{{ trans('ui.text.name_ur') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.category-name') }}»</span>

                                            <div class="controls">
                                                {!! Form::text("name_ur", (isset($category->name->ar))?$category->name->ar :null, ['class' => 'form-control', 'id' => 'name', 'required']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label">{{ trans('ui.text.color') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «#F46C22»</span>

                                            <div class="controls">
                                                {!! Form::text("hex", null, ['id' => 'hex', 'class' => 'form-control hex-input ltr', 'required', 'autocomplete' => 'off']) !!}
                                            </div>
                                        </div>

                                        @include('layouts.webarch.elements.mediapicker.handler', ['picker_id' => 'category-image', 'picker_name' => 'image', 'picker_type' => \App\Media::TYPE_IMAGE, 'picker_required' => false, 'picker_label' => trans('ui.text.image'), 'picker_preview' => (isset($category->media['thumbnail'])) ? $category->media['thumbnail']->file->url() : null])
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 text-left margin-top-20">
                                        <a href="{{ route('dashboard.categories.index') }}" class="btn btn-white btn-cons">{{ trans('ui.btn.cancel') }}</a>
                                        <button type="submit" class="btn btn-success btn-cons">{{ trans('ui.btn.submit') }}</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.webarch.elements.mediapicker.modal', ['image' => true, 'video' => false, 'audio' => false, 'active' => 'image'])
@stop