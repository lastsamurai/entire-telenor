@extends('layouts.webarch.layout')

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => 'dashboard.settings.index', 'label' => 'ui.sidebar.settings.label'])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title no-border">
                            <h3 class="d-inline-block"><span class="semi-bold">{{ trans('ui.text.edit') }}</span> {{ trans('ui.sidebar.settings.label') }}</h3>
                        </div>

                        <div class="grid-body no-border" style="display: block;">
                            <div class="row-fluid">
                                {!! Form::model($settings, ['route' => 'dashboard.settings.update', 'method' => 'PUT', 'files' => true, 'class' => 'needs-validation validate']) !!}
                                {!! Form::hidden('id', $settings->id) !!}
                                <div class="row">
                                    <br>

                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="form-label" for="site-description">{{ trans('ui.text.site-description') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.site-description') }}»</span>

                                            <div class="controls">
                                                {!! Form::text("site_description", null, ['class' => 'form-control', 'id' => 'site-description', 'required', 'minlength' => 150, 'maxlength' => 200]) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="site-description">{{ trans('ui.text.site-keywords') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.site-keywords') }}»</span>

                                            <div class="controls">
                                                {!! Form::text("site_keywords", null, ['class' => 'form-control', 'id' => 'site-keywords', 'required', 'maxlength' => 250]) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label">{{ trans('ui.text.favicon') }}</label>

                                            <div class="controls">
                                                <input type="file" name="favicon" id="image-uploader" class="form-control" accept="image/*" />
                                            </div>

                                            @if($settings->favicon->url())
                                                <img src="{{ $settings->favicon->url() }}" class="d-block mt-2" style="width: 150px">
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label">{{ trans('ui.text.site-image') }}</label>

                                            <div class="controls">
                                                <input type="file" name="image" id="image-uploader" class="form-control" accept="image/*" />
                                            </div>

                                            @if($settings->image->url())
                                                <img src="{{ $settings->image->url() }}" class="d-block mt-2" style="width: 150px">
                                            @endif
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 text-left margin-top-20">
                                        <a href="{{ route('dashboard.settings.index') }}" class="btn btn-white btn-cons">{{ trans('ui.btn.cancel') }}</a>
                                        <button type="submit" class="btn btn-success btn-cons">{{ trans('ui.btn.submit') }}</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop