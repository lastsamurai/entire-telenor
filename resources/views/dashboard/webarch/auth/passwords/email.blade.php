@extends('layouts.dashboard-auth')

<!-- Main Content -->
@section('content')
    <div class="row col-xs-12 no-margin no-padding">
        <div class="col-xs-12 no-padding panel panel-default">
            <div class="panel-heading">فراموشی کلمه عبور</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/dashboard/auth/password/email') }}">
                    {{ csrf_field() }}

                    <div class="col-xs-12 no-padding form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="material-form-group">
                            <input id="email" type="email" class="material-form-control" name="email" placeholder="ایمیل" value="{{ old('email') }}" required>
                            <span class="bar"></span>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <footer class="row no-margin no-padding form-group flex-items-xs-middle">
                        <div class="col-xs-6 no-padding"></div>

                        <div class="col-xs-6 no-padding text-xs-left">
                            <button type="submit" class="btn btn-submit">ارسال لینک تغییر کلمه عبور</button>
                        </div>
                    </footer>
                </form>

                @if (session('status'))
                    <div class="modal fade flash-modal" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="success-modal" aria-hidden="true">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-body text-xs-center">
                                    <i class="icon success-icon"></i>
                                    <b>موفق!</b>
                                    <span>{{ session('status') }}</span>
                                </div>

                                <div class="modal-footer text-xs-center">
                                    <button type="button" class="btn btn-submit" data-dismiss="modal">OK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script>
                        $('#success-modal').modal('show');
                    </script>
                @endif
            </div>
        </div>
    </div>
@endsection
