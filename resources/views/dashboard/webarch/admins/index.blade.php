@extends('layouts.webarch.layout')

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => 'dashboard.admins.index', 'label' => 'ui.sidebar.admins.list'])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title">
                            <h4><span class="semi-bold">{{ trans('ui.sidebar.admins.label') }}</span></h4>

                            <div class="tools">
                                <a href="{{ route('dashboard.admins.add') }}" class="btn btn-primary">
                                    <i class="fa fa-plus-square-o"></i>
                                    <span>{{ trans('ui.btn.add') }}</span>
                                </a>
                            </div>

                        </div>

                        <div class="grid-body">
                            @if(isset($admins) and $admins->count())
                                <table class="table table-bordered no-more-tables">
                                    <thead>
                                    <tr>
                                        <th class="text-right">{{ trans('ui.text.name') }}</th>
                                        <th class="text-center">{{ trans('ui.text.email') }}</th>
                                        <th class="text-center" style="width: 85px">{{ trans('ui.text.image') }}</th>
                                        <th class="text-center">{{ trans('ui.text.role') }}</th>
                                        <th class="text-center">{{ trans('ui.text.created-at') }}</th>
                                        <th class="text-center" width="75px"></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($admins as $admin)
                                        <tr class="show-on-hover-container">
                                            <td class="text-right">{{ $admin->name }}</td>

                                            <td class="text-center">
                                                <code>{{ $admin->email }}</code>
                                            </td>

                                            <td class="text-center">
                                                @if($admin->image->url())
                                                    <img src="{{ $admin->image->url() }}" width="75px">
                                                @endif
                                            </td>

                                            <td class="text-center">{{ $admin->role->name }}</td>

                                            <td class="text-center">
                                                <span class="ltr d-block text-center">{{ datetime($admin->created_at, 'date') }}</span>
                                            </td>

                                            <td class="text-center list-tools">
                                                @if($update)
                                                    <a href="{{ route('dashboard.admins.edit', ['id' => $admin->id]) }}" class="show-on-hover-visible" data-toggle="tooltip" data-placement="top" title="{{ trans('ui.text.edit') }}">
                                                        <i class="fa fa-pencil color-dark-gray"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <nav aria-label="Page navigation" class="text-left pagination-container">
                                    {!! $admins->links('vendor/pagination/webarch') !!}
                                </nav>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop