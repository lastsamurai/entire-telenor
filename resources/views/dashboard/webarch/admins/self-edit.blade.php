@extends('layouts.webarch.layout')

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => 'dashboard.admins.index', 'label' => 'ui.sidebar.admins.label'], ['route' => 'dashboard.admins.self.edit', 'label' => 'ui.text.user-profile'])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title no-border">
                            <h3 class="d-inline-block"><span class="semi-bold">{{ trans('ui.text.edit') }}</span> {{ trans('ui.text.user-profile') }}</h3>
                        </div>

                        <div class="grid-body no-border" style="display: block;">
                            <div class="row-fluid">
                                {!! Form::model($admin, ['route' => 'dashboard.admins.self.update', 'method' => 'PUT', 'files' => true, 'class' => 'needs-validation validate']) !!}
                                <div class="row">
                                    <br>

                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="form-label" for="name">{{ trans('ui.text.name') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.name') }}»</span>

                                            <div class="controls">
                                                {!! Form::text("name", null, ['class' => 'form-control', 'id' => 'name', 'required']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="email">{{ trans('ui.text.email') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «admin@domain.com»</span>

                                            <div class="controls">
                                                {!! Form::email("email", null, ['class' => 'form-control ltr-input', 'id' => 'email', 'disabled']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label">{{ trans('ui.text.image') }}</label>

                                            <div class="controls">
                                                <input type="file" name="image" id="image-uploader" class="form-control" accept="image/*" />
                                            </div>

                                            @if($admin->image->url())
                                                <img src="{{ url($admin->image->url()) }}" style="display: block; width: 100px; padding: 7px 0">
                                            @endif
                                        </div>

                                        <div class="form-group mt-4 pt-2">
                                            <label class="form-label" for="current-password">{{ trans('ui.text.current-password') }}</label>

                                            <div class="controls">
                                                {!! Form::password("old_password", ['class' => 'form-control', 'id' => 'current-password', 'minlength' => '8']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group mt-4 pt-2">
                                            <label class="form-label" for="new-password">{{ trans('ui.text.new-password') }}</label>

                                            <div class="controls">
                                                {!! Form::password("password", ['class' => 'form-control', 'id' => 'new-password', 'minlength' => '8']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group mt-4 pt-2">
                                            <label class="form-label" for="retype-new-password">{{ trans('ui.text.retype-new-password') }}</label>

                                            <div class="controls">
                                                {!! Form::password("password_confirmation", ['class' => 'form-control', 'id' => 'retype-new-password', 'minlength' => '8']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 text-left margin-top-20">
                                        <a href="{{ route('dashboard.admins.index') }}" class="btn btn-white btn-cons">{{ trans('ui.btn.cancel') }}</a>
                                        <button type="submit" class="btn btn-success btn-cons">{{ trans('ui.btn.submit') }}</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop