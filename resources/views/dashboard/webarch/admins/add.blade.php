@extends('layouts.webarch.layout')

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => 'dashboard.admins.index', 'label' => 'ui.sidebar.admins.label'], ['route' => 'dashboard.admins.add', 'label' => 'ui.text.add'])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title no-border">
                            <h3 class="d-inline-block"><span class="semi-bold">{{ trans('ui.text.add') }}</span> {{ trans('ui.sidebar.admins.name') }}</h3>
                        </div>

                        <div class="grid-body no-border" style="display: block;">
                            <div class="row-fluid">
                                {!! Form::open(['route' => 'dashboard.admins.store', 'method' => 'POST', 'files' => true, 'class' => 'needs-validation validate']) !!}
                                <div class="row">
                                    <br>

                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="form-label" for="name">{{ trans('ui.text.name') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.name') }}»</span>

                                            <div class="controls">
                                                {!! Form::text("name", null, ['class' => 'form-control', 'id' => 'name', 'required']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="email">{{ trans('ui.text.email') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «admin@domain.com»</span>

                                            <div class="controls">
                                                {!! Form::email("email", null, ['class' => 'form-control ltr-input', 'id' => 'email', 'required']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="email">{{ trans('ui.sidebar.roles.name') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.role-name') }}»</span>

                                            <div class="controls">
                                                {!! Form::select("role_id", $roles, null, ['required', 'id' => 'source-b', 'class' => 'custom-select-2', 'style' => 'width:100%']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label">{{ trans('ui.text.image') }}</label>

                                            <div class="controls">
                                                <input type="file" name="image" id="image-uploader" class="form-control" accept="image/*" />
                                            </div>
                                        </div>

                                        <div class="form-group mt-4 pt-2">
                                            <div class="checkbox check-info">
                                                {{ Form::checkbox('generate_password', 1, true, ['id' => 'generate-password']) }}
                                                <label for="generate-password">{{ trans('ui.text.generate-password') }}</label>
                                            </div>
                                        </div>

                                        <div class="form-group mt-4 pt-2">
                                            <label class="form-label" for="admin-password">{{ trans('ui.text.password') }}</label>

                                            <div class="controls">
                                                @if(!old('generate_password'))
                                                    {!! Form::password("password", ['class' => 'form-control', 'id' => 'admin-password', 'required']) !!}
                                                @else
                                                    {!! Form::password("password", ['class' => 'form-control', 'id' => 'admin-password', 'required', 'disabled']) !!}
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 text-left margin-top-20">
                                        <a href="{{ route('dashboard.admins.index') }}" class="btn btn-white btn-cons">{{ trans('ui.btn.cancel') }}</a>
                                        <button type="submit" class="btn btn-success btn-cons">{{ trans('ui.btn.submit') }}</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop