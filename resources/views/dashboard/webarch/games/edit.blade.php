@extends('layouts.webarch.layout')

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => "dashboard.games.index", 'label' => "ui.sidebar.games.label"], ['route' => "dashboard.games.edit", 'label' => 'ui.text.edit'])])
    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title no-border">
                            <h3 class="d-inline-block"><span class="semi-bold">{{ trans('ui.text.edit') }}</span> {{ trans("ui.sidebar.games.name") }}</h3>
                        </div>

                        <div class="grid-body no-border" style="display: block;">
                            <div class="row-fluid">
                                {!! Form::model($game, ['route' => "dashboard.games.update", 'method' => 'PUT', 'files' => true, 'class' => 'needs-validation validate']) !!}
                                {!! Form::hidden('id', $game->id) !!}

                                <div class="row">
                                    <br>

                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="form-label" for="name">{{ trans('ui.text.name') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.game-name') }}»</span>

                                            <div class="controls">
                                                {!! Form::text("title_en", (isset($game->title->en))?$game->title->en:null, ['class' => 'form-control', 'id' => 'name', 'required']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="name">{{ trans('ui.text.name_ur') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.game-name') }}»</span>

                                            <div class="controls">
                                                {!! Form::text("title_ur", (isset($game->title->ar))?$game->title->ar:null, ['class' => 'form-control', 'id' => 'name', 'required']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="name">{{ trans('ui.text.link') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «google.com»</span>

                                            <div class="controls">
                                                {!! Form::url("link", $game->link, ['class' => 'form-control', 'id' => 'link', 'required']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="category">{{ trans('ui.sidebar.categories.name') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.category-name') }}»</span>

                                            <div class="controls">
                                                {!! Form::select("category_id[]", $categories, $game->categories->pluck('id')->toArray(), ['required', 'multiple', 'id' => 'category', 'class' => 'custom-select-2', 'style' => 'width:100%']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="">{{ trans('ui.text.description') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.game-description') }}»</span>

                                            <div class="controls">
                                                {!! Form::textarea("description_en", (isset($game->description->en))?$game->description->en:null, ['id' => 'description', 'class' => 'form-control']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="">{{ trans('ui.text.description_ur') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.game-description') }}»</span>

                                            <div class="controls">
                                                {!! Form::textarea("description_ur", (isset($game->description->ar))? $game->description->ar:null, ['id' => 'description', 'class' => 'form-control']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="form-label" for="">{{ trans('ui.text.supported-browsers') }}</label>
                                        </div>

                                        @foreach($browsers as $key => $browser)
                                            <div class="form-group mb-0">
                                                <div class="checkbox check-info">
                                                    {{ Form::checkbox("browsers[$key]", $browser['id'], (in_array($browser['id'], $game->supported_browsers)) ? true : false, ['id' => $browser['slug']]) }}
                                                    <label for="{{ $browser['slug'] }}">{{ $browser["label-$locale"] }}</label>
                                                </div>
                                            </div>
                                        @endforeach

                                        @include('layouts.webarch.elements.mediapicker.handler', ['picker_id' => 'game-image', 'picker_name' => 'image', 'picker_type' => \App\Media::TYPE_IMAGE, 'picker_required' => false, 'picker_label' => trans('ui.text.image'), 'picker_preview' => (isset($game->media['thumbnail'])) ? $game->media['thumbnail']->file->url() : null])

                                        <div class="form-group">
                                            <label class="form-label" for="tags">{{ trans('ui.text.tag') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.tag') }}»</span>

                                            <div class="controls">
                                                {!! Form::text("tags", implode(',', $game->tags->pluck('name')->toArray()), ['id' => 'tags', 'class' => 'form-control', 'required', 'data-role' => 'tagsinput']) !!}
                                            </div>
                                        </div>

                                        @if($publish)
                                            <div class="form-group mt-4 mb-0">
                                                <div class="checkbox check-info">
                                                    {{ Form::checkbox('published', 1, null, ['id' => 'publish-flag']) }}
                                                    <label for="publish-flag">{{ trans('ui.text.publish') }}</label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="checkbox check-info">
                                                    {{ Form::checkbox('editor_choice', 1, null, ['id' => 'editor-choice-flag']) }}
                                                    <label for="editor-choice-flag">{{ trans('ui.text.editor-choice') }}</label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="checkbox check-info">
                                                    {{ Form::checkbox('push', 1, null, ['id' => 'notification-flag']) }}
                                                    <label for="notification-flag">{{ trans('ui.text.push') }}</label>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 text-left margin-top-20">
                                        <a href="{{ route('dashboard.categories.index') }}" class="btn btn-white btn-cons">{{ trans('ui.btn.cancel') }}</a>
                                        <button type="submit" class="btn btn-success btn-cons">{{ trans('ui.btn.submit') }}</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.webarch.elements.mediapicker.modal', ['image' => true, 'video' => false, 'audio' => false, 'active' => 'image'])
@stop