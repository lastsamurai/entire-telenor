@extends('layouts.webarch.layout')

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => "dashboard.games.index", 'label' => "ui.sidebar.games.list"])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title">
                            <h4><span class="semi-bold">{{ trans("ui.sidebar.games.label") }}</span></h4>

                            <div class="tools">
                                <a href="{{ route("dashboard.games.add") }}" class="btn btn-primary">
                                    <i class="fa fa-plus-square-o"></i>
                                    <span>{{ trans('ui.btn.add') }}</span>
                                </a>
                            </div>

                        </div>

                        <div class="grid-body">
                            @if(isset($games) and $games->count())
                                <table class="table table-bordered no-more-tables">
                                    <thead>
                                    <tr>
                                        <th class="text-right">{{ trans('ui.text.name') }}</th>
                                        <th class="text-center">{{ trans('ui.text.description') }}</th>
                                        <th class="text-right" width="95px" data-hide="phone">{{ trans('ui.text.image') }}</th>
                                        <th class="text-center">{{ trans('ui.text.category') }}</th>
                                        <th class="text-center" width="100px"></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($games as $game)
                                        <tr class="show-on-hover-container">
                                            <td class="text-right">
                                                <span>{{ $game->title }}</span>

                                                @if($game->published == \App\game::NOT_PUBLISHED)
                                                    <span class="badge badge-warning">{{ trans('ui.text.not-published') }}</span>
                                                @endif
                                            </td>
                                            <td class="text-right">{{ str_limit(strip_tags($game->description), 85) }}</td>
                                            <td class="text-center">
                                                @if(isset($game->media['thumbnail']))
                                                    <img src="{{ url($game->media['thumbnail']->file->url()) }}" width="85px">
                                                @endif
                                            </td>
                                            <td class="text-center">{{ $game->categories->first()['name'] }}</td>

                                            <td class="text-center list-tools">
                                                @if($update)
                                                    <a href="{{ route("dashboard.games.edit", ['id' => $game->id]) }}" class="show-on-hover-visible" data-toggle="tooltip" data-placement="top" title="{{ trans('ui.text.edit') }}">
                                                        <i class="fa fa-pencil color-dark-gray"></i>
                                                    </a>
                                                @endif


                                                @if($publish)
                                                    @if($game->published)
                                                        <a href="{{ route("dashboard.games.push", ['id' => $game->id]) }}" class="show-on-hover-visible" data-toggle="tooltip" data-placement="top" title="{{ trans('ui.text.push') }}">
                                                            <i class="fa fa-volume-up color-dark-gray"></i>
                                                        </a>

                                                        <a href="{{ route("dashboard.games.unpublish", ['id' => $game->id]) }}" class="show-on-hover-visible" data-toggle="tooltip" data-placement="top" title="{{ trans('ui.text.unpublish') }}">
                                                            <i class="fa fa-times color-dark-gray"></i>
                                                        </a>
                                                    @else
                                                        <a href="{{ route("dashboard.games.publish", ['id' => $game->id]) }}" class="show-on-hover-visible" data-toggle="tooltip" data-placement="top" title="{{ trans('ui.text.publish') }}">
                                                            <i class="fa fa-check color-dark-gray"></i>
                                                        </a>
                                                    @endif
                                                @endif

                                                @if($delete)
                                                    <a href="javascript:;" class="show-on-hover-visible change-modal-id" data-id="{{ $game->id }}" data-action="{{ route("dashboard.games.destroy") }}" data-toggle="modal" data-target="#activate-modal">
                                                        <i class="fa fa-trash-o color-red"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <nav aria-label="Page navigation" class="text-left pagination-container">
                                    {!! $games->links('vendor/pagination/webarch') !!}
                                </nav>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.webarch.elements.modal', ['type' => 'confirm', 'modalId' => 'activate-modal'])
@stop