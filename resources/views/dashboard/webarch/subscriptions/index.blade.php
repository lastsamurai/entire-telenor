@extends('layouts.webarch.layout')

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => 'dashboard.subscriptions.index', 'label' => 'ui.sidebar.subscriptions.label'])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title no-border">
                            <h3 class="d-inline-block"><span class="semi-bold">{{ trans('ui.text.edit') }}</span> {{ trans('ui.sidebar.subscriptions.label') }}</h3>
                        </div>

                        <div class="grid-body no-border" style="display: block;">
                            <div class="row-fluid">
                                {!! Form::open(['route' => 'dashboard.subscriptions.update', 'method' => 'PUT', 'files' => true, 'class' => 'needs-validation validate']) !!}
                                @foreach($subscriptions as $key => $subscription)
                                    <div class="row">
                                        {!! Form::hidden("subscriptions[$key][id]", $subscription->id) !!}

                                        <div class="col-md-6 col-xs-12">
                                            <div class="form-group">
                                                <h3>{{ \App\Subscription::typeToString($subscription->type) }}</h3>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label" for="price-{{ $subscription->id }}">{{ trans('ui.text.price') }}</label>
                                                <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.price') }}»</span>

                                                <div class="controls">
                                                    {!! Form::text("subscriptions[$key][price]", $subscription->price, ['class' => 'form-control', 'id' => "price-$subscription->id", 'required']) !!}
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label" for="unit-{{ $subscription->id }}">{{ trans('ui.text.unit') }}</label>
                                                <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.unit') }}»</span>

                                                <div class="controls">
                                                    {!! Form::select("subscriptions[$key][unit]", $units, $subscription->getOriginal('unit'), ['required', 'id' => "unit-$subscription->id", 'class' => 'custom-select-2 ltr', 'style' => 'width:100%']) !!}
                                                </div>
                                            </div>

                                            <br><br>
                                        </div>
                                    </div>
                                @endforeach


                                <div class="row">
                                    <div class="col-xs-12 text-left margin-top-20">
                                        <a href="{{ route('dashboard.subscriptions.index') }}" class="btn btn-white btn-cons">{{ trans('ui.btn.cancel') }}</a>
                                        <button type="submit" class="btn btn-success btn-cons">{{ trans('ui.btn.submit') }}</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop