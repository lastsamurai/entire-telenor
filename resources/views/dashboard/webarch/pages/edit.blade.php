@extends('layouts.webarch.layout')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/froala_editor.pkgd.css') }}" />
@stop
@section('js')
    <script type="text/javascript" src="{{ URL::asset('js/froala_editor.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/froala_fa.js') }}"></script>
@stop

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => 'dashboard.pages.index', 'label' => 'ui.sidebar.pages.label'], ['route' => 'dashboard.pages.add', 'label' => 'ui.text.edit'])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title no-border">
                            <h3 class="d-inline-block"><span class="semi-bold">{{ trans('ui.text.edit') }}</span> {{ trans('ui.sidebar.pages.name') }}</h3>

                            <div class="tools">
                                <a href="{{ route('pages.detail', ['slug' => $page->slug]) }}" target="_blank" class="link"></a>
                            </div>
                        </div>

                        <div class="grid-body no-border" style="display: block;">
                            <div class="row-fluid">
                                {!! Form::model($page, ['route' => 'dashboard.pages.update', 'method' => 'PUT', 'class' => 'needs-validation validate']) !!}
                                {!! Form::hidden('id', $page->id) !!}

                                <div class="row">
                                    <br>

                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="form-label" for="title">{{ trans('ui.text.name') }}</label>
                                            <span class="help">{{ trans('ui.text.example') }}: «{{ trans('ui.example.page-name') }}»</span>

                                            <div class="controls">
                                                {!! Form::text("title", null, ['class' => 'form-control', 'id' => 'title', 'required']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="form-label">{{ trans('ui.text.description') }}</label>

                                            <div class="controls">
                                                {!! Form::textarea("description", null, ['id' => 'description', 'class' => 'froala', 'required']) !!}
                                            </div>
                                        </div>

                                        @if($publish)
                                            <div class="form-group">
                                                <div class="controls">
                                                    <div class="row col-xs-12 no-margin">
                                                        <div class="checkbox check-info">
                                                            {{ Form::checkbox('published', 1, null, ['id' => 'published']) }}
                                                            <label for="published">{{ trans('ui.text.publish') }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 text-left margin-top-20">
                                        <a href="{{ route('dashboard.pages.index') }}" class="btn btn-white btn-cons">{{ trans('ui.btn.cancel') }}</a>
                                        <button type="submit" class="btn btn-success btn-cons">{{ trans('ui.btn.submit') }}</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop