@extends('layouts.webarch.layout')

@section('content')
    @include('layouts.webarch.elements.breadcrumb', ['items' => array(['route' => 'dashboard.pages.index', 'label' => 'ui.sidebar.pages.list'])])

    <div class="row">
        <div class="col-xs-12">
            <div class="row-fluid">
                <div class="span12">
                    <div class="grid simple">
                        <div class="grid-title">
                            <h4><span class="semi-bold">{{ trans('ui.sidebar.pages.label') }}</span></h4>

                            <div class="tools">
                                <a href="{{ route('dashboard.pages.add') }}" class="btn btn-primary">
                                    <i class="fa fa-plus-square-o"></i>
                                    <span>{{ trans('ui.btn.add') }}</span>
                                </a>

                            </div>

                        </div>

                        <div class="grid-body">
                            @if(isset($pages) and $pages->count())
                                <table class="table table-bordered no-more-tables">
                                    <thead>
                                    <tr>
                                        <th class="text-right">{{ trans('ui.text.name') }}</th>
                                        <th class="text-right" data-hide="phone">{{ trans('ui.text.description') }}</th>
                                        <th class="text-center">{{ trans('ui.text.link') }}</th>
                                        <th class="text-center" width="100px"></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($pages as $page)
                                        <tr class="show-on-hover-container">
                                            <td class="text-right">
                                                <span>{{ $page->title }}</span>

                                                @if($page->published == \App\Page::NOT_PUBLISHED)
                                                    <span class="badge badge-warning">{{ trans('ui.text.not-published') }}</span>
                                                @endif
                                            </td>
                                            <td class="text-right">{{ str_limit(strip_tags($page->description), 75) }}</td>
                                            <td class="text-center">
                                                <a href="{{ route('pages.detail', ['slug' => $page->slug]) }}" target="_blank">{{ trans('ui.text.link') }}</a>
                                            </td>

                                            <td class="text-center list-tools">
                                                @if($update)
                                                    <a href="{{ route('dashboard.pages.edit', ['id' => $page->id]) }}" class="show-on-hover-visible" data-toggle="tooltip" data-placement="top" title="{{ trans('ui.text.edit') }}">
                                                        <i class="fa fa-pencil color-dark-gray"></i>
                                                    </a>
                                                @endif

                                                @if($delete)
                                                    <a href="javascript:;" class="show-on-hover-visible change-modal-id" data-id="{{ $page->id }}" data-action="{{ route('dashboard.pages.destroy') }}" data-toggle="modal" data-target="#activate-modal">
                                                        <i class="fa fa-trash-o color-red"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <nav aria-label="Page navigation" class="text-left pagination-container">
                                    {!! $pages->links('vendor/pagination/webarch') !!}
                                </nav>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.webarch.elements.modal', ['type' => 'confirm', 'modalId' => 'activate-modal'])
@stop