@extends('layouts.dashboard')

@section('content')
    <div class="container col-xs-12 has-popover" id="dashboard-content">
        @if($write)
            {!! Form::open(['url' => '/dashboard/admins/add', 'method' => 'POST', 'files' => true]) !!}
            <div class="row table-section mt-1">
                <div class="row col-xs-12 media-container">
                    <header class="row col-xs-12 p-0" id="toggle-admin-create">
                        <strong>ایجاد ادمین جدید</strong>
                    </header>

                    <div class="row col-xs-12 m-0 p-0" id="admin-create-section" style="display: none">
                        <div class="row col-xs-12 no-margin mb-2 p-0">
                            {!! Form::text("name", null, ['class' => 'w-50', 'placeholder' => 'نام*', 'required']) !!}
                        </div>

                        <div class="row col-xs-12 no-margin mb-2 p-0">
                            {!! Form::text("email", null, ['class' => '', 'placeholder' => 'ایمیل*', 'required']) !!}
                        </div>

                        <div class="row col-xs-6 no-margin mb-2 p-0">
                            <input type="file" name="image" id="image-uploader" class="inputfile inputfile-6" accept="image/*" />
                            <label for="image-uploader">
                                <span class="uploader-label"><p class="text-xs-right placeholder-color">عکس</p></span>
                                <strong>آپلود</strong>
                            </label>
                        </div>

                        <div class="row col-xs-12 no-margin mb-2 p-0">
                            {!! Form::select("role_id", $roles, null, ['required', 'class' => 'correct-select']) !!}
                        </div>

                        <div class="row no-margin col-xs-12 no-padding" id="submit-area">
                            <button type="submit" class="btn-submit full-width" data-toggle="modal" data-target="#push-notification-modal">ارسال</button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        @endif

        <div class="row table-section">
            <div class="col-xs-12 no-padding">
                <div class="row col-xs-12 no-margin no-padding sortable-header">
                    <div class="col-xs-3" title="نام">نام</div>
                    <div class="col-xs-3">ایمیل</div>
                    <div class="col-xs-2">نقش</div>
                    <div class="col-xs-2">تاریخ ایجاد</div>

                    <div class="col-xs-2" title=""></div>
                </div>

                @if(!empty($admins))
                    @foreach($admins as $value)
                        <div class="row col-xs-12 no-margin no-padding sortable-rows show-on-hover-container fixed-height">
                            <div class="col-xs-3">
                                <span>{{ $value->name }}</span>
                            </div>

                            <div class="col-xs-3">
                                <span>{{ $value->email }}</span>
                            </div>

                            <div class="col-xs-2">
                                <span>{{ $value->role->name }}</span>
                            </div>

                            <div class="col-xs-2">
                                <span>{{ jDate::forge($value->created_at)->format('y/m/d') }}</span>
                            </div>

                            <div class="col-xs-2">
                                @if($update)
                                    <a href="{{ URL("dashboard/admins/edit/$value->id") }}" class="show-on-hover-visible">ویرایش</a>
                                @endif
                            </div>
                        </div>
                    @endforeach

                    <nav aria-label="Page navigation" class="text-xs-center">
                        {!! $admins->links('vendor/pagination/admin') !!}
                    </nav>
                @endif

            </div>
        </div>
    </div>
@stop