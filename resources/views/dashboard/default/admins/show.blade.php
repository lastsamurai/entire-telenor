@extends('layouts.dashboard')

@section('content')
    <div class="container col-xs-12" id="dashboard-content">
        <div class="row col-xs-12 flex-items-xs-right" id="admin-search-container">
            {{--@include(".dashboard.search")--}}
        </div>

        <div class="row" id="page-icon">
            <div class="col-xs-12" id="user-detail-head">
                <div class="media flex-items-xs-middle">
                    <a class="media-left" href="#">
                        <img src="{{ URL("img/admin/avatar_placeholder_100px.png") }}" alt="{{ $admin->name }}">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">{{ $admin->name }}</h4>
                        <p>{{ $admin->email }}</p>
                        <p class="user-status {{ $admin->isOnline() ? ' online' : 'offline' }}">{{ $admin->isOnline() ? ' online' : 'offline' }}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="user-details">
            <div class="col-xs-12">
                <table>
                    <tr>
                        <td>User:</td>
                        <td>{{ $admin->name }}</td>
                    </tr>
                    <tr>
                        <td>Email:</td>
                        <td>{{ $admin->email }}</td>
                    </tr>
                    <tr>
                        <td>Joined:</td>
                        <td>{{ date_format($admin->created_at, 'Y-m-d h:i:s') }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@stop