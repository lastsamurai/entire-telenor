@extends('layouts.dashboard')

@section('content')
    <div class="container col-xs-12 has-popover" id="dashboard-content">
        {!! Form::open(['url' => '/dashboard/admins/update', 'method' => 'PUT', 'files' => true]) !!}
        {!! Form::hidden('id', $admin->id) !!}
        <div class="row table-section mt-1">
            <div class="row col-xs-12 media-container">
                <header><strong>ایجاد ادمین جدید</strong></header>

                <div class="row col-xs-12 no-margin mb-2">
                    {!! Form::text("name", $admin->name, ['class' => '', 'placeholder' => 'نام*', 'required']) !!}
                </div>

                <div class="row col-xs-12 no-margin mb-2">
                    {!! Form::text("email", $admin->email, ['class' => '', 'placeholder' => 'ایمیل*', 'required', 'disabled']) !!}
                </div>

                <div class="row col-xs-12 no-margin">
                    <div class="col-xs-6 no-padding-right pb-2">
                        <img src="{{ $admin->image->url('medium') }}" style="display: inline-block; width: 185px; padding: 7px 0">
                        <input type="file" name="image" id="image-uploader" class="inputfile inputfile-6" accept="image/*" />
                        <label for="image-uploader">
                            <span class="uploader-label"><p class="text-xs-right placeholder-color">عکس</p></span>
                            <strong>آپلود</strong>
                        </label>
                    </div>
                </div>

                <div class="row col-xs-12 no-margin mb-2">
                    {!! Form::select("role_id", $roles, $admin->role_id, ['required', 'class' => 'correct-select']) !!}
                </div>

                <div class="row no-margin col-xs-12 no-padding" id="submit-area">
                    <button type="submit" class="btn-submit full-width" data-toggle="modal" data-target="#push-notification-modal">ارسال</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop