@extends('layouts.dashboard')

@section('content')
    <div class="container col-xs-12" id="dashboard-content">
        <div class="row table-section" id="force-update-page">
            <div class="col-xs-12 no-padding">
                <div class="row col-xs-12 no-margin no-padding sortable-header">
                    <div class="col-xs-4 text-right">نوع سیستم</div>
                    <div class="col-xs-2 text-right">نسخه حداقل</div>
                    <div class="col-xs-2 text-right">آخرین نسخه</div>
                    <div class="col-xs-2 text-right">تاریخ به روز رسانی</div>
                    <div class="col-xs-2"></div>

                </div>

                @if(!empty($force_update))
                    @foreach($force_update as $value)
                        <div class="row col-xs-12 no-margin no-padding sortable-rows show-on-hover-container fixed-height" data-id="{{ $value->id }}">
                            <div class="col-xs-4 name">
                                <span>
                                    @if($value->device == \App\ForceUpdate::ANDROID_DEVICE)
                                        Android
                                    @else
                                        iOS
                                    @endif
                                </span>
                            </div>

                            <div class="col-xs-2 minimum">
                                <span>{{ $value->minimum }}</span>
                            </div>

                            <div class="col-xs-2 latest">
                                <span>{{ $value->latest }}</span>
                            </div>

                            <div class="col-xs-2">
                                <span>{{ jDate::forge($value->updated_at)->format('l، j  F Y') }}</span>
                            </div>

                            <div class="col-xs-2">
                                @if($update)
                                    <button type="button" data-toggle="modal" data-target="#edit-force-update" data-id="{{ $value->id }}" class="change-modal-id full-opacity edit-force-update show-on-hover-visible">
                                        <span>ویرایش</span>
                                    </button>
                                @endif
                            </div>
                        </div>
                    @endforeach
                @endif

                @if($update)
                    <div class="modal fade" id="edit-force-update" tabindex="-1" role="dialog" aria-labelledby="edit-force-update" aria-hidden="true">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                {!! Form::open(['url' => 'dashboard/force-update/update', 'method' => 'PUT']) !!}
                                {!! Form::hidden('id', null, ['class' => 'modal-id']) !!}
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">ویرایش</h4>
                                </div>

                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="minimum" class="mb-1">نسخه حداقل</label>
                                        {!! Form::text('minimum', null, ['class' => 'material-form-control', 'placeholder' => 'نسخه حداقل*', 'required', 'id' => 'minimum']) !!}
                                    </div>

                                    <div class="form-group">
                                        <label for="latest" class="mb-1">آخرین نسخه</label>
                                        {!! Form::text('latest', null, ['class' => 'material-form-control', 'placeholder' => 'آخرین نسخه*', 'required', 'id' => 'latest']) !!}
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-close" data-dismiss="modal">بستن</button>
                                    <button type="submit" class="btn btn-submit">ثبت</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@stop