@extends('layouts.dashboard')

@section('content')
    <div class="container col-xs-12" id="dashboard-content">
        <div class="row table-section ">
            <div class="row col-xs-12 media-container">
                <div class="row col-xs-12 edit-setting-row no-padding no-margin show-on-hover-container">
                    {!! Form::open(['url'=>'/dashboard/settings/update/logout', 'method'=>'POST', 'class' => 'row no-margin col-xs-12 no-padding']) !!}
                    {!! Form::hidden('slug', \App\Setting::LOGOUT_SLUG) !!}
                    <header class="row col-xs-12 no-margin">
                        <div class="col-xs-6 no-padding">
                            <strong>متن خروج از اپلیکیشن</strong>
                        </div>
                        <div class="col-xs-6 no-padding text-xs-left show-on-hover-visible">
                            <a href="javascript:void(0);" class="color-green hover-color-green edit-btn">ویرایش</a>
                            <a href="javascript:void(0);" class="color-black hover-color-black no-edit-btn" style="display: none">لغو</a>
                        </div>

                    </header>

                    <div class="row col-xs-12 no-margin bold-span">
                        <p>{!! $logout['value'] !!}</p>
                    </div>

                    <div class="row col-xs-12 no-margin">
                        <label for="contact">شماره</label>
                        {!! Form::text("contact", $logout['contact'], ['id' => 'contact', 'required', 'disabled']) !!}
                    </div>

                    <div class="row col-xs-12 no-margin">
                        <label for="number">عبارت</label>
                        {!! Form::text("number", $logout['number'], ['id' => 'number', 'required', 'disabled']) !!}
                    </div>


                    <div class="row no-margin col-xs-12 no-padding submit-area" style="display: none">
                        <button type="submit" class="btn-submit full-width" data-toggle="modal" data-target="#push-notification-modal">ذخیره</button>
                    </div>
                    {!! Form::close() !!}
                </div>


                <div class="row col-xs-12 edit-setting-row no-padding no-margin mt-2 show-on-hover-container">
                    {!! Form::open(['url'=>'/dashboard/settings/update/general', 'method' => 'POST', 'class' => 'row no-margin col-xs-12 no-padding']) !!}
                    {!! Form::hidden('slug', \App\Setting::DOWNLOAD_APP_SLUG) !!}
                    <header class="row col-xs-12 no-margin">
                        <div class="col-xs-6 no-padding">
                            <strong>لینک به اشتراک گذاری اپلیکیشن</strong>
                        </div>
                        <div class="col-xs-6 no-padding text-xs-left show-on-hover-visible">
                            <a href="javascript:void(0);" class="color-green hover-color-green edit-btn">ویرایش</a>
                            <a href="javascript:void(0);" class="color-black hover-color-black no-edit-btn" style="display: none">لغو</a>
                        </div>

                    </header>

                    <div class="row col-xs-12 no-margin">
                        {!! Form::url("value", $download_app, ['id' => 'contact', 'required', 'disabled']) !!}
                    </div>

                    <div class="row no-margin col-xs-12 no-padding submit-area" style="display: none">
                        <button type="submit" class="btn-submit full-width" data-toggle="modal" data-target="#push-notification-modal">ذخیره</button>
                    </div>
                    {!! Form::close() !!}
                </div>

                <div class="row col-xs-12 edit-setting-row no-padding no-margin mt-2 show-on-hover-container">
                    {!! Form::open(['url'=>'/dashboard/settings/update/general', 'method'=>'POST', 'class' => 'row no-margin col-xs-12 no-padding']) !!}
                    {!! Form::hidden('slug', \App\Setting::APP_STORE_SLUG) !!}
                    <header class="row col-xs-12 no-margin">
                        <div class="col-xs-6 no-padding">
                            <strong>لینک اپ استور</strong>
                        </div>
                        <div class="col-xs-6 no-padding text-xs-left show-on-hover-visible">
                            <a href="javascript:void(0);" class="color-green hover-color-green edit-btn">ویرایش</a>
                            <a href="javascript:void(0);" class="color-black hover-color-black no-edit-btn" style="display: none">لغو</a>
                        </div>

                    </header>

                    <div class="row col-xs-12 no-margin">
                        {!! Form::url("value", $ios, ['id' => 'contact', 'required', 'disabled']) !!}
                    </div>

                    <div class="row no-margin col-xs-12 no-padding submit-area" style="display: none">
                        <button type="submit" class="btn-submit full-width" data-toggle="modal" data-target="#push-notification-modal">ذخیره</button>
                    </div>
                    {!! Form::close() !!}
                </div>


                <div class="row col-xs-12 edit-setting-row no-padding no-margin mt-2 show-on-hover-container">
                    {!! Form::open(['url'=>'/dashboard/settings/update/general', 'method'=>'POST', 'class' => 'row no-margin col-xs-12 no-padding']) !!}
                    {!! Form::hidden('slug', \App\Setting::GOOGLE_PLAY_SLUG) !!}
                    <header class="row col-xs-12 no-margin">
                        <div class="col-xs-6 no-padding">
                            <strong>لینک گوگل پلی</strong>
                        </div>
                        <div class="col-xs-6 no-padding text-xs-left show-on-hover-visible">
                            <a href="javascript:void(0);" class="color-green hover-color-green edit-btn">ویرایش</a>
                            <a href="javascript:void(0);" class="color-black hover-color-black no-edit-btn" style="display: none">لغو</a>
                        </div>

                    </header>

                    <div class="row col-xs-12 no-margin">
                        {!! Form::url("value", $android, ['id' => 'contact', 'required', 'disabled']) !!}
                    </div>

                    <div class="row no-margin col-xs-12 no-padding submit-area" style="display: none">
                        <button type="submit" class="btn-submit full-width" data-toggle="modal" data-target="#push-notification-modal">ذخیره</button>
                    </div>
                    {!! Form::close() !!}
                </div>


                <div class="row col-xs-12 edit-setting-row no-padding no-margin mt-2 show-on-hover-container">
                    {!! Form::open(['url' => '/dashboard/settings/update/general', 'method'=>'POST', 'class' => 'row no-margin col-xs-12 no-padding']) !!}
                    {!! Form::hidden('slug', \App\Setting::TERMS_SLUG) !!}
                    <header class="row col-xs-12 no-margin">
                        <div class="col-xs-6 no-padding">
                            <strong>شرایط و ضوابط</strong>
                        </div>
                        <div class="col-xs-6 no-padding text-xs-left show-on-hover-visible">
                            <a href="javascript:void(0);" class="color-green hover-color-green edit-btn">ویرایش</a>
                            <a href="javascript:void(0);" class="color-black hover-color-black no-edit-btn" style="display: none">لغو</a>
                        </div>

                    </header>

                    <div class="row col-xs-12 no-margin">
                        {!! Form::textarea("value", $terms, ['id' => 'contact', 'required', 'disabled']) !!}
                    </div>

                    <div class="row no-margin col-xs-12 no-padding submit-area" style="display: none">
                        <button type="submit" class="btn-submit full-width" data-toggle="modal" data-target="#push-notification-modal">ذخیره</button>
                    </div>
                    {!! Form::close() !!}
                </div>


                <div class="row col-xs-12 edit-setting-row no-padding no-margin mt-2 show-on-hover-container">
                    {!! Form::open(['url'=>'/dashboard/settings/update/general', 'method'=>'POST', 'class' => 'row no-margin col-xs-12 no-padding']) !!}
                    {!! Form::hidden('slug', \App\Setting::PRIVACY_SLUG) !!}
                    <header class="row col-xs-12 no-margin">
                        <div class="col-xs-6 no-padding">
                            <strong>حریم خصوصی</strong>
                        </div>
                        <div class="col-xs-6 no-padding text-xs-left show-on-hover-visible">
                            <a href="javascript:void(0);" class="color-green hover-color-green edit-btn">ویرایش</a>
                            <a href="javascript:void(0);" class="color-black hover-color-black no-edit-btn" style="display: none">لغو</a>
                        </div>

                    </header>

                    <div class="row col-xs-12 no-margin">
                        {!! Form::textarea("value", $privacy, ['id' => 'contact', 'required', 'disabled']) !!}
                    </div>

                    <div class="row no-margin col-xs-12 no-padding submit-area" style="display: none">
                        <button type="submit" class="btn-submit full-width" data-toggle="modal" data-target="#push-notification-modal">ذخیره</button>
                    </div>
                    {!! Form::close() !!}
                </div>


                <div class="row col-xs-12 edit-setting-row no-padding no-margin mt-2 show-on-hover-container">
                    {!! Form::open(['url'=>'/dashboard/settings/update/general', 'method'=>'POST', 'class' => 'row no-margin col-xs-12 no-padding']) !!}
                    {!! Form::hidden('slug', \App\Setting::ANDROID_LIBRARIES_SLUG) !!}
                    <header class="row col-xs-12 no-margin">
                        <div class="col-xs-6 no-padding">
                            <strong>کتابخانه‌‌های متن باز اندروید</strong>
                        </div>
                        <div class="col-xs-6 no-padding text-xs-left show-on-hover-visible">
                            <a href="javascript:void(0);" class="color-green hover-color-green edit-btn">ویرایش</a>
                            <a href="javascript:void(0);" class="color-black hover-color-black no-edit-btn" style="display: none">لغو</a>
                        </div>

                    </header>

                    <div class="row col-xs-12 no-margin">
                        {!! Form::textarea("value", $android_libraries, ['id' => 'contact', 'required', 'disabled']) !!}
                    </div>

                    <div class="row no-margin col-xs-12 no-padding submit-area" style="display: none">
                        <button type="submit" class="btn-submit full-width" data-toggle="modal" data-target="#push-notification-modal">ذخیره</button>
                    </div>
                    {!! Form::close() !!}
                </div>

                <div class="row col-xs-12 edit-setting-row no-padding no-margin mt-2 show-on-hover-container">
                    {!! Form::open(['url'=>'/dashboard/settings/update/general', 'method'=>'POST', 'class' => 'row no-margin col-xs-12 no-padding']) !!}
                    {!! Form::hidden('slug', \App\Setting::IOS_LIBRARIES_SLUG) !!}
                    <header class="row col-xs-12 no-margin">
                        <div class="col-xs-6 no-padding">
                            <strong>کتابخانه‌‌های متن باز آی او اس</strong>
                        </div>
                        <div class="col-xs-6 no-padding text-xs-left show-on-hover-visible">
                            <a href="javascript:void(0);" class="color-green hover-color-green edit-btn">ویرایش</a>
                            <a href="javascript:void(0);" class="color-black hover-color-black no-edit-btn" style="display: none">لغو</a>
                        </div>

                    </header>

                    <div class="row col-xs-12 no-margin">
                        {!! Form::textarea("value", $ios_libraries, ['id' => 'contact', 'required', 'disabled']) !!}
                    </div>

                    <div class="row no-margin col-xs-12 no-padding submit-area" style="display: none">
                        <button type="submit" class="btn-submit full-width" data-toggle="modal" data-target="#push-notification-modal">ذخیره</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
@stop