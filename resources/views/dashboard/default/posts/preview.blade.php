<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{ $request->title }}</title>
    <link rel="icon" type="image/png" href="{{ URL("/img/rooma-favicon.png") }}">

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/mediaelementplayer.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/posts.css') }}" />

    <script type="text/javascript" src="{{ url::asset('js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ url::asset('js/tether.min.js') }}"></script>
    <script type="text/javascript" src="{{ url::asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ url::asset('js/mediaelement-and-player.min.js') }}"></script>

    <style>
        tooltip {
            background: red;
            color: white;
            border-radius: 3px;
            padding: 0 2px;
        }

        .tooltip-inner {
            font-family: 'IranSans';
            text-align: right;
            padding: 15px;
            max-width: 85%;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $('tooltip').tooltip({trigger: 'click'});
        });
    </script>
</head>

<body>

<article class="container-fluid">
    <section class="row" id="category-container">
        <div class="col-12">
            @foreach($categories as $category)
                <span class="category-name">{{ $category['name']['fa'] }}</span>
            @endforeach
        </div>
    </section>

    <section class="row" id="post-details">
        <div class="col-12 mt-1">
            @if(isset($image) and $image and $request->type != \App\Post::TYPE_VIDEO)
                <div class="row">
                    <div class="col-12">
                        <img src="{{ $image }}" style="width: 100%; margin-bottom: 15px;">
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-12 video-player">
                    @if($request->type == \App\Post::TYPE_VIDEO)
                        <video width="100%" poster="{{ (isset($image) and $image) ? $image : '' }}" controls style="margin-bottom: 10px">
                            <source src="{{ $file }}" type="video/mp4">
                        </video>
                    @elseif($request->type == \App\Post::TYPE_PODCAST)
                        <audio width="100%" controls style="margin-bottom: 15px">
                            <source src="{{ $file }}" type="audio/mp3">
                        </audio>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <h1 style="margin-top: 0;">{{ $request->title }}</h1>

                    @if(isset($request->short_description))
                        <p>{{ $request->short_description }}</p>
                    @endif
                </div>
            </div>

            @if(isset($request->description))
                <div class="row" id="post-body">
                    <div class="col-12">
                        {!! $request->description !!}
                    </div>
                </div>
            @endif
        </div>
    </section>

    <section class="row" id="post-footer">
        <div class="col-12">
            <div class="row author">
                <div class="col-4 mt-1 mb-1">
                    @if(isset($request->sources))
                        <span class="creator">منبع: {{ $request->sources }}</span>
                    @endif
                </div>

                <div class="col align-self-center pr-0 text-left">
                    <div class="color-blue" style="display: inline-block">
                        @foreach($categories as $category)
                            <span class="category-name">{{ $category['name']['fa'] }}</span>
                        @endforeach
                    </div>
                    <span> | </span>
                    <span class="created-at">{{ PersianNumber(jDate::forge($now)->ago()) }}</span>
                </div>
            </div>

            <div class="row mb-1">
                <div class="col-12 mt-1">
                    @foreach($tags as $tag)
                        <a href="{{ config('settings.deeplinking.tags') . $tag->tag_slug }}" class="tags">{{ $tag->tag_name }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
</article>

<script>
    $('video, audio').mediaelementplayer({
        enableAutosize: false,
        hideVideoControlsOnLoad: true,
        pluginPath: 'https://cdnjs.com/libraries/mediaelement/',
        shimScriptAccess: 'always'
    });
</script>


</body>
</html>
