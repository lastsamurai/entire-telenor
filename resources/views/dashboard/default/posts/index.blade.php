@extends('layouts.dashboard')

@section('content')
    <div class="container col-xs-12{{ ($publish) ? ' publisher' : '' }}" id="dashboard-content">
        @if($write)
            {!! Form::open(['url' => "/dashboard/$segment/add", 'method' => 'POST', 'files' => true]) !!}
            {!! Form::hidden('type', $type) !!}
            <div class="row table-section mt-1">
                <div class="row col-xs-12 media-container">
                    <header><strong>ایجاد پست جدید</strong></header>

                    <div class="row col-xs-12 no-margin">
                        <div class="col-xs-6 p-0">
                            {!! Form::text("title", null, ['class' => '', 'placeholder' => 'عنوان*', 'required']) !!}
                        </div>
                    </div>

                    {{--<div class="row col-xs-12 no-margin">
                        <div class="col-xs-3 mb-1 p-0">
                            {!! Form::checkbox('editor_choice', 1, false, ['id' => 'editor-choice']) !!}
                            <label for="editor-choice" class="material-icons"></label>
                            <label for="editor-choice">انتخاب سردبیر</label>
                        </div>
                    </div>--}}

                    <div class="row col-xs-12 no-margin">
                        <div class="col-xs-12 pr-0 mb-2">
                            <select class="correct-select searchable-select" name="category_id[]" id="select-category" data-placeholder="انتخاب دسته بندی" required multiple>
                                @foreach($categories as $id => $name)
                                    @if (is_array(old('category_id')))
                                        <option value="{{ $id }}" {{ (in_array($id, old('category_id'))) ? 'selected' : '' }}>{{ $name }}</option>
                                    @else
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    @if($type == \App\Post::TYPE_VIDEO)
                        <div class="row col-xs-12 no-margin">
                            <div class="col-xs-6 pr-0 pb-1">
                                <input type="file" name="video" id="video-uploader" class="inputfile inputfile-6" accept="video/mp4" required />
                                <label for="video-uploader">
                                    <span class="uploader-label"><p class="text-xs-right placeholder-color">ویدیو*</p></span>
                                    <strong>آپلود</strong>
                                </label>
                            </div>
                        </div>
                    @elseif($type == \App\Post::TYPE_PODCAST)
                        <div class="row col-xs-12 no-margin">
                            <div class="col-xs-6 pr-0 pb-1">
                                <input type="file" name="audio" id="audio-uploader" class="inputfile inputfile-6" accept="audio/mpeg" required />
                                <label for="audio-uploader">
                                    <span class="uploader-label"><p class="text-xs-right placeholder-color">صدا*</p></span>
                                    <strong>آپلود</strong>
                                </label>
                            </div>
                        </div>
                    @endif

                    <div class="row col-xs-12 no-margin">
                        <div class="col-xs-12 p-0 mb-1">
                            {!! Form::textarea("description", null, ['class' => '', 'placeholder' => 'توضیحات*', 'required']) !!}
                        </div>
                    </div>

                    <div class="row col-xs-12 no-margin">
                        <div class="col-xs-6 pr-0 pb-1">
                            <input type="file" name="thumbnail" id="image-uploader" class="inputfile inputfile-6" accept="image/*" required />
                            <label for="image-uploader">
                                <span class="uploader-label"><p class="text-xs-right placeholder-color">عکس*</p></span>
                                <strong>آپلود</strong>
                            </label>
                        </div>
                    </div>

                    <div class="row col-xs-12 m-0 mb-1">
                        <div class="col-xs-12 p-0">
                            {!! Form::text("tags", null, ['class' => 'tagsinput', 'placeholder' => 'برچسب‌ها']) !!}
                        </div>
                    </div>

                    @if($publish)
                        <div class="row col-xs-12 no-margin">
                            <div class="col-xs-3 mb-1 p-0">
                                {!! Form::checkbox('published', 1, false, ['id' => 'publish-flag']) !!}
                                <label for="publish-flag" class="material-icons"></label>
                                <label for="publish-flag">منتشر کردن</label>
                            </div>
                        </div>

                        <div class="row col-xs-12 no-margin">
                            <div class="col-xs-3 mb-1 p-0">
                                {!! Form::checkbox('push', 1, false, ['id' => 'notification-flag']) !!}
                                <label for="notification-flag" class="material-icons"></label>
                                <label for="notification-flag">ارسال نوتیفیکیشن</label>
                            </div>
                        </div>
                    @endif

                    <div class="row col-xs-12 p-0 no-margin">
                        <div class="col-xs-12 pb-1" id="submit-area">
                            <button type="submit" class="btn-submit full-width" id="submit-btn">ذخیره</button>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        @endif

        <div class="row table-section">
            <div class="row col-xs-12 no-margin no-padding">
                <div class="row col-xs-12 no-margin no-padding sortable-header">
                    {{--<div class="col-xs-1"></div>--}}
                    <div class="col-xs-4 text-right">عنوان</div>
                    <div class="col-xs-3 text-right">متن</div>
                    <div class="col-xs-2 text-right">عکس</div>
                    <div class="col-xs-1 text-right">دسته</div>
                    <div class="col-xs-2 text-right"></div>
                </div>

                @if(isset($posts) and !empty($posts))
                    @foreach($posts as $value)
                        <div class="row col-xs-12 no-margin no-padding sortable-rows show-on-hover-container flex-items-xs-middle {{ (!$value->published) ? 'disabled' : '' }}">
                            {{--<div class="col-xs-1">
                                @if($value->editor_choice)
                                    <i class="star-icon"></i>
                                @endif
                            </div>--}}

                            <div class="col-xs-4">
                                <span class="post-title">{{ $value->title }}</span>
                            </div>

                            <div class="col-xs-3 over text-truncate">
                                <span class="">{{ str_limit(strip_tags($value->description), 75) }}</span>
                            </div>

                            <div class="col-xs-2">
                                <img src="{{ $value->media['thumbnail']['file'] }}" width="85px">
                            </div>

                            <div class="col-xs-1 align-content-center">
                                <span>{{ $value->categories->first()['name'] }}</span>
                            </div>

                            <div class="col-xs-2">
                                <a href="{{ URL("dashboard/$segment/edit/$value->id") }}" class="show-on-hover-visible">ویرایش</a>

                                @if($publish)
                                    <span class="show-on-hover-visible"> | </span>
                                    @if($value->published)
                                        <a href="javascript:;" data-link="{{ url("dashboard/$segment/push/$value->id") }}" class="show-on-hover-visible push-item">نوتیفیکیشن</a>
                                        <span class="show-on-hover-visible"> | </span>
                                        <a href="{{ url("dashboard/$segment/unpublish/$value->id") }}" class="show-on-hover-visible btn-link">عدم انتشار</a>
                                    @else
                                        <a href="{{ url("dashboard/$segment/publish/$value->id") }}" class="show-on-hover-visible">انتشار</a>
                                    @endif
                                @endif

                                @if($delete)
                                    <span class="show-on-hover-visible"> | </span>
                                    <a href="{{ URL("dashboard/$segment/delete/$value->id") }}" class="show-on-hover-visible btn-link">حذف</a>
                                @endif
                            </div>
                        </div>
                    @endforeach

                    <nav aria-label="Page navigation" class="text-xs-center pagination-container">
                        {!! $posts->links('vendor/pagination/admin') !!}
                    </nav>
                @endif
            </div>
        </div>
    </div>


    <div class="modal fade reject-modal" id="push-modal" tabindex="-1" role="dialog" aria-labelledby="push-modal" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <header class="row col-xs-12 flex-items-xs-center">
                        <strong>آیا مطمئنید که می خواهید این پست را به صورت پوش نوتیفیکیشن ارسال کنید؟</strong>

                        <code></code>
                    </header>
                    <br/><br/>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-close" data-dismiss="modal">خیر</button>
                    <a href="#" id="push-link" class="btn btn-remove">بله</a>
                </div>
            </div>
        </div>
    </div>
@stop