@extends('layouts.dashboard')

@section('content')
    <div class="container col-xs-12{{ ($publish) ? ' publisher' : '' }}" id="dashboard-content">
        {!! Form::model($post, ['url' => "/dashboard/$segment/update", 'method' => 'PUT', 'files' => true]) !!}
        {!! Form::hidden('id', $post->id) !!}
        {!! Form::hidden('type', $post->type) !!}
        <div class="row table-section mt-1">
            <div class="row col-xs-12 media-container">
                <header><strong>ویرایش پست</strong></header>

                <div class="row col-xs-12 no-margin">
                    <div class="col-xs-6 p-0">
                        {!! Form::text("title", null, ['class' => '', 'placeholder' => 'عنوان*', 'required']) !!}
                    </div>
                </div>

                {{--<div class="row col-xs-12 no-margin">
                    <div class="col-xs-3 mb-1 p-0">
                        {!! Form::checkbox('editor_choice', 1, null, ['id' => 'editor-choice']) !!}
                        <label for="editor-choice" class="material-icons"></label>
                        <label for="editor-choice">انتخاب سردبیر</label>
                    </div>
                </div>--}}

                <div class="row col-xs-12 no-margin">
                    <div class="col-xs-6 pr-0 mb-1">
                        <select class="correct-select searchable-select" name="category_id[]" data-placeholder="انتخاب دسته بندی" required multiple>
                            @foreach($categories as $id => $name)
                                @if (is_array(old('category_id')))
                                    <option value="{{ $id }}" {{ (in_array($id, old('category_id'))) ? 'selected' : '' }}>{{ $name }}</option>
                                @elseif($post->categories->count())
                                    <option value="{{ $id }}" {{ (in_array($id, $post->categories->pluck('id')->toArray())) ? 'selected' : '' }}>{{ $name }}</option>
                                @else
                                    <option value="{{ $id }}">{{ $name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>

                @if($post->type == \App\Post::TYPE_VIDEO)
                    <div class="row col-xs-12 no-margin">
                        <div class="col-xs-6 pr-0 pb-1">
                            {!! Form::hidden('video_id', $post->media['video']['id']) !!}
                            <video width="320" height="240" controls>
                                <source src="{{ $post->media['video']['file'] }}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                            <input type="file" name="video" id="video-uploader" class="inputfile inputfile-6" accept="video/mp4,video/quicktime" />
                            <label for="video-uploader">
                                <span class="uploader-label"><p class="text-xs-right placeholder-color">ویدیو</p></span>
                                <strong>آپلود</strong>
                            </label>
                        </div>
                    </div>
                @elseif($post->type == \App\Post::TYPE_PODCAST)
                    <div class="row col-xs-12 no-margin">
                        <div class="col-xs-6 pr-0 pb-1">
                            {!! Form::hidden('audio_id', $post->media['audio']['id']) !!}
                            <audio controls>
                                <source src="{{ $post->media['audio']['file'] }}" type="audio/mpeg">
                                Your browser does not support the video tag.
                            </audio>
                            <input type="file" name="audio" id="audio-uploader" class="inputfile inputfile-6" accept="audio/mpeg" />
                            <label for="audio-uploader">
                                <span class="uploader-label"><p class="text-xs-right placeholder-color">صدا</p></span>
                                <strong>آپلود</strong>
                            </label>
                        </div>
                    </div>
                @endif

                {!! Form::textarea("description", null, ['class' => '', 'placeholder' => 'توضیحات*', 'required']) !!}

                <div class="row col-xs-12 no-margin">
                    <div class="col-xs-6 no-padding-right pb-2">
                        {!! Form::hidden('thumbnail_id', $post->media['thumbnail']['id']) !!}
                        <img src="{{ $post->media['thumbnail']['file'] }}" style="display: inline-block; width: 185px; padding: 7px 0">
                        <input type="file" name="thumbnail" id="image-uploader" class="inputfile inputfile-6" accept="image/*" />
                        <label for="image-uploader">
                            <span class="uploader-label"><p class="text-xs-right placeholder-color">عکس</p></span>
                            <strong>آپلود</strong>
                        </label>
                    </div>
                </div>

                <div class="row col-xs-12 m-0 mb-1">
                    <div class="col-xs-12 p-0">
                        {!! Form::text("tags", $post->tagged->implode('tag_name', ','), ['class' => 'tagsinput', 'placeholder' => 'برچسب‌ها']) !!}
                    </div>
                </div>

                @if($publish)
                    <div class="row col-xs-12 no-margin">
                        <div class="col-xs-3 mb-1 p-0">
                            {!! Form::checkbox('published', 1, null, ['id' => 'publish-flag']) !!}
                            <label for="publish-flag" class="material-icons"></label>
                            <label for="publish-flag">منتشر کردن</label>
                        </div>
                    </div>

                    <div class="row col-xs-12 no-margin">
                        <div class="col-xs-3 mb-1 p-0">
                            @if($post->notification)
                                {!! Form::checkbox('no_push', 1, true, ['id' => 'notification-flag', 'disabled']) !!}
                            @else
                                {!! Form::checkbox('push', 1, false, ['id' => 'notification-flag']) !!}
                            @endif
                            <label for="notification-flag" class="material-icons"></label>
                            <label for="notification-flag">ارسال نوتیفیکیشن</label>
                        </div>
                    </div>
                @endif

                <div class="row col-xs-12 no-margin">
                    <div class="col-xs-12 pb-1" id="submit-area">
                        <button type="submit" class="btn-submit full-width" id="submit-btn" {{ (($post->creator_id != $admin->id) && !$update) ? 'disabled' : '' }}>ذخیره</button>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop