<a href="{{ URL("dashboard/users/$status") }}?{{ \App\Http\Controllers\Admin\UserController::BuildParams($params, $col) }}">
    <i class="icon normal-sort-icon @if(Request::capture()->query($col)) {{ Request::capture()->query($col) }} @endif"></i>
    <span>{{ $label }}</span>
</a>