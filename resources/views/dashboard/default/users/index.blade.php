@extends('layouts.dashboard')

@section('content')
    <div class="container col-xs-12 has-popover" id="dashboard-content">
        <div class="row multi-tab">
            <div class="col-xs-8 flex-xs-bottom">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link {{ ($route=="dashboard/users") ? 'active': '' }}" href="{{ URL("dashboard/users") }}">
                            <span>کاربران فعال</span>
                            <span class="badge badge-pill badge-default">{{ PersianNumber($active_count) }}</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link {{ ($route=="dashboard/users/de-active") ? 'active': '' }}" href="{{ URL("dashboard/users/de-active") }}">
                            <span>کاربران غیرفعال</span>
                            <span class="badge">{{ PersianNumber($de_active_count) }}</span>
                        </a>
                    </li>

                    @if($route=="dashboard/users/search")
                        <li class="nav-item">
                            <a class="nav-link active" href="#">
                                <span>جستجو</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>

            <div class="col-xs-4 flex-xs-middle text-xs-left" id="admin-search-container">
                @include(".dashboard.search", ['url' => 'dashboard/users/search'])
            </div>
        </div>

        <div class="row table-section">
            <div class="col-xs-12 no-padding">
                <div class="row col-xs-12 no-margin no-padding sortable-header">
                    <div class="col-xs-2" title="شماره همراه">
                        <span>شماره همراه</span>
                    </div>

                    <div class="col-xs-4" title="ایمیل">
                        <span>ایمیل</span>
                    </div>

                    <div class="col-xs-2" title="تعداد خرید ها">
                        @include('dashboard.users.sortable-links', ["status" => $status, 'label' => 'تعداد خرید ها', 'col' => 'purchases', 'params' => $params])
                    </div>

                    <div class="col-xs-2" title="تاریخ عضویت">
                        @include('dashboard.users.sortable-links', ["status" => $status, 'label' => 'تاریخ عضویت', 'col' => 'created', 'params' => $params])
                    </div>

                    <div class="col-xs-2"></div>
                </div>

                @if(!empty($users))
                    @foreach($users as $user)
                        <div class="row col-xs-12 no-margin no-padding sortable-rows show-on-hover-container fixed-height">
                            <div class="col-xs-2">
                                <span dir="ltr">{{ PersianNumber($user->phone) }}</span>
                            </div>

                            <div class="col-xs-4">
                                <span dir="ltr">{{ $user->email }}</span>
                            </div>

                            <div class="col-xs-2">
                                <span>{!! PersianNumber($user->purchases->count()) !!}</span>
                            </div>

                            <div class="col-xs-2">
                                <span>{{ jDate::forge($user->created_at)->format('y/m/d') }}</span>
                            </div>

                            <div class="col-xs-2">
                                @if($update)
                                    <a href="{{ url("dashboard/users/user/$user->id") }}" class="show-on-hover-visible">صفحه کاربر</a>
                                    <span class="show-on-hover-visible"> | </span>

                                    @if($user->active == \App\User::ACTIVE)
                                        <button type="button" data-toggle="modal" data-target="#deactivate-modal" data-id="{{ $user->id }}" class="show-on-hover-visible change-modal-id">
                                            <span>غیر فعال کردن</span>
                                        </button>
                                    @else
                                        <a href="{{ URL("dashboard/users/activate/$user->id") }}" class="show-on-hover-visible"><span>فعال کردن</span></a>
                                    @endif
                                @endif
                            </div>
                        </div>
                    @endforeach

                    <nav aria-label="Page navigation" class="text-xs-center">
                        {!! $users->links('vendor/pagination/admin') !!}
                    </nav>
                @else
                @endif

                @if($route == "dashboard/users")
                    <div class="modal fade reject-modal" id="deactivate-modal" tabindex="-1" role="dialog" aria-labelledby="deactivate-modal" aria-hidden="true">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                {!! Form::open(['url' => '/dashboard/users/deactivate', 'method' => 'PUT']) !!}
                                {!! Form::hidden('id', null, ['class' => 'modal-id']) !!}
                                <div class="modal-body">
                                    <header class="row col-xs-12 flex-items-xs-center reject-order-header">
                                        <strong>آیا می خواهید این کاربر غیرفعال شود؟</strong>
                                    </header>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-close" data-dismiss="modal">لغو</button>
                                    <button type="submit" class="btn btn-remove">تایید</button>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                @else

                @endif

            </div>
        </div>
    </div>
@stop