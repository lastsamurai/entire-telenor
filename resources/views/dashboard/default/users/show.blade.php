@extends('layouts.dashboard')

@section('content')
    <div class="container col-xs-12 has-popover" id="dashboard-content">
        <div class="row table-section">
            @if($user->pending->count())
                <div class="col-xs-12 no-padding">
                    <div class="row col-xs-12 no-margin no-padding" style="margin-bottom: 15px;">
                        <div class="col-xs-12">
                            <span>درخواست های در حال بررسی</span>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="row col-xs-12 no-margin no-padding sortable-header">
                        <div class="col-xs-2">لوگو</div>
                        <div class="col-xs-3">نوع کارت</div>
                        <div class="col-xs-2">قیمت</div>
                        <div class="col-xs-2">تعداد</div>
                        <div class="col-xs-3">تاریخ سفارش</div>
                    </div>

                    @foreach($user->pending as $key => $value)
                        <div class="row col-xs-12 no-margin no-padding sortable-rows">
                            <div class="col-xs-2 text-xs-center">
                                <img src="{{ url($value->item->card->logo_color->url()) }}" style="width: 85px; margin: 8px auto 0 auto;">
                            </div>

                            <div class="col-xs-3">
                                <span>{{ $value->item->card->name }}</span>
                            </div>

                            <div class="col-xs-2">
                                <span>{{ PersianNumber($value->item->price) }} دلار</span>
                            </div>

                            <div class="col-xs-2">
                                <span>{{ PersianNumber($value->number) }}</span>
                            </div>

                            <div class="col-xs-3 text-xs-center">
                                <span>{{ jDate::forge($value->created_at)->format('H:i Y-m-d') }}</span>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif

            @if($user->serials->count())
                <div class="col-xs-12 no-padding" style="margin-top: 15px;">
                    <div class="row col-xs-12 no-margin no-padding" style="margin-bottom: 15px;">
                        <div class="col-xs-12">
                            <span>خرید های انجام شده</span>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 no-padding">
                    <div class="row col-xs-12 no-margin no-padding sortable-header">
                        <div class="col-xs-2">لوگو</div>
                        <div class="col-xs-3">نوع کارت</div>
                        <div class="col-xs-2">قیمت</div>
                        <div class="col-xs-2">سریال</div>
                        <div class="col-xs-3">تاریخ سفارش</div>
                    </div>

                    @foreach($user->serials as $key => $value)
                        <div class="row col-xs-12 no-margin no-padding sortable-rows">
                            <div class="col-xs-2 text-xs-center">
                                <img src="{{ url($value->item->card->logo_color->url()) }}" style="width: 85px; margin: 8px auto 0 auto;">
                            </div>

                            <div class="col-xs-3">
                                <span>{{ $value->item->card->name }}</span>
                            </div>

                            <div class="col-xs-2">
                                <span>{{ PersianNumber($value->item->price) }} دلار</span>
                            </div>

                            <div class="col-xs-2">
                                <code>{{ $value->serial }}</code>
                            </div>

                            <div class="col-xs-3 text-xs-center">
                                <span>{{ jDate::forge($value->created_at)->format('H:i Y-m-d') }}</span>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@stop