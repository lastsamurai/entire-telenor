@extends('layouts.dashboard')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/admin/colorpicker.css') }}" />
@stop
@section('js')
    <script type="text/javascript" src="{{ URL::asset('js/admin/colorpicker.js') }}"></script>
@stop

@section('content')
    <div class="container col-xs-12" id="dashboard-content">
        {!! Form::model($category, ['url' => '/dashboard/categories/update', 'method' => 'PATCH', 'files' => true]) !!}
        {!! Form::hidden('id', $category->id) !!}
        <div class="row table-section">
            <div class="row col-xs-12 media-container">
                <header><strong>ویرایش دسته</strong></header>

                <div class="row col-xs-12 no-margin">
                    {!! Form::text("name", null, ['class' => '', 'placeholder' => 'نام*', 'required']) !!}
                </div>

                <div class="row col-xs-12 no-margin">
                    <div class="col-xs-6 no-padding-right">
                        {!! Form::text("hex", null, ['id' => 'start-gradient', 'class' => 'gradient-input', 'placeholder' => 'رنگ*', 'required', 'autocomplete' => 'off']) !!}
                    </div>
                </div>

                <input type="hidden" name="parent_id" value="0">
                {{--<div class="row col-xs-12 no-margin">
                    <div class="col-xs-6 no-padding-right">
                        {!! Form::select("parent_id", $parents, null, ['required']) !!}
                    </div>
                </div>--}}

                <div class="row col-xs-12 no-margin">
                    <div class="col-xs-6 no-padding-right pb-2">
                        @if($category->image->url())
                            <img src="{{ url($category->image->url()) }}" style="display: inline-block; width: 100px; padding: 7px 0">
                        @endif
                        <input type="file" name="image" id="image-uploader" class="inputfile inputfile-6" accept="image/*" />
                        <label for="image-uploader">
                            <span class="uploader-label"><p class="text-xs-right placeholder-color">عکس</p></span>
                            <strong>آپلود</strong>
                        </label>
                    </div>
                </div>

                <div class="row no-margin col-xs-12 no-padding" id="submit-area">
                    <button type="submit" class="btn-submit full-width" data-toggle="modal" data-target="#push-notification-modal">ذخیره</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop