@extends('layouts.dashboard')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/admin/colorpicker.css') }}" />
@stop
@section('js')
    <script type="text/javascript" src="{{ URL::asset('js/admin/colorpicker.js') }}"></script>
@stop

@section('content')
    <div class="container col-xs-12" id="dashboard-content">
        <div class="row multi-tab">
            <div class="col-xs-12 flex-xs-bottom">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link @if($type == \App\Category::TYPE_GAME) active @endif" href="{{ URL('dashboard/categories/posts') }}"><span>مطالب</span></a>
                    </li>

                    {{--<li class="nav-item">
                        <a class="nav-link @if($type == \App\Category::TYPE_ARTICLE) active @endif" href="{{ URL('dashboard/categories/articles') }}"><span>مقالات</span></a>
                    </li>--}}
                </ul>
            </div>
        </div>

        @if($write)
            {!! Form::open(['url' => '/dashboard/categories/add', 'method' => 'POST', 'files' => true]) !!}
            {!! Form::hidden('type', $type) !!}
            <div class="row table-section mt-1">
                <div class="row col-xs-12 media-container">
                    <header><strong>ایجاد دسته جدید</strong></header>

                    <div class="row col-xs-12 no-margin">
                        {!! Form::text("name", null, ['class' => '', 'placeholder' => 'نام*', 'required']) !!}
                    </div>

                    <div class="row col-xs-12 no-margin">
                        <div class="col-xs-6 no-padding-right">
                            {!! Form::text("hex", null, ['id' => 'start-gradient', 'class' => 'gradient-input', 'placeholder' => 'رنگ*', 'required', 'autocomplete' => 'off']) !!}
                        </div>
                    </div>

                    <input type="hidden" name="parent_id" value="0">
                    {{--<div class="row col-xs-12 no-margin">
                        <div class="col-xs-6 no-padding-right">
                            {!! Form::select("parent_id", $parents, null, ['required']) !!}
                        </div>
                    </div>--}}

                    <div class="row col-xs-12 no-margin">
                        <div class="col-xs-6 no-padding-right pb-2">
                            <input type="file" name="image" id="image-uploader" class="inputfile inputfile-6" accept="image/*" />
                            <label for="image-uploader">
                                <span class="uploader-label"><p class="text-xs-right placeholder-color">عکس</p></span>
                                <strong>آپلود</strong>
                            </label>
                        </div>
                    </div>

                    <div class="row no-margin col-xs-12 no-padding" id="submit-area">
                        <button type="submit" class="btn-submit full-width" data-toggle="modal" data-target="#push-notification-modal">ذخیره</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        @endif

        <div class="row table-section">
            <div class="row col-xs-12 no-margin no-padding">
                <div class="row col-xs-12 no-margin no-padding sortable-header">
                    <div class="col-xs-4 text-right">نام</div>
                    <div class="col-xs-2 text-right">پدر</div>
                    <div class="col-xs-2 text-right">عکس</div>
                    <div class="col-xs-2 text-right">رنگ</div>
                    <div class="col-xs-2 text-right"></div>
                </div>

                @if(!empty($categories))
                    @foreach($categories as $value)
                        <div class="row col-xs-12 no-margin no-padding sortable-rows show-on-hover-container flex-items-xs-middle">
                            <div class="col-xs-4">
                                <span>{{ $value->name }}</span>
                            </div>

                            <div class="col-xs-2">
                                <span>{{ (!empty($value->getAncestors()->toArray())) ? implode(',', $value->getAncestors()->pluck('name')->toArray()) : 'ندارد' }}</span>
                            </div>

                            <div class="col-xs-2">
                                @if($value->image->url())
                                    <img src="{{ url($value->image->url()) }}" width="85px">
                                @endif
                            </div>

                            <div class="col-xs-2 align-content-center">
                                <span style="background-color: {{ $value->hex }}" class="color-preview">{{ $value->hex }}</span>
                            </div>

                            <div class="col-xs-2">
                                @if($update)
                                    <a href="{{ URL("dashboard/categories/edit/$value->id") }}" class="show-on-hover-visible">ویرایش</a>
                                @endif

                                @if($delete)
                                    <span class="show-on-hover-visible"> | </span>
                                    <a href="{{ URL("dashboard/categories/delete/$value->id") }}" class="show-on-hover-visible btn-link">حذف</a>
                                @endif
                            </div>
                        </div>
                    @endforeach

                    <nav aria-label="Page navigation" class="text-xs-center pagination-container">
                        {!! $categories->links('vendor/pagination/admin') !!}
                    </nav>
                @endif
            </div>
        </div>
    </div>
@stop