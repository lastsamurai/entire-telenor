@extends('layouts.dashboard')

@section('content')
    <div class="container col-xs-12 has-popover" id="dashboard-content">


        @if($write)
            {!! Form::open(['url'=>'/dashboard/roles/add', 'method'=>'POST']) !!}
            <div class="row table-section mt-1">
                <div class="row col-xs-12 media-container">
                    <header><strong>ایجاد نقش جدید</strong></header>

                    <div class="row col-xs-12 no-margin mb-2">
                        {!! Form::text("name", null, ['class' => '', 'placeholder' => 'نام نقش*', 'required']) !!}
                    </div>

                    @foreach(config('acl.sections') as $key => $value)
                        <div class="row col-xs-12 no-margin mb-2 role-row">
                            <div class="col-xs-4">
                                <span>{{ $value['name'] }}</span>
                                {!! Form::hidden("controller[$key]", $value['controller']) !!}
                            </div>

                            <div class="col-xs-8">
                                <div class="row no-margin">
                                    <div class="col-xs-2">
                                        {!! Form::hidden("read[$key]", 0) !!}
                                        {!! Form::checkbox("read[$key]", 1, false, ['id' => "{$value['id']}-read"]) !!}
                                        <label for="{{ $value['id'] }}-read" class="material-icons"></label>
                                        <label for="{{ $value['id'] }}-read">خواندن</label>
                                    </div>

                                    <div class="col-xs-2">
                                        {!! Form::hidden("write[$key]", 0) !!}
                                        {!! Form::checkbox("write[$key]", 1, false, ['id' => "{$value['id']}-write"]) !!}
                                        <label for="{{ $value['id'] }}-write" class="material-icons"></label>
                                        <label for="{{ $value['id'] }}-write">نوشتن</label>
                                    </div>

                                    <div class="col-xs-4">
                                        {!! Form::hidden("update[$key]", 0) !!}
                                        {!! Form::checkbox("update[$key]", 1, false, ['id' => "{$value['id']}-update", 'class' => 'update-role']) !!}
                                        <label for="{{ $value['id'] }}-update" class="material-icons"></label>
                                        <label for="{{ $value['id'] }}-update">بروز رسانی / بررسی</label>
                                    </div>

                                    <div class="col-xs-2">
                                        {!! Form::hidden("delete[$key]", 0) !!}
                                        {!! Form::checkbox("delete[$key]", 1, false, ['id' => "{$value['id']}-delete"]) !!}
                                        <label for="{{ $value['id'] }}-delete" class="material-icons"></label>
                                        <label for="{{ $value['id'] }}-delete">حذف</label>
                                    </div>

                                    <div class="col-xs-2 {{ !$value['has_publish']? 'disabled' : '' }}">
                                        {!! Form::hidden("publish[$key]", 0) !!}
                                        @if($value['has_publish'])
                                            {!! Form::checkbox("publish[$key]", 1, false, ['id' => "{$value['id']}-publish", 'class' => 'publish-role']) !!}
                                        @else
                                            {!! Form::checkbox("publish[$key]", 1, false, ['id' => "{$value['id']}-publish", 'class' => 'publish-role', 'disabled']) !!}
                                        @endif
                                        <label for="{{ $value['id'] }}-publish" class="material-icons"></label>
                                        <label for="{{ $value['id'] }}-publish">انتشار</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    @foreach(config('acl.dynamic.toggle') as $key => $value)
                        <div class="row col-xs-12 no-margin mb-2 mt-3">
                            <div class="col-xs-4">
                                <span>{{ $value['name'] }}</span>
                                {!! Form::hidden("dynamic[toggle][$key][slug]", $value['slug']) !!}
                            </div>

                            <div class="col-xs-8">
                                <div class="row no-margin">
                                    @foreach(DynamicToggleAcl($value['driver'], $value['provider'], $value['type_slug'], $value['type_value']) as $index => $row)
                                        <div class="col-xs-2 mb-1">
                                            {{--{!! Form::hidden("dynamic[toggle][$key][fields][id-" . $row[$value['id']] . "]", 0) !!}--}}
                                            {!! Form::checkbox("dynamic[toggle][$key][fields][id-" . $row[$value['id']] . "]", $row[$value['id']], false, ['id' => "dynamic-toggle-{$row[$value['id']]}"]) !!}

                                            <label for="dynamic-toggle-{{ $row[$value['id']] }}" class="material-icons"></label>
                                            <label for="dynamic-toggle-{{ $row[$value['id']] }}">{{ $row[$value['label']] }}</label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach


                    <div class="row no-margin col-xs-12 no-padding" id="submit-area">
                        <button type="submit" class="btn-submit full-width" data-toggle="modal" data-target="#push-notification-modal">ارسال</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        @endif

        <div class="row table-section">
            <div class="col-xs-12 no-padding">
                <div class="row col-xs-12 no-margin no-padding sortable-header">
                    <div class="col-xs-4 text-right" title="نام">نام</div>

                    <div class="col-xs-3" title="تاریخ عضویت">تاریخ ایجاد</div>
                    <div class="col-xs-3" title="آخرین تغییر">آخرین تغییر</div>

                    <div class="col-xs-2" title="تاریخ عضویت"></div>
                </div>

                @if(!empty($roles))
                    @foreach($roles as $value)
                        <div class="row col-xs-12 no-margin no-padding sortable-rows show-on-hover-container fixed-height">
                            <div class="col-xs-4">
                                <span>{{ $value->name }}</span>
                            </div>

                            <div class="col-xs-3">
                                <span>{{ jDate::forge($value->created_at)->format('y/m/d') }}</span>
                            </div>

                            <div class="col-xs-3">
                                <span>{{ jDate::forge($value->updated_at)->format('y/m/d') }}</span>
                            </div>

                            <div class="col-xs-2">
                                @if($update)
                                    <a href="{{ URL("dashboard/roles/edit/$value->id") }}" class="show-on-hover-visible">ویرایش</a>
                                @endif
                            </div>
                        </div>
                    @endforeach

                    <nav aria-label="Page navigation" class="text-xs-center">
                        {!! $roles->links('vendor/pagination/admin') !!}
                    </nav>
                @endif

            </div>
        </div>
    </div>
@stop