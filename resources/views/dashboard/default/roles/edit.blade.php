@extends('layouts.dashboard')

@section('content')
    <div class="container col-xs-12 has-popover" id="dashboard-content">

        {!! Form::open(['url'=>'/dashboard/roles/update', 'method'=>'PUT']) !!}
        {!! Form::hidden('id', $role->id) !!}
        <div class="row table-section">
            <div class="row col-xs-12 media-container">
                <header><strong>ویرایش نقش</strong></header>

                <div class="row col-xs-12 no-margin mb-2">
                    {!! Form::text("name", $role->name, ['class' => '', 'placeholder' => 'نام نقش*', 'required']) !!}
                </div>

                @foreach($role->permissions['sections'] as $key => $value)
                    <div class="row col-xs-12 no-margin mb-2 role-row">
                        <div class="col-xs-4">
                            <span>{{ $value['name'] }}</span>
                            {!! Form::hidden("controller[$key]", $value['controller']) !!}
                        </div>

                        <div class="col-xs-8">
                            <div class="row no-margin">
                                <div class="col-xs-2">
                                    {!! Form::hidden("read[$key]", 0) !!}

                                    @if($value['read'] == 1)
                                        {!! Form::checkbox("read[$key]", 1, true, ['id' => "{$value['id']}-read"]) !!}
                                    @else
                                        {!! Form::checkbox("read[$key]", 1, false, ['id' => "{$value['id']}-read"]) !!}
                                    @endif

                                    <label for="{{ $value['id'] }}-read" class="material-icons"></label>
                                    <label for="{{ $value['id'] }}-read">خواندن</label>
                                </div>

                                <div class="col-xs-2">
                                    {!! Form::hidden("write[$key]", 0) !!}

                                    @if($value['write'] == 1)
                                        {!! Form::checkbox("write[$key]", 1, true, ['id' => "{$value['id']}-write"]) !!}
                                    @else
                                        {!! Form::checkbox("write[$key]", 1, false, ['id' => "{$value['id']}-write"]) !!}
                                    @endif

                                    <label for="{{ $value['id'] }}-write" class="material-icons"></label>
                                    <label for="{{ $value['id'] }}-write">نوشتن</label>
                                </div>

                                <div class="col-xs-4">
                                    {!! Form::hidden("update[$key]", 0) !!}

                                    @if($value['update'] == 1)
                                        {!! Form::checkbox("update[$key]", 1, true, ['id' => "{$value['id']}-update", 'class' => 'update-role']) !!}
                                    @else
                                        {!! Form::checkbox("update[$key]", 1, false, ['id' => "{$value['id']}-update", 'class' => 'update-role']) !!}
                                    @endif

                                    <label for="{{ $value['id'] }}-update" class="material-icons"></label>
                                    <label for="{{ $value['id'] }}-update">بروز رسانی / بررسی</label>
                                </div>

                                <div class="col-xs-2">
                                    {!! Form::hidden("delete[$key]", 0) !!}

                                    @if($value['delete'] == 1)
                                        {!! Form::checkbox("delete[$key]", 1, true, ['id' => "{$value['id']}-delete"]) !!}
                                    @else
                                        {!! Form::checkbox("delete[$key]", 1, false, ['id' => "{$value['id']}-delete"]) !!}
                                    @endif

                                    <label for="{{ $value['id'] }}-delete" class="material-icons"></label>
                                    <label for="{{ $value['id'] }}-delete">حذف</label>
                                </div>

                                <div class="col-xs-2 {{ !$value['has_publish']? 'disabled' : '' }}">
                                    {!! Form::hidden("publish[$key]", 0) !!}

                                    @if($value['has_publish'])
                                        @if($value['publish'] == 1)
                                            {!! Form::checkbox("publish[$key]", 1, true, ['id' => "{$value['id']}-publish", 'class' => 'publish-role']) !!}
                                        @else
                                            {!! Form::checkbox("publish[$key]", 1, false, ['id' => "{$value['id']}-publish", 'class' => 'publish-role']) !!}
                                        @endif
                                    @else
                                        {!! Form::checkbox("publish[$key]", 1, false, ['id' => "{$value['id']}-publish", 'class' => 'publish-role', 'disabled']) !!}
                                    @endif

                                    <label for="{{ $value['id'] }}-publish" class="material-icons"></label>
                                    <label for="{{ $value['id'] }}-publish">انتشار</label>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                @foreach($role->permissions['dynamic']['toggle'] as $key => $value)
                <div class="row col-xs-12 no-margin mb-2 mt-3">

                    <div class="col-xs-4">
                        <span>{{ $value['name'] }}</span>
                        {!! Form::hidden("dynamic[toggle][$key][slug]", $value['slug']) !!}
                    </div>

                    <div class="col-xs-8">
                        <div class="row no-margin">
                            @if(!empty($value['fields']))
                                @foreach($value['fields']['value'] as $index => $field)
                                    <div class="col-xs-2 mb-1">
                                        {!! Form::checkbox("dynamic[toggle][$key][fields][$index]", $field['id'], $field['status'], ['id' => "dynamic-toggle-$index"]) !!}
                                        <label for="dynamic-toggle-{{ $index }}" class="material-icons"></label>
                                        <label for="dynamic-toggle-{{ $index }}">{{ $value['fields']['label'][$index] }}</label>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach


                <div class="row no-margin col-xs-12 no-padding" id="submit-area">
                    <button type="submit" class="btn-submit full-width" data-toggle="modal" data-target="#push-notification-modal">ارسال</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@stop