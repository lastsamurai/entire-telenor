@extends('layouts.dashboard-auth')

@section('content')
    <div class="row col-xs-12 no-margin no-padding">
        <div class="col-xs-12 no-padding panel panel-default">
            <div class="panel-heading">ورود</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/dashboard/auth/login') }}">
                    {{ csrf_field() }}

                    <div class="col-xs-12 no-padding form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="form-group">
                            <input id="email" type="email" class="" name="email" placeholder="ایمیل" value="{{ old('email') }}" required autofocus>
                            <span class="bar"></span>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="form-group">
                            <input id="password" type="password" class="" name="password" placeholder="کلمه عبور" required>
                            <span class="bar"></span>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4 no-padding">
                            <input type="checkbox" name="remember" id="remember" checked>
                            <label for="remember" class="material-icons"></label>
                            <label for="remember">مرا به خاطر بسپار</label>
                        </div>
                    </div>

                    <footer class="row no-margin no-padding form-group flex-items-xs-middle">
                        <div class="col-xs-12 no-padding text-xs-right">
                            <button type="submit" class="btn btn-submit">ورود</button>
                        </div>

                        <div class="col-xs-12 no-padding mt-1">
                            <a href="{{ url('dashboard/auth/password/reset') }}">کلمه عبور خود را فراموش گرده اید؟</a>
                        </div>
                    </footer>
                </form>
            </div>
        </div>
    </div>
@endsection
