<?php

return [
    'responses' => [
        'code-1'   => 'Already exist',
        'code-2'   => 'MT not found',
        'code-3'   => 'Not valid input',
        'code-4'   => 'Not implemented yet',
        'code-5'   => 'Identifier code not found',
        'code-6'   => 'Transaction serial not found',
        'code-7'   => 'Verification code not found',
        'code-200' => 'Success',
        'code-400' => 'Unknown error',
    ],
];