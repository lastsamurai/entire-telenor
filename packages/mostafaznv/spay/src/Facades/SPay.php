<?php

namespace Mostafaznv\SPay\Facades;

use Illuminate\Support\Facades\Facade;
use Mostafaznv\SPay\SPay as SPayClass;

/**
 * @see \Mostafaznv\SPay\SPay
 */
class SPay extends Facade
{
    protected static function getFacadeAccessor()
    {
        return SPayClass::class;
    }
}