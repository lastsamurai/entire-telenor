<?php

namespace Mostafaznv\SPay\Exceptions;

class InvalidInputException extends SPayException
{
    protected $code    = 2;
    protected $message = 'Input is invalid';
}