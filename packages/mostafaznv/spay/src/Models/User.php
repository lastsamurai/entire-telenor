<?php

namespace Mostafaznv\SPay\Models;

class User extends Model
{
    protected $table = 'users_spay';
    protected $fillable = ['msisdn'];
}
