<?php

namespace Mostafaznv\Spay;

class Enum
{
    const ALREADY_EXISTS_CODE               = 1;
    const MT_NOT_FOUND_CODE                 = 2;
    const NOT_VALID_INPUTS_CODE             = 3;
    const NOT_IMPLEMENTED_YET               = 4;
    const IDENTIFIER_NOT_FOUND_CODE         = 5;
    const TRANSACTION_SERIAL_NOT_FOUND_CODE = 6;
    const VERIFICATION_CODE_FOUND_CODE      = 7;
    const SUCCESS_CODE                      = 200;
    const UNKNOWN_ERROR_CODE                = 400;
}