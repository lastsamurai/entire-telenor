<?php

namespace Mostafaznv\SPay;

use Illuminate\Support\ServiceProvider;

class SPayServiceProvider extends ServiceProvider
{
    const VERSION = '0.0.1';

    // todo - test

    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../translations', 'spay');

        if ($this->app->runningInConsole()) {
            $this->publishes([__DIR__ . '/../config/config.php' => config_path('spay.php')], 'config');
            $this->publishes([__DIR__ . '/../migrations/' => database_path('migrations')], 'migrations');
            $this->publishes([__DIR__ . '/../translations/' => resource_path('lang/vendor/spay')]);
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'spay');

        $this->app->singleton('spay', function() {
            $spay = new SPay();
            return $spay;
        });
    }
}