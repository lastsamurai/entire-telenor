<?php

namespace Mostafaznv\SPay;

use Illuminate\Support\Collection;
use GuzzleHttp\Client as Guzzle;
use Illuminate\Support\Facades\Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Mostafaznv\SPay\Exceptions\InvalidInputException;
use Exception;

class SPay
{
    /**
     * Keep loaded configuration for current driver.
     *
     * @var array
     */
    protected $config;

    /**
     * Monolog instance to write all events to log file
     *
     * @var string
     */
    private $logger;

    /**
     * Attach current request to class.
     *
     * @var \Illuminate\Http\Request
     */
    public $request;

    /**
     * Enable/Disable logging.
     * @var bool
     */
    protected $logStatus = true;

    /**
     * Load User model dynamically.
     *
     * @var \Mostafaznv\Spay\Models\User
     */
    protected $userModel;

    /**
     * Load Log model dynamically.
     *
     * @var \Mostafaznv\Spay\Models\Log
     */
    protected $logModel;

    /**
     * Init configuration for current driver.
     *
     * @param $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * Prevent values from logging.
     *
     * @var array
     */
    protected $hidden = ['service_provider_id'];

    const SUCCESS_MSG ='OK';

    /**
     * SPay constructor.
     *
     */
    public function __construct()
    {
        $this->initLog();
        $this->setConfig(config('spay'));

        $this->request = app('request');
        $this->logModel = $this->config['models']['log'];
        $this->userModel = $this->config['models']['user'];
        $this->logStatus = $this->config['log'];
    }

    /**
     * Init log system.
     */
    private function initLog()
    {
        $path = config('spay.log_path');
        $path = "$path/spay.log";

        $this->logger = new Logger('spay');
        $this->logger->pushHandler(new StreamHandler($path, Logger::INFO));
    }

    /**
     * Print events to log file.
     *
     * @param $message
     * @param string $level
     */
    protected function log($message, $level = 'info')
    {
        if ($this->logStatus) {
            $message = $this->secureLog($message);

            $this->logger->{$level}($message);
        }
    }

    /**
     * Remove unwanted values from log.
     *
     * @param $message
     * @return array|Collection|string
     */
    protected function secureLog($message)
    {
        $star = '****';

        if (is_array($message) or $message instanceof Collection) {
            if ($message instanceof Collection) {
                $message = $message->toArray();
            }

            foreach ($this->hidden as $item) {
                if (isset($message[$item])) {
                    $message[$item] = $star;
                }
            }

            $message = json_encode($message);
        }
        else if (is_object($message)) {
            foreach ($this->hidden as $item) {
                if (isset($message->$item)) {
                    $message->$item = $star;
                }
            }

            $message = json_encode($message);
        }

        return $message;
    }

    /**
     * Generate Unique ID.
     * @return string
     */
    protected function uniqid()
    {
        return md5(uniqid(rand(), true));
    }

    /**
     * Validate all inputs with given rules.
     *
     * @param array $input
     * @param $rules
     * @param bool $throw
     * @return array
     * @throws InvalidInputException
     */
    protected function validate(Array $input, $rules, $throw = true)
    {
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {

            if ($throw) {
                throw new InvalidInputException($validator->errors()->first());
            }
            else {
                return [
                    'status' => false,
                    'errors' => $validator->errors()->all()
                ];
            }
        }

        return [
            'status' => true,
            'errors' => []
        ];
    }

    /**
     * Patch path to baseurl to generate url.
     *
     * @param $path
     * @return string
     */
    protected function url($path)
    {
        return $this->config['baseurl'] . $path;
    }

    /**
     * Generate response.
     *
     * @param $status
     * @param $code
     * @param null $message
     * @param array $data
     * @return object
     */
    protected function response($status, $code, $message = null, $data = [])
    {
        if (is_null($message)) {
            $message = trans("spay::messages.responses.code-$code");
        }

        $response = [
            'status'  => $status,
            'code'    => $code,
            'message' => $message,
            'data'    => $data,
        ];

        return json_decode(json_encode($response));
    }

    /**
     * Store Request Logs Into Database.
     *
     * @param $msisdn
     * @param $type
     * @param $data
     * @param null $status
     * @param string $ip
     * @return mixed
     */
    protected function requestLog($msisdn, $type, $data, $status = null, $ip = '0:0:0:0')
    {
        $log = new $this->logModel;
        $log->msisdn = $msisdn;
        $log->type = $type;
        $log->data = $data;
        $log->status = $status;
        $log->ip = $ip;

        return $log->save();
    }

    /**
     * Send Otp.
     *
     * @param $msisdn
     * @return object
     */
    public function sendOtp($msisdn)
    {
        $this->log('start send otp to ' . $msisdn);

        $url = $this->url('SmsVerification/');
        $requestBody = [
            'msisdn'            => $msisdn,
            'ServiceProviderId' => $this->config['service_provider_id'],
            'ServiceId'         => $this->config['service_id'],
        ];

        $this->log($requestBody);

        try {
            $client = new Guzzle();
            $response = $client->request('POST', $url, ['form_params' => $requestBody]);
                $this->log('after guzzle');
            if ($response->getStatusCode() == 200) {
                $result = json_decode($response->getBody());
                $logData = (string)$response->getBody();
                $this->log(json_encode($logData));


                if ($result->status_code == 0 && $result->status_txt == self::SUCCESS_MSG) {
                    $this->requestLog($msisdn, 'send_otp', $logData, true, $this->request->getClientIp());

                    $userModel = $this->userModel;

                    $user = $userModel::firstOrNew(['msisdn' => $msisdn]);
                    $user->spay_code_identifier = $result->data;
                    $user->save();

                    $this->log('done');

                    return $this->response(true, Enum::SUCCESS_CODE);
                }
                else {
                    $this->requestLog($msisdn, 'send_otp', $logData, false, $this->request->getClientIp());

                    $this->log('unknown error', 'error');
                    $message = isset($result->status_txt) ? $result->status_txt : null;

                    return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $message);
                }
            }
        }
        catch (Exception $e) {
            $this->requestLog($msisdn, 'send_otp', $e->getMessage(), false, $this->request->getClientIp());

            $this->log($e->getMessage(), 'error');

            return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $e->getMessage());
        }
    }

    /**
     * Confirm OTP.
     *
     * @param $msisdn
     * @param $code
     * @return object
     */
    public function confirmOtp($msisdn, $code)
    {
        $this->log('start confirm otp for ' . $msisdn);

        $userModel = $this->userModel;
        $user = $userModel::where('msisdn', $msisdn)->first();

        if ($user and $user->spay_code_identifier) {

            $url = $this->url('GenerateTransaction/');
            $requestBody = [
                'msisdn'            => $msisdn,
                'ServiceProviderId' => $this->config['service_provider_id'],
                'ServiceId'         => $this->config['service_id'],
                'CodeIdentifier'    => $user->spay_code_identifier,
                'Code'              => $code,
                'CUID'              => $user->id,
            ];

            $this->log($requestBody);

            try {
                $client = new Guzzle();
                $response = $client->request('POST', $url, ['form_params' => $requestBody]);

                if ($response->getStatusCode() == 200) {
                    $result = json_decode($response->getBody());
                    $logData = (string)$response->getBody();
                    $this->log(json_encode($logData));

                    if ($result->status_code == 0) {
                        $this->requestLog($msisdn, 'confirm_otp', $logData, true, $this->request->getClientIp());

                        $user->spay_transaction_serial = $result->data;
                        $user->spay_verification_code = $code;
                        $user->save();

                        $this->log('done');

                        return $this->response(true, Enum::SUCCESS_CODE);
                    }
                    else {
                        $this->requestLog($msisdn, 'confirm_otp', $logData, false, $this->request->getClientIp());

                        $this->log('unknown error', 'error');
                        $message = isset($result->status_txt) ? $result->status_txt : null;

                        return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $message);
                    }
                }
            }
            catch (Exception $e) {
                $this->requestLog($msisdn, 'confirm_otp', $e->getMessage(), false, $this->request->getClientIp());

                $this->log($e->getMessage(), 'error');

                return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $e->getMessage());
            }
        }
        else {
            $this->log('code identifier not found', 'error');

            return $this->response(false, Enum::IDENTIFIER_NOT_FOUND_CODE);
        }
    }

    public function payment($msisdn)
    {
        $this->log('start payment ' . $msisdn);

        $userModel = $this->userModel;
        $user = $userModel::where('msisdn', $msisdn)->first();

        if ($user and $user->spay_code_identifier and $user->spay_transaction_serial and $user->spay_verification_code) {
            $url = $this->url('Payment/');
            $requestBody = [
                'msisdn'            => $msisdn,
                'ServiceProviderId' => $this->config['service_provider_id'],
                'ServiceId'         => $this->config['service_id'],
                'CodeIdentifier'    => $user->spay_code_identifier,
                'TransactionSerial' => $user->spay_transaction_serial,
                'Code'              => $user->spay_verification_code,
            ];

            $this->log($requestBody);

            try {
                $client = new Guzzle();
                $response = $client->request('POST', $url, ['form_params' => $requestBody]);

                if ($response->getStatusCode() == 200) {
                    $result = json_decode($response->getBody());
                    $logData = (string)$response->getBody();
                    $this->log($logData);

                    if ($result->status_code == 0) {
                        $this->requestLog($msisdn, 'payment', $logData, true, $this->request->getClientIp());

                        if ($result->data == "Transction completed successfully") {
                            $this->log('done');

                            return $this->response(true, Enum::SUCCESS_CODE);
                        }
                        else {
                            $this->log('error');

                            return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $result->data);
                        }
                    }
                    else {
                        $this->requestLog($msisdn, 'payment', $logData, false, $this->request->getClientIp());

                        $this->log('unknown error', 'error');
                        $message = isset($result->status_txt) ? $result->status_txt : null;

                        return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $message);
                    }
                }
            }
            catch (Exception $e) {
                $this->requestLog($msisdn, 'confirm_otp', $e->getMessage(), false, $this->request->getClientIp());

                $this->log($e->getMessage(), 'error');

                return $this->response(false, Enum::UNKNOWN_ERROR_CODE);
            }
        }
        else {
            if (!$user->spay_code_identifier) {
                $this->log('code identifier not found', 'error');
                return $this->response(false, Enum::IDENTIFIER_NOT_FOUND_CODE);
            }

            if (!$user->spay_verification_code) {
                $this->log('verification code not found', 'error');
                return $this->response(false, Enum::VERIFICATION_CODE_FOUND_CODE);
            }

            if (!$user->spay_transaction_serial) {
                $this->log('transaction serial not found', 'error');
                return $this->response(false, Enum::TRANSACTION_SERIAL_NOT_FOUND_CODE);
            }
        }
    }

    public function unsubscribe($msisdn)
    {
        $this->log('start unsubscribe for ' . $msisdn);

        $userModel = $this->userModel;
        $user = $userModel::where('msisdn', $msisdn)->first();

        if ($user and $user->spay_code_identifier) {

            $url = $this->url('Unsub/');
            $requestBody = [
                'msisdn'            => $msisdn,
                'ServiceProviderId' => $this->config['service_provider_id'],
                'ServiceId'         => $this->config['service_id'],
                'CodeIdentifier'    => $user->spay_code_identifier,
            ];

            $this->log($requestBody);

            try {
                $client = new Guzzle();
                $response = $client->request('POST', $url, ['form_params' => $requestBody]);

                if ($response->getStatusCode() == 200) {
                    $result = json_decode($response->getBody());
                    $logData = (string)$response->getBody();
                    $this->log(json_encode($logData));

                    if ($result->status_code == 0 && in_array($result->data,['un-subscription process completed successfully','Your code was already Revoked'])) {
                        $this->requestLog($msisdn, 'unsubscribe', $logData, true, $this->request->getClientIp());

                        $this->log('done');

                        return $this->response(true, Enum::SUCCESS_CODE);
                    }
                    else {
                        $this->requestLog($msisdn, 'unsubscribe', $logData, false, $this->request->getClientIp());

                        $this->log('unknown error', 'error');
                        $message = isset($result->status_txt) ? $result->status_txt : null;

                        return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $message);
                    }
                }
            }
            catch (Exception $e) {
                $this->requestLog($msisdn, 'unsubscribe', $e->getMessage(), false, $this->request->getClientIp());

                $this->log($e->getMessage(), 'error');

                return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $e->getMessage());
            }
        }
        else {
            $this->log('code identifier not found', 'error');

            return $this->response(false, Enum::IDENTIFIER_NOT_FOUND_CODE);
        }
    }
}