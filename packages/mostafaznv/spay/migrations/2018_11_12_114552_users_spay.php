<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersSpay extends Migration
{
    public function up()
    {
        Schema::create('users_spay', function (Blueprint $table) {
            $table->increments('id');
            $table->string('msisdn', 25)->index();
            $table->string('spay_code_identifier')->nullable();
            $table->string('spay_verification_code', 15)->nullable();
            $table->string('spay_transaction_serial')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users_spay');
    }
}
