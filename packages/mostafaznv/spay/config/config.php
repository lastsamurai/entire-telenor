<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Models
    |--------------------------------------------------------------------------
    |
    | You create your own or use package default config.
    |
    */

    'models' => [
        'user'  => \Mostafaznv\SPay\Models\User::class,
        'log'   => \Mostafaznv\SPay\Models\Log::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Log Status
    |--------------------------------------------------------------------------
    |
    | Enable/Disable logging.
    |
    */

    'log' => true,


    /*
    |--------------------------------------------------------------------------
    | Log path
    |--------------------------------------------------------------------------
    |
    | You can specify log path to log all events.
    |
    */

    'log_path' => storage_path('logs/spay'),


    /*
    |--------------------------------------------------------------------------
    | AKO Configuration
    |--------------------------------------------------------------------------
    |
    | Configuration values for ako
    |
    */

    'baseurl'                   => 'http://212.0.129.101:8700/samsson-gateway/',
    'service_provider_id'       => env('SPAY_SERVICE_PROVIDER_ID'),
    'service_id'                => env('SPAY_SERVICE_ID'),
    'confirm_otp_timeout'       => 5,
    'trans_prefix'              => 'mt'
];