<?php

namespace Ghorbannezhad\Dot\Facades;

use Illuminate\Support\Facades\Facade;
use Ghorbannezhad\Dot\Dot as DotClass;

/**
 * @see \Ghorbannezhad\Dot\Dot
 */
class Dot extends Facade
{
    protected static function getFacadeAccessor()
    {
        return DotClass::class;
    }
}