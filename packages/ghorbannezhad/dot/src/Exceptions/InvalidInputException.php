<?php

namespace Ghorbannezhad\Dot\Exceptions;

class InvalidInputException extends DotException
{
    protected $code    = 2;
    protected $message = 'Input is invalid';
}