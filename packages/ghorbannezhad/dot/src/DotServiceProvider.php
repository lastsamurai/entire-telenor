<?php

namespace Ghorbannezhad\Dot;

use Illuminate\Support\ServiceProvider;

class DotServiceProvider extends ServiceProvider
{
    const VERSION = '0.0.1';

    // todo - test

    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../translations/', 'dot');

        if ($this->app->runningInConsole()) {
            $this->publishes([__DIR__ . '/../config/config.php' => config_path('dot.php')], 'config');
            $this->publishes([__DIR__ . '/../migrations/' => database_path('migrations')], 'migrations');
            $this->publishes([__DIR__ . '/../translations/' => resource_path('lang/vendor/dot')]);
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'dot');

        $this->app->singleton('dot', function() {
            $dot = new Dot();
            return $dot;
        });
    }
}