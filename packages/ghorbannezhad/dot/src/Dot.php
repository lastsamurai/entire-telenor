<?php

namespace Ghorbannezhad\Dot;

use Ghorbannezhad\Dot\Models\Enum;
use GuzzleHttp\Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use GuzzleHttp\Client as Guzzle;
use Illuminate\Support\Facades\Validator;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Ghorbannezhad\Dot\Exceptions\InvalidInputException;
use Exception;
use \App\User;
use Illuminate\Support\Facades\Cache;

class Dot
{
    /**
     * Keep loaded configuration for current driver.
     *
     * @var array
     */
    protected $config;

    /**
     * Monolog instance to write all events to log file
     *
     * @var string
     */
    private $logger;

    /**
     * Attach current request to class.
     *
     * @var \Illuminate\Http\Request
     */
    public $request;

    /**
     * Header request.
     *
     * @var array
     */
    public $header;

    /**
     * Enable/Disable logging.
     * @var bool
     */
    protected $logStatus = true;

    /**
     * Load User model dynamically.
     *
     * @var \Ghorbannezhad\Dot\Models\User
     */
    protected $userModel;

    /**
     * Load Log model dynamically.
     *
     * @var \Ghorbannezhad\Dot\Models\Log
     */
    protected $logModel;

    /**
 * Load Purchase model dynamically.
 *
 * @var \Ghorbannezhad\Dot\Models\UserPurchases
 */
    protected $purchaseModel;

    /**
     * Load Unsubscribe model dynamically.
     *
     * @var \Ghorbannezhad\Dot\Models\UserPurchases
     */
    protected $unsubscribeModel;

    /**
     * Init configuration for current driver.
     *
     * @param $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * Prevent values from logging.
     *
     * @var array
     */
    protected $hidden = ['service_provider_id'];

    const SUCCESS_MSG ='OK';
    const SUCCESS_CODE = 100;

    /**
     * Dot constructor.
     *
     */
    public function __construct()
    {
        $this->initLog();
        $this->setConfig(config('dot'));

        $this->request = app('request');
        $this->logModel = $this->config['models']['log'];
        $this->terminatedModel = $this->config['models']['mobile_terminated'];
        $this->userModel = $this->config['models']['user'];
        $this->otpModel = $this->config['models']['otp'];
        $this->logStatus = $this->config['log'];
    }

    /**
     * Init log system.
     */
    private function initLog()
    {
        $path = config('dot.log_path');
        $path = "$path/dot.log";

        $this->logger = new Logger('Dot');
        $this->logger->pushHandler(new StreamHandler($path, Logger::INFO));
    }

    /**
     * Print events to log file.
     *
     * @param $message
     * @param string $level
     */
    protected function log($message, $level = 'info')
    {
        if ($this->logStatus) {
            $message = $this->secureLog($message);
            
            $this->logger->{$level}($message);
        }
    }

    /**
     * Remove unwanted values from log.
     *
     * @param $message
     * @return array|Collection|string
     */
    protected function secureLog($message)
    {
        $star = '****';

        if (is_array($message) or $message instanceof Collection) {
            if ($message instanceof Collection) {
                $message = $message->toArray();
            }

            foreach ($this->hidden as $item) {
                if (isset($message[$item])) {
                    $message[$item] = $star;
                }
            }

            $message = json_encode($message);
        }
        else if (is_object($message)) {
            foreach ($this->hidden as $item) {
                if (isset($message->$item)) {
                    $message->$item = $star;
                }
            }

            $message = json_encode($message);
        }

        return $message;
    }

    /**
     * Generate Unique ID.
     * @return string
     */
    protected function uniqid()
    {
        return md5(uniqid(rand(), true));
    }

    /**
     * Validate all inputs with given rules.
     *
     * @param array $input
     * @param $rules
     * @param bool $throw
     * @return array
     * @throws InvalidInputException
     */
    protected function validate(Array $input, $rules, $throw = true)
    {
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {

            if ($throw) {
                throw new InvalidInputException($validator->errors()->first());
            }
            else {
                return [
                    'status' => false,
                    'errors' => $validator->errors()->all()
                ];
            }
        }

        return [
            'status' => true,
            'errors' => []
        ];
    }

    /**
     * Patch path to baseurl to generate url.
     *
     * @param $path
     * @return string
     */
    protected function url($path)
    {
        return $this->config['baseurl'] . $path;
    }

    /**
     * Generate header.
     * @return array
     */
    protected function setHeader()
    {
        if(empty($this->header)){
            $username = $this->config['username'];
            $password = $this->config['password'];

            $header = [
                'Authorization'    =>'Basic '.base64_encode($username.':'.$password),
                'PartnerId'        => $this->config['partner_id'],
                'Content-Type'     => 'application/json'
            ];

            $this->header = $header;
        }
        return $this->header;

    }

    /**
     * Generate response.
     *
     * @param $status
     * @param $code
     * @param null $message
     * @param array $data
     * @return object
     */
    protected function response($status, $code, $message = null, $data = [])
    {
        if (is_null($message)) {
            $message = trans("Dot::messages.responses.code-$code");
        }

        $response = [
            'status'  => $status,
            'code'    => $code,
            'message' => $message,
            'data'    => $data,
        ];

        return json_decode(json_encode($response));
    }

    /**
     * Store Request Logs Into Database.
     *
     * @param $msisdn
     * @param $type
     * @param $data
     * @param null $status
     * @param string $ip
     * @return mixed
     */
    protected function requestLog($msisdn, $type, $data, $status = null, $ip = '0:0:0:0')
    {
        $log = new $this->logModel;
        $log->msisdn = $msisdn;
        $log->type = $type;
        $log->data = $data;
        $log->status = $status;
        $log->ip = $ip;

        return $log->save();
    }

    /**
     * Send Mt.
     *
     * @param $msisdn
     * @param $text
     * @return object
     */
    public function sendMt($msisdn,$text)
    {
        $this->log('start send mt to ' . $msisdn);

        $url = $this->url('PartnersMTSMS/');
        $requestBody = [
            'partnerId'         => $this->config['partner_id'],
            'opId'              => $this->config['op_id'],
            'serviceId'         => $this->config['service_id'],
            'sender'            => $this->config['sender'],
            'msisdn'            => $msisdn,
            'text'              => $text,
        ];

        $this->log($requestBody);
        $requestHeader = $this->setHeader();

        try {

            $client = new Guzzle();

            $response = $client->request('POST', $url, [
                'json' => $requestBody,
                'headers' => $requestHeader
            ]);

            $this->log('after guzzle');

            if ($response->getStatusCode() == 200)
            {
                $result = json_decode($response->getBody());
                $logData = (string)$response->getBody();
                $this->log(json_encode($logData));


                if ($result == self::SUCCESS_CODE) {

                    $terminatedModel = $this->terminatedModel;

                    $log = new $terminatedModel;
                    $log->msisdn = $msisdn;
                    $log->text = $text;
                    $log->save();

                    $this->log('done');

                    return $this->response(true, Enum::SUCCESS_CODE);
                }
                else {
                    $this->requestLog($msisdn, 'send_otp', $logData, false, $this->request->getClientIp());

                    $this->log('unknown error', 'error');
                    $message = isset($result->status_txt) ? $result->status_txt : null;

                    return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $message);
                }
            }
        }

        catch (Exception $e) {
            $this->requestLog($msisdn, 'send_otp', $e->getMessage(), false, $this->request->getClientIp());

            $this->log($e->getMessage(), 'error');

            return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $e->getMessage());
        }

    }

    /**
     * Send Otp.
     *
     * @param $msisdn
     * @return object
     */
    public function sendOtp($msisdn, $ip)
    {
        

        $this->log('start send otp to ' . $msisdn);

        $url = $this->url('PartnersDirectBilling/otp/send/');

        $transaction_id = $this->uniqid();

        $requestHeader = $this->setHeader();


        $requestBody = [

            'msisdn'            => $msisdn,
            'opId'              => $this->config['op_id'],
            'serviceId'         => $this->config['service_id'],
            'partnerTransId'    => $transaction_id,
            'amount'            => $this->config['amount'],
            'otpMessage'        => trans('dot::messages.otp'),

        ];


        $this->log(\GuzzleHttp\json_encode($requestBody));
        
        try {
            
            $client = new Guzzle();
            $clientHandler = $client->getConfig('handler');

           /* $tapMiddleware = Middleware::tap(function ($request) {
                echo $request->getHeaderLine('Content-Type');
                // application/json
                echo $request->getBody();
                // {"foo":"bar"}
            });*/

            

            $response = $client->request('POST', $url, [

                'json' => $requestBody,
                'headers' => $requestHeader,
//                'handler' =>$tapMiddleware($clientHandler),
            ]);
                
                $this->log('after guzzle');
                
            if ($response->getStatusCode() == 201) {
                

                $result = json_decode($response->getBody());

                
                $logData = (string)$response->getBody();

                $this->log(json_encode($logData));

                if ($result->resultCode == 0) {

                    $otpModel = $this->otpModel;
                    $model = new $otpModel;
                    $model->msisdn = $msisdn;
                    $model->transaction_id  = $transaction_id;
                    $model->otp_id = $result->otpId;
                    $model->ip = $ip;
                    $model->save();

                    $this->log('done');

                    return $this->response(true, Enum::SUCCESS_CODE);
                }
                else {

                    $this->requestLog($msisdn, 'send_otp', $logData, false, $this->request->getClientIp());

                    $this->log('unknown error', 'error');
                    $message = isset($result->status_txt) ? $result->status_txt : null;

                    return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $message);
                }
            }
        }
        catch (Exception $e) {
            $this->requestLog($msisdn, 'send_otp', $e->getMessage(), false, $this->request->getClientIp());

            $this->log($e->getMessage(), 'error');

            return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $e->getMessage());
        }
    }

    /**
     * Confirm OTP.
     *
     * @param $msisdn
     * @param $code
     * @return object
     */
    public function confirmOtp($msisdn, $code)
    {
        $this->log('start confirm otp for ' . $msisdn);

        $otpModel = $this->otpModel;

        //add expiration check to query

        $otp = $otpModel::where('msisdn', $msisdn)->orderBy('id','desc')->first();

        $this->log(\GuzzleHttp\json_encode($otp));

        if ($otp) {

            $url = $this->url('PartnersDirectBilling/otp/check/');
            $requestHeader = $this->setHeader();

            $requestBody = [
               'otpId'  => $otp->otp_id,
                'pin'   =>$code,
            ];

            $this->log($requestBody);

           try {
                $client = new Guzzle();

                $response = $client->request('POST', $url, [
                    'json' => $requestBody,
                    'headers' => $requestHeader
                ]);

                if ($response->getStatusCode() == 201) {

                    $result = json_decode($response->getBody());
                    $logData = (string)$response->getBody();
                    $this->log(json_encode($logData));

                    if ($result->resultCode == 0) {
                        $otp->dot_trans_id = $result->dotTransId;

                        $otp->save();

                        $this->log('done');

                        return $this->response(true, Enum::SUCCESS_CODE);
                    }
                    else {
                        $this->requestLog($msisdn, 'confirm_otp', $logData, false, $this->request->getClientIp());

                        $this->log('unknown error', 'error');
                        $message = isset($result->resultDesc) ? $result->resultDesc : null;

                        return $this->response(false, Enum::NOT_VALID_INPUTS_CODE, $message);
                    }

                }elseif($response->getStatusCode() == 200){

                    $result = json_decode($response->getBody());

                    $logData = (string)$response->getBody();
                    $this->log(json_encode($logData));

                    $this->requestLog($msisdn, 'confirm_otp', $logData, false, $this->request->getClientIp());

                    $this->log('unknown error', 'info');

                    $message = isset($result->resultDesc) ? $result->resultDesc : null;

                    return $this->response(false, Enum::NOT_VALID_INPUTS_CODE, $message);

                }else{

                    $message = 'Unknown error';
                    $this->log('Unknow error','error');

                    return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $message);
                }
            }
            catch (Exception $e) {
                $this->requestLog($msisdn, 'confirm_otp', $e->getMessage(), false, $this->request->getClientIp());

                $this->log($e->getMessage(), 'error');

                return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $e->getMessage());
            }
        }
        else {
            $this->log('code identifier not found', 'error');

            return $this->response(false, Enum::IDENTIFIER_NOT_FOUND_CODE);
        }
    }

    public function payment($msisdn)
    {
        $this->log('start payment ' . $msisdn);


        $transaction_id = $this->uniqid();

        $requestHeader = $this->setHeader();

        $url = $this->url('PartnersDirectBilling/');
        $amount = $this->config['amount'];

        $requestBody = [
            'partnerTransId'    => $transaction_id,
            'opId'              => $this->config['op_id'],
            'msisdn'            => $msisdn,
            'amount'            => $amount,
            'serviceId'         => $this->config['service_id'],
        ];

        $this->log($requestBody);

        try {

            $client = new Guzzle();

            $response = $client->request('POST', $url, ['json' => $requestBody, 'headers' => $requestHeader]);

            if ($response->getStatusCode() == 201) {

                $result = json_decode($response->getBody());

                $logData = (string)$response->getBody();

                $this->log($logData);

                if ($result->resultCode == 0) {

                    $this->requestLog($msisdn, 'payment', $logData, true, $this->request->getClientIp());
                    $purchase_model = $this->purchaseModel;
                    $purchase_model->msisdn = $msisdn;
                    $purchase_model->amount = $amount;
                    $purchase_model->transaction_id = $transaction_id;
                    $purchase_model->dot_trans_id = $result->dotTransId;

                    $purchase_model->save();

                    $this->requestLog($msisdn, 'payment', $logData, false, $this->request->getClientIp());

                    return $this->response(false, Enum::SUCCESS_CODE, $result->resultDesc);

                }
                else {

                    $this->requestLog($msisdn, 'payment', $logData, false, $this->request->getClientIp());

                    return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $result->resultDesc);
                }
            }elseif ($response->getStatusCode()== 200) {

                $result = json_decode($response->getBody());

                $logData = (string)$response->getBody();

                $this->requestLog($msisdn, 'payment', $logData, false, $this->request->getClientIp());

                return $this->response(false, Enum::ALREADY_EXISTS_CODE, $result->resultDesc);

            }else {
                    $this->log('unknown error', 'error');

                    return $this->response(false, Enum::UNKNOWN_ERROR_CODE);
            }
        }
        catch (Exception $e) {

            $this->requestLog($msisdn, 'payment', $e->getMessage(), false, $this->request->getClientIp());

            $this->log($e->getMessage(), 'error');

            return $this->response(false, Enum::UNKNOWN_ERROR_CODE);
        }
    }

    public function subscribe_landing_method($msisdn,$transId){
        $requestBody = [
            'msisdn'            => $msisdn,
            'ServiceId'         => $this->config['service_id'],
            'opId'              => env('DOT_OP_ID'),
            //'ServiceProviderId' => $this->config['service_provider_id'],
            //'CodeIdentifier'    => $user->Dot_code_identifier,
            'lpTransId'         => $transId
        ];
        return $this->subscribe($requestBody);
    }

    public function subscribe_otp_api($msisdn,$otpId,$otpPin){
        $requestBody = [
            'msisdn'            => $msisdn,
            'ServiceId'         => $this->config['service_id'],
            'opId'              => env('DOT_OP_ID'),
            'ServiceProviderId' => $this->config['service_provider_id'],
            'CodeIdentifier'    => $user->Dot_code_identifier,
            'otpId'             => $otpId,
            'otpPIN'            => $otpPin
        ];
        return $this->subscribe($requestBody);
    }

    private function subscribe($requestBody) //used by the above two methods
    {
        
        $msisdn=$requestBody['msisdn'];

        $this->log('start subscribe for ' . $msisdn);

/*
        $userModel = $this->userModel;

        $user = $userModel::where('msisdn', $msisdn)->first();
*/      

        if (1/*$user and $user->Dot_code_identifier*/) {

            $url = $this->url('partners-subscription/subscribe');

            $this->log($requestBody);

            try {
                $client = new Guzzle();
                
                $response = $client->request('POST', $url, [
                    'headers' => [
                        'Authorization'     =>  base64_encode( env('DOT_USERNAME').":".env('DOT_PASSWORD') ),
                        'PartnerId' => env('DOT_PARTNER_ID')
                    ],
                    'json' => $requestBody
                ]);
                

                if ($response->getStatusCode() == 201) {
                    $result = json_decode($response->getBody());
                    
                    $logData = (string)$response->getBody();
                    $this->log(json_encode($logData));

                    if ($result->status_code == 0 && in_array($result->data,['subscription process completed successfully','Your code was already Revoked'])) {
                        $this->requestLog($msisdn, 'subscribe', $logData, true, $this->request->getClientIp());

                        $this->log('done');

                        return $this->response(true, Enum::SUCCESS_CODE);
                    }
                    else {
                        $this->requestLog($msisdn, 'unsubscribe', $logData, false, $this->request->getClientIp());

                        $this->log('unknown error', 'error');
                        $message = isset($result->status_txt) ? $result->status_txt : null;

                        return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $message);
                    }
                }
            }
            catch (Exception $e) {
                $this->requestLog($msisdn, 'subscribe', $e->getMessage(), false, $this->request->getClientIp());

                $this->log($e->getMessage(), 'error');

                return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $e->getMessage());
            }
        }
        else {
            
            $this->log('code identifier not found', 'error');

            return $this->response(false, Enum::IDENTIFIER_NOT_FOUND_CODE);
        }
    }

    public function unsubscribe($msisdn, $id, $ip) //   METHOD to be checked !!!!!
    {
        $this->log('start unsubscribe ' . $msisdn);

        $requestHeader = $this->setHeader();

        $url = $this->url("partners-subscription/unsubscribe/{$msisdn}/{$this->config['op_id']}/{$this->config['service_id']}");


        try {

            $client = new Guzzle();

            $response = $client->request('DELETE', $url, ['headers' => $requestHeader]);

            if ($response->getStatusCode() == 200) {

                $result = json_decode($response->getBody());

                $logData = (string)$response->getBody();

                $this->log($logData);

                if ($result->resultCode == 0) {

                    $unsubscribe_model = $this->unsubscribeModel;
                    $unsubscribe_model->user_id = $id ;
                    $unsubscribe_model->ip = $ip;

                    $unsubscribe_model->save();

                    $this->requestLog($msisdn, 'payment', $logData, false, $this->request->getClientIp());

                    return $this->response(false, Enum::SUCCESS_CODE, $result->resultDesc);
                }
                else {
                    $this->requestLog($msisdn, 'payment', $logData, false, $this->request->getClientIp());

                    return $this->response(false, Enum::UNKNOWN_ERROR_CODE, $result->resultDesc);
                }
            }else {
                $this->log('unknown error', 'error');

                return $this->response(false, Enum::UNKNOWN_ERROR_CODE);
            }
        }
        catch (Exception $e) {

            $this->requestLog($msisdn, 'payment', $e->getMessage(), false, $this->request->getClientIp());

            $this->log($e->getMessage(), 'error');

            return $this->response(false, Enum::UNKNOWN_ERROR_CODE);
        }
    }
}