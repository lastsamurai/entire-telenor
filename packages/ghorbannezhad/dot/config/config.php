<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Models
    |--------------------------------------------------------------------------
    |
    | You create your own or use package default config.
    |
    */

    'models' => [
        'user'              => \Ghorbannezhad\Dot\Models\User::class,
        'log'               => \Ghorbannezhad\Dot\Models\Log::class,
        'mobile_terminated' => \Ghorbannezhad\Dot\Models\MobileTerminated::class,
        'otp'               => \Ghorbannezhad\Dot\Models\OtpRequest::class,
        'purchase_model'    => \Ghorbannezhad\Dot\Models\UserPurchases::class,
        'unsubscribe_model'    => \Ghorbannezhad\Dot\Models\UserPurchases::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Log Status
    |--------------------------------------------------------------------------
    |
    | Enable/Disable logging.
    |
    */

    'log' => true,


    /*
    |--------------------------------------------------------------------------
    | Log path
    |--------------------------------------------------------------------------
    |
    | You can specify log path to log all events.
    |
    */

    'log_path' => storage_path('logs/dot'),


    /*
    |--------------------------------------------------------------------------
    | AKO Configuration
    |--------------------------------------------------------------------------
    |
    | Configuration values for ako
    |
    */

    'baseurl'                   => 'https://dot-jo.biz/lb2/',
    'username'                  => env('DOT_USERNAME'),
    'password'                  => env('DOT_PASSWORD'),
    'partner_id'                => env('DOT_PARTNER_ID'),
    'op_id'                     => env('DOT_OP_ID'),
    'service_id'                => env('DOT_SERVICE_ID'),
    'sender'                    => env('DOT_SENDR'),
    'confirm_otp_timeout'       => 5,
    'trans_prefix'              => 'mt',
    'amount'                    =>1.00 //amount of subscription in ....
];