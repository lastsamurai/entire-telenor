<?php

namespace App\Traits;

use Exception;
use App\Media;
use Illuminate\Database\Eloquent\Builder;
use App\Mediable as MediableModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Database\Eloquent\Collection;

trait Mediable
{
    protected static $fields = null;
    protected static $mediaCache = [];

    public static function bootMediable()
    {
        if (!isset(self::$mediaFields))
            throw new Exception("mediaFields is not configured correctly");

        self::$fields = collect(self::$mediaFields);

        static::deleted(function (Model $model) {
            $model->handleMediableDeletion();
        });
    }

    public function mediaAll()
    {
        return $this->morphToMany(Media::class, 'mediable')->using(MediableModel::class)->whereNull('mediables.deleted_at')->withPivot('field');
    }

    public function getMediaAttribute()
    {

        if (isset(self::$mediaCache[$this->id]))
            return self::$mediaCache[$this->id];

        $media = $this->mediaAll;
        $data = [];

        foreach (self::$fields as $field)
        {
            $mediaTemp = $media->where('type', $field['type']);
            if ($mediaTemp->count())
            {
                $result = $mediaTemp->filter(function ($value) use ($field) {
                    if (isset($value->pivot->field))
                        return $value->pivot->field === $field['name'];
                    return false;
                });

                $result = $result->map(function ($item) {
                    unset($item['pivot']);
                    return $item;
                });
            }
            else
                $result = collect([]);


            if ($field['multipliable'] == false)
                $result = $result->first();


            if ($result)
                $data[$field['name']] = $result;
        }


        if (count($data) == 0)
            $data = null;

        //$data = json_decode(json_encode($data), false);

        self::$mediaCache[$this->id] = $data;
        return $data;
    }

    public function scopeWhereHasMedia(Builder $q, $tags, $match_all = false)
    {
        if ($match_all && is_array($tags) && count($tags) > 1)
            return $this->scopeWhereHasMediaMatchAll($q, $tags);

        $q->whereHas('mediaAll', function (Builder $q) use ($tags) {
            $q->whereIn('tag', (array)$tags);
        });
    }

    public function scopeWithMedia(Builder $q)
    {
        $q->with('mediaAll');
    }

    public function scopeNotDeleted(Builder $q)
    {
        $q->whereNull('mediables.deleted_at');
    }

    public function loadMedia()
    {
        $this->load('mediaAll');

        return $this;
    }

    public function hasMedia()
    {
        return count($this->getMedia()) > 0;
    }

    public function getMedia()
    {
        return $this->media
            //remove duplicate media
            ->keyBy(function (Media $media) {
                return $media->getKey();
            })->values();
    }

    public function firstMedia()
    {
        return $this->getMedia()->first();
    }

    public function lastMedia()
    {
        return $this->getMedia()->last();
    }

    public function load($relations)
    {
        if (is_string($relations))
        {
            $relations = func_get_args();
        }

        if (array_key_exists('mediaAll', $relations) || in_array('mediaAll', $relations))
        {
            $this->media_dirty_tags = [];
        }

        return parent::load($relations);
    }

    public function attachMedia($media, $field)
    {
        $field = $this->fieldResolver($field);

        $mediable = $this->mediaAll();
        $morphType = $mediable->getMorphType();
        $morphClass = $mediable->getMorphClass();
        $qualifiedForeignKey = $this->mediaQualifiedForeignKey();
        $qualifiedRelatedKey = $this->mediaQualifiedRelatedKey();
        $mediaId = $this->id;

        $ids = $this->extractIds($media);

        if ($field['multipliable'] == false and count($ids) > 1)
            throw new Exception("field '{$field['name']}' does not support multiple rows");

        foreach ($ids as $id)
        {
            $mediable = MediableModel::withTrashed()->firstOrNew([$morphType => $morphClass, $qualifiedForeignKey => $mediaId, $qualifiedRelatedKey => $id]);
            $mediable->deleted_at = null;
            $mediable->field = $field['name'];
            $mediable->save();
        }
    }

    public function detachMedia($field, $detachAll)
    {
        $media = $this->mediaAll();
        $morphType = $media->getMorphType();
        $morphClass = $media->getMorphClass();
        $qualifiedForeignKey = $this->mediaQualifiedForeignKey();

        if ($detachAll)
            MediableModel::where($morphType, $morphClass)->where($qualifiedForeignKey, $this->getKey())->delete();
        else
            MediableModel::where('field', $field)->where($morphType, $morphClass)->where($qualifiedForeignKey, $this->getKey())->delete();
    }

    public function syncMedia($media, $field, $detachAll = false)
    {
        $this->detachMedia($field, $detachAll);
        $this->attachMedia($media, $field);
    }

    private function extractIds($input)
    {
        if ($input instanceof Collection)
            return $input->modelKeys();

        if ($input instanceof Media)
            return [$input->getKey()];

        return (array)$input;
    }

    private function mediaQualifiedForeignKey()
    {
        $relation = $this->mediaAll();

        if (method_exists($relation, 'getQualifiedForeignPivotKeyName'))
            return $relation->getQualifiedForeignPivotKeyName();
        elseif (method_exists($relation, 'getQualifiedForeignKeyName'))
            return $relation->getQualifiedForeignKeyName();

        return $relation->getForeignKey();
    }

    private function mediaQualifiedRelatedKey()
    {
        $relation = $this->mediaAll();
        // Laravel 5.5
        if (method_exists($relation, 'getQualifiedRelatedPivotKeyName'))
            return $relation->getQualifiedRelatedPivotKeyName();

        // Laravel 5.4
        elseif (method_exists($relation, 'getQualifiedRelatedKeyName'))
            return $relation->getQualifiedRelatedKeyName();

        // Laravel <= 5.3
        return $relation->getOtherKey();
    }

    protected function handleMediableDeletion()
    {
        // only cascade soft deletes when configured
        if (static::hasGlobalScope(SoftDeletingScope::class) && !$this->forceDeleting)
        {
            if (config('mediable.detach_on_soft_delete'))
                $this->mediaAll()->detach();
        }
        else
            $this->mediaAll()->detach();
    }

    protected function fieldResolver($fieldName)
    {
        if ($field = self::$fields->where('name', $fieldName)->first() and $field['name'] === $fieldName)
            return $field;

        throw new Exception("field name '{$fieldName}' does not found");
    }
}
