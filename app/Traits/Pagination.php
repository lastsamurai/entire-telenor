<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

trait Pagination
{
    public function scopePagination(Builder $query, $num = 15, $path = null, $full = false)
    {
        $path = $this->path($path, $full);

        return $query->paginate($num)->setPath($path);
    }

    private function path($path, $full)
    {
        if ($path == null)
        {
            $path = Request::capture()->getRequestUri();
            $path = $this->normalizeQueryParams($path);
        }
        else if (!empty(app('request')->query()))
        {
            $path = $this->normalizeQueryParams($path);
        }

        if ($full)
            return url($path);
        return $path;
    }

    private function normalizeQueryParams($path)
    {
        $params = app('request')->query();
        if(!empty($params))
        {
            unset($params['page']);

            $path = explode('?', $path);
            $path = $path[0] . '?' . http_build_query($params, '', '&');
        }


        $path = explode('/api/', $path);
        $path = end($path);

        return $path;
    }
}