<?php

namespace App\Traits;

use App\Bookmark;
use Illuminate\Support\Facades\Request;

trait Bookmarkable
{
    public function bookmarksCascade()
    {
        return $this->hasMany(Bookmark::class, 'bookmarkable_id')->where('bookmarkable_type', self::class);
    }

    public function bookmarks()
    {
        return $this->morphMany(Bookmark::class, 'bookmarkable');
    }

    public function getIsBookmarkedAttribute()
    {
        $userId = Request::header('userid');

        if (isset(self::$showBookmarkedFlag) and !self::$showBookmarkedFlag)
            $like = null;
        else
        {
            if ($userId)
                $like = $this->bookmarks()->whereUserId($userId)->first();
            else
                $like = null;
        }

        return ($like) ? true : false;
    }
}
