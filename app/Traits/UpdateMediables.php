<?php

namespace App\Traits;

use App\Classes\ExtendedRinvex;

trait UpdateMediables
{
    public static function bootUpdateMediables()
    {
        self::created(function ($model) {
            ExtendedRinvex::updateCache($model);
        });

        self::saved(function ($model) {
            ExtendedRinvex::updateCache($model);
        });

        self::updated(function ($model) {
            ExtendedRinvex::updateCache($model);
        });
    }
}
