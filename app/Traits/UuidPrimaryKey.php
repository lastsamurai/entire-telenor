<?php

namespace App\Traits;

use Uuid;

trait UuidPrimaryKey
{
    public static function bootUuidPrimaryKey()
    {
        static::creating(function ($model) {
            $model->incrementing = false;
            $model->id = Uuid::generate(4)->string;
        });
    }
}
