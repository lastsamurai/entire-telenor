<?php

namespace App\Traits;

use Exception;
use App\Post;
use App\Ads;
use Illuminate\Database\Eloquent\Builder;

trait Adsable
{
    // TODO - get this values from database
    protected static $adsPerPage = 2;
    protected static $adsOffset = 3;

    public function scopeToAds(Builder $query, $fetch = 'get', $num = 15, $path = null, $full = false)
    {
        switch ($fetch)
        {
            case 'get':
                $result = $query->limit($num)->get();
                break;

            case 'paginate':
                $result = $query->paginate($num);
                break;

            case 'pagination':
                $result = $query->pagination($num, $path, $full);
                break;

            default:
                throw new Exception("fetch '$fetch' is not supported");

        }

        $items = $result->toArray();
        $queryOffset = count($items);
        $class = self::class;
        $page = (app('request')->query('page')) ? (int)app('request')->query('page') : 1;
        $ads = Ads::cache();

        if ($queryOffset)
        {
            if (self::validatePage($page))
            {
                $position = self::getAdsPosition($page, $ads);
                $model = self::generateModel($class, $items, $ads, $position);

                if ($model)
                {
                    $offset = self::$adsOffset - 1;

                    if (isset($items['data']))
                    {
                        if (self::$adsOffset < $queryOffset)
                            array_splice($items['data'], $offset, 0, [$offset => $model]);
                        else
                            array_push($items['data'], $model);
                    }
                    else
                    {
                        if (self::$adsOffset < $queryOffset)
                            array_splice($items, $offset, 0, [$offset => $model]);
                        else
                            array_push($items, $model);
                    }
                }
            }
        }

        return $items;
    }

    protected static function validatePage($page)
    {
        $perPage = (int)self::$adsPerPage + 1;

        if (($page - 1) % $perPage == 0)
            return true;
        return false;
    }

    protected static function getAdsPosition($page, $ads)
    {
        $originalPerPage = (int)self::$adsPerPage;
        $perPage = $originalPerPage + 1;
        $adsCount = $ads->count();

        $x = $originalPerPage + $page;
        $y = $x / $perPage;

        return (($y - 1) % $adsCount) + 1;
    }

    protected static function generateModel($class, $items, $ads, $position)
    {
        if (isset($items['data']) and isset($items['data'][0]))
            $item = self::cleanedModel($items['data'][0]);
        else if (isset($items[0]))
            $item = self::cleanedModel($items[0]);
        else
            return null;


        $type = null;
        //$item = self::cleanedModel((isset($items['data'])) ? $items['data'][0] : $items[0]);

        if ($ads = $ads->where('position', $position)->first())
        {
            switch ($class)
            {
                case Post::class:
                    $columns = ['id' => 'id', 'title' => 'title', 'description' => 'description', 'media' => 'media', 'type' => null, 'link' => null];
                    $type = Post::TYPE_ADS;
                    break;

                default:
                    throw new Exception("Model '{$class}' does not support toAds yet");
            }

            if ($columns)
            {
                $ads['type'] = $type;

                foreach ($columns as $key => $value)
                {
                    if ($value == null)
                        $index = $key;
                    else
                        $index = $value;

                    $item[$index] = $ads[$key];
                }

                return $item;
            }
        }

        return null;
    }

    protected static function cleanedModel($model)
    {
        foreach ($model as $key => $value)
        {
            if (is_array($value))
                $model[$key] = [];
            else if (is_integer($value))
                $model[$key] = 0;
            else if (is_string($value))
                $model[$key] = null;
            else
                $model[$key] = null;
        }

        return $model;
    }
}