<?php

namespace App\Traits;

use App\Classes\ExtendedRinvex;
use App\Mediable;

trait CascadeMediaSoftDelete
{
    public static function bootCascadeMediaSoftDelete()
    {
        self::deleted(function ($model) {
            ExtendedRinvex::updateCache($model);

            Mediable::where('media_id', $model->id)->delete();
        });
    }
}
