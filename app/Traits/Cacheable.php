<?php

namespace App\Traits;

use Exception;

trait Cacheable
{
    public static function bootCacheable()
    {
        if (method_exists(self::class, 'UpdateCache'))
        {
            self::created(function () {
                self::UpdateCache('create');
            });

            self::updated(function () {
                self::UpdateCache('update');
            });

            self::deleted(function () {
                self::UpdateCache('delete');
            });
        }
        else
        {
            throw new Exception("This function is undefined: UpdateCache");
        }
    }
}
