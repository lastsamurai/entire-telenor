<?php

namespace App\Traits;

use App\Scopes\ActiveScope;

trait Active
{
    public static function bootActive()
    {
        if (self::$active_scope)
            static::addGlobalScope(new ActiveScope);
    }
}
