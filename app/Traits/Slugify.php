<?php

namespace App\Traits;

use App\Scopes\PublishedScope;
use Exception;

trait Slugify
{
    public static function bootSlugify()
    {
        if (isset(self::$slugify))
        {
            $slugify = self::$slugify;
            $slugFrom = $slugify['from'];
            $slugColumn = $slugify['column'];

            self::saving(function ($model) use ($slugFrom, $slugColumn) {
                $slug = self::uniqueSlug($model, $slugFrom, $slugColumn);
                $model->$slugColumn = $slug;
            });
        }
        else
        {
            throw new Exception("slugify is not configured correctly");
        }
    }

    public static function uniqueSlug($model, $slugFrom, $slugColumn)
    {
        $slug = slug($model->$slugFrom);
        $repeatedSlug = null;
        $done = false;
        $index = 0;

        do {
            if ($index)
                $repeatedSlug = $slug . "-$index";

            if (isset($model->published))
                $exist = self::withoutGlobalScope(PublishedScope::class)->where($slugColumn, ($repeatedSlug) ? $repeatedSlug : $slug)->count();
            else
                $exist = self::where($slugColumn, ($repeatedSlug) ? $repeatedSlug : $slug)->count();

            if ($exist)
            {
                $index++;
                continue;
            }

            if ($repeatedSlug)
                $slug = $repeatedSlug;
            $done = true;
        }
        while (!$done);

        return $slug;
    }
}
