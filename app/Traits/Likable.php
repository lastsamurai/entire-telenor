<?php

namespace App\Traits;

use App\Like;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

trait Likable
{
    public function likesCascade()
    {
        return $this->hasMany(Like::class, 'likable_id')->where('likable_type', self::class);
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'likable');
    }

    public function getIsLikedAttribute()
    {
        $user = Auth::user();


        if (isset(self::$showLikedFlag) and !self::$showLikedFlag)
            $like = null;
        else
        {
            if ($user){
                $like = $this->likes()->whereUserId($user->id)->first();
            }

            else
                $like = null;
        }

        return ($like) ? true : false;
    }
}
