<?php

namespace App\Traits;

trait SortableDelete
{
    public static function bootSortableDelete()
    {
        self::deleting(function ($model) {
            $model->next()->decrement('position');
        });
    }
}
