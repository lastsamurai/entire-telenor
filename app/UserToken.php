<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    protected $table = "user_token";
    protected $fillable = ['user_id','jwt'];


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}
