<?php

namespace App;

use App\Traits\AdminActivity;
use App\Traits\CascadeMediaSoftDelete;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use Illuminate\Database\Eloquent\Model;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;
use Exception;
use FFMpeg\FFProbe;
use Illuminate\Database\Eloquent\SoftDeletes;
use Symfony\Component\HttpFoundation\File\File;

class Media extends Model implements StaplerableInterface
{
    use AdminActivity, EloquentTrait, SoftDeletes, CascadeMediaSoftDelete;

    protected $table = 'media';
    protected $attributes = ['width' => null, 'height' => null, 'duration' => null];
    protected $hidden = ['type', 'created_at', 'updated_at', 'deleted_at', 'creator_id', 'updater_id', 'deleter_id', 'creator_ip', 'updater_ip', 'deleter_ip', 'file_file_name', 'file_file_size', 'file_content_type', 'file_updated_at', 'thumbnail_file_name', 'thumbnail_file_size', 'thumbnail_content_type', 'thumbnail_updated_at'];
    protected $casts = ['id' => 'string'];
    protected $appends = ['file'];
    protected $cascadeDeletes = ['mediable'];

    public static $checkEvent = true;
    public static $allowedMimeTypes = [
        'video/mp4', 'video/3gpp', 'video/avi', 'video/mpeg', 'video/quicktime', 'video/ogg', 'video/webm',
        'image/gif', 'image/png', 'image/jpeg', 'image/svg+xml'
    ];

    const TYPE_IMAGE = 1;
    const TYPE_VIDEO = 2;
    const TYPE_AUDIO = 3;

    const ADMIN_EVENTS = ['create' => true, 'update' => true, 'delete' => true];

    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('file', [
            'styles' => [
                'medium' => '300x300',
                'thumb' => '100x100'
            ],
            'preserve_files' => true
        ]);

        $this->hasAttachedFile('thumbnail', [
            'styles' => [
                'medium' => '300x300',
                'thumb' => '100x100'
            ],
            'preserve_files' => true
        ]);

        parent::__construct($attributes);
    }

    public function getAttributes()
    {
        return parent::getAttributes();
    }

    public function setAttribute($key, $value)
    {
        if (array_key_exists($key, $this->attachedFiles))
        {
            if ($value)
            {
                $attachedFile = $this->attachedFiles[$key];
                $attachedFile->setUploadedFile($value);

                if ($key != 'thumbnail')
                    $this->metaData($value);
            }

            return;
        }

        parent::setAttribute($key, $value);
    }

    private function metaData($value)
    {
        $this->attributes['type'] = self::getFileType($value);

        switch ($this->attributes['type'])
        {
            case self::TYPE_IMAGE:
                list($width, $height) = getimagesize($value->getRealPath());
                $this->attributes['width'] = $width;
                $this->attributes['height'] = $height;

                break;

            case self::TYPE_VIDEO:
                $videoPath = $value->getRealPath();

                $ffprobe = FFProbe::create();
                $information = $ffprobe->streams($videoPath)->videos()->first()->all();

                if ($thumbnail = self::videoThumbnail($videoPath))
                    $this->thumbnail = $thumbnail;

                $this->attributes['width'] = $information['width'];
                $this->attributes['height'] = $information['height'];
                $this->attributes['duration'] = intval($information['duration']);
                break;

            case self::TYPE_AUDIO:
                $ffprobe = FFProbe::create();
                $information = $ffprobe->streams($value->getRealPath())->audios()->first()->all();

                $this->attributes['duration'] = intval($information['duration']);
                break;

        }
    }

    public static function getFileType($file)
    {
        if ($file)
        {
            $mime = $file->getMimeType();

            if (strstr($mime, "image/"))
                return self::TYPE_IMAGE;
            else if (strstr($mime, "video/"))
                return self::TYPE_VIDEO;
            else if (strstr($mime, "audio/"))
                return self::TYPE_AUDIO;
        }

        return null;
    }

    public static function videoThumbnail($video)
    {
        $tempPath = config('laravel-stapler.stapler.temp_path');

        try {
            $ffmpeg = FFMpeg::create();
            $from = TimeCode::fromSeconds(1);
            $time = time();
            $path = public_path("$tempPath/$time.jpg");

            $ffmpeg->open($video)->frame($from)->save($path);
            $thumbnail = new File($path);

            return $thumbnail;
        }
        catch (Exception $e) {
            return null;
        }
    }

    public function getFileAttribute()
    {
        return url($this->file->url());
    }

    public static function humanType($type)
    {
        switch ($type)
        {
            case self::TYPE_IMAGE:
                return 'image';
            case self::TYPE_VIDEO:
                return 'video';
            case self::TYPE_AUDIO:
                return 'audio';
        }

        return null;
    }
}
