<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPurchaseMci extends Model
{
    protected $table = 'user_purchase_mci';
    protected $casts = ['id' => 'string', 'price' => 'string'];
    protected $hidden = ['created_at', 'updated_at', 'updater_id', 'creator_ip', 'deleter_ip'];
    protected $appends = ['service_id'];


    const SUBSCRIBE_TYPE = 'SUBSCRIBE';
    const UNSUBSCRIBE_TYPE = 'UNSUBSCRIBE';
    const RENEWAL_TYPE = 'RENEWAL';

    const NOT_DEFINED = 'NOT DEFINED';

    const APP_CHANNEL = 'APP';
    const WAP_CHANNEL = 'WAP';
    const SMS_CHANNEL = 'SMS';
    const CRM_CHANNEL = 'CRM';

    const SUBSCRIBE_AKO = 0;
    const UNSUBSCRIBE_AKO = 5;


    public function getServiceIdAttribute()
    {
        if ($this->attributes['type'])
            return config('settings.mci.service_key');
    }

    public static function ChannelAttribute($value)
    {
        $channels = [
            self::SMS_CHANNEL => 1,
            self::APP_CHANNEL => 2,
            self::WAP_CHANNEL => 3,
            self::CRM_CHANNEL => 4,
            self::NOT_DEFINED => 0,
        ];
        return $channels[$value];
    }

    public function getChannelAttribute($value)
    {
        $channels = [
            1 => self::SMS_CHANNEL,
            2 => self::APP_CHANNEL,
            3 => self::WAP_CHANNEL,
            4 => self::CRM_CHANNEL,
            0 => self::NOT_DEFINED,
        ];
        return $channels[$value];
    }

    public static function getTypes($value)
    {
        $types = [
            self::SUBSCRIBE_TYPE => 1,
            self::UNSUBSCRIBE_TYPE => 2,
            self::RENEWAL_TYPE => 3,
            self::NOT_DEFINED => 0,
        ];

        return $types[$value];
    }

}