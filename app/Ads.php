<?php

namespace App;

use App\Scopes\PublishedScope;
use App\Traits\AdminActivity;
use App\Traits\Cacheable;
use App\Traits\Mediable;
use App\Traits\Pagination;
use App\Traits\SortableDelete;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Mediable as MediableModel;
use Rutorika\Sortable\SortableTrait;

class Ads extends Model
{
    use SoftDeletes, AdminActivity, Pagination, Mediable, SortableTrait, SortableDelete, Cacheable;

    protected $table = 'ads';
    protected $attributes = ['description' => null, 'published' => self::PUBLISHED, 'type' => self::TYPE_APP];
    protected $hidden = ['mediaAll', 'published', 'creator_id', 'updater_id', 'deleter_id', 'creator_ip', 'updater_ip', 'deleter_ip', 'deleted_at'];
    protected $casts = ['id' => 'string', 'published' => 'boolean'];
    protected $appends = ['media'];

    protected static $sortableGroupField = 'type';

    public static $checkEvent = true;
    const ADMIN_EVENTS = ['create' => true, 'update' => true, 'delete' => true];

    public static $cache_events = ['create', 'update', 'delete'];

    public static $mediaFields = [
        MediableModel::THUMBNAIL_FIELD => ['name' => MediableModel::THUMBNAIL_FIELD, 'type' => Media::TYPE_IMAGE, 'multipliable' => false],
        MediableModel::BANNER_FIELD => ['name' => MediableModel::BANNER_FIELD, 'type' => Media::TYPE_IMAGE, 'multipliable' => false],
        MediableModel::VIDEO_FIELD => ['name' => MediableModel::VIDEO_FIELD, 'type' => Media::TYPE_VIDEO, 'multipliable' => false],
    ];

    const TYPE_APP = 1;
    const TYPE_WEBSITE = 2;

    const PUBLISHED = 1;
    const NOT_PUBLISHED = 0;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PublishedScope);
    }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = trim($value);
    }

    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = html_entity_decode($value);
    }

    public static function UpdateCache($event)
    {
        if (in_array($event, self::$cache_events))
        {
            $cache = Ads::sorted()->get();
            Cache::forever('ads', $cache);

            return $cache;
        }
    }

    public static function cache()
    {
        $cache = Cache::get('ads');

        if ($cache and $cache->count())
            return $cache;

        return self::UpdateCache('update');
    }
}
