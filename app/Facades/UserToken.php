<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class UserToken extends Facade
{
    protected static function getFacadeAccessor() { return 'Token'; }
}