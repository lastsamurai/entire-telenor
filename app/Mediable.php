<?php

namespace App;

use App\Traits\AdminActivity;
use App\Traits\HasCompositePrimaryKey;
use App\Traits\UpdateMediables;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\MorphPivot;

class Mediable extends MorphPivot
{
    use AdminActivity, SoftDeletes, HasCompositePrimaryKey, UpdateMediables;

    protected $table = "mediables";
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $primaryKey = ['media_id', 'mediable_type', 'mediable_id'];
    public $incrementing = false;

    public static $checkEvent = true;
    const ADMIN_EVENTS = ['create' => true, 'update' => true, 'delete' => true];

    const THUMBNAIL_FIELD = 'thumbnail';
    const VIDEO_FIELD = 'video';
    const AUDIO_FIELD = 'audio';
    const BANNER_FIELD = 'banner';

    public function getCreatedAtColumn()
    {
        return 'created_at';
    }

    public function getUpdatedAtColumn()
    {
        return 'updated_at';
    }

    public function getDeletedAtColumn()
    {
        return 'deleted_at';
    }

    public function mediable()
    {
        return $this->morphTo();
    }
}
