<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPurchaseTigo extends Model
{
    use SoftDeletes;

    protected $table = 'user_purchases_tigo';
    protected $fillable = ['user_id', 'product_id'];
}