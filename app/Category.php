<?php

namespace App;

use App\Traits\AdminActivity;
use App\Traits\Bookmarkable;
use App\Traits\Cacheable;
use App\Traits\Pagination;
use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Rinvex\Categories\Models\Category as RinvexCategory;
use App\Traits\Mediable;
use App\Mediable as MediableModel;

class Category extends RinvexCategory
{
    use Cacheable, AdminActivity, SoftDeletes, Pagination, Mediable, Bookmarkable, CascadeSoftDeletes;

    protected $attributes = ['bookmarks' => 0, 'views' => 0, 'games' => 0, 'type' => self::TYPE_GAME];
    protected $hidden = ['mediaAll', 'creator_id', 'updater_id', 'deleter_id', 'creator_ip', 'updater_ip', 'deleter_ip', 'deleted_at', '_lft', '_rgt'];
    protected $casts = ['id' => 'string', 'parent_id' => 'string'];
    protected $appends = ['media', 'is_bookmarked'];
    protected $cascadeDeletes = ['bookmarksCascade'];

    public static $checkEvent = true;
    public static $cache_events = ['create', 'update', 'delete'];
    public static $mediaFields = [
        MediableModel::THUMBNAIL_FIELD => ['name' => MediableModel::THUMBNAIL_FIELD, 'type' => Media::TYPE_IMAGE, 'multipliable' => false]
    ];

    public static $getLocale = false;
    public static $getLocaleAttribute = true;

    const ADMIN_EVENTS = ['create' => true, 'update' => true, 'delete' => true];

    const TYPE_GAME = 1;

    public function getNameAttribute()
    {
        if (self::$getLocaleAttribute) {
            if(self::$getLocale){
                $locale = app()->getLocale();
                $value = json_decode($this->attributes['name'], false);
                return isset($value->$locale) ? $value->$locale : null;
            }

            return json_decode($this->attributes['name'], false);
        }

        return $this->attributes['name'];
    }

    public static function UpdateCache($event)
    {
        if (in_array($event, self::$cache_events))
        {
            $cache = Category::orderBy('id', 'asc')->get();
            Cache::forever('categories', $cache);

            return $cache;
        }
    }

    public static function cache()
    {
        $cache = Cache::get('categories');

        if ($cache and $cache->count())
            return $cache;

        return self::UpdateCache('update');
    }

    public static function htmlSelect($return = 'array', $prepend = [], $type = null)
    {
        $options = [];

        if ($type)
            $tree = ($cache = Cache::get('categories')) ? $cache->where('type', $type)->toTree()->toArray() : [];
        else
            $tree = ($cache = Cache::get('categories')) ? $cache->toTree()->toArray() : [];

        $options = self::htmlOption($tree, $options, 0, '');
        $options = collect($options);

        $keys = $options->pluck('id');
        $values = $options->pluck('label');


        $options = $keys->combine($values);

        if ($prepend)
            $options->prepend($prepend[0], $prepend[1]);

        if ($return == 'array')
            return $options->toArray();
        else if ($return == 'collection')
            return $options;

        return [];
    }

    public static function htmlOption($tree, $options, $depth = 0, $label = '')
    {
        foreach ($tree as $key => $value)
        {

            $locale = app()->getLocale();
            if ($depth == 0)
                $current_label = (json_decode($value['name']))->$locale;
            else
                $current_label = $label . " > " . (json_decode($value['name']))->$locale;

            $options[] = [
                'id' => $value['id'],
                'label' => $current_label,
            ];


            if (!empty($value['children']))
            {
                if ($depth == 0)
                    $new_label = $value['name'][$locale];
                else
                    $new_label = $label . " > " . $value['name'][$locale];

                $options = self::htmlOption($value['children'], $options, $depth + 1, $new_label);
            }
        }

        return $options;
    }

    public static function integerID($ids, $id = null)
    {
        foreach ($ids as $key => $value)
            $ids[$key] = (int)$value;

        if ($id)
            array_push($ids, (int)$id);

        return $ids;
    }
}
