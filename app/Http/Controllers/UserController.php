<?php

namespace App\Http\Controllers;

use App\Classes\Jwt;
use App\Http\Controllers\API\UserDotController;
use App\Scopes\ActiveScope;
use App\User;
use App\UserToken;
use Carbon\Carbon;
use Ghorbannezhad\Dot\Facades\Dot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use SPay;

class UserController extends Controller
{
    const TOKEN_EXPIRE_DAY = 10;
    const TOKEN_EXPIRE_MINUTES = 14400;
    const TOKEN_VALID_NUMBER = 5;

    public function signUp(Request $request)
    {
        
        info(json_encode($request->all()));
        
        $this->validate($request, [
            'msisdn' => ['required', 'regex:/^(\+?0?92|0)([3]{1}[4]{1}[5-7]{1})([0-9]{7})$/'],
        ]);

        
        $phone = SetClearPhone($request->msisdn);
        $msisdn = SetMsisdn($request->msisdn);

        info($phone);
        
        $user = User::withoutGlobalScope(ActiveScope::class)->firstOrNew(['phone' => $phone]);

        $user_subscription_time = false;

        

        if(!empty($user->sub_at)){
            
            $sub_date = new Carbon($user->sub_at);
            if(Carbon::now()->format('Y-m-d H:i:s') <= $sub_date->addDay(1)->format('Y-m-d H:i:s')){
                $user_subscription_time = true;
            }
        }
        
    
        if ($user->active == User::ACTIVE || $user_subscription_time) {
            
            $jwt = $request->jwt;
            $user_access = false;
            if(empty($jwt)){
                $user_token_count = UserToken::where('user_id',$user->id)->where('expire_at','>=',Carbon::now()->format('Y-m-d H:i:s'))->count();
                if($user_token_count < self::TOKEN_VALID_NUMBER){
                    $jwt = new Jwt();
                    $uuid = Uuid::uuid4()->toString();
                    $new_jwt = $jwt->set($uuid);

                    $user_token = new UserToken();
                    $user_token->user_id = $user->id;
                    $user_token->jwt = $new_jwt;
                    $user_token->expire_at = Carbon::now()->addDay(self::TOKEN_EXPIRE_DAY)->format('Y-m-d H:i:s');
                    if($user_token->save()){
                        $user_access = true;
                    }
                }

            }else{
                $user_access = true;
            }

            if($user_access){

                Auth::login($user);
                return response()->json(['status' => 'success', 'action' => 'redirect', 'message' => null,'jwt' =>$new_jwt], 200);
            }else{
                return response()->json(['status' => 'success', 'action' => 'error', 'message' => trans('messages.reach_to_max_login')], 200);
            }

        }
        else {
            Auth::logout();

            info('Phone of user before send OTP'.$phone);
            
            $result = Dot::sendOtp($msisdn, $request->ip());

//            if($result->code == self::HTTP_OK){
            if(1){

                $message = 'You’re almost there, please check your SMS for the final step.';
            }else{
                $message = 'try_again_later';
            }
            return response()->json(['status' => 'success', 'action' => 'message', 'message' => $message], 200);
        }
    }

    public function confirmOtp(Request $request){

        $this->validate($request, [
            'msisdn' => ['required', 'regex:/^(\+?0?92|0)([3]{1}[4]{1}[5-7]{1})([0-9]{7})$/'],
            'code' => ['required','digits:4']
        ]);

        info(json_encode($request->all()));

        $phone = SetClearPhone($request->msisdn, 0);

        $msisdn = SetMsisdn($request->msisdn);

        $user = User::withoutGlobalScope(ActiveScope::class)->firstOrNew(['phone' => $phone]);

        info(json_encode($user));

        $user->msisdn = $msisdn;
        $user->ip = $request->ip();

        $user_subscription_time = false;

        if(!empty($user->sub_at)){
            $sub_date = new Carbon($user->sub_at);
            if(Carbon::now()->format('Y-m-d H:i:s') <= $sub_date->addDay(1)->format('Y-m-d')){
                $user_subscription_time = true;
            }
        }

        if ($user->active == User::ACTIVE || $user_subscription_time) {

            info('user is active');

            $jwt = $request->jwt;
            $user_access = false;

            if(empty($jwt)){

                $user_token_count = UserToken::where('user_id',$user->id)->where('expire_at','>=',Carbon::now()->format('Y-m-d H:i:s'))->count();

                if($user_token_count < self::TOKEN_VALID_NUMBER){

                    $jwt = new Jwt();
                    $uuid = Uuid::uuid4()->toString();
                    $new_jwt = $jwt->set($uuid);

                    $user_token = new UserToken();

                    $user_token->user_id = $user->id;
                    $user_token->jwt = $new_jwt;

                    $user_token->expire_at = Carbon::now()->addDay(self::TOKEN_EXPIRE_DAY)->format('Y-m-d H:i:s');

                    if($user_token->save()){
                        $user_access = true;
                    }
                }

            }else{
                $user_access = true;
            }

            if($user_access){
                Auth::login($user);
                return response()->json(['status' => 'success', 'action' => 'redirect', 'message' => null,'jwt' =>$new_jwt], 200);
            }else{
                return response()->json(['status' => 'success', 'action' => 'error', 'message' => trans('messages.reach_to_max_login')], 200);
            }

        }
        else {
            info('start of send confirm request');

            $result = Dot::confirmOtp($msisdn,$request->code);

            dd($result);//***************************************** */

            info(json_encode($result));

            if($result->code == self::HTTP_OK){

                $user->active = User::ACTIVE;
                $user->sub_at = Carbon::now()->format('Y-m-d H:i:s');
                $user->save();

                Auth::login($user);
                return response()->json(['status' => 'success', 'action' => 'redirect', 'message' => null], 200);

               /* $payment_result = Dot::payment($phone);
                if($payment_result->code == self::HTTP_OK){
                    $user->active = User::ACTIVE;
                    $user->sub_at = Carbon::now()->format('Y-m-d H:i:s');
                    $user->save();
                    Auth::login($user);
                    return response()->json(['status' => 'success', 'action' => 'redirect', 'message' => null], 200);
                }else{
                    $message = trans('messages.unsuccessful_payment');
                    return response()->json(['status' => 'success', 'action' => 'message', 'message' => $message], 200);
                }*/

            }else{
                $message = trans('wrong_code');
                return response()->json(['status' => 'success', 'action' => 'message', 'message' => $message], 200);
            }
        }
    }

    public function subscribe_landing_method(Request $request){

        $result=Dot::subscribe_landing_method($request->msisdn,$request->lpTransId);
        
        if($result->code == self::HTTP_OK){
            //redirect to service page & log user i
            
        }else{
            return dd($result->message);
        }
    }

    public function subscribe_otp_api(Request $request){

        $result=Dot::subscribe_otp_api($request->msisdn,$request->otpId,$request->otpPin);

    }

    public function unsubscribe(Request $request){

        $this->validate($request, [
            'id' => ['required'],
        ]);

        $user = User::withoutGlobalScope(ActiveScope::class)->find($request->id);

        if ($user->active == User::ACTIVE) {
            Auth::logout();
            $user->active = User::NOT_ACTIVE;
            if($user->save()){
                return response()->json(['status' => 'success', 'action' => 'message', 'message' => trans('messages.logout_successfully')], 200);
            }
            /*$result = Dot::unsubscribe($user->msisdn, $user->id, $request->ip());
            if($result->code == self::HTTP_OK) {
                $user->active = User::NOT_ACTIVE;
                if($user->save()){
                    Auth::logout();
                    return response()->json(['status' => 'success', 'action' => 'message', 'message' => trans('messages.logout_successfully')], 200);
                }
            }*/

            return response()->json(['status' => 'success', 'action' => 'message', 'message' => 'Logout was not successful'], 200);
        }
        else {
            return response()->json(['status' => 'success', 'action' => 'message', 'message' => 'You have to sign in first'], 200);
        }
    }

    public function CGRedirect(Request $request){
        /* 
            middleware VerifyDotSignature has already checked the following:
                * that required parameters exist
                * that signature is valid
                * that such a user with such a 'msisdn' exists in the database
        */

        define('SUCCESS',0);
        define('INTERNAL_ERROR',1009);
        define('MSISDN_NOT_DETECTED',1012);
        define('OTHER_DETECTED',1013);
        define('DUP_TRANS_ID',1014);
        define('INVALID_REQUEST',1015);
        define('MISSING_PARAMS',1016);

        $reason_code=$request->get('reason_code');
        $service_id=$request->get('service_id');
        $dot_txid=$request->get('dot_txid');
        $transId=$request->get('lpTransId');
        $partnerId=$request->get('partner_txid');

        switch($reason_code){
            case SUCCESS://success
                //subscribe user to service
                
                $this->subscribe_landing_method($request);
                
                
                //log user in
                /*
                info(json_encode($request->all()));
                $phone = SetClearPhone($request->msisdn);
                $msisdn = SetMsisdn($request->msisdn);
                info($phone);
                
                $user = User::withoutGlobalScope(ActiveScope::class)->firstOrNew(['phone' => $phone]);
                Auth::login($user,true);

                //redirect to respective service page
                return redirect()->route('home');
                //return view('frontend.landing.index')->with('user',$user);
                return redirect('/play/'.$service_id);//redirect to app with [service id]=service_id
                */

                break;
            case INTERNAL_ERROR:
            case MSISDN_NOT_DETECTED:
            case OTHER_DETECTED:
            case DUP_TRANS_ID:
            case INVALID_REQUEST:
            case MISSING_PARAMS:
            default: //failure ; try the OTP API
                return redirect('/users/enter-number');
                break;
        }



        return $reason_code;
    }

    public function getPhoneNumber(){
        return view('frontend.otp.getPhoneNumber');
    }

    public function sendPin(Request $request){
        
        
        /*$this->validate($request, [
            'phone' => ['required','regex:/^(\+?0?92|0)([3]{1}[4]{1}[5-7]{1})([0-9]{7})$/']// add regex to check phone number
        ]);
        */
        
        //$phone=$request->phone;      
        
        $msisdn=$request->msisdn;        
        

        $this->signUp($request);

        return view('frontend.otp.getPin')->with('msisdn',$msisdn);
    }

    public function checkPin(Request $request){
        
        return $res=$this->confirmOtp($request);

        //if code is correct then

        $this->subscribe_otp_api($request);

        //  subscribe user to service 

    }
}