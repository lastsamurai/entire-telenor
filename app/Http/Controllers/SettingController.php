<?php

namespace App\Http\Controllers;

use App\Category;
use App\Game;
use App\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SettingController extends Controller
{
    public function setLang(Request $request)
    {
        $this->validate($request, ['lang'=>'required']);
        session(['lang' => $request->lang]);
        info('session lang from setting controller: '.session('lang'));
        return response()->json(true,200);
    }
}
