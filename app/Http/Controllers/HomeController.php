<?php

namespace App\Http\Controllers;

use App\Category;
use App\Game;
use App\Like;
use Carbon\Carbon;
use Ghorbannezhad\Dot\Facades\Dot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use function Sodium\add;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        
        Game::$getLocale = true;

        Category::$getLocale = true;
        $lang = app()->getLocale();

        if ($request->category)
            $games = Game::orderBy('id', 'desc')->withCategories([(int)$request->category])->limit(18)->get();
        else
            $games = Game::orderBy('id', 'desc')->limit(18)->get();

        $categories = Category::where('games','>',0)->get();
//        $categories = Category::get();


        $user_id = null;

        if($request->likes){
            if(Auth::check()){
                $user = Auth::user();
                $likes = Like::where('id', $user->id)->where('likable_type',Game::class)->pluck('id')->toArray();
                $games = Game::orderBy('id', 'desc')->whereIn('id',$likes)->limit(18)->get();
                $user_id = $user->id;
            }
        }


        $top_app = Game::orderBy('likes', 'desc')->with('categories')->first();
        $most_viewed = Game::orderBy('views', 'desc')->limit(3)->get();

        $pageId = 'landing-page';
        return view('frontend/landing/index', compact('pageId', 'games', 'categories', 'top_app', 'most_viewed','user_id','lang'));
    }
}
