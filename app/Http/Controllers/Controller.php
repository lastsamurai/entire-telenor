<?php

namespace App\Http\Controllers;

use App\DeviceToken;
use App\Http\Controllers\API\UserMtnController;
use App\Subscription;
use App\User;
use App\UserToken;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $dashboardTemplate = 'webarch';
    protected $paginationLimit = 15;

    const HTTP_OK = 200;
    const HTTP_NO_CONTENT = 204;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_NOT_FOUND = 404;
    const NOT_ACCEPTABLE = 406;
    const HTTP_UNPROCESSABLE_ENTITY = 422;
    const HTTP_ERROR = 500;


    public function __construct()
    {
        $subscription = Subscription::where('type',Subscription::DAILY_TYPE)->first();
        if(!empty(session('lang'))){

            app()->setLocale(session('lang'));
        }

        $lang = app()->getLocale();

        View::Share('lang',$lang);
        View::share('subscription',$subscription);
    }


    protected function hasUpdatePermission(Request $request)
    {
        if ($request->session()->has('update') and $request->session()->get('update'))
            return true;

        return false;
    }

    protected function publishStatus(Request $request, $edit = false)
    {
        if ($edit)
        {
            if ($request->session()->has('publish') and $request->session()->get('publish'))
                return true;
        }
        else
        {
            if ($request->session()->has('publish') and $request->session()->get('publish') and $request->published)
                return true;
        }

        return false;
    }

    protected function editorChoiceStatus(Request $request, $edit = false)
    {
        if ($edit)
        {
            if ($request->session()->has('publish') and $request->session()->get('publish'))
                return true;
        }
        else
        {
            if ($request->session()->has('publish') and $request->session()->get('publish') and $request->editor_choice)
                return true;
        }

        return false;
    }

    public function logoutProcess($userId, $token = null)
    {
        $user = User::where('id', $userId)->first();
        if ($user)
        {
            switch ($user->mobile_service_provider)
            {
                case User::MTN_TYPE:
                    $tokens = UserToken::where('user_id', $user->id)->get();
                    if ($tokens->count())
                    {
                        $tokens = $tokens->pluck('id')->toArray();

                        UserToken::whereIn('id', $tokens)->update(['token' => null]);
                        DeviceToken::whereIn('token_id', $tokens)->delete();
                    }

                    $mtn = new UserMtnController;
                    $mtn->revokeTokens($user->id);

                    break;

                case User::MCI_TYPE:
                    if ($token)
                    {
                        $userToken = UserToken::where(['user_id' => $user->id, 'token' => $token])->first();
                        $userToken->token = null;
                        $userToken->save();

                        $device_token = DeviceToken::where(['token_id' => $userToken->id])->first();
                        if ($device_token)
                            $device_token->delete();
                    }
                    else
                    {
                        $tokens = UserToken::where('user_id', $user->id)->get();
                        if ($tokens->count())
                        {
                            $tokens = $tokens->pluck('id')->toArray();

                            UserToken::whereIn('id', $tokens)->update(['token' => null]);
                            DeviceToken::whereIn('token_id', $tokens)->delete();
                        }
                    }
                    break;
            }
        }
    }
}
