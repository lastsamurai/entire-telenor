<?php

namespace App\Http\Controllers;

use App\Category;
use App\Game;
use App\Like;
use App\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AppController extends Controller
{
    public function details($id, $slug = null)
    {
        Game::$getLocale = true;
        Category::$getLocale = true;
        $game = Game::relations()->findOrFail($id);
        $favorite = $game->isLiked;

        $user_id = null;
        $user = Auth::user();

        if($user){
            $user_id = $user->id;
        }

        if ($game->categories->count())
        {
            $categories = $game->categories->pluck('id')->toArray();
            $categories = Category::integerID($categories);

            $related = Game::withAnyCategories($categories)->where('id', '<>', $game->id)->relations()->inRandomOrder()->limit(10)->get();
        }
        else
            $related = collect([]);

        return view('frontend/apps/details', compact('game', 'related', 'subscriptions','favorite','user_id'));
    }

    public function play($id)
    {
        if (Auth::check()) {
            $game = Game::relations()->findOrFail($id);
            return view('frontend/apps/play', compact('game'));
        }

        abort(403);
    }

    public function addFavorite(Request $request){
        $this->validate($request,['user_id'=>'required','game_id'=>'required']);

        $game = Game::findOrFail($request->game_id);

        $like = Like::firstOrNew(['user_id'=>$request->user_id,'likable_id' =>$request->game_id,'likable_type' => Game::class ]);
        $like->likable_type = Game::class;
        $status = false;
        if($like->save()){
            $status = true;
        }
        return response()->json($status,self::HTTP_OK);

    }
}
