<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Mediable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
    protected $type = Category::TYPE_GAME;

    public function index()
    {
        Category::$getLocale = true;

        $categories = Category::where('type', $this->type)->orderBy('id', 'desc')->paginate($this->paginationLimit);
//        $parents = Category::htmlSelect('collection', [trans('ui.text.category-parent') . "*", 0], $this->type);

        return view("dashboard/$this->dashboardTemplate/categories/index", compact('categories'));
    }

    public function add()
    {
        $type = $this->type;
        return view("dashboard/$this->dashboardTemplate/categories/add", compact('type'));
    }

    public function store(Request $request)
    {
        $this->validate($request, ['name_en' => 'required','name_ur' => 'required', 'type' => ['required', Rule::in([Category::TYPE_GAME])], 'hex' => 'required', 'parent_id' => 'required', 'image' => 'media:image']);
        $category = new Category;
        Category::$getLocale = true;
        $category->name = [
           'en'=>$request->name_en,
           'ar'=>$request->name_ur,
        ];
        $category->hex = $request->hex;
        $category->type = $request->type;

        if ($request->parent_id > 0)
            $category->parent_id = $request->parent_id;

        if($category->save())
        {
            if ($request->image)
                $category->attachMedia($request->image, Mediable::THUMBNAIL_FIELD);

            RoleController::setCategoryPermission($category);
            session()->flash('success_flash', trans('messages.item_created'));
        }
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->back();
    }

    public function edit($id)
    {
        $category = Category::with('mediaAll')->findOrFail($id);

//        $parents = Category::htmlSelect('collection', [trans('ui.text.category-parent') . "*", 0], $category->type);

        $type = $this->type;

        return view("dashboard/$this->dashboardTemplate/categories/edit", compact('category','type'));
    }

    public function update(Request $request)
    {

        $this->validate($request, ['id' => 'required|different:parent_id', 'name_ur' => 'required', 'name_en' => 'required', 'hex' => 'required', 'parent_id' => 'required', 'image' => 'media:image']);

        Category::$getLocaleAttribute = false;
        $category = Category::findOrFail($request->id);

        $category->name = [
            'en'=>$request->name_en,
            'ar'=>$request->name_ur
        ];

        $category->hex = $request->hex;

        if ($request->parent_id == 0)
            $category->parent_id = null;
        else
            $category->parent_id = $request->parent_id;

        if ($request->image)
            $category->syncMedia($request->image, Mediable::THUMBNAIL_FIELD);

        if($category->save())
            session()->flash('success_flash', trans('messages.item_updated'));
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->back();
    }

    public function destroy(Request $request)
    {
        $this->validate($request, ['id' => 'required']);

        $category = Category::findOrFail($request->id);
        if ($category)
        {
            if ($category->delete())
                session()->flash('success_flash', trans('messages.item_deleted'));
            else
                session()->flash('error_flash', trans('messages.unknown_error'));
        }
        else
            session()->flash('error_flash', trans('messages.record_not_found'));

        return redirect()->back();
    }
}
