<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Notifications\CreateAdminNotification;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    protected $guard = 'admin';

    public function index()
    {
        $admins = Admin::orderBy('name', 'asc')->with('role')->paginate($this->paginationLimit);
        return view("dashboard/$this->dashboardTemplate/admins/index", compact('admins'));
    }

    public function add()
    {
        $roles = Role::get()->pluck('name', 'id')->toArray();

        return view("dashboard/$this->dashboardTemplate/admins/add", compact('roles'));
    }

    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'email' => 'required|email|unique:admins', 'role_id' => 'required', 'image' => 'nullable|image']);

        $admin = new Admin;
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->role_id = $request->role_id;

        if ($request->password)
            $admin->password = bcrypt($request->password);
        else
            $admin->password = bcrypt(str_random(20));

        if ($request->image)
            $admin->image = $request->image;

        if ($admin->save())
        {
            if (!$request->password)
            {
                $passwordBroker = app('auth.password')->broker('admins');
                $tokens = $passwordBroker->getRepository();
                $token = $tokens->create($admin);

                $admin->notify(new CreateAdminNotification($token));
            }

            session()->flash('success_flash', trans('messages.item_created'));
        }
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->route('dashboard.admins.index');
    }

    public function edit($id)
    {
        $admin = Admin::where('id', $id)->first();
        if ($admin)
        {
            $roles = Role::get()->pluck('name', 'id')->toArray();
            return view("dashboard/$this->dashboardTemplate/admins/edit", compact('admin', 'roles'));
        }

        abort(404);
    }

    public function update(Request $request)
    {
        $this->validate($request, ['id' => 'required', 'name' => 'required', 'role_id' => 'required', 'image' => 'nullable|image']);

        $admin = Admin::findOrFail($request->id);
        $admin->name = $request->name;
        $admin->role_id = $request->role_id;

        if ($request->image)
            $admin->image = $request->image;

        if ($admin->save())
            session()->flash('success_flash', trans('messages.item_updated'));
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->route('dashboard.admins.index');
    }

    public function selfEdit(Request $request)
    {
        $admin = $request->user($this->guard);
        return view("dashboard/$this->dashboardTemplate/admins/self-edit", compact('admin'));
    }

    public function selfUpdate(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'image' => 'image', 'password' => 'nullable|confirmed|min:8']);

        $admin = $request->user($this->guard);
        $admin->name = $request->name;
        unset($admin->role_name);

        if ($request->image)
            $admin->image = $request->image;


        if ($request->old_password and $request->password)
        {
            if (Auth::guard($this->guard)->attempt(['email' => $admin->email, 'password' => $request->old_password]))
            {
                $admin->password = bcrypt($request->password);
                $admin->save();

                if (Auth::guard($this->guard)->attempt(['email' => $admin->email, 'password' => $request->password]))
                {
                    session()->flash('success_flash', trans('messages.item_updated'));
                    return redirect()->back();
                }
                else
                {
                    session()->flash('error_flash', trans('messages.unknown_error'));
                    return redirect()->back()->withInput();
                }
            }
            else
            {
                session()->flash('error_flash', trans('messages.incorrect_old_pass'));
                return redirect()->back()->withInput();
            }
        }

        if ($admin->save())
            session()->flash('success_flash', trans('messages.item_updated'));
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->back();
    }
}
