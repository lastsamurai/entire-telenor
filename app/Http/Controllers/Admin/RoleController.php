<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Role;

class RoleController extends Controller
{
    protected $locale;

    public function __construct()
    {
        $this->locale = config('app.locale');
    }

    public function index()
    {
        $roles = Role::orderBy('name', 'asc')->paginate($this->paginationLimit);

        return view("dashboard/$this->dashboardTemplate/roles/index", compact('roles'));
    }

    public function add()
    {
        return view("dashboard/$this->dashboardTemplate/roles/add");
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), ['name' => 'required', 'controller' => 'required', 'read' => 'required', 'write' => 'required', 'update' => 'required', 'delete' => 'required', 'publish' => 'required', 'dynamic' => 'required'])->validate();

        $permissions = $this->GeneratePermission($request->controller, $request->read, $request->write, $request->update, $request->delete, $request->publish, $request->dynamic);

        $role = new Role;
        $role->name = $request->name;
        $role->permissions = json_encode($permissions);

        if ($role->save())
            session()->flash('success_flash', trans('messages.item_created'));
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->route('dashboard.roles.index');
    }

    public function edit($id)
    {
        $role = Role::where('id', $id)->first();
        $role->permissions = json_decode($role->permissions, true);
        $permissions = config('acl');

        // sections
        foreach ($permissions['sections'] as $key => $value)
        {
            if (isset($role->permissions['sections'][$value['controller']]))
            {
                $permissions['sections'][$key]['read'] = $role->permissions['sections'][$value['controller']]['read'];
                $permissions['sections'][$key]['write'] = $role->permissions['sections'][$value['controller']]['write'];
                $permissions['sections'][$key]['update'] = $role->permissions['sections'][$value['controller']]['update'];
                $permissions['sections'][$key]['delete'] = $role->permissions['sections'][$value['controller']]['delete'];
                $permissions['sections'][$key]['publish'] = $role->permissions['sections'][$value['controller']]['publish'];
            }
            else
            {
                $permissions['sections'][$key]['read'] = 0;
                $permissions['sections'][$key]['write'] = 0;
                $permissions['sections'][$key]['update'] = 0;
                $permissions['sections'][$key]['delete'] = 0;
                $permissions['sections'][$key]['publish'] = 0;
            }
        }

        // dynamic toggle
        foreach ($permissions['dynamic']['toggle'] as $key => $value)
        {
            $toggles = DynamicToggleAcl($value['driver'], $value['provider'], $value['type_slug'], $value['type_value']);
            if (isset($role->permissions['dynamic']['toggle'][$value['slug']]))
            {
                foreach ($toggles as $toggle)
                {
                    $id = 'id-' . $toggle[$value['id']];
                    $val = [
                        'status' => in_array($toggle[$value['id']], $role->permissions['dynamic']['toggle'][$value['slug']]),
                        'id' => $toggle[$value['id']]
                    ];

                    $permissions['dynamic']['toggle'][$key]['fields']['value'][$id] = $val;
                    $permissions['dynamic']['toggle'][$key]['fields']['label'][$id] = $toggle[$value['label']];
                }
            }
            else
            {
                if(!empty($toggles))
                {
                    foreach ($toggles as $toggle)
                    {
                        $val = [
                            'status' => false,
                            'id' => $toggle[$value['id']]
                        ];
                        $permissions['dynamic']['toggle'][$key]['fields']['value']['id-' . $toggle[$value['id']]] = $val;
                        $permissions['dynamic']['toggle'][$key]['fields']['label']['id-' . $toggle[$value['id']]] = $toggle[$value['label']];
                    }
                }
                else
                {
                    $permissions['dynamic']['toggle'][$key]['fields'] = [];
                }
            }
        }

        $role->permissions = $permissions;

        return view("dashboard/$this->dashboardTemplate/roles/edit", compact('role'));
    }

    public function update(Request $request)
    {
        Validator::make($request->all(), ['id' => 'required', 'name' => 'required', 'controller' => 'required', 'read' => 'required', 'write' => 'required', 'update' => 'required', 'delete' => 'required', 'publish' => 'required', 'dynamic' => 'required'])->validate();

        $permissions = $this->GeneratePermission($request->controller, $request->read, $request->write, $request->update, $request->delete, $request->publish, $request->dynamic);

        $role = Role::findOrFail($request->id);
        $role->name = $request->name;
        $role->permissions = json_encode($permissions);

        if ($role->save())
            session()->flash('success_flash', trans('messages.item_updated'));
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->back();
    }

    private function GeneratePermission($controller, $read, $write, $update, $delete, $publish, $dynamic)
    {
        $permissions = [];
        foreach ($controller as $key => $value)
        {
            $permissions['sections'][$value]['read'] = $read[$key];
            $permissions['sections'][$value]['write'] = $write[$key];
            $permissions['sections'][$value]['delete'] = $delete[$key];

            if ($publish[$key])
            {
                $permissions['sections'][$value]['update'] = $publish[$key]; // force on
                $permissions['sections'][$value]['publish'] = $publish[$key];
            }
            else
            {
                $permissions['sections'][$value]['update'] = $update[$key];
                $permissions['sections'][$value]['publish'] = $publish[$key];
            }
        }


        foreach ($dynamic['toggle'] as $key => $value)
        {
            if (isset($value['fields']))
            {
                foreach ($value['fields'] as $index => $field)
                {
                    $permissions['dynamic']['toggle'][$value['slug']][] = $field;
                }
            }
            else
            {
                $permissions['dynamic']['toggle'][$value['slug']] = [];
            }
        }


        return $permissions;
    }

    public static function setCategoryPermission(Category $category)
    {
        $user = Auth::guard('admin')->user();
        if ($user and $user->role_id)
        {
            $role = Role::where('id', $user->role_id)->first();
            if ($role)
            {
                $permissions = json_decode($role->permissions, true);

                switch ($category->type)
                {
                    case Category::TYPE_GAME:
                        $toggle = 'GameCategory';
                        break;
                }

                if ($toggle)
                {
                    array_push($permissions['dynamic']['toggle'][$toggle], $category->id);
                    $permissions['dynamic']['toggle'][$toggle] = array_unique($permissions['dynamic']['toggle'][$toggle]);


                    $role->permissions = json_encode($permissions);
                    if ($role->save())
                        return true;
                }

            }
        }
        return false;
    }
}
