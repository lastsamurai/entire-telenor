<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{
    public function index()
    {
        $subscriptions = Subscription::all();

        $units = collect(config('settings.units'));
        $units = $units->pluck('label', 'id');

        return view("dashboard/$this->dashboardTemplate/subscriptions/index", compact('subscriptions', 'units'));
    }

    public function update(Request $request)
    {
        $this->validate($request, ['subscriptions' => 'array|required', 'subscription.*.id' => 'required', 'subscription.*.price' => 'required', 'subscription.*.unit' => 'required']);

        try {
            foreach ($request->subscriptions as $subscription)
            {
                $sub = Subscription::findOrFail($subscription['id']);
                $sub->price = $subscription['price'];
                $sub->unit = $subscription['unit'];
                $sub->save();
            }

            session()->flash('success_flash', trans('messages.item_created'));
        }
        catch (Exception $exception) {
            session()->flash('error_flash', trans('messages.unknown_error'));
        }

        return redirect()->back();
    }
}
