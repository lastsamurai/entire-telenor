<?php

namespace App\Http\Controllers\Admin;

use App\Scopes\ActiveScope;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::withoutGlobalScope(ActiveScope::class)->orderBy('id', 'desc')->get();

        return view("dashboard/$this->dashboardTemplate/users/index", compact('users'));
    }

    public function deactivate(Request $request)
    {
        $this->validate($request, ['id' => 'required']);

        $user = User::withoutGlobalScope(ActiveScope::class)->where('id', $request->id)->first();
        if ($user)
        {
            $user->active = User::NOT_ACTIVE;
            if ($user->save())
            {
                parent::logoutProcess($user->id);

                session()->flash('success_flash', trans('messages.item_updated'));
            }
            else
                session()->flash('error_flash', trans('messages.unknown_error'));
        }
        else
            session()->flash('error_flash', trans('messages.record_not_found'));


        return redirect()->back();
    }

    public function activate(Request $request)
    {
        $this->validate($request, ['id' => 'required']);

        $user = User::withoutGlobalScope(ActiveScope::class)->where('id', $request->id)->first();
        if ($user)
        {
            $user->active = User::ACTIVE;
            if ($user->save())
                session()->flash('success_flash', trans('messages.item_updated'));
            else
                session()->flash('error_flash', trans('messages.unknown_error'));
        }
        else
            session()->flash('error_flash', trans('messages.record_not_found'));


        return redirect()->back();
    }
}
