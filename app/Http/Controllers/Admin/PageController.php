<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Page;
use App\Scopes\PublishedScope;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        $pages = Page::withoutGlobalScope(PublishedScope::class)->orderBy('title', 'asc')->paginate($this->paginationLimit);
        return view("dashboard/$this->dashboardTemplate/pages/index", compact('pages'));
    }

    public function add()
    {
        return view("dashboard/$this->dashboardTemplate/pages/add");
    }

    public function store(Request $request)
    {
        $this->validate($request, ['title' => 'required']);

        $page = new Page;
        $page->title = $request->title;
        $page->description = ($request->description) ? $request->description : null;

        if (parent::publishStatus($request))
            $page->published = Page::PUBLISHED;

        if ($page->save())
            session()->flash('success_flash', trans('messages.item_created'));
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->route('dashboard.pages.index');
    }

    public function edit($id)
    {
        $page = Page::withoutGlobalScope(PublishedScope::class)->findOrFail($id);
        return view("dashboard/$this->dashboardTemplate/pages/edit", compact('page'));
    }

    public function update(Request $request)
    {
        $this->validate($request, ['id' => 'required', 'title' => 'required']);

        $page = Page::withoutGlobalScope(PublishedScope::class)->findOrFail($request->id);
        $page->title = $request->title;
        $page->description = ($request->description) ? $request->description : null;


        if (parent::publishStatus($request, true))
        {
            if ($request->published)
                $page->published = Page::PUBLISHED;
            else
                $page->published = Page::NOT_PUBLISHED;
        }

        if ($page->save())
            session()->flash('success_flash', trans('messages.item_updated'));
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->back();
    }

    public function destroy(Request $request)
    {
        $this->validate($request, ['id' => 'required']);

        $page = Page::findOrFail($request->id);
        if ($page)
        {
            if ($page->delete())
                session()->flash('success_flash', trans('messages.item_deleted'));
            else
                session()->flash('error_flash', trans('messages.unknown_error'));
        }
        else
            session()->flash('error_flash', trans('messages.record_not_found'));

        return redirect()->back();
    }
}
