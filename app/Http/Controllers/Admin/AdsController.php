<?php

namespace App\Http\Controllers\Admin;

use App\Ads;
use App\Mediable;
use Exception;
use App\Scopes\PublishedScope;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdsController extends Controller
{
    protected $limit = 15;

    public function index()
    {
        $ads = Ads::withoutGlobalScope(PublishedScope::class)->sorted()->get();
        return view("dashboard/$this->dashboardTemplate/ads/index", compact('ads'));
    }

    public function add()
    {
        return view("dashboard/$this->dashboardTemplate/ads/add");
    }

    public function store(Request $request)
    {
        $this->validate($request, ['title' => 'required', 'description' => 'required', 'url' => 'required', 'image' => 'required|media:image', 'banner' => 'required|media:image', 'video' => 'media:video']);

        $ads = new Ads;
        $ads->title = $request->title;
        $ads->description = $request->description;
        $ads->link = $request->url;

        if (parent::publishStatus($request))
            $ads->published = Ads::PUBLISHED;

        if ($ads->save())
        {
            $ads->attachMedia($request->image, Mediable::THUMBNAIL_FIELD);
            $ads->attachMedia($request->banner, Mediable::BANNER_FIELD);

            if ($request->video)
                $ads->attachMedia($request->video, Mediable::VIDEO_FIELD);

            session()->flash('success_flash', trans('messages.item_created'));
        }
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->back();
    }

    public function edit($id)
    {
        $ads = Ads::withoutGlobalScope(PublishedScope::class)->where('id', $id)->first();

        if ($ads)
            return view("dashboard/$this->dashboardTemplate/ads/edit", compact('ads'));

        abort(404);
    }

    public function update(Request $request)
    {
        $this->validate($request, ['id' => 'required', 'title' => 'required', 'description' => 'required', 'url' => 'required', 'image' => 'media:image', 'banner' => 'media:image', 'video' => 'media:video']);

        $ads = Ads::withoutGlobalScope(PublishedScope::class)->where('id', $request->id)->first();
        if ($ads)
        {
            $ads->title = $request->title;
            $ads->description = $request->description;
            $ads->link = $request->url;

            if (parent::publishStatus($request, true))
            {
                if ($request->published)
                    $ads->published = Ads::PUBLISHED;
                else
                    $ads->published = Ads::NOT_PUBLISHED;
            }

            if ($ads->save())
            {
                if ($request->video)
                    $ads->syncMedia($request->video, Mediable::VIDEO_FIELD);

                if ($request->banner)
                    $ads->syncMedia($request->banner, Mediable::BANNER_FIELD);

                if ($request->image)
                    $ads->syncMedia($request->image, Mediable::THUMBNAIL_FIELD);

                session()->flash('success_flash', trans('messages.item_updated'));
            }
            else
                session()->flash('error_flash', trans('messages.unknown_error'));

            return redirect()->route('dashboard.ads.index');
        }
        else
            abort(404);
    }

    public function destroy(Request $request)
    {
        $this->validate($request, ['id' => 'required']);

        $ads = Ads::withoutGlobalScope(PublishedScope::class)->where('id', $request->id)->first();
        if ($ads)
        {
            if ($ads->delete())
                session()->flash('success_flash', trans('messages.item_deleted'));
            else
                session()->flash('error_flash', trans('messages.unknown_error'));
        }
        else
            session()->flash('error_flash', trans('messages.record_not_found'));

        return redirect()->back();
    }

    public function publish($id)
    {
        $ads = Ads::withoutGlobalScope(PublishedScope::class)->where('id', $id)->first();
        if ($ads)
        {
            $ads->published = Ads::PUBLISHED;
            if ($ads->save())
                session()->flash('success_flash', trans('messages.item_updated'));
            else
                session()->flash('error_flash', trans('messages.unknown_error'));
        }
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->back();
    }

    public function unPublish($id)
    {
        $ads = Ads::withoutGlobalScope(PublishedScope::class)->where('id', $id)->first();
        if ($ads)
        {
            $ads->published = Ads::NOT_PUBLISHED;
            if ($ads->save())
                session()->flash('success_flash', trans('messages.item_updated'));
            else
                session()->flash('error_flash', trans('messages.unknown_error'));
        }
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->back();
    }

    public function sort(Request $request)
    {
        $this->validate($request, ['model' => 'required', 'swap' => 'required', 'type' => ['required', 'in:moveAfter,moveBefore']]);

        $ads = Ads::whereIn('id', [$request->model, $request->swap])->get();

        if ($ads->count() == 2)
        {
            try {
                $model = $ads->where('id', $request->model)->first();
                $swap = $ads->where('id', $request->swap)->first();

                if ($request->type == 'moveBefore')
                    $model->moveBefore($swap);
                else
                    $model->moveAfter($swap);

                $data = ['status' => 1, 'message' => trans('messages.success')];
            }
            catch (Exception $e) {
                $data = ['status' => 0, 'message' => $e->getMessage()];
            }
        }
        else
        {
            $data = ['status' => 0, 'message' => trans('messages.unknown_error')];
        }


        return response()->json($data, 200);
    }
}
