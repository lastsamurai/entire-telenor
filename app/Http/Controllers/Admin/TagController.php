<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Conner\Tagging\Model\Tag;
use Conner\Tagging\Model\Tagged;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TagController extends Controller
{
    public function index()
    {
        $tags = Tag::orderBy('id', 'desc')->get();
        return view("dashboard/$this->dashboardTemplate/tags/index", compact('tags'));
    }

    public function edit($id)
    {
        $tag = Tag::findOrFail($id);
        return view("dashboard/$this->dashboardTemplate/tags/edit", compact('tag'));
    }

    public function update(Request $request)
    {
        $this->validate($request, ['id' => 'required', 'name' => 'required']);

        $tag = Tag::findOrFail($request->id);
        $oldName = $tag->name;
        $tag->name = $request->name;

        if ($tag->save())
        {
            if ($oldName != $tag->name)
                Tagged::where('tag_name', $oldName)->update(['tag_name' => $tag->name, 'tag_slug' => $tag->slug]);

            session()->flash('success_flash', trans('messages.item_created'));
        }
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->back();
    }

    public function destroy(Request $request)
    {
        $this->validate($request, ['id' => 'required']);

        $tag = Tag::findOrFail($request->id);
        if ($tag)
        {
            if ($tag->delete())
            {
                Tagged::where('tag_name', $tag->name)->delete();

                session()->flash('success_flash', trans('messages.item_deleted'));
            }
            else
                session()->flash('error_flash', trans('messages.unknown_error'));
        }
        else
            session()->flash('error_flash', trans('messages.record_not_found'));

        return redirect()->back();
    }

    public function json()
    {
        $tags = DB::table('tagging_tags')->get()->pluck('name');

        return response()->json($tags, 200);
    }
}
