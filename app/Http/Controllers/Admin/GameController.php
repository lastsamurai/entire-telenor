<?php

namespace App\Http\Controllers\Admin;

use App\Bookmark;
use App\Category;
use App\Mediable;
use Exception;
use App\Classes\Push;
use App\Notification;
use App\Game;
use App\Scopes\PublishedScope;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    protected $limit = 15;
    protected $toggleType = 'GameCategory';
    protected $admin = false;

    public function index(Request $request)
    {
        $categories = $this->categories($request, false);
        Game::$getLocale = true;
        Category::$getLocale = true;

        if ($this->hasUpdatePermission($request))
        {
            if (count($categories) >= 1)
                $games = Game::withoutGlobalScope(PublishedScope::class)->withCategories($categories->keys())->orderBy('id', 'desc')->paginate($this->limit);
            else
                $games = Game::withoutGlobalScope(PublishedScope::class)->withCategories([0])->orderBy('id', 'desc')->paginate($this->limit);
        }
        else
        {
            if (count($categories) >= 1)
                $games = Game::withoutGlobalScope(PublishedScope::class)->where('creator_id', $this->admin()->id)->withCategories($categories->keys())->orderBy('id', 'desc')->paginate($this->limit);
            else
                $games = Game::withoutGlobalScope(PublishedScope::class)->where('creator_id', $this->admin()->id)->withCategories([0])->orderBy('id', 'desc')->paginate($this->limit);
        }


        return view("dashboard/$this->dashboardTemplate/games/index", compact('games', 'categories', 'segment', 'type'));
    }

    public function add(Request $request)
    {
        $locale = app()->getLocale();
        $browsers = config('settings.browsers');
        $categories = $this->categories($request, false);
        return view("dashboard/$this->dashboardTemplate/games/add", compact('categories', 'browsers','locale'));
    }

    public function store(Request $request)
    {
        $validation = $this->validation($request, false);
        Validator::make($request->all(), $validation)->validate();

        $game = new Game;
        $game->title = [
            'en' =>$request->title_en,
            'ar' =>$request->title_ur,
        ];
        $game->link = $request->link;
        $game->description = [
            'en'=>$request->description_en,
            'ar'=>$request->description_ur
        ];
        $game->supported_browsers = (count($request->browsers)) ? array_values($request->browsers) : [];


        if (parent::publishStatus($request))
            $game->published = Game::PUBLISHED;

        if (parent::editorChoiceStatus($request))
            $game->editor_choice = Game::EDITOR_CHOICE_ON;

        $this->toSearchableArray($request);

        if ($game->save())
        {
            $categories = $this->intCategories($request->category_id);
            $game->attachCategories($categories);
            $this->categoryStats($request->category_id, [], false);
            $this->attachTags($request, $game, true);

            $game->attachMedia($request->image, Mediable::THUMBNAIL_FIELD);

            if (parent::publishStatus($request) and $request->push)
                $this->handlePush($game);

            session()->flash('success_flash', trans('messages.item_created'));
        }
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->back();
    }

    public function edit(Request $request, $id)
    {
        $browsers = config('settings.browsers');
        $admin = $this->admin();
        $locale = app()->getLocale();

        if ($this->hasUpdatePermission($request))
            $game = Game::withoutGlobalScope(PublishedScope::class)->where('id', $id)->with('categories', 'tagged', 'mediaAll', 'notification')->first();
        else
            $game = Game::withoutGlobalScope(PublishedScope::class)->where('id', $id)->where('creator_id', $admin->id)->with('categories', 'tagged', 'mediaAll', 'notification')->first();
        if ($game)
        {
            Category::$getLocale = true;
            $categories = $this->categories($request, false);
            return view("dashboard/$this->dashboardTemplate/games/edit", compact('game', 'categories', 'admin', 'browsers','locale'));
        }

        abort(404);
    }

    public function update(Request $request)
    {
        $validation = $this->validation($request, true);
        Validator::make($request->all(), $validation)->validate();

        if ($this->hasUpdatePermission($request))
            $game = Game::withoutGlobalScope(PublishedScope::class)->where('id', $request->id)->first();
        else
            $game = Game::withoutGlobalScope(PublishedScope::class)->where('creator_id', $this->admin()->id)->where('id', $request->id)->first();


        if ($game)
        {
            $game->title = [
                'en' =>$request->title_en,
                'ar' =>$request->title_ur,
            ];
            $game->link = $request->link;
            $game->description = [
                'en'=>$request->description_en,
                'ar'=>$request->description_ur
            ];
            $game->supported_browsers = (count($request->browsers)) ? array_values($request->browsers) : [];

            if (parent::publishStatus($request, true))
            {
                if ($request->published)
                    $game->published = Game::PUBLISHED;
                else
                    $game->published = Game::NOT_PUBLISHED;
            }

            if (parent::editorChoiceStatus($request, true))
            {
                if ($request->editor_choice)
                    $game->editor_choice = Game::EDITOR_CHOICE_ON;
                else
                    $game->editor_choice = Game::EDITOR_CHOICE_OFF;
            }

            $this->toSearchableArray($request);

            if ($game->save())
            {
                $old_categories = $game->categories;

                $categories = $this->intCategories($request->category_id);
                $game->syncCategories($categories);
                $this->categoryStats($categories, $old_categories, false);
                $this->attachTags($request, $game, false);

                if ($request->image)
                    $game->syncMedia($request->image, Mediable::THUMBNAIL_FIELD);

                if (parent::publishStatus($request) and $request->push)
                    $this->handlePush($game);


                session()->flash('success_flash', trans('messages.item_updated'));
            }
            else
                session()->flash('error_flash', trans('messages.unknown_error'));

            return redirect()->route("dashboard.games.index");
        }
        else
            abort(404);
    }

    public function destroy(Request $request)
    {
        $this->validate($request, ['id' => 'required']);

        $categories = $this->categories($request)->keys();
        $post = Game::withoutGlobalScope(PublishedScope::class)->where('id', $request->id)->withCategories($categories)->first();

        if ($post)
        {
            $categories = $post->categories;

            if ($post->delete())
            {
                $this->categoryStats($categories, [], true);
                session()->flash('success_flash', trans('messages.item_deleted'));
            }
            else
                session()->flash('error_flash', trans('messages.unknown_error'));
        }
        else
            session()->flash('error_flash', trans('messages.record_not_found'));

        return redirect()->back();
    }

    public function publish($id)
    {
        $game = Game::withoutGlobalScope(PublishedScope::class)->where('id', $id)->first();
        if ($game)
        {
            $game->published = Game::PUBLISHED;
            if ($game->save())
                session()->flash('success_flash', trans('messages.item_updated'));
            else
                session()->flash('error_flash', trans('messages.unknown_error'));
        }
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->back();
    }

    public function unPublish($id)
    {
        $game = Game::withoutGlobalScope(PublishedScope::class)->where('id', $id)->first();
        if ($game)
        {
            $game->published = Game::NOT_PUBLISHED;
            if ($game->save())
            {
                Bookmark::where('bookmarkable_id', $game->id)->where('bookmarkable_type', Game::class)->delete();
                session()->flash('success_flash', trans('messages.item_updated'));
            }
            else
                session()->flash('error_flash', trans('messages.unknown_error'));
        }
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->back();
    }

    public function push($id)
    {
        $game = Game::where('id', $id)->with('mediaAll')->first();
        if ($game and $game->published == Game::PUBLISHED)
        {
            $this->handlePush($game);
            session()->flash('success_flash', trans('messages.push_sent'));
        }
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->back();
    }


    /******* protected functions ***************************************************************************************/


    protected function categories(Request $request, $placeholder = true)
    {
        if ($request->session()->has('acl'))
        {
            $acl = $request->session()->get('acl');
            $toggle = $acl['dynamic']['toggle'];

            if (isset($toggle[$this->toggleType]) and !empty($toggle[$this->toggleType]))
            {
                Category::$getLocale = true;
                if ($placeholder)
                    return Category::cache()->where('type', Category::TYPE_GAME)->whereIn('id', $toggle[$this->toggleType])->pluck('name', 'id')->prepend(trans('ui.text.category') . '*', 0);

                return Category::cache()->where('type', Category::TYPE_GAME)->whereIn('id', $toggle[$this->toggleType])->pluck('name', 'id');
            }
        }

        if ($placeholder)
            return [null => trans('ui.text.category')];
       return [];
    }

    protected function validation(Request $request, $edit = false)
    {
        // get toggle acl
        $toggle = [];
        if ($request->session()->has('acl'))
        {
            $acl = $request->session()->get('acl');
            $toggle = $acl['dynamic']['toggle'];

            if (isset($toggle[$this->toggleType]) and !empty($toggle[$this->toggleType]))
                $toggle = $toggle[$this->toggleType];
        }


        // generate validation rules
        $validation = [
            'title_en' => 'required',
            'title_ur' => 'required',
            'image' => ($edit) ? 'media:image' : 'required|media:image',
            'category_id' => 'required|array',
            'category_id.*' => [Rule::in($toggle)],
            'browsers' => 'array',
            'link' => 'required|url',
        ];

        if ($edit)
            $validation['id'] = 'required';
        
        return $validation;
    }

    protected function toSearchableArray(Request $request)
    {
        if ($request->tags)
        {
            $tags = str_replace(',', ', ', $request->tags);
            Game::$tags = ['tags' => $tags];
        }

        if ($request->category_id)
        {
            $category = Category::cache()->where('id', $request->category_id)->first();
            if ($category)
            {
                Game::$categories = [
                    'category_name' => $category->name,
                    'category_description' => $category->description,
                    'category_slug' => $category->slug,
                ];
            }
        }
    }

    protected function attachTags(Request $request, Game $game, $insert = true)
    {
        if ($request->tags and !empty($tags = explode(',', $request->tags)))
        {
            if ($insert)
                $game->tag($tags);
            else
                $game->retag($tags);
        }
    }

    protected function categoryStats($categories, $old_categories = [], $delete = false)
    {
        if ($old_categories instanceof Collection and $old_categories->count())
        {
            $old_categories = $old_categories->pluck('id')->sort()->toArray();

            sort($categories);
            sort($old_categories);

            if ($categories == $old_categories)
                return;
        }

        if (!$delete)
        {
            foreach ($old_categories as $old_category)
                $this->decrement('games', $old_category);
        }

        foreach($categories as $category)
        {
            if ($delete)
                $this->decrement('games', (int)$category->id);
            else
                $this->increment('games', $category);
        }
    }

    protected function decrement($col, $category)
    {
        $category = Category::where('id', $category)->with('ancestors')->first();

        $ancestors = [];
        if ($category->ancestors->count())
            $ancestors = $category->ancestors->pluck('id')->toArray();
        $ancestors[] = $category->id;
        $ancestors = array_unique($ancestors);

        Category::whereIn('id', $ancestors)->update([$col => DB::raw("$col - 1")]);
    }

    protected function increment($col, $category)
    {
        $category = Category::where('id', $category)->with('ancestors')->first();
        $ancestors = $category->ancestors->pluck('id')->toArray();
        $ancestors[] = $category->id;
        $ancestors = array_unique($ancestors);

        Category::whereIn('id', $ancestors)->update([$col => DB::raw("$col + 1")]);
    }

    protected function intCategories($categories)
    {
        foreach ($categories as $key => $category)
            $categories[$key] = (int)$category;

       return $categories;
    }

    protected function admin()
    {
        if (!$this->admin)
            $this->admin = Auth::guard('admin')->user();
       return $this->admin;
    }

    protected function handlePush(Game $game)
    {
        try {
            if (isset($game->media['thumbnail']) and $game->media['thumbnail'])
                $image = $game->media['thumbnail']->file->url();
            else
                $image = null;
        }
        catch (Exception $e) {
            $image = null;
        }

        $message = [
            'title' => str_limit($game->title, 250),
            'body' => str_limit($game->short_description, 55),
            'type' => Notification::GAME_TYPE,
            'url' => url("posts/$game->id"),
            'image' => $image,
            'dynamic_data' => [
                'id' => $game->id,
                'url' => url("posts/$game->id")
            ]
        ];

        $push = new Push($message, Notification::ALL_DEVICE_TYPE);
        $push->post(true);
    }
}
