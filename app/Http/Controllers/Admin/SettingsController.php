<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Support\Facades\Validator;

class SettingsController extends Controller
{
    public function index()
    {
        $settings = Setting::first();

        return view("dashboard/$this->dashboardTemplate/settings/index", compact('settings'));
    }

    public function update(Request $request)
    {
        Validator::make($request->all(), ['site_description' => 'required|min:150|max:200', 'site_keywords' => 'required|max:250', 'favicon' => 'image', 'image' => 'image'])->validate();

        $settings = Setting::first();
        $settings->site_description = $request->site_description;
        $settings->site_keywords = $request->site_keywords;

        if ($request->favicon)
            $settings->favicon = $request->favicon;

        if ($request->image)
            $settings->image = $request->image;

        if ($settings->save())
            session()->flash('success_flash', trans('messages.item_created'));
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->back();
    }
}
