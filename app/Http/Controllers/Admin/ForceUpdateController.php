<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ForceUpdate;

class ForceUpdateController extends Controller
{
    public function index()
    {
        $forceUpdate = ForceUpdate::get();
        return view("dashboard/$this->dashboardTemplate/force-update/index", compact('forceUpdate'));
    }

    public function update(Request $request)
    {
        $this->validate($request, ['id' => 'required|numeric', 'latest' => 'required', 'minimum' => 'required']);

        $force_update = ForceUpdate::findOrFail($request->id);
        $force_update->minimum = $request->minimum;
        $force_update->latest = $request->latest;

        if ($force_update->save())
            session()->flash('success_flash', trans('messages.item_updated'));
        else
            session()->flash('error_flash', trans('messages.unknown_error'));

        return redirect()->route('dashboard.force-update.index');
    }
}
