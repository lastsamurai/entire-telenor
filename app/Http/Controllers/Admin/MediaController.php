<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Media;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class MediaController extends Controller
{
    protected $paginationLimit = 24;

    public function index(Request $request)
    {
        if ($request->type)
            $media = Media::where('type', $request->type)->orderBy('id', 'desc')->paginate($this->paginationLimit);
        else
            $media = Media::orderBy('id', 'desc')->paginate($this->paginationLimit);

        if ($request->response == 'json')
        {
            $response_code = self::HTTP_OK;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.not_valid_input')
                ],
                'data' => $media
            ];

            return response()->json($response, $response_code);
        }
        else if ($request->response == 'html')
            return view("dashboard/$this->dashboardTemplate/media/content", compact('media'));

        return view("dashboard/$this->dashboardTemplate/media/index", compact('media'));
    }

    public function add()
    {
        return view("dashboard/$this->dashboardTemplate/media/add");
    }

    public function store(Request $request)
    {
        $validation = $this->validation();
        Validator::make($request->all(), $validation)->validate();

        try {
            $media = new Media;
            $media->file = $request->file;

            if ($media->save())
            {
                $response_code = self::HTTP_OK;
                $response = [
                    'id' => $media->id,
                    'url' => $media->file->url(),
                    'message' => trans('messages.success')
                ];
            }
            else
            {
                $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                $response = [
                    'id' => null,
                    'url' => null,
                    'message' => $media->errors()
                ];
            }
        }
        catch (Exception $e) {
            $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
            $response = [
                'id' => null,
                'url' => null,
                'message' => $e->getMessage()
            ];
        }

        return response()->json($response, $response_code);
    }

    public function edit($id)
    {
        $media = Media::findOrFail($id);
        return view("dashboard/$this->dashboardTemplate/media/edit", compact('media'));
    }

    public function update(Request $request)
    {
        $this->validate($request, $this->validation(true));

        try {
            $media = Media::findOrFail($request->id);
            $media->file = $request->file;

            if ($media->save())
                session()->flash('success_flash', trans('messages.item_updated'));
            else
                session()->flash('error_flash', trans('messages.unknown_error'));
        }
        catch (Exception $e) {
            session()->flash('error_flash', $e->getMessage());
        }


        return redirect()->back();
    }

    public function destroy(Request $request)
    {
        $this->validate($request, ['id' => 'required']);

        $media = Media::findOrFail($request->id);
        if ($media)
        {
            //dd($media->toArray());
            if ($media->delete())
                session()->flash('success_flash', trans('messages.item_deleted'));
            else
                session()->flash('error_flash', trans('messages.unknown_error'));
        }
        else
            session()->flash('error_flash', trans('messages.record_not_found'));

        return redirect()->back();
    }

    protected function validation($edit = false)
    {
        $mimeTypes = implode(',', Media::$allowedMimeTypes);

        if ($edit)
            $validation = ['id' => 'required', 'file' => "required|mimetypes:$mimeTypes"];
        else
            $validation = ['file' => "required|mimetypes:$mimeTypes"];

        return $validation;
    }
}
