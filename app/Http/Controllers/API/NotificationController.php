<?php

namespace App\Http\Controllers\API;

use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Token;
use App\UserToken;
use App\DeviceToken;

class NotificationController extends ApiController
{
    /**
     * @api {get} user/notification/list List Notifications
     * @apiName List Notifications
     * @apiGroup notifications
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + salt)
     *
     * @apiParam {string} type `1` : PUSH_TYPE <br> `2` : POST_TYPE
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": {
     *      "current_page": 1,
     *      "data": [
     *      {
     *          "id": 1,
     *          "title": "دستور پخت کباب بختیاری",
     *          "message": null,
     *          "url": "http://vp.site/posts/1",
     *          "media": {
     *              "thumbnail": {
     *                  "id": "30",
     *                  "width": 2560,
     *                  "height": 1600,
     *                  "duration": null,
     *                  "file": "http://vp.site/uploads/App/Media/files/000/000/030/original/Lines_and_Lines.png"
     *              }
     *          },
     *          "type": 2,
     *          "notificable_id": 1,
     *          "device_type": 3,
     *          "created_at": "2018-02-26 10:01:31",
     *          "updated_at": "2018-02-26 10:01:31"
     *      }],
     *      "first_page_url": "http://vp.site/api/user/notification/list?page=1",
     *      "from": 1,
     *      "last_page": 1,
     *      "last_page_url": "http://vp.site/api/user/notification/list?page=1",
     *      "next_page_url": null,
     *      "path": "http://vp.site/api/user/notification/list",
     *      "per_page": 10,
     *      "prev_page_url": null,
     *      "to": 1,
     *      "total": 1
     *  }
     * }
     *
     */
    public function index(Request $request)
    {
        if (!CheckHash([$request->header('userid'), $request->header('token')], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $notification = Notification::orderBy('id', 'desc')->paginate($this->paginationLimit);

            $response_code = self::HTTP_OK;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.success')
                ],
                'data' => $notification,
            ];
        }

        return response()->json($response, $response_code);
    }

    /**
     * @api {post} user/notification/send-token Send Token (Device Token)
     * @apiName Send Token (Device Token)
     * @apiGroup notifications
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + reg_id + salt)
     *
     * @apiParam {string} reg_id Register ID (Device Token)
     * @apiParam {integer} type `1` Android <br> `2` iOS
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": null
     * }
     *
     */
    public function sendToken(Request $request)
    {
        $valid_type = [DeviceToken::ANDROID, DeviceToken::IOS];
        $valid_type = implode(",", $valid_type);
        $validator = Validator::make($request->all(), ['reg_id' => 'required', 'type' => "required|in:$valid_type"]);

        if ($validator->fails())
        {
            $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
            $response = [
                'meta' => [
                    'error' => $validator->errors(),
                    'message' => trans('messages.not_valid_input')
                ],
                'data' => null
            ];
        }
        else if (!CheckHash([$request->header('userid'), $request->header('token'), $request->reg_id], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $user_token = UserToken::where('token', $request->header('token'))->first();

            $exist_reg_id = DeviceToken::where('reg_id', $request->reg_id)->first();
            if ($exist_reg_id)
            {
                $done = DeviceToken::where('reg_id', $request->reg_id)->update(['token_id' => $user_token->id]);
            }
            else
            {
                $device_token = DeviceToken::firstOrNew(['token_id' => $user_token->id]);
                $device_token->reg_id = $request->reg_id;
                $device_token->type = $request->type;
                $done = $device_token->save();
            }


            if ($done)
            {
                $response_code = self::HTTP_OK;
                $response = [
                    'meta' => [
                        'error' => null,
                        'message' => trans('messages.success')
                    ],
                    'data' => null
                ];
            }
            else
            {
                $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                $response = [
                    'meta' => [
                        'error' => $device_token->errors(),
                        'message' => trans('messages.unknown_error')
                    ],
                    'data' => null
                ];
            }
        }

        return response()->json($response, $response_code);
    }

    /**
     * @api {GET} notifications/push Push Notification (client)
     * @apiName Push Notification (client)
     * @apiGroup notifications
     *
     * @apiParam {string} type `1` : PUSH_TYPE <br> `2` : POST_TYPE
     *
     * @apiSuccessExample Type 2
     * HTTP/1.1 200 OK
     * {
     *  'title': 'title',
     *  'body': 'message',
     *  'type': 2,
     *  'url': 'http://google.com',
     *  'image': null,
     *  'dynamic_data': [
     *      'id': "124"
     *      'url': "http://google.com"
     *  ],
     *  'sound': 'default'
     * }
     *
     */
}