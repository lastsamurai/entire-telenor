<?php

namespace App\Http\Controllers\API;

use App\Article;
use App\Bookmark;
use App\Console\Commands\GuestCheck;
use App\Page;
use App\Post;
use App\Video;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use App\ForceUpdate;
use App\Setting;
use App\UserToken;
use DB;

class SettingController extends ApiController
{
    /**
     * @api {get} settings/force-update/{device}/{version} Force Update
     * @apiName Force Update
     * @apiGroup settings
     *
     *
     * @apiHeader {integer} deviceid Unique Device ID
     * @apiHeader {string} devicemodel  Device Model
     *
     * @apiParam (integer) device `1` Android <br> `2` iOS
     * @apiParam (string) version version of your app e.g. `1.2.2`
     *
     * @apiSuccess {integer} status `1` You’re updated <br> `2` Minor Update <br> `3` Force Update
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": {
     *      "status": 2,
     *      "message": "شما از حداقل نسخه مورد نیاز استفاده می کنید اما نسخه جدیدتر هم وجود دارد.",
     *      "url": "https://itunes.apple.com/us/app"
     *  }
     * }
     *
     */
    public function forceUpdate(Request $request, $device, $version)
    {
        $force_update = ForceUpdate::where('device', $device)->first();

        if ($device == ForceUpdate::ANDROID_DEVICE)
        {
            $url = config('settings.android_app_url');
            $type = 'android ';
        }
        else if ($device == ForceUpdate::IOS_DEVICE)
        {
            $url = config('settings.ios_app_url');
            $type = 'ios ';
        }

        $device_id = $request->header('deviceid');
        if(isset($device_id) && $device_id != null)
        {
            $device_model = $request->header('devicemodel');
            if(isset($device_model) && $device_model != null)
            {
                UserToken::where('device_id', $device_id)->update(['version' => $type . $version, 'device_model' => $device_model]);
            }
            else
            {
                UserToken::where('device_id', $device_id)->update(['version' => $type . $version]);
            }

        }


        if (version_compare($version, $force_update->latest, ">="))
        {
            $response = ['status' => 1, 'message' => trans('messages.you_are_updated'), 'url' => null];
        }
        else if ((version_compare($version, $force_update->latest, "<")) && (version_compare($version, $force_update->minimum, ">=")))
        {
            $response = ['status' => 2, 'message' => trans('messages.minor_update_msg'), 'url' => $url];
        }
        else
        {
            $response = ['status' => 3, 'message' => trans('messages.force_update_msg'), 'url' => $url];
        }

        $response_code = self::HTTP_OK;
        $response = [
            'meta' => [
                'error' => null,
                'message' => trans('messages.success')
            ],
            'data' => $response
        ];

        return response()->json($response, $response_code);
    }

    /**
     * @api {get} settings/general General Settings
     * @apiName General Settings
     * @apiGroup settings
     *
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + salt)
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": {
     *      "pages": [
     *      {
     *          "id": "1",
     *          "title": "حریم خصوصی",
     *          "slug": "حریم-خصوصی",
     *          "url": "http://vp.site/pages/%D8%AD%D8%B1%DB%8C%D9%85-%D8%AE%D8%B5%D9%88%D8%B5%DB%8C"
     *      },
     *      {
     *          "id": "2",
     *          "title": "درباره ما",
     *          "slug": "درباره-ما",
     *          "url": "http://vp.site/pages/%D8%AF%D8%B1%D8%A8%D8%A7%D8%B1%D9%87-%D9%85%D8%A7"
     *      },
     *      {
     *          "id": "4",
     *          "title": "قوانین و شرایط استفاده",
     *          "slug": "قوانین-و-شرایط-استفاده",
     *          "url": "http://vp.site/pages/%D9%82%D9%88%D8%A7%D9%86%DB%8C%D9%86-%D9%88-%D8%B4%D8%B1%D8%A7%DB%8C%D8%B7-%D8%A7%D8%B3%D8%AA%D9%81%D8%A7%D8%AF%D9%87"
     *      }],
     *      "support_mail": "support@vp.site",
     *      "download_app": "http://google.com"
     *  }
     * }
     *
     */
    public function general(Request $request)
    {
        if (!CheckHash([$request->header('userid'), $request->header('token')], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $userId = $request->header('userid');

            $checkGuest = new GuestCheck(false);
            $revokeTokens = $checkGuest->handle($userId);

            if ($revokeTokens)
            {
                $response_code = self::HTTP_UNAUTHORIZED;
                $response = [
                    'meta' => [
                        'error' => null,
                        'message' => trans('messages.invalid_token')
                    ],
                    'data' => null
                ];
            }
            else
            {
                $pages = Page::cache();
                $pages = $pages->map(function ($item) {
                    return [
                        'id' => $item->id,
                        'title' => $item->title,
                        'slug' => $item->slug,
                        'url' => url("pages/" . urlencode($item->slug))
                    ];
                });


                $data = [
                    'pages' => $pages,
                    'support_mail' => config('settings.support_mail'),
                    'download_app' => config('settings.download_app'), // TODO - Get it from database
                ];

                $response_code = self::HTTP_OK;
                $response = [
                    'meta' => [
                        'error' => null,
                        'message' => trans('messages.success')
                    ],
                    'data' => $data
                ];
            }
        }

        return response()->json($response, $response_code);
    }

    /**
     * @api {get} settings/general-light General Settings (Without Authentication)
     * @apiName General Settings (Without Authentication)
     * @apiGroup settings
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": {
     *      "pages": [
     *      {
     *          "id": "1",
     *          "title": "حریم خصوصی",
     *          "slug": "حریم-خصوصی",
     *          "url": "http://vp.site/pages/%D8%AD%D8%B1%DB%8C%D9%85-%D8%AE%D8%B5%D9%88%D8%B5%DB%8C"
     *      },
     *      {
     *          "id": "2",
     *          "title": "درباره ما",
     *          "slug": "درباره-ما",
     *          "url": "http://vp.site/pages/%D8%AF%D8%B1%D8%A8%D8%A7%D8%B1%D9%87-%D9%85%D8%A7"
     *      },
     *      {
     *          "id": "4",
     *          "title": "قوانین و شرایط استفاده",
     *          "slug": "قوانین-و-شرایط-استفاده",
     *          "url": "http://vp.site/pages/%D9%82%D9%88%D8%A7%D9%86%DB%8C%D9%86-%D9%88-%D8%B4%D8%B1%D8%A7%DB%8C%D8%B7-%D8%A7%D8%B3%D8%AA%D9%81%D8%A7%D8%AF%D9%87"
     *      }],
     *      "support_mail": "support@vp.site",
     *      "download_app": "http://google.com"
     *  }
     * }
     *
     */
    public function GeneralLight()
    {
        $pages = Page::cache();
        $pages = $pages->map(function ($item) {
            return [
                'id' => $item->id,
                'title' => $item->title,
                'slug' => $item->slug,
                'url' => url("pages/" . urlencode($item->slug))
            ];
        });


        $data = [
            'pages' => $pages,
            'support_mail' => config('settings.support_mail'),
            'download_app' => config('settings.download_app'), // TODO - Get it from database
        ];
        $response_code = self::HTTP_OK;
        $response = [
            'meta' => [
                'error' => null,
                'message' => trans('messages.success')
            ],
            'data' => $data
        ];

        return response()->json($response, $response_code);
    }
}
