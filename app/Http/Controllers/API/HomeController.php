<?php

namespace App\Http\Controllers\API;

use App\Post;
use Conner\Tagging\Model\Tag;
use Illuminate\Http\Request;

class HomeController extends ApiController
{
    protected $limit = 15;
    protected $tagsOffset = 4; // TODO - get from database
    protected $tagsLimit = 15; // TODO - get from database

    /**
     * @api {get} home/ Home
     * @apiName Home
     * @apiGroup home
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + salt)
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": {
     *      "trending": [POST_MODEL],
     *      "posts": [POST_MODEL_PAGINATION],
     *      "tags": {
     *          "offset": 4,
     *          "items": [
     *          {
     *              "id": 4,
     *              "tag_group_id": null,
     *              "slug": "صدا",
     *              "name": "صدا",
     *              "suggest": 0,
     *              "count": 4,
     *              "views": 0
     *          }]
     *      }
     *  }
     * }
     *
     */
    public function index(Request $request)
    {
        if (!CheckHash([$request->header('userid'), $request->header('token')], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $trendingPosts = Post::trendingAndPosts(1, $this->limit);

            $response_code = self::HTTP_OK;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.success')
                ],
                'data' => [
                    'trending' => $trendingPosts['trending'],
                    'posts' => $trendingPosts['posts'],
                    'tags' => $this->tags(),
                ],
            ];
        }

        return response()->json($response, $response_code);
    }

    protected function tags()
    {
        $countLimit = 20;
        $limit = 50;

        $tags = Tag::where('count', '>', $countLimit)->orderBy('count', 'desc')->limit($limit)->get();
        if ($tags->count())
        {
            $tags = $tags->shuffle()->take($this->tagsLimit)->toArray();
            return [
                'offset' => $this->tagsOffset,
                'items' => $tags
            ];
        }

        return null;
    }
}