<?php

namespace App\Http\Controllers\API;

use App\Bookmark;
use App\Category;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class BookmarkController extends ApiController
{
    public function __construct()
    {
        Post::$checkEvent = false;
        Category::$checkEvent = false;
    }

    /**
     * @api {post} bookmarks/add Add Bookmark
     * @apiName Add Bookmark
     * @apiGroup bookmark
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + bookmarkable_id + salt)
     *
     * @apiParam {integer} bookmarkable_id
     * @apiParam {integer} type: `1` POST <br> `2` CATEGORY
     *
     * @apiSuccess {integer} data number of bookmarks
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": 165
     * }
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), ['bookmarkable_id' => "required", 'type' => ['required', Rule::in([Bookmark::POST_TYPE, Bookmark::CATEGORY_TYPE])]]);
        if($validator->fails())
        {
            $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
            $response = [
                'meta' => [
                    'error' => $validator->errors(),
                    'message' => trans('messages.not_valid_input')
                ],
                'data' => null
            ];
        }
        else if (!CheckHash([$request->header('userid'), $request->header('token'), $request->bookmarkable_id], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            switch ($request->type)
            {
                case Bookmark::POST_TYPE:
                    $bookmarkable = Post::where('id', $request->bookmarkable_id)->first();
                    break;

                case Bookmark::CATEGORY_TYPE:
                    $bookmarkable = Category::where('id', $request->bookmarkable_id)->first();
                    break;

                default:
                    $bookmarkable = null;
                    break;
            }


            if ($bookmarkable)
            {
                $userId = $request->header('userid');
                $bookmark = Bookmark::withoutGlobalScopes()->firstOrNew(['user_id' => $userId, 'bookmarkable_id' => $bookmarkable->id, 'bookmarkable_type' => $bookmarkable->getMorphClass()]);

                if ($bookmark->exists && $bookmark->deleted_at == null)
                    $bookmarked = false;
                else
                    $bookmarked = true;

                $bookmark->deleted_at = null;

                if ($bookmark->save())
                {
                    if ($bookmarked)
                    {
                        $bookmarkable->bookmarks += 1;
                        $bookmarkable->save();
                    }

                    $response_code = self::HTTP_OK;
                    $response = [
                        'meta' => [
                            'error' => null,
                            'message' => trans('messages.success')
                        ],
                        'data' => $bookmarkable->bookmarks
                    ];
                }
                else
                {
                    $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                    $response = [
                        'meta' => [
                            'error' => null,
                            'message' => trans('messages.unknown_error')
                        ],
                        'data' => null
                    ];
                }
            }
            else
            {
                $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                $response = [
                    'meta' => [
                        'error' => null,
                        'message' => trans('messages.record_not_found')
                    ],
                    'data' => null
                ];
            }
        }

        return response()->json($response, $response_code);
    }

    /**
     * @api {post} bookmarks/destroy Remove Bookmark
     * @apiName Remove Bookmark
     * @apiGroup bookmark
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + bookmarkable_id + salt)
     *
     * @apiParam {integer} bookmarkable_id
     * @apiParam {integer} type: `1` POST <br> `2` CATEGORY
     *
     * @apiSuccess {integer} data number of bookmarks
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": 165
     * }
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), ['bookmarkable_id' => "required", 'type' => ['required', Rule::in([Bookmark::POST_TYPE, Bookmark::CATEGORY_TYPE])]]);
        if($validator->fails())
        {
            $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
            $response = [
                'meta' => [
                    'error' => $validator->errors(),
                    'message' => trans('messages.not_valid_input')
                ],
                'data' => null
            ];
        }
        else if (!CheckHash([$request->header('userid'), $request->header('token'), $request->bookmarkable_id], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $bookmarkable = null;

            switch ($request->type)
            {
                case Bookmark::POST_TYPE:
                    $bookmarkable = Post::where('id', $request->bookmarkable_id)->first();
                    break;

                case Bookmark::CATEGORY_TYPE:
                    $bookmarkable = Category::where('id', $request->bookmarkable_id)->first();
                    break;

                default:
                    $bookmarkable = null;
                    break;
            }


            if ($bookmarkable)
            {
                $bookmark = Bookmark::where('user_id', $request->header('userid'))->where('bookmarkable_id', $bookmarkable->id)->where('bookmarkable_type', $bookmarkable->getMorphClass())->first();

                if ($bookmark)
                {
                    if ($bookmark->delete())
                    {
                        $bookmarkable->bookmarks -= 1;
                        $bookmarkable->save();
                    }
                }

                $response_code = self::HTTP_OK;
                $response = [
                    'meta' => [
                        'error' => null,
                        'message' => trans('messages.success')
                    ],
                    'data' => $bookmarkable->bookmarks
                ];


            }
            else
            {
                $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                $response = [
                    'meta' => [
                        'error' => null,
                        'message' => trans('messages.record_not_found')
                    ],
                    'data' => null
                ];
            }
        }

        return response()->json($response, $response_code);
    }
}
