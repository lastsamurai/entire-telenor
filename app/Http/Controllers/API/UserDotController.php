<?php

namespace App\Http\Controllers\API;

use App\MobileTerminated;
use App\Scopes\ActiveScope;
use App\TigoMessage;
use App\User;
use App\UserPurchaseTigo;
use App\UserToken;
use Carbon\Carbon;
use Ghorbannezhad\Dot\Facades\Dot;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ServerException;


class UserDotController extends ApiController
{
    protected $config;
    protected $products;
    protected $pricePoints;

    public function __construct()
    {
        $config = config('settings.tigo');
        $this->config = json_decode(json_encode($config));

        $this->products = collect($this->config->products);
        $this->pricePoints = collect($this->config->price_points);
    }

    public function sendMessage($msisdn, $message = 'default message')
    {
        $client = new GuzzleClient();
        $responseMessage = null;
        $status = false;
        $phone = clearTigoMsisdn($msisdn,0);
     //   try {



            $body = [
                'msisdn' => $phone,
                'ServiceProviderId' => config('sudatel_settings.service_provider'),
                'ServiceId' => config('sudatel_settings.service_id')
            ];


            $response = $client->request('POST', 'http://79.175.163.219:9090/samsson-gateway/smsverification/', ['json' => $body]);
            $this->log('send message response',$response);
            if ($response->getStatusCode() == 200) {
                $this->log('send message body',$response->getBody());
                $api_response = json_decode($response->getBody());
                if ($api_response->status_code == 200) {
                    $status = true;
                    $responseMessage = $api_response;
                    $mobile_terminated = new MobileTerminated();
                    $mobile_terminated->msisdn = $phone;
                    $mobile_terminated->transaction_id = $api_response->data;
                    $mobile_terminated->type = MobileTerminated::OTP_TYPE;
                    $mobile_terminated->save();
                }
            }

            $body['status'] = 'normal';
            $this->log('send message', $body);
   //     }
       /* catch (ServerException $e) {
            $responseMessage = $e->getMessage();

            $body['status'] = $responseMessage;
            $this->log('send message', $body);
        }*/

        return [
            'status' => $status,
            'message' => $responseMessage,
        ];
    }

    public function optIn(Request $request)
    {
        $this->log('opt in', $request->all());

        $validator = $this->validation($request);
        if ($validator->fails()) {
            $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
            $response = [
                'meta' => [
                    'error' => $validator->errors(),
                    'message' => trans('messages.not_valid_input')
                ],
                'data' => null
            ];
        }
        else {
            // todo - confirm that user is subscribed from sdp server
            $confirmed = true;

            if ($confirmed) {
                $phone = clearTigoMsisdn($request->msisdn, 0);

                if ($phone) {
                    $user = User::withoutGlobalScope(ActiveScope::class)->firstOrNew(['phone' => $phone]);
                    $user->msisdn = $request->msisdn;
                    $user->ip = $request->ip();
                    $user->mobile_service_provider = User::TANZANIA_TIGO_TYPE;
                    $user->active = User::ACTIVE;

                    try {
                        $user->save();

                        $product = $this->products->where('id', $request->productId)->first();
                        $now = Carbon::now()->format('Y-m-d H:i:s');
                        $expiresAt = Carbon::now()->addDays($product->days)->format('Y-m-d H:i:s');

                        $purchase = UserPurchaseTigo::withTrashed()->firstOrNew(['user_id' => $user->id, 'product_id' => $request->productId]);
                        $purchase->price_point_id = $request->pricepointId;
                        $purchase->text = $request->text;
                        $purchase->transaction_uuid = $request->transactionUUID;
                        $purchase->purchased_at = $now;
                        $purchase->expires_at = $expiresAt;
                        $purchase->deleted_at = null;

                        if ($purchase->save()) {
                            // todo - how we want to handle multiple products
                            UserPurchaseTigo::where('user_id', $user->id)->where('id', '<>', $purchase->id)->delete();

                            $response_code = self::HTTP_OK;
                            $response = [
                                'meta' => [
                                    'error' => null,
                                    'message' => trans('messages.success')
                                ],
                                'data' => []
                            ];
                        }
                        else {
                            $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                            $response = [
                                'meta' => [
                                    'error' => null,
                                    'message' => trans('messages.unknown_error')
                                ],
                                'data' => []
                            ];
                        }
                    } catch (QueryException $exception) {
                        $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                        $response = [
                            'meta' => [
                                'error' => $exception->getMessage(),
                                'message' => trans('messages.unknown_error')
                            ],
                            'data' => []
                        ];
                    }
                }
                else {
                    $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                    $response = [
                        'meta' => [
                            'error' => null,
                            'message' => trans('messages.wrong_msisdn')
                        ],
                        'data' => []
                    ];
                }
            }
            else {
                $response_code = self::HTTP_UNAUTHORIZED;
                $response = [
                    'meta' => [
                        'error' => null,
                        'message' => trans('messages.purchase_error')
                    ],
                    'data' => []
                ];
            }
        }

        return response()->json($response, $response_code);
    }

    public function optOut(Request $request)
    {
        $this->log('opt out', $request->all());

        $validator = $this->validation($request);
        if ($validator->fails()) {
            $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
            $response = [
                'meta' => [
                    'error' => $validator->errors(),
                    'message' => trans('messages.not_valid_input')
                ],
                'data' => null
            ];
        }
        else {
            // todo - confirm that user is subscribed from sdp server
            $confirmed = true;

            if ($confirmed) {
                $phone = clearTigoMsisdn($request->msisdn, 0);

                if ($phone) {
                    $user = User::withoutGlobalScope(ActiveScope::class)->where('phone', $phone)->first();

                    if ($user) {
                        $user->active = User::NOT_ACTIVE;

                        try {
                            $user->save();

                            UserPurchaseTigo::where('user_id', $user->id)->where('product_id', $request->productId)->delete();
                            UserToken::where('user_id', $user->id)->update(['token' => null]);

                            $response_code = self::HTTP_OK;
                            $response = [
                                'meta' => [
                                    'error' => null,
                                    'message' => trans('messages.success')
                                ],
                                'data' => []
                            ];
                        } catch (QueryException $exception) {
                            $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                            $response = [
                                'meta' => [
                                    'error' => $exception->getMessage(),
                                    'message' => trans('messages.unknown_error')
                                ],
                                'data' => []
                            ];
                        }
                    }
                    else {
                        $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                        $response = [
                            'meta' => [
                                'error' => null,
                                'message' => trans('messages.user_not_found')
                            ],
                            'data' => []
                        ];
                    }
                }
                else {
                    $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                    $response = [
                        'meta' => [
                            'error' => null,
                            'message' => trans('messages.wrong_msisdn')
                        ],
                        'data' => []
                    ];
                }
            }
            else {
                $response_code = self::HTTP_UNAUTHORIZED;
                $response = [
                    'meta' => [
                        'error' => null,
                        'message' => trans('messages.purchase_error')
                    ],
                    'data' => []
                ];
            }
        }

        return response()->json($response, $response_code);
    }

    public function incomingMessage(Request $request)
    {
        $validator = $this->validation($request);

        if ($validator->fails()) {
            $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
            $response = [
                'meta' => [
                    'error' => $validator->errors(),
                    'message' => trans('messages.not_valid_input')
                ],
                'data' => null
            ];
        }
        else {
            $phone = clearTigoMsisdn($request->msisdn, 0);

            if ($phone) {
                $user = User::withoutGlobalScope(ActiveScope::class)->firstOrNew(['phone' => $phone]);

                if (!$user->exists) {
                    $user->msisdn = $request->msisdn;
                    $user->ip = $request->ip();
                    $user->mobile_service_provider = User::TANZANIA_TIGO_TYPE;
                }

                try {
                    $user->save();

                    $message = new TigoMessage;
                    $message->user_id = $user->id;
                    $message->product_id = $request->productId;
                    $message->transaction_uuid = $request->transactionUUID;
                    $message->price_point_id = $request->pricepointId;
                    $message->text = $request->text;

                    if ($message->save()) {
                        $response_code = self::HTTP_OK;
                        $response = [
                            'meta' => [
                                'error' => null,
                                'message' => trans('messages.success')
                            ],
                            'data' => []
                        ];
                    }
                    else {
                        $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                        $response = [
                            'meta' => [
                                'error' => null,
                                'message' => trans('messages.unknown_error')
                            ],
                            'data' => []
                        ];
                    }
                } catch (QueryException $exception) {
                    $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                    $response = [
                        'meta' => [
                            'error' => $exception->getMessage(),
                            'message' => trans('messages.unknown_error')
                        ],
                        'data' => []
                    ];
                }
            }
            else {
                $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                $response = [
                    'meta' => [
                        'error' => null,
                        'message' => trans('messages.wrong_msisdn')
                    ],
                    'data' => []
                ];
            }
        }

        return response()->json($response, $response_code);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    protected function validation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'productId' => ['required', Rule::in($this->products->pluck('id')->toArray())],
            'pricepointId' => ['required', Rule::in($this->pricePoints->pluck('id')->toArray())],
            'mcc' => ['required', Rule::in([$this->config->mcc])],
            'mnc' => ['required', Rule::in([$this->config->mnc])],
            'text' => 'required',
            'msisdn' => ['required', 'regex:/^(\+?255|0)([7]{1}[1]{1}[2-9]{1}|[6]{1}[57]{1}[2-9]{1})([0-9]{6})$/'],
            'largeAccount' => ['required', Rule::in([$this->config->large_account])],
            'transactionUUID' => 'required',
        ]);

        return $validator;
    }

    protected function log($action, Array $request)
    {
        $json = json_encode($request);
        info($action);
        info($json);
        info('_______________________________');
    }
    public function test(){
        $sendOtp = Dot::sendOtp('962788888888');
        dd($sendOtp);
    }
}