<?php

namespace App\Http\Controllers\API;

use App\Bookmark;
use App\Category;
use Illuminate\Http\Request;

class CategoryController extends ApiController
{
    /**
     * @api {get} categories/tree List Categories - Tree
     * @apiName List Categories - Tree
     * @apiGroup categories
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + salt)
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": [
     *  {
     *      "id": "1",
     *      "slug": "msafrt",
     *      "name": "مسافرت",
     *      "description": null,
     *      "type": 1,
     *      "hex": "#8c0e8c",
     *      "views": 5,
     *      "bookmarks": 0,
     *      "video": 8,
     *      "podcast": 2,
     *      "parent_id": null,
     *      "created_at": "2018-03-04 14:06:24",
     *      "updated_at": "2018-04-14 16:02:16",
     *      "media": {
     *          "thumbnail": {
     *              "id": "71",
     *              "width": 2880,
     *              "height": 1800,
     *              "duration": null,
     *              "file": "http://vp.site/uploads/App/Media/files/000/000/071/original/Big_dipper.png"
     *          }
     *      },
     *      "is_bookmarked": false,
     *      "children": []
     *  }]
     * }
     *
     */
    public function tree(Request $request)
    {
        if (!CheckHash([$request->header('userid'), $request->header('token')], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $tree = Category::get()->toTree();

            $response_code = self::HTTP_OK;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.success')
                ],
                'data' => $tree
            ];
        }

        return response()->json($response, $response_code);
    }

    /**
     * @api {get} categories/root List Categories - Root
     * @apiName List Categories - Root
     * @apiGroup categories
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + salt)
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": [
     *  {
     *      "id": "5",
     *      "slug": "ghtha",
     *      "name": "غذا",
     *      "description": null,
     *      "type": 1,
     *      "hex": "#ba231e",
     *      "views": 0,
     *      "bookmarks": 0,
     *      "video": 2,
     *      "podcast": 2,
     *      "parent_id": null,
     *      "created_at": "2018-04-08 11:52:29",
     *      "updated_at": "2018-04-14 12:38:46",
     *      "media": {
     *          "thumbnail": {
     *              "id": "18",
     *              "width": 2560,
     *              "height": 1600,
     *              "duration": null,
     *              "file": "http://vp.site/uploads/App/Media/files/000/000/018/original/Energy_sunset.png"
     *          }
     *      },
     *      "is_bookmarked": false
     *  }]
     * }
     *
     */
    public function root(Request $request)
    {
        if (!CheckHash([$request->header('userid'), $request->header('token')], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $tree = Category::orderBy('name', 'asc')->whereIsRoot()->get()->toFlatTree();

            $response_code = self::HTTP_OK;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.success')
                ],
                'data' => $tree
            ];
        }

        return response()->json($response, $response_code);
    }

    /**
     * @api {get} categories/bookmarked Bookmarked Categories
     * @apiName Bookmarked Categories
     * @apiGroup categories
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + salt)
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": CATEGORY_MODEL_PAGINATION
     * }
     *
     */
    public function bookmarked(Request $request)
    {
        if (!CheckHash([$request->header('userid'), $request->header('token')], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $userId = $request->header('userid');

            $bookmarkedCategories = Bookmark::where('user_id', $userId)->where('bookmarkable_type', Category::class)->get();
            if ($bookmarkedCategories->count())
                $bookmarkedCategories = $bookmarkedCategories->pluck('bookmarkable_id');

            $categories = Category::whereIn('id', $bookmarkedCategories->toArray())->orderBy('id', 'desc')->pagination($this->paginationLimit);

            $response_code = self::HTTP_OK;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.success')
                ],
                'data' => $categories
            ];
        }

        return response()->json($response, $response_code);
    }
}