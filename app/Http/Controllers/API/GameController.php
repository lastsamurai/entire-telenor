<?php

namespace App\Http\Controllers\API;

use App\Bookmark;
use App\Category;
use App\Post;
use App\PostView;
use Conner\Tagging\Model\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PostController extends ApiController
{
    /**
     * @api {get} posts/list List Posts
     * @apiName List Posts
     * @apiGroup posts
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + salt)
     *
     * @apiParam {array} type `optional` post types: `video` , `podcast`
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": POSTS_MODEL_PAGINATION
     * }
     *
     */
    public function index(Request $request)
    {
        if (!CheckHash([$request->header('userid'), $request->header('token')], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $types = Post::slugToType();
            $posts = Post::whereIn('type', $types)->relations()->orderBy('id', 'desc')->toAds('pagination', $this->paginationLimit);

            $response_code = self::HTTP_OK;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.success')
                ],
                'data' => $posts
            ];
        }
        return response()->json($response, $response_code);
    }

    /**
     * @api {get} posts/tag/{name} Post by Tag Name
     * @apiName Post by Tag Name
     * @apiGroup posts
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + salt)
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": POSTS_MODEL_PAGINATION
     * }
     *
     */
    public function byTag(Request $request, $name)
    {
        if (!CheckHash([$request->header('userid'), $request->header('token')], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $data = Post::withAnyTag([$name])->relations()->pagination($this->paginationLimit);

            $response_code = self::HTTP_OK;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.success')
                ],
                'data' => $data
            ];
        }
        return response()->json($response, $response_code);
    }

    /**
     * @api {get} posts/category/{id} Post by Category
     * @apiName Post by Category
     * @apiGroup posts
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + salt)
     *
     * @apiParam {integer} id category id
     * @apiParam {array} type `optional` post types: `video` , `podcast`
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": {
     *      "category": CATEGORY_MODEL,
     *      "posts": POST_MODEL_PAGINATION,
     *      "tags": TAGS_MODEL,
     *  }
     * }
     *
     */
    public function byCategory(Request $request, $id)
    {
        if (!CheckHash([$request->header('userid'), $request->header('token')], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $types = Post::slugToType();

            $category = Category::where('id', $id)->first();

            if ($category)
            {
                $categoryIds = Category::integerID([$category->id]);

                $posts = Post::whereIn('type', $types)->withAnyCategories($categoryIds)->relations()->orderBy('id', 'desc')->pagination($this->paginationLimit);

                if (!$request->query('page') or ($request->query('page') and $request->query('page') == 1))
                {
                    $allPostIds = DB::table('categorizables')->where('category_id', $category->id)->where('categorizable_type', Post::class)->get()->pluck('categorizable_id');

                    $tags = Post::whereIn('type', $types)->whereIn('id', $allPostIds)->with('tagged')->get()->pluck('tags');
                    $tags = $tags->flatten()->unique()->values()->shuffle()->take(7);
                }
                else
                    $tags = [];

                $response_code = self::HTTP_OK;
                $response = [
                    'meta' => [
                        'error' => null,
                        'message' => trans('messages.success')
                    ],
                    'data' => [
                        'category' => $category,
                        'posts' => $posts,
                        'tags' => $tags,
                    ]
                ];
            }
            else
            {
                $response_code = self::HTTP_NOT_FOUND;
                $response = [
                    'meta' => [
                        'error' => null,
                        'message' => trans('messages.not_found')
                    ],
                    'data' => null
                ];
            }


        }
        return response()->json($response, $response_code);
    }

    /**
     * @api {get} posts/details/{id} Post Details
     * @apiName Post Details
     * @apiGroup posts
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + salt)
     *
     * @apiSuccess {integer} type `VIDEO` 1 <br> `PODCAST` 2 <br> `ADS` 10
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": {
     *      "details": {
     *          "id": "4",
     *          "title": "چگونه به راحتی قوطی کنسرو را باز کنیم؟!",
     *          "description": "توضیحات",
     *          "type": 2,
     *          "editor_choice": false,
     *          "likes": 0,
     *          "bookmarks": 0,
     *          "views": 0,
     *          "created_at": "2018-04-08 16:12:44",
     *          "updated_at": "2018-04-08 16:12:58",
     *          "media": {
     *              "thumbnail": {
     *                  "id": "37",
     *                  "width": 2560,
     *                  "height": 1600,
     *                  "duration": null,
     *                  "file": "http://vp.site/uploads/App/Media/files/000/000/037/original/cassette_2.png"
     *              },
     *              "audio": {
     *                  "id": "65",
     *                  "width": null,
     *                  "height": null,
     *                  "duration": 67,
     *                  "file": "http://vp.site/uploads/App/Media/files/000/000/065/original/sample.mp3"
     *              }
     *          },
     *          "is_bookmarked": false,
     *          "is_viewed": false,
     *          "tags": [
     *          {
     *              "id": 10,
     *              "tag_group_id": null,
     *              "slug": "تست-صدا",
     *              "name": "تست صدا",
     *              "suggest": 0,
     *              "count": 1,
     *              "views": 0
     *          }],
     *          "categories": [
     *          {
     *              "id": "2",
     *              "slug": "fotbal",
     *              "name": "فوتبال",
     *              "description": null,
     *              "type": 1,
     *              "hex": "#074e7a",
     *              "views": 2,
     *              "video": 7,
     *              "podcast": 1,
     *              "is_bookmarked": false,
     *              "parent_id": null,
     *              "created_at": "2018-03-04 22:08:37",
     *              "updated_at": "2018-04-09 12:15:45",
     *              "media": {
     *                  "thumbnail": {
     *                      "id": "62",
     *                      "width": 2560,
     *                      "height": 1600,
     *                      "duration": null,
     *                      "file": "http://vp.site/uploads/App/Media/files/000/000/062/original/cassette_2.png"
     *                  }
     *              },
     *              "pivot": {
     *                  "categorizable_id": 4,
     *                  "category_id": 2,
     *                  "created_at": "2018-04-08 16:12:44",
     *                  "updated_at": "2018-04-08 16:12:44"
     *              }
     *          }]
     *      },
     *      "related": [POST_MODEL]
     *  }
     * }
     *
     */
    public function details(Request $request, $id)
    {
        if (!CheckHash([$request->header('userid'), $request->header('token')], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $post = Post::where('id', $id)->relations()->first();
            if ($post)
            {
                $response_code = self::HTTP_OK;
                $message = trans('messages.success');

                $related = $this->relatedPosts($post->id, $post);
                $related = $related['response']['data'];
            }
            else
            {
                $response_code = self::HTTP_NOT_FOUND;
                $message = trans('messages.not_found');

                $post = null;
                $related = [];
            }


            $response = [
                'meta' => [
                    'error' => null,
                    'message' => $message
                ],
                'data' => [
                    'details' => $post,
                    'related' => $related,
                ]
            ];
        }
        return response()->json($response, $response_code);
    }

    /**
     * @api {get} posts/bookmarked Bookmarked Posts
     * @apiName Bookmarked Posts
     * @apiGroup posts
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + salt)
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": {
     *      "current_page": 1,
     *      "data": POSTS_MODEL,
     *      "first_page_url": "posts/bookmarked?page=1",
     *      "from": 1,
     *      "last_page": 1,
     *      "last_page_url": "posts/bookmarked?page=1",
     *      "next_page_url": null,
     *      "path": "posts/bookmarked",
     *      "per_page": 10,
     *      "prev_page_url": null,
     *      "to": 1,
     *      "total": 1
     *  }
     * }
     *
     */
    public function bookmarked(Request $request)
    {
        if (!CheckHash([$request->header('userid'), $request->header('token')], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $userId = $request->header('userid');

            $bookmarkedPosts = Bookmark::where('user_id', $userId)->where('bookmarkable_type', Post::class)->get();
            if ($bookmarkedPosts->count())
                $bookmarkedPosts = $bookmarkedPosts->pluck('bookmarkable_id');

            $posts = Post::whereIn('id', $bookmarkedPosts->toArray())->relations()->orderBy('id', 'desc')->pagination($this->paginationLimit);

            $response_code = self::HTTP_OK;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.success')
                ],
                'data' => $posts
            ];
        }

        return response()->json($response, $response_code);
    }

    /**
     * @api {post} posts/increment Increment Post Views
     * @apiName Increment Post Views
     * @apiGroup posts
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + post_id + salt)
     *
     * @apiParam {string} post_id
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": POST_MODEL
     * }
     *
     */
    public function increment(Request $request)
    {
        $validator = Validator::make($request->all(), ['post_id' => 'required']);
        if ($validator->fails())
        {
            $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
            $response = [
                'meta' => [
                    'error' => $validator->errors(),
                    'message' => trans('messages.not_valid_input')
                ],
                'data' => null
            ];
        }
        else if (!CheckHash([$request->header('userid'), $request->header('token'), $request->post_id], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $post = Post::where('id', $request->post_id)->relations()->first();
            if ($post)
            {
                $userId = $request->header('userid');
                $increment = true;

                $userView = PostView::firstOrNew(['user_id' => $userId, 'post_id' => $post->id]);
                if ($userView->exists)
                    $increment = false;

                if ($userView->save())
                {
                    if ($increment)
                    {
                        $post->increment('views');

                        $categories = $post->categories->pluck('id');
                        if ($categories->count())
                            Category::whereIn('id', $categories->toArray())->update(['views' => DB::raw("views + 1")]);

                        $tags = $post->tags->pluck('id');
                        if ($tags->count())
                            Tag::whereIn('id', $tags->toArray())->update(['views' => DB::raw("views + 1")]);
                    }


                    $response_code = self::HTTP_OK;
                    $response = [
                        'meta' => [
                            'error' => null,
                            'message' => trans('messages.success'),
                        ],
                        'data' => $post
                    ];
                }
                else
                {
                    $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                    $response = [
                        'meta' => [
                            'error' => null,
                            'message' => trans("messages.unknown_error"),
                        ],
                        'data' => null
                    ];
                }
            }
            else
            {
                $response_code = self::HTTP_NOT_FOUND;
                $response = [
                    'meta' => [
                        'error' => null,
                        'message' => trans("messages.not_found"),
                    ],
                    'data' => null
                ];
            }
        }

        return response()->json($response, $response_code);
    }

    /**
     * @api {post} posts/related/{id} Related Posts
     * @apiName Related Posts
     * @apiGroup posts
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + salt)
     *
     * @apiParam {string} id `url segment` post id
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": POST_MODEL
     * }
     *
     */
    public function related(Request $request, $id)
    {
        if (!CheckHash([$request->header('userid'), $request->header('token')], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $result = $this->relatedPosts($id);

            $response_code = $result['response_code'];
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => $result['response']['meta']['message'],
                ],
                'data' => $result['response']['data']
            ];
        }

        return response()->json($response, $response_code);
    }

    protected function relatedPosts($id, Post $post = null)
    {
        if (!$post)
            $post = Post::where('id', $id)->relations()->first();

        if ($post)
        {
            $response_code = self::HTTP_OK;
            $message = trans("messages.success");
            $posts = [];

            if ($post->categories->count())
            {
                $categories = $post->categories->pluck('id')->toArray();
                $categories = Category::integerID($categories);

                $posts = Post::withAnyCategories($categories)->where('id', '<>', $id)->relations()->inRandomOrder()->limit(4)->get();
            }
        }
        else
        {
            $response_code = self::HTTP_NOT_FOUND;
            $message = trans("messages.not_found");
            $posts = [];
        }


        $response = [
            'meta' => [
                'error' => null,
                'message' => $message,
            ],
            'data' => $posts
        ];

        return compact('response_code', 'response');
    }
}