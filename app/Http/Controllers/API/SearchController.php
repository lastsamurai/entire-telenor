<?php

namespace App\Http\Controllers\API;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends ApiController
{
    /**
     * @api {get} search/posts?type[]=podcast&query=کنسرو Search Posts
     * @apiName Search Posts
     * @apiGroup search
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + salt)
     *
     * @apiParam {string} query `query param` search keyword
     * @apiParam {array} type `optional` post types: `video` , `podcast`
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": POST_MODEL_PAGINATION
     * }
     *
     */
    public function posts(Request $request)
    {
        if (!CheckHash([$request->header('userid'), $request->header('token')], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $posts = $this->searchPost($request);

            $response_code = self::HTTP_OK;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.success')
                ],
                'data' => $posts
            ];
        }

        return response()->json($response, $response_code);
    }

    protected function searchPost(Request $request)
    {
        $types = Post::slugToType();

        $search = Post::search($request->query('query'))->get()->pluck('id');
        $order = implode(',', $search->toArray());

        $posts = Post::whereIn('id', $search)->whereIn('type', $types)->relations()->orderByRaw(DB::raw("FIELD(id, $order)"))->pagination(1, 'search/posts');


        $tags = Post::whereIn('type', $types)->whereIn('id', $search)->with('tagged')->get()->pluck('tags');
        $tags = $tags->flatten()->unique()->values()->shuffle()->take(7);

        return compact('posts','tags');
    }
}