<?php

namespace App\Http\Controllers\API;

use App\Like;
use App\Category;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class LikeController extends ApiController
{
    public function __construct()
    {
        Post::$checkEvent = false;
        Category::$checkEvent = false;
    }

    /**
     * @api {post} likes/add Add Like
     * @apiName Add Like
     * @apiGroup like
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + likable_id + salt)
     *
     * @apiParam {integer} likable_id
     * @apiParam {integer} type: `1` POST <br> `2` CATEGORY
     *
     * @apiSuccess {integer} data number of likes
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": 165
     * }
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), ['likable_id' => "required", 'type' => ['required', Rule::in([Like::POST_TYPE])]]);
        if($validator->fails())
        {
            $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
            $response = [
                'meta' => [
                    'error' => $validator->errors(),
                    'message' => trans('messages.not_valid_input')
                ],
                'data' => null
            ];
        }
        else if (!CheckHash([$request->header('userid'), $request->header('token'), $request->likable_id], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            switch ($request->type)
            {
                case Like::POST_TYPE:
                    $likable = Post::where('id', $request->likable_id)->first();
                    break;

                default:
                    $likable = null;
                    break;
            }


            if ($likable)
            {
                $userId = $request->header('userid');
                $like = Like::withoutGlobalScopes()->firstOrNew(['user_id' => $userId, 'likable_id' => $likable->id, 'likable_type' => $likable->getMorphClass()]);

                if ($like->exists && $like->deleted_at == null)
                    $liked = false;
                else
                    $liked = true;

                $like->deleted_at = null;

                if ($like->save())
                {
                    if ($liked)
                    {
                        $likable->likes += 1;
                        $likable->save();
                    }

                    $response_code = self::HTTP_OK;
                    $response = [
                        'meta' => [
                            'error' => null,
                            'message' => trans('messages.success')
                        ],
                        'data' => $likable->likes
                    ];
                }
                else
                {
                    $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                    $response = [
                        'meta' => [
                            'error' => null,
                            'message' => trans('messages.unknown_error')
                        ],
                        'data' => null
                    ];
                }
            }
            else
            {
                $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                $response = [
                    'meta' => [
                        'error' => null,
                        'message' => trans('messages.record_not_found')
                    ],
                    'data' => null
                ];
            }
        }

        return response()->json($response, $response_code);
    }

    /**
     * @api {post} likes/destroy Remove Like
     * @apiName Remove Like
     * @apiGroup like
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + likable_id + salt)
     *
     * @apiParam {integer} likable_id
     * @apiParam {integer} type: `1` POST <br> `2` CATEGORY
     *
     * @apiSuccess {integer} data number of likes
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": 165
     * }
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), ['likable_id' => "required", 'type' => ['required', Rule::in([Like::POST_TYPE])]]);
        if($validator->fails())
        {
            $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
            $response = [
                'meta' => [
                    'error' => $validator->errors(),
                    'message' => trans('messages.not_valid_input')
                ],
                'data' => null
            ];
        }
        else if (!CheckHash([$request->header('userid'), $request->header('token'), $request->likable_id], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $likable = null;

            switch ($request->type)
            {
                case Like::POST_TYPE:
                    $likable = Post::where('id', $request->likable_id)->first();
                    break;

                case Like::CATEGORY_TYPE:
                    $likable = Category::where('id', $request->likable_id)->first();
                    break;

                default:
                    $likable = null;
                    break;
            }


            if ($likable)
            {
                $like = Like::where('user_id', $request->header('userid'))->where('likable_id', $likable->id)->where('likable_type', $likable->getMorphClass())->first();

                if ($like)
                {
                    if ($like->delete())
                    {
                        $likable->likes -= 1;
                        $likable->save();
                    }
                }

                $response_code = self::HTTP_OK;
                $response = [
                    'meta' => [
                        'error' => null,
                        'message' => trans('messages.success')
                    ],
                    'data' => $likable->likes
                ];


            }
            else
            {
                $response_code = self::HTTP_UNPROCESSABLE_ENTITY;
                $response = [
                    'meta' => [
                        'error' => null,
                        'message' => trans('messages.record_not_found')
                    ],
                    'data' => null
                ];
            }
        }

        return response()->json($response, $response_code);
    }
}
