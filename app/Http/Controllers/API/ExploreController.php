<?php

namespace App\Http\Controllers\API;

use App\Bookmark;
use App\Category;
use App\Post;
use Conner\Tagging\Model\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ExploreController extends ApiController
{
    /**
     * @api {post} explore Explore
     * @apiName Explore
     * @apiGroup explore
     *
     * @apiHeader {integer} userid must have 11 digits
     * @apiHeader {string} token
     * @apiHeader {string} hash md5(userid + token + salt)
     *
     * @apiSuccess {integer} data number of bookmarks
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *  "meta": {
     *      "error": null,
     *      "message": "موفق"
     *  },
     *  "data": {
     *      'categories': CATEGORY_MODEL,
     *      'trending': POST_MODEL,
     *      'tags': TAG_MODEL,
     *      'posts_by_tag': POSTS_MODEL_PAGINATION,
     *  }
     * }
     */
    public function index(Request $request)
    {
        if (!CheckHash([$request->header('userid'), $request->header('token')], $request->header('hash')))
        {
            $response_code = self::HTTP_UNAUTHORIZED;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.invalid_hash')
                ],
                'data' => null
            ];
        }
        else
        {
            $categoryLimit = config('settings.category');
            $categoryIds = Category::cache()->shuffle()->take($categoryLimit)->pluck('id')->toArray();
            if (count($categoryIds))
            {
                $categoryOrder = implode(',', $categoryIds);
                $categories = Category::whereIn('id', $categoryIds)->orderByRaw(DB::raw("FIELD(id, $categoryOrder)"))->limit($categoryLimit)->get();
            }
            else
                $categories = [];

            $trendingPosts = Post::trendingAndPosts();

            $tagsLimit = config('settings.most_viewed_tags');
            $tags = Tag::orderBy('views', 'desc')->inRandomOrder()->limit($tagsLimit)->get();
            if ($tags->count())
            {
                $tag = $tags->first();
                $postsByTag = Post::withAnyTag([$tag->name])->relations()->pagination($this->paginationLimit, 'posts/tag/' . urlencode($tag->name));
            }
            else
                $postsByTag = [];


            $response_code = self::HTTP_OK;
            $response = [
                'meta' => [
                    'error' => null,
                    'message' => trans('messages.success')
                ],
                'data' => [
                    'categories' => $categories,
                    'trending' => $trendingPosts['trending'],
                    'tags' => $tags,
                    'posts_by_tag' => $postsByTag,
                ]
            ];
        }

        return response()->json($response, $response_code);
    }
}
