<?php

namespace App\Http\Controllers;

use App\Page;

class PageController extends Controller
{
    public function index($slug)
    {
        $page = Page::where('slug', $slug)->first();
        if ($page)
            return view('frontend/pages/index', compact('page'));

        abort(404);
    }
}
