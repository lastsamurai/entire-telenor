<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DevController extends Controller
{
    public function headers(Request $request)
    {
        foreach ($request->headers->all() as $key => $value)
        {
            if (!in_array($key, ['cookie']))
            {
                $index = ucfirst($key);
                $item = implode(',', $value);
                echo "<code><b>$index</b> : $item</code>" . '<br>';
            }
        }

        return;
    }
}