<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use Auth;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, ['phone' => 'required|string|regex:/^(09{1})+([1-3]{1})+(\d{8})$/']);
    }

    public function SendCode(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
        {
            return response()->json(['status' => false, 'message' => trans('messages.unknown_error'), 'data' => [], 'errors' => $validator->errors()], 200);
        }
        else
        {
            $result = $this->send($request->all());

            if ($result['id'])
                return response()->json(['status' => true, 'message' => $result['message'], 'data' => $result['id'], 'errors' => []], 200);
            else
                return response()->json(['status' => false, 'message' => $result['message'], 'data' => [], 'errors' => []], 200);
        }
    }

    protected function send(array $data)
    {
        $now = Carbon::now();

        $code = random_int(1000, 9999);
        $expire = Carbon::now()->addMinutes(10);

        $user = User::where(['phone' => $data['phone']])->first();
        if ($user)
        {
            if ($user->forgot_expired_at)
                $user->forgot_expired_at = Carbon::createFromFormat('Y-m-d H:i:s', $user->forgot_expired_at);

            if ($user->forgot_expired_at == null || $now->gte($user->forgot_expired_at))
            {
                $user->forgot_code = $code;
                $user->forgot_expired_at = $expire;

                if ($user->save())
                {
                    // TODO - send sms
                    return ['id' => $user->id, 'message' => trans('messages.sms_sent')];
                }
                else
                {
                    $message = trans('messages.unknown_error');
                }
            }
            else
            {
                $message = trans('messages.wait_until_expire');
            }
        }
        else
        {
            $message = trans('messages.user_not_found');
        }

        return ['id' => null, 'message' => $message];
    }

    protected function guard()
    {
        return Auth::guard();
    }

    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|size:4',
            'phone' => 'required|string|regex:/^(09{1})+([1-3]{1})+(\d{8})$/',
            'password' => 'required|string|min:8'
        ]);

        if($validator->fails())
        {
            $response = ['status' => false, 'message' => trans('messages.validation_error'), 'data' => [], 'errors' => $validator->errors()];
        }
        else
        {
            $code = EnglishNumber($request->code);
            $user = User::where('phone', $request->phone)->where('forgot_code', $code)->where('forgot_expired_at', '>=', Carbon::now())->first();
            if ($user)
            {
                $user->password = bcrypt($request->password);
                $user->forgot_code = null;
                $user->forgot_expired_at = null;
                $user->remember_token = null;

                if ($user->save())
                {
                    $this->guard()->login($user, true);

                    $response = ['status' => true, 'message' => trans('messages.success_reset_pass'), 'data' => [], 'errors' => []];
                }
                else
                {
                    $response = ['status' => false, 'message' => trans('messages.unknown_error'), 'data' => [], 'errors' => null];
                }
            }
            else
            {
                $response = ['status' => false, 'message' => trans('messages.invalid_code'), 'data' => [], 'errors' => []];
            }
        }

        return response($response, 200);
    }
}
