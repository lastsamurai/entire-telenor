<?php

namespace App\Http\Controllers\Auth;

use App\Scopes\ActiveScope;
use App\User;
use App\Http\Controllers\Controller;
use App\UserTemp;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Auth;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'phone' => 'required|string|regex:/^(09{1})+([1-3]{1})+(\d{8})$/',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:8',
        ]);
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
        {
            return response()->json(['status' => false, 'message' => trans('messages.not_valid_input'), 'data' => [], 'errors' => $validator->errors()], 200);
        }
        else
        {
            event(new Registered($result = $this->create($request->all())));

            return $this->registered($request, $result);
        }
    }

    protected function create(array $data)
    {
        $now = Carbon::now();

        $code = random_int(1000, 9999);
        $expire = Carbon::now()->addMinutes(10);

        $user = User::withoutGlobalScope(ActiveScope::class)->firstOrNew(['phone' => $data['phone']]);

        if ($user->sms_expired_at)
            $user->sms_expired_at = Carbon::createFromFormat('Y-m-d H:i:s', $user->sms_expired_at);

        if ($user->sms_expired_at == null || $now->gte($user->sms_expired_at))
        {
            $user->sms_code = $code;
            $user->sms_expired_at = $expire;

            if ($user->save())
            {
                $temp = new UserTemp;
                $temp->phone = $data['phone'];
                $temp->email = $data['email'];
                $temp->password = bcrypt($data['password']);
                $temp->save();

                // TODO - send sms
                return ['id' => $temp->id, 'message' => trans('messages.sms_sent')];
            }
            else
            {
                $message = trans('messages.unknown_error');
            }
        }
        else
        {
            $message = trans('messages.wait_until_expire');
        }

        return ['id' => null, 'message' => $message];
    }

    protected function guard()
    {
        return Auth::guard();
    }


    protected function registered(Request $request, $result)
    {
        if ($result['id'])
            return response()->json(['status' => true, 'message' => $result['message'], 'data' => $result['id'], 'errors' => []], 200);
        else
            return response()->json(['status' => false, 'message' => $result['message'], 'data' => [], 'errors' => []], 200);
    }

    public function verify(Request $request)
    {
        $validator = Validator::make($request->all(), ['code' => 'required|size:4', 'phone' => 'required', 'id' => 'required']);

        if($validator->fails())
        {
            $response = ['status' => false, 'message' => trans('messages.not_valid_input'), 'data' => [], 'errors' => $validator->errors()];
        }
        else
        {
            $code = EnglishNumber($request->code);
            $user = User::withoutGlobalScope(ActiveScope::class)->where('phone', $request->phone)->where('sms_code', $code)->where('sms_expired_at', '>=', Carbon::now())->first();
            if ($user)
            {
                $temp = UserTemp::where('id', $request->id)->where('phone', $request->phone)->first();
                if ($temp)
                {
                    $user->phone = $temp->phone;
                    $user->email = $temp->email;
                    $user->password = $temp->password;
                    $user->active = User::ACTIVE;
                    $user->sms_code = null;
                    $user->sms_expired_at = null;

                    if ($user->save())
                    {
                        $this->guard()->login($user, true);

                        $temp->delete();
                        $response = ['status' => true, 'message' => trans('messages.success_verify'), 'data' => [], 'errors' => []];
                    }
                    else
                    {
                        $response = ['status' => false, 'message' => trans('messages.unknown_error'), 'data' => [], 'errors' => null];
                    }
                }
                else
                {
                    $response = ['status' => false, 'message' => trans('messages.invalid_code'), 'data' => [], 'errors' => []];
                }
            }
            else
            {
                $response = ['status' => false, 'message' => trans('messages.invalid_code'), 'data' => [], 'errors' => []];
            }
        }


        return response($response, 200);
    }
}
