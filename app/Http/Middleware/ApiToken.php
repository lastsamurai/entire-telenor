<?php

namespace App\Http\Middleware;

use Closure;
use Validator;
use App\UserToken;

class ApiToken
{
    public function handle($request, Closure $next)
    {
        $validator = Validator::make($request->header(), ['userid' => 'required', 'token' => 'required']);
        if ($validator->fails())
        {
            $response = [
                'meta' => [
                    'error' => $validator->errors(),
                    'message' => trans('messages.login_invalid')
                ],
                'data' => null
            ];

            return response()->json($response, 401);
        }
        else
        {
            $exist = UserToken::where(['user_id' => $request->header('userid'), 'token' => $request->header('token')])->first();
            if (!$exist)
            {
                $response = [
                    'meta' => [
                        'error' => null,
                        'message' => trans('messages.invalid_token')
                    ],
                    'data' => null
                ];

                return response()->json($response, 401);
            }
        }

        return $next($request);
    }
}
