<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class VerifyDotSignature
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //checks that all required parameters exist
        //verifies signature
        
        $x=$request->validate([
            'reason_code' => 'required',
            'reason_desc'=> 'required',
            'msisdn'=> 'required|exists:users,msisdn',
            'service_id'=> 'required',
            'op_id'=> 'required',
            'partner_txid'=> 'required',
            'dot_txid'=> 'required',
            //'lpTransId'=> 'required',
            'signature'=> 'required'
        ]);
        
        $str=env('DOT_USERNAME')."-".$request->get('reason_code')."-".$request->get('reason_desc')."-".$request->get('msisdn')."-".$request->get('service_id')."-".$request->get('op_id')."-".$request->get('partner_txid')."-".$request->get('dot_txid')."-".env('password');
        $hash=hash("sha256",$str);

        //return new Response($hash);
        
        if(!($hash===$request->get('signature')))
            return new Response("Invalid Signature",401);

        return $next($request);
    }
}
