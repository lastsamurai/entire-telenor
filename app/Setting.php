<?php

namespace App;

use App\Traits\Cacheable;
use Illuminate\Database\Eloquent\Model;
use App\Traits\AdminActivity;
use Illuminate\Support\Facades\Cache;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Setting extends Model implements StaplerableInterface
{
    use AdminActivity, Cacheable, EloquentTrait;

    protected $table = "settings";
    protected $casts = ['id' => 'string'];

    public static $checkEvent = true;
    public static $cache_events = ['create', 'update', 'delete'];

    const ADMIN_EVENTS = ['create' => false, 'update' => true, 'delete' => false];

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('favicon', ['preserve_files' => true]);
        $this->hasAttachedFile('image', ['preserve_files' => true]);

        parent::__construct($attributes);
    }

    public function getAttributes()
    {
        return parent::getAttributes();
    }

    public static function UpdateCache($event)
    {
        if (in_array($event, self::$cache_events))
        {
            $cache = Setting::all();
            Cache::forever('settings', $cache);

            return $cache;
        }
    }

    public static function cache()
    {
        $cache = Cache::get('settings');

        if ($cache and $cache->count())
            return $cache;

        return self::UpdateCache('update');
    }
}
