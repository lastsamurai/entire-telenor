<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AdminActivity;

class ForceUpdate extends Model
{
    use AdminActivity;

    protected $table = "force_update";
    protected $hidden = ['updater_id', 'updater_ip'];

    public static $checkEvent = true;

    const ADMIN_EVENTS = ['create' => false, 'update' => true, 'delete' => false];
    const ANDROID_DEVICE = 1;
    const IOS_DEVICE = 2;

    public static function TypeLabel($type)
    {
        switch ($type)
        {
            case self::MINIMUM_VERSION:
                return trans('messages.minimum_version');

            case self::LATEST_VERSION:
                return trans('messages.latest_version');
        }
    }
}
