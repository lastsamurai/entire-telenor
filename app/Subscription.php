<?php

namespace App;

use App\Traits\Cacheable;
use Illuminate\Database\Eloquent\Model;
use App\Traits\AdminActivity;
use Illuminate\Support\Facades\Cache;

class Subscription extends Model
{
    use AdminActivity, Cacheable;

    protected $table = 'subscriptions';
    protected $casts = ['id' => 'string'];
    protected $appends = ['text'];

    public static $checkEvent = true;
    public static $cache_events = ['create', 'update', 'delete'];

    const ADMIN_EVENTS = ['create' => false, 'update' => true, 'delete' => false];

    const DAILY_TYPE = 1;
    const WEEKLY_TYPE = 2;
    const MONTHLY_TYPE = 3;

    public function getUnitAttribute($value)
    {
        $units = collect(config('settings.units'));

        return ($unit = $units->where('id', $value)->first()) ? $unit['label'] : null;
    }

    public function getTextAttribute()
    {
        return trans('messages.subscription-text', ['price' => $this->price, 'type' => self::typeToString($this->type), 'unit' => $this->unit]);
    }

    public static function UpdateCache($event)
    {
        if (in_array($event, self::$cache_events))
        {
            $cache = Subscription::all();
            Cache::forever('subscriptions', $cache);

            return $cache;
        }
    }

    public static function cache()
    {
        $cache = Cache::get('subscriptions');

        if ($cache and $cache->count())
            return $cache;

        return self::UpdateCache('update');
    }

    public static function typeToString($type) : string
    {
        switch ($type)
        {
            case self::DAILY_TYPE:
                return trans('ui.text.day');

            case self::WEEKLY_TYPE:
                return trans('ui.text.week');

            case self::MONTHLY_TYPE:
                return trans('ui.text.month');
        }

        return '';
    }
}
