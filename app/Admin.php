<?php

namespace App;

use App\Traits\Pagination;
use Illuminate\Notifications\Notifiable;
use App\Notifications\AdminResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cache;
use Exception;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Admin extends Authenticatable implements StaplerableInterface
{
    use Notifiable, Pagination, EloquentTrait;

    protected $guard = "admins";
    protected $fillable = ['name', 'email', 'password'];
    protected $hidden = ['email', 'role_id', 'password', 'remember_token', 'image_file_name', 'image_file_size', 'image_content_type', 'image_updated_at'];
    protected $appends = ['categories', 'image'];


    public static $appendCategories = false;

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('image', [
            'styles' => [
                'medium' => '300x300',
                'thumb' => '100x100'
            ],
            'preserve_files' => true
        ]);

        parent::__construct($attributes);
    }

    public function getAttributes()
    {
        return parent::getAttributes();
    }

    public function getImageAttribute()
    {
        return [
            'original' => url($this->image->url()),
            'medium' => url($this->image->url('medium')),
            'thumb' => url($this->image->url('thumb')),
        ];
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }

    public function isOnline()
    {
        return Cache::has('admin-is-online-' . $this->id);
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function getCategoriesAttribute()
    {
        if (self::$appendCategories)
        {
            try {
                $role = Role::where('id', $this->role_id)->first();
                $permissions = json_decode($role->permissions, false);

                $categories = $permissions->dynamic->toggle->ArticleCategory;

                if (!empty($categories))
                {
                    return Category::whereIn('id', $categories)->get();
                }
            }
            catch (Exception $e) {
                // do nothing , return empty
            }
        }

        return [];
    }
}
