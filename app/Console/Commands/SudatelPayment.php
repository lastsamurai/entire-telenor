<?php

namespace App\Console\Commands;

use App\UserPurchase;
use App\UserToken;
use Illuminate\Console\Command;
use App\User;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use SPay;

class SudatelPayment extends Command
{
    protected $signature = 'sudatel:payment';
    protected $description = 'Sudatel Payement';

    protected $client;

    const HTTP_OK = 200;


    public function __construct()
    {
        $this->client = new GuzzleClient();

        parent::__construct();
    }

    public function handle()
    {
        info('payment called');
        //Remove today register
        $users = User::whereDate('sub_at','<',Carbon::now()->format('Y-m-d'))->get();

        foreach ($users as $key => $user)
        {
            $payment_result = SPay::payment($user->phone);
            if($payment_result->code == self::HTTP_OK){
             $this->info($user->phone.'charged');
             info($user->phone.'charged');
            }else{
                $user->active = User::NOT_ACTIVE;
                $user->remember_token = null;
                if($user->save()){
                    $this->info($user->phone.' deavtivated');
                    info($user->phone.' deavtivated');
                }
            }

            $this->info('____________________________________');
            $this->info(' ');
        }

        $this->info('finished ' . Carbon::now()->format('Y-m-d H:i:s'));
        info('finished ' . Carbon::now()->format('Y-m-d H:i:s'));
    }

}
