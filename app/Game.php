<?php

namespace App;

use App\Scopes\PublishedScope;
use App\Traits\AdminActivity;
use App\Traits\Bookmarkable;
use App\Traits\Likable;
use App\Traits\Mediable;
use App\Traits\Pagination;
use Conner\Tagging\Taggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;
use Rinvex\Categories\Traits\Categorizable;
use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Support\Facades\Request;
use App\Mediable as MediableModel;

class Game extends Model
{
    use SoftDeletes, AdminActivity, Categorizable, Pagination, /*Searchable,*/ CascadeSoftDeletes, Taggable, Mediable, Bookmarkable, Likable;

    protected $table = 'games';
    protected $attributes = ['description' => null, 'supported_browsers' => null, 'editor_choice' => self::EDITOR_CHOICE_OFF, 'bookmarks' => 0, 'likes' => 0, 'views' => 0, 'published' => self::NOT_PUBLISHED];
    protected $hidden = ['mediaAll', 'tagged', 'published', 'creator_id', 'updater_id', 'deleter_id', 'creator_ip', 'updater_ip', 'deleter_ip', 'deleted_at'];
    protected $casts = ['id' => 'string', 'published' => 'boolean', 'editor_choice' => 'boolean', 'supported_browsers' => 'json'];
    protected $appends = ['media', 'is_bookmarked', 'is_liked', 'is_viewed', 'tags', 'browsers'];
    protected $cascadeDeletes = ['bookmarksCascade', 'likesCascade'];

    public static $categories = [];
    public static $tags = [];

    const ADMIN_EVENTS = ['create' => true, 'update' => true, 'delete' => true];
    public static $checkEvent = true;
    public static $getLocale = false;

    public static $mediaFields = [
        MediableModel::THUMBNAIL_FIELD => ['name' => MediableModel::THUMBNAIL_FIELD, 'type' => Media::TYPE_IMAGE, 'multipliable' => false],
    ];

    const TYPE_VIDEO = 1;
    const TYPE_PODCAST = 2;
    const TYPE_ADS = 10;

    const PUBLISHED = 1;
    const NOT_PUBLISHED = 0;

    const EDITOR_CHOICE_ON = 1;
    const EDITOR_CHOICE_OFF = 0;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PublishedScope);
    }

    public function getTitleAttribute($value)
    {
        if(self::$getLocale){
            $locale = app()->getLocale();
            $value = json_decode($value, false);

            return isset($value->$locale) ? $value->$locale : null;

        }else{
            $value = json_decode($value, false);
            return $value;
        }

    }

    public function getDescriptionAttribute($value)
    {
        if(self::$getLocale) {
            $locale = app()->getLocale();
            $value = json_decode($value, false);

            return isset($value->$locale) ? $value->$locale : null;
        }else{

            $value = json_decode($value, false);
            return $value;
        }
    }

    public function setTitleAttribute($value)
    {
        $value = json_encode($value);
        $this->attributes['title'] = $value;
    }


    public function setDescriptionAttribute($value)
    {
        $value = json_encode($value);
        $this->attributes['description'] = $value;
    }

   /* public function toSearchableArray()
    {
        $locale = app()->getLocale();
        $title = json_decode($this->title, false);
        $description = json_decode($this->descripton, false);

        $array =  [
            'id' => $this->id,
            'title' => $title->$locale,
            'description' => $description->$locale,
        ];


        if (!empty(self::$categories))
            $array = array_merge($array, self::$categories);

        if (!empty(self::$tags))
            $array = array_merge($array, self::$tags);

        return $array;
    }*/

    public function scopeRelations($query)
    {
        return $query->with('mediaAll', 'categories', 'tagged');
    }

    public function views()
    {
        return $this->hasOne(GameView::class, 'game_id');
    }

    public function getIsViewedAttribute()
    {
        $userId = Request::header('userid');
        $view = $this->views()->where('user_id', $userId)->first();

        return ($view) ? true : false;
    }

    public function notification()
    {
        return $this->hasOne(Notification::class, 'notificable_id');
    }

    public function getTagsAttribute()
    {
        if ($this->relationLoaded('tagged'))
            return $this->tagged->pluck('tag');

        return [];
    }

    public function getBrowsersAttribute()
    {
        if ($this->attributes['supported_browsers'])
        {
            $browsers = collect(config('settings.browsers'));
            $supportedBrowsers = json_decode($this->attributes['supported_browsers']);

            foreach ($supportedBrowsers as $key => $browser){
                $locale = app()->getLocale();
                $supportedBrowsers[$key] = $browsers->where('id', $browser)->first()["label-$locale"];
            }


            return implode(', ', $supportedBrowsers);
        }

        return null;
    }
}
