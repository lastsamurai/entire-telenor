<?php

namespace App;

use App\Scopes\PublishedScope;
use App\Traits\Cacheable;
use App\Traits\Slugify;
use Illuminate\Database\Eloquent\Model;
use App\Traits\AdminActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class Page extends Model
{
    use SoftDeletes, AdminActivity, Cacheable, Slugify;

    protected $table = "pages";
    protected $attributes = ['description' => null, 'published' => self::NOT_PUBLISHED];
    protected $casts = ['id' => 'string'];

    public static $checkEvent = true;
    public static $cache_events = ['create', 'update', 'delete'];
    protected static $slugify = ['from' => 'title', 'column' => 'slug'];

    const ADMIN_EVENTS = ['create' => false, 'update' => true, 'delete' => false];

    const PUBLISHED = 1;
    const NOT_PUBLISHED = 0;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new PublishedScope);
    }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = trim($value);
    }

    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = html_entity_decode($value);
    }

    public static function UpdateCache($event)
    {
        if (in_array($event, self::$cache_events))
        {
            $cache = Page::all();
            Cache::forever('pages', $cache);

            return $cache;
        }
    }

    public static function cache()
    {
        $cache = Cache::get('pages');

        if ($cache and $cache->count())
            return $cache;

        return self::UpdateCache('update');
    }
}
