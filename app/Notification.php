<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AdminActivity;
use App\Traits\Mediable;
use App\Mediable as MediableModel;

class Notification extends Model
{
    use AdminActivity, Mediable;

    protected $table = "notifications";
    protected $fillable = ['notificable_id', 'type'];
    protected $attributes = ['title' => null, 'message' => null, 'image' => null, 'type' => self::PUSH_TYPE, 'notificable_id' => null, 'device_type' => self::ALL_DEVICE_TYPE];
    protected $hidden = ['mediaAll', 'creator_id', 'creator_ip'];
    protected $casts = ['id' => 'integer'];
    protected $appends = ['media'];


    public static $checkEvent = true;

    const ADMIN_EVENTS = ['create' => true, 'update' => false, 'delete' => false];

    const ANDROID_DEVICE_TYPE = 1;
    const IOS_DEVICE_TYPE = 2;
    const ALL_DEVICE_TYPE = 3;

    const PUSH_TYPE = 1;
    const GAME_TYPE = 2;

    public static $mediaFields = [
        MediableModel::THUMBNAIL_FIELD => ['name' => MediableModel::THUMBNAIL_FIELD, 'type' => Media::TYPE_IMAGE, 'multipliable' => false],
    ];

    public static function TypeLabel($type)
    {
        switch ($type)
        {
            case self::ALL_DEVICE_TYPE:
                return trans('messages.all_devices');

            case self::ANDROID_DEVICE_TYPE:
                return trans('messages.android_devices');

            case self::IOS_DEVICE_TYPE:
                return trans('messages.ios_devices');
        }
    }
}
