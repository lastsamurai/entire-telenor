<?php

namespace App;

use App\Traits\Active;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Scout\Searchable;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class User extends Authenticatable implements StaplerableInterface
{
    use Notifiable, Active, Searchable, EloquentTrait;

    protected $fillable = ['phone', 'msisdn'];
    protected $attributes = ['name' => null, 'active' => self::NOT_ACTIVE, 'mobile_service_provider' => self::MTN_TYPE, 'notification_status' => self::NOTIFICATION_OFF, 'verification_code' => null, 'verification_expire' => null];
    protected $hidden = ['image_file_name', 'image_file_size', 'image_content_type', 'image_updated_at', 'active', 'ip', 'unsubscribed_sms_at', 'top_up', 'verification_code', 'verification_expire', 'mobile_service_provider', 'remember_token', 'deleter_id', 'deleter_ip', 'deleted_at'];
    protected $casts = ['id' => 'string', 'notification_status' => 'boolean'];
    protected $appends = ['image'];

    public static $active_scope = true;

    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    const NOTIFICATION_ON = 1;
    const NOTIFICATION_OFF = 2;

    const MTN_TYPE = 1;
    const MCI_TYPE = 2;
    const TCI_TYPE = 3;
    const TANZANIA_TIGO_TYPE = 4;

    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('image', [
            'styles' => [
                'medium' => '300x300',
                'thumb' => '100x100'
            ],
            'preserve_files' => true
        ]);

        parent::__construct($attributes);
    }

    public function getAttributes()
    {
        return parent::getAttributes();
    }

    public function getImageAttribute()
    {
        if ($this->image and $this->image->url())
        {
            return [
                'original' => ($this->image->url()) ? $this->image->url() : null,
                'medium' => ($this->image->url('medium')) ? $this->image->url('medium') : null,
                'thumb' => ($this->image->url('thumb')) ? $this->image->url('thumb') : null,
            ];
        }

        return null;
    }

    public function toSearchableArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
        ];
    }

    public function token()
    {
        return $this->hasMany('App\UserToken');
    }

    public function purchases()
    {
        return $this->hasMany(UserPurchase::class, 'user_id');
    }

    public function tigoPurchases()
    {
        return $this->hasMany(UserPurchaseTigo::class, 'user_id');
    }

    public function mtnPurchases()
    {
        return $this->hasMany(UserPurchase::class, 'user_id')->whereIn('type', [UserPurchase::MTN_APP_PURCHASE_TYPE, UserPurchase::MTN_WEB_PURCHASE_TYPE]);
    }

    public function mtnAppPurchase()
    {
        return $this->hasOne(UserPurchase::class, 'user_id')->where('type', UserPurchase::MTN_APP_PURCHASE_TYPE);
    }

    public function mtnWebPurchase()
    {
        return $this->hasOne(UserPurchase::class, 'user_id')->where('type', UserPurchase::MTN_WEB_PURCHASE_TYPE);
    }


    public function mciPurchase()
    {
        return $this->hasMany(UserPurchase::class, 'user_id')->whereIn('type', [UserPurchase::MCI_APP_TYPE, UserPurchase::MCI_SMS_TYPE]);
    }

    public function mciAppPurchase()
    {
        return $this->hasOne(UserPurchase::class, 'user_id')->where('type', UserPurchase::MCI_APP_TYPE);
    }

    public function mciSmsPurchase()
    {
        return $this->hasOne(UserPurchase::class, 'user_id')->where('type', UserPurchase::MCI_SMS_TYPE);
    }

    public function tciPurchase()
    {
        return $this->hasMany(UserPurchase::class, 'user_id')->where('type', UserPurchase::TCI_TYPE);
    }

    public function payload()
    {
        return $this->hasOne('App\UserPayload', 'user_id');
    }
}
