<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPurchase extends Model
{
    use SoftDeletes;

    protected $table = "user_purchases";
    protected $fillable = ['user_id', 'type', 'token'];

    const MTN_APP_PURCHASE_TYPE = 1;
    const MTN_WEB_PURCHASE_TYPE = 2;
    const MCI_APP_TYPE = 3;
    const MCI_SMS_TYPE = 4;
    const TCI_TYPE = 5;

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
