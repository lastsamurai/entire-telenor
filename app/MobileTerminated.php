<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MobileTerminated extends Model
{
    protected $table = 'mobile_terminated';
    protected $attributes = ['subscription_status' => false, 'type' => self::DEFAULT_TYPE];
    protected $casts = ['id' => 'string'];
    protected $hidden = ['created_at', 'updated_at'];

    const DEFAULT_TYPE = 0;
    const OTP_TYPE = 1;

    const DELIVERED = 1;
    const NOT_DELIVERED = 0;

}