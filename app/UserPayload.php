<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPayload extends Model
{
    protected $table = "user_payload";
    protected $fillable = ['user_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
