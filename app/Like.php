<?php

namespace App;

use App\Traits\Pagination;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Like extends Model
{
    use SoftDeletes, Pagination;

    protected $table = 'likes';
    protected $fillable = ['user_id', 'likable_id', 'likable_type'];
    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];
    protected $casts = ['id' => 'string', 'user_id' => 'string', 'likable_id' => 'string'];

    const POST_TYPE = 1;

    public function likable()
    {
        return $this->morphTo();
    }
    public function games_like(){
        return $this->belongsTo('App\Game','likable_id');
    }

}
