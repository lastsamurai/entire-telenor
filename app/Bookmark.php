<?php

namespace App;

use App\Traits\Pagination;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bookmark extends Model
{
    use SoftDeletes, Pagination;

    protected $table = 'bookmarks';
    protected $fillable = ['user_id', 'bookmarkable_id', 'bookmarkable_type'];
    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];
    protected $casts = ['id' => 'string', 'user_id' => 'string', 'bookmarkable_id' => 'string'];

    const POST_TYPE = 1;
    const CATEGORY_TYPE = 2;

    public function bookmarkable()
    {
        return $this->morphTo();
    }
}
