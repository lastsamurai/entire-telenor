<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceToken extends Model
{
    protected $table = "device_token";
    protected $fillable = ['token_id', 'reg_id', 'type'];

    const ANDROID = 1;
    const IOS = 2;

    public function userToken()
    {
        return $this->belongsTo('App\UserToken', 'token_id');
    }
}
