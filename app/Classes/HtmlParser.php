<?php
namespace App\Classes;

use DOMWrap\Document;
use Exception;
use Illuminate\Support\Facades\File;

class HtmlParser
{
    protected $html;
    protected $information = ['p' => 'TEXT', 'img' => 'IMAGE', 'em' => 'ITALIC', 'u' => 'UNDERLINE', 'sup' => 'SUPERSCRIPT', 'strong' => 'BOLD', 'ul' => 'UNSORTED_LIST', 'ol' => 'SORTED_LIST', 'a' => 'LINK'];
    protected $lineBreakTags = ['br'];
    protected $nestedTags = ['ul', 'ol', 'blockquote'];
    protected $notAllowedTags = ['hr'];
    protected $notAllowedContents = ['\n'];
    protected $selfClosedTags = ['img'];
    protected $deleteFlag = 'DELETE_FLAG';
    protected $withDepth;
    protected $flag = true;

    const SPACE = '&nbsp;';

    public function __construct($source, $depth = true)
    {
        $this->withDepth = $depth;

        if (file_exists($source))
            $content = $this->removeBom(File::get($source));
        else
            $content = $source;

        if ($content)
        {
            $doc = new Document();
            $this->html = $doc->html($content);

            $this->stripTags();
        }
        else
            $this->flag = false;
    }

    public function toArray()
    {
        $tree = [];

        if($this->flag)
        {
            foreach ($this->html->find('body > *') as $child)
            {
                $tree = $this->traverse($child, $tree);
            }
        }

        return $tree;
    }

    public function toJson(Array $tree)
    {
        return json_encode($tree);
    }

    private function traverse($html, $tree)
    {
        if (in_array($html->nodeName, $this->nestedTags))
        {
            $tree = $this->setNode($tree, $html);
        }
        else if ($html->children()->count())
        {
            $list = [];

            $html->wrapInner('<tag>');
            foreach ($html->find('tag > *') as $item)
            {
                $list = $this->traverse($item, $list);
                $item->replaceWith($this->deleteFlag);
            }

            $list = $this->setSortedNodes($list, $html);


            if (!empty($list))
            {
                if (count($list) == 1)
                {
                    $tree = array_merge($tree, $list);
                }
                else
                {
                    $tree[] = [
                        'tag' => $html->nodeName,
                        'content' => $this->withDepth ? $list : $this->concatContents($list),
                        'attributes' => $this->setAttribute($html),
                        'has_child' => $this->withDepth ? true : false
                    ];
                }
            }
        }
        else
        {
            $tree = $this->setChild($tree, $html);
        }

        return $tree;
    }

    private function setSortedNodes($list, $child)
    {
        $i = 0;
        $nodes = [];
        $count = count($list) - 1;
        $texts = explode($this->deleteFlag, $child->text());

        foreach ($texts as $text)
        {
            $str = str_replace($this->deleteFlag, '', $text);
            $str = $this->normalizeText($str);

            if ($str and !in_array($str, $this->notAllowedContents))
            {
                $nodes[] = [
                    'tag' => $child->nodeName,
                    'content' => $str,
                    'attributes' => $this->setAttribute($child),
                    'has_child' => false
                ];
            }


            if ($count >= $i)
            {
                $nodes[] = $list[$i];
                $i++;
            }
        }

        if ($count >= $i)
        {
            for ($j=$i; $j<$count; $j++)
                $nodes[] = $list[$i];
        }

        return $nodes;
    }

    private function concatContents(Array $list)
    {
        $content = '';
        foreach ($list as $item)
        {
            $content .= $item['content'] . " ";
        }

        return trim($content);
    }

    private function setChild($tree, $child)
    {
        $child->setText($this->normalizeText($child->text()));

        if (in_array($child->nodeName, $this->selfClosedTags))
        {
            $content = $this->selfClosedTags($child);
        }
        else if (!$child->text() or in_array($child->text(), $this->notAllowedContents))
        {
            return $tree;
        }
        else
        {
            $content = $child->text();
        }

        $tree[] = [
            'tag' => $child->nodeName,
            'content' => $content,
            'attributes' => $this->setAttribute($child),
            'has_child' => false
        ];

        return $tree;
    }

    private function setNode($tree, $child)
    {
        switch ($child->nodeName)
        {
            case 'ul':
                $content = $this->ulTag($child);
                break;

            case 'blockquote':
                $content = $this->blockQuoteTag($child);
                break;

            default:
                throw new Exception("{$child->nodeName} is not supported yet");
        }

        if ($content)
        {
            $tree[] = [
                'tag' => $child->nodeName,
                'content' => $content,
                'attributes' => $this->setAttribute($child),
                'has_child' => false
            ];
        }

        return $tree;
    }

    private function setAttribute($item)
    {
        $attributes = [];

        foreach ($item->attributes as $name => $value)
            $attributes[$name] = $value->value;

        return $attributes;
    }

    private function ulTag($ul)
    {
        $list = [];
        foreach ($ul->find('li') as $li)
        {
            $list[] = $this->normalizeText($li->text());
        }

        return $list;
    }

    private function blockQuoteTag($tag)
    {
        return $this->normalizeText($tag->text());
    }

    private function selfClosedTags($tag)
    {
        switch ($tag->nodeName)
        {
            case 'img':
                return $tag->attr('src');

        }

        return null;
    }

    private function normalizeText($text)
    {
        return trim(strip_tags(html_entity_decode($text)));
    }

    private function stripTags()
    {
        if (!empty($this->notAllowedTags))
            $this->html->find(implode(', ' , $this->notAllowedTags))->remove();

        if (!empty($this->lineBreakTags))
            $this->html->find(implode(', ' , $this->lineBreakTags))->replaceWith('\n');

        foreach ($this->html->find('tooltip') as $tooltip)
        {
            $tooltip->replaceWith(self::SPACE . $tooltip->text());
        }
        /*echo $this->html->html();
        dd();*/
    }

    private function removeBom($text)
    {
        $bom = pack('H*', 'EFBBBF');
        $text = preg_replace("/^$bom/", '', $text);
        return $text;
    }
}
