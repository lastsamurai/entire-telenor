<?php

namespace App\Classes;

use App\Notification;
use App\DeviceToken;
use App\UserToken;
use Edujugon\PushNotification\PushNotification;

class Push
{
    protected $message;
    protected $type;
    protected $android_tokens = [];
    protected $ios_tokens = [];

    public function __construct($message, $type, $target_id = null, $tokens = [])
    {
        $this->message = $message;
        $this->type = $type;

        $this->setTokens($target_id, $tokens);
    }

    public function send()
    {
        switch ($this->type)
        {
            case Notification::ALL_DEVICE_TYPE:
                $this->android();
                $this->ios();
                break;

            case Notification::ANDROID_DEVICE_TYPE:
                $this->android();
                break;

            case Notification::IOS_DEVICE_TYPE:
                $this->ios();
                break;
        }
    }

    public function post($duplicate = false)
    {
        if ($duplicate)
        {
            $notification = new Notification;
            $notification->notificable_id = $this->message['dynamic_data']['id'];
            $notification->type = $this->message['type'];
            $notification->title = $this->message['title'];
            $notification->message = $this->message['body'];
            $notification->url = $this->message['url'];
            $notification->image = $this->message['image'];
            $notification->device_type = $this->type;
            $notification->save();

            $this->send();
        }
        else
        {
            $notification = Notification::firstOrNew(['notificable_id' => $this->message['dynamic_data']['id'], 'type' => $this->message['type']]);
            if (!$notification->exists)
            {
                $notification->title = $this->message['title'];
                $notification->message = $this->message['body'];
                $notification->url = $this->message['url'];
                $notification->image = $this->message['image'];
                $notification->device_type = $this->type;
                $notification->save();

                $this->send();
            }
        }
    }

    private function android()
    {
        $message = $this->setMessage(Notification::ANDROID_DEVICE_TYPE);

        $push = new PushNotification('fcm');
        $push->setMessage($message);
        $push->setDevicesToken($this->android_tokens);
        $push->send();

        $feedback = $push->getFeedback();

        return (bool)$feedback->success;
    }

    private function ios()
    {
        $message = $this->setMessage(Notification::IOS_DEVICE_TYPE);

        $push = new PushNotification('apn');
        $push->setMessage($message);
        $push->setDevicesToken($this->ios_tokens);
        $push->send();
        $feedback = $push->getFeedback();
        $unregistered_tokens = $push->getUnregisteredDeviceTokens();

        if (is_array($unregistered_tokens) and count($unregistered_tokens) > 0)
        {
            $revoke = [];
            foreach ($unregistered_tokens as $unregistered_token)
                if ($unregistered_token and $unregistered_tokens != "")
                    $revoke[] = $unregistered_token;

            if (count($revoke) > 0)
                DeviceToken::whereIn('reg_id', $revoke)->update(['reg_id' => null]);
        }

        if (isset($feedback->success))
            return (bool)$feedback->success;

        return false;
    }

    private function setTokens($target_id, $tokens)
    {
        $exist = false;

        if (is_array($tokens) && !empty($tokens))
        {
            $exist = true;
        }
        else if ($tokens)
        {
            $exist = true;
            $tokens = [$tokens];
        }


        if (!$exist)
        {
            switch ($this->type)
            {
                case Notification::ALL_DEVICE_TYPE:
                    if ($target_id)
                        $tokens = $this->targetTokens($target_id);
                    else
                        $tokens = $this->getAllTokens();

                    $this->ios_tokens = $tokens['ios'];
                    $this->android_tokens = $tokens['android'];
                    break;

                case Notification::ANDROID_DEVICE_TYPE:
                    $this->android_tokens = $this->getAndroidTokens();
                    break;

                case Notification::IOS_DEVICE_TYPE:
                    $this->ios_tokens = $this->getIosTokens();
                    break;
            }
        }
        else
        {
            switch ($this->type)
            {
                case Notification::ALL_DEVICE_TYPE:
                    $this->android_tokens = $tokens;
                    break;

                case Notification::ANDROID_DEVICE_TYPE:
                    $this->android_tokens = $tokens;
                    break;

                case Notification::IOS_DEVICE_TYPE:
                    $this->ios_tokens = $tokens;
                    break;
            }
        }
    }

    private function targetTokens($target_id)
    {
        $android = [];
        $ios = [];
        $user_tokens = UserToken::where('user_id', $target_id)->get();

        if ($user_tokens->count())
        {
            $user_tokens = $user_tokens->pluck('id');
            $tokens = DeviceToken::whereIn('token_id', $user_tokens)->whereNotNull('reg_id')->get();

            if ($tokens->count())
            {
                $ios = $tokens->where('type', DeviceToken::IOS);
                if ($ios->count())
                    $ios = $ios->pluck('reg_id')->toArray();


                $android = $tokens->where('type', DeviceToken::ANDROID);
                if ($android->count())
                    $android = $android->pluck('reg_id')->toArray();
            }
        }

        return [
            'ios' => $ios,
            'android' => $android,
        ];
    }

    private function getAllTokens()
    {
        $tokens = DeviceToken::whereNotNull('reg_id')->groupBy('reg_id')->get();

        $ios = $tokens->where('type', DeviceToken::IOS)->pluck('reg_id')->toArray();
        $android = $tokens->where('type', DeviceToken::ANDROID)->pluck('reg_id')->toArray();

        return [
            'ios' => $ios,
            'android' => $android
        ];
    }

    private function getAndroidTokens()
    {
        $tokens = DeviceToken::where('type', DeviceToken::ANDROID)->whereNotNull('reg_id')->groupBy('reg_id')->get()->pluck('reg_id')->toArray();
        return $tokens;
    }

    private function getIosTokens()
    {
        $tokens = DeviceToken::where('type', DeviceToken::IOS)->whereNotNull('reg_id')->groupBy('reg_id')->get()->pluck('reg_id')->toArray();
        return $tokens;
    }

    private function setMessage($type)
    {
        if ($type == Notification::IOS_DEVICE_TYPE)
        {
            if ($this->message['title'])
            {
                return [
                    'aps' => [
                        'alert' => [
                            'title' => $this->message['title'],
                            'body' => $this->message['body']
                        ],
                        'sound' => 'default'
                    ],
                    'payload' => [
                        'type' => $this->message['type'],
                        'url' => $this->message['url'],
                        'image' => $this->message['image'],
                        'dynamic_data' => $this->message['dynamic_data']
                    ]
                ];
            }
            else
            {
                return [
                    'aps' => [
                        'alert' => [
                            'body' => $this->message['body']
                        ],
                        'sound' => 'default'
                    ],
                    'payload' => [
                        'type' => $this->message['type'],
                        'url' => $this->message['url'],
                        'image' => $this->message['image'],
                        'dynamic_data' => $this->message['dynamic_data']
                    ]
                ];
            }
        }
        else
        {
            if ($this->message['title'])
            {
                return [
                    'data' => [
                        'title' => $this->message['title'],
                        'body' => $this->message['body'],
                        'type' => $this->message['type'],
                        'url' => $this->message['url'],
                        'image' => $this->message['image'],
                        'dynamic_data' => $this->message['dynamic_data'],
                        'sound' => 'default'
                    ]
                ];
            }
            else
            {
                return [
                    'data' => [
                        'body' => $this->message['body'],
                        'type' => $this->message['type'],
                        'url' => $this->message['url'],
                        'image' => $this->message['image'],
                        'dynamic_data' => $this->message['dynamic_data'],
                        'sound' => 'default'
                    ]
                ];
            }
        }
    }
}