<?php

function PersianNumber($string)
{
    $fa_num = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
    $en_num = range(0, 9);
    $string = str_replace($en_num, $fa_num, $string);

    return $string;
}

function EnglishNumber($string)
{
    $fa_num = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    $en_num = range(0, 9);
    $string = str_replace($fa_num, $en_num, $string);

    return $string;
}

function CheckHash(Array $input, $hash)
{
    if (count($input) == 1)
        $string = implode("", $input[0]) . config('settings.salt');
    else
        $string = implode("", $input) . config('settings.salt');

    $true_hash = md5($string);

    if ($hash == $true_hash)
        return true;
    return false;
}

function CurrentRoute()
{
    $route = \Route::getCurrentRoute()->getActionName();
    $route = explode('@', $route);

    return $route[1];
}

function SetClearPhone($phone, $phone_code = 0)
{
    $clear_phones = null;

    $re = '/^(\+?0?92|0)([3]{1}[4]{1}[5-7]{1})([0-9]{7})$/';

    $phone = str_replace(" ", "", $phone);

    preg_match_all($re, $phone, $matches);

    if (count($matches) == 4) {
        $clear_phones = $phone_code . $matches[2][0].$matches[3][0];
    }

    return $clear_phones;
}

/**
 * Convert tanzania tigo msisdn to clear(local) phone number
 *
 * @param $msisdn
 * @param int $phoneCode
 * @return null|string
 */

function SetMsisdn($msisdn, $phoneCode = 92)
{
    $phone = null;

    $re = '/^(\+?0?92|0)([3]{1}[4]{1}[5-7]{1})([0-9]{7})$/';

    $msisdn = str_replace(" ", "", $msisdn);

    preg_match_all($re, $msisdn, $matches);

    if (count($matches) == 4) {
        $phone = $phoneCode . $matches[2][0] . $matches[3][0];
    }

    return $phone;
}


function OperatorDetector($phone, $phone_code = 0)
{
    $clear_phone = SetClearPhone($phone, $phone_code);

    if (starts_with($clear_phone, config('settings.phone_code.mtn')))
        return 'mtn';
    else if (starts_with($clear_phone, config('settings.phone_code.mci')))
        return 'mci';

    return null;
}

function CheckAccess($permissions, $controller, $access)
{
    if (isset($permissions[$controller])) {
        if ($permissions[$controller][$access] == 1)
            return true;
    }

    return false;
}

function DynamicToggleAcl($driver, $provider, $type_slug, $type_value)
{
    switch ($driver) {
        case 'cache':
            if ($type_slug)
                $result = ($cache = \Cache::get($provider)) ? $cache->where($type_slug, $type_value) : collect([]);
            else
                $result = \Cache::get($provider);

            break;

        case 'eloquent':
            if ($type_slug)
                $result = $provider::where($type_slug, $type_value)->get();
            else
                $result = $provider::get();

            break;
    }

    return $result->toArray();
}

function numbers($string)
{
    if (app()->getLocale() == 'fa')
        return PersianNumber($string);

    return $string;
}

function datetime($datetime, $type = 'datetime')
{
    if (app()->getLocale() == 'fa') {
        switch ($type) {
            case 'ago':
                return \Parsidev\Jalali\jDate::forge($datetime)->ago();
                break;

            case 'date':
                return \Parsidev\Jalali\jDate::forge($datetime)->format('Y-m-d');
                break;

            case 'string-date':
                return \Parsidev\Jalali\jDate::forge($datetime)->format('l j  F Y');
                break;

            default:
                return \Parsidev\Jalali\jDate::forge($datetime)->format('Y-m-d H:i:s');
        }
    }

    return $datetime;
}

function slug($string, $separator = '-')
{
    if ($string)
        return str_replace(' ', $separator, $string);

    return null;
}