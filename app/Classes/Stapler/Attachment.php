<?php

namespace App\Classes\Stapler;

use Codesleeve\Stapler\Attachment as StaplerAttachment;
use Codesleeve\Stapler\Interfaces\Interpolator as InterpolatorInterface;
use Codesleeve\Stapler\Interfaces\Resizer as ResizerInterface;
use Codesleeve\Stapler\AttachmentConfig;
use Illuminate\Support\Facades\Storage;

class Attachment extends StaplerAttachment
{
    protected $ftpStatus = false;
    protected $fileNameMethod = 'default';
    protected $imageStyles = [];
    protected $tempPath;

    public function __construct(AttachmentConfig $config, InterpolatorInterface $interpolator, ResizerInterface $resizer)
    {
        parent::__construct($config, $interpolator, $resizer);

        ini_set('max_execution_time', 600);

        $this->ftpStatus = config('settings.ftp_status');
        $this->fileNameMethod = config('laravel-stapler.stapler.filename_method');
        $this->imageStyles = config('laravel-stapler.stapler.image_styles');
        $this->tempPath = config('laravel-stapler.stapler.temp_path');

        if ($this->ftpStatus)
            $this->config->path = $this->config->path_ftp;
    }

    protected function flushWrites()
    {
        foreach ($this->queuedForWrite as $style)
        {
            if ($style->dimensions && $this->uploadedFile->isImage())
                $file = $this->resizer->resize($this->uploadedFile, $style);
            else
                $file = $this->uploadedFile->getRealPath();

            $filePath = $this->path($style->name);

            if ($this->ftpStatus)
            {
                $path = $file;
                $file = file_get_contents($path);
                Storage::disk('ftp')->put($filePath, $file);

                unlink($path);
            }
            else
                $this->move($file, $filePath);
        }

        $this->queuedForWrite = [];
    }

    protected function queueAllForWrite()
    {
        if (!$this->uploadedFile->isImage())
        {
            $styles = $this->styles;
            foreach ($styles as $key => $style)
            {
                if (in_array($style->name, $this->imageStyles))
                    unset($styles[$key]);
            }

            $this->queuedForWrite = array_values($styles);
        }
        else
            $this->queuedForWrite = $this->styles;
    }

    protected function flushDeletes()
    {
        if ($this->ftpStatus and !empty($this->queuedForDeletion))
            Storage::disk('ftp')->delete($this->queuedForDeletion);
        else
            $this->remove($this->queuedForDeletion);

        $this->queuedForDeletion = [];
    }

    public function url($styleName = '')
    {
        if ($this->ftpStatus)
            $baseurl = config('filesystems.disks.ftp.url');
        else
            $baseurl = url('/');


        if ($fileName = $this->originalFilename())
        {
            $path = $this->storageDriver->url($styleName, $this);
            $url = str_replace($fileName, rawurlencode($fileName), $path);

            return $baseurl . $url;
        }


        return $this->defaultUrl($styleName);

    }
}