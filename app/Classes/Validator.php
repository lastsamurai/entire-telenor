<?php

namespace App\Classes;

use App\Media;
use Illuminate\Validation\Validator as Validation;

class Validator  extends Validation
{
    public function validateMedia($attribute, $value, $parameters)
    {
        if (is_null($value))
            return true;

        $media = Media::where('id', $value)->first();
        if ($media)
        {
            $type = Media::humanType($media->type);

            if ($type == $parameters[0])
                return true;
        }

        return false;
    }

    public function message()
    {
        return 'The validation error message.';
    }
}
