<?php

namespace App\Classes;

use Illuminate\Support\Facades\App;
use App\Mediable;
use App\Category;

class ExtendedRinvex
{
    public static function updateCache($model)
    {
        $mediableTableName = (new Mediable)->getTable();

        if ($model->getTable() == $mediableTableName)
        {
            //$cacheable = $model;
            self::flushCache($model);
        }
        else
        {
            $cacheable = Mediable::where('media_id', $model->id)->whereIn('mediable_type', [Category::class])->groupBy('mediable_type')->get();

            if ($cacheable->count())
            {
                foreach ($cacheable as $value)
                    self::flushCache($value);
            }
        }
    }

    public static function flushCache($type)
    {
        $model = $type->mediable_type;
        $model = App::make($model);

        if (method_exists($type->mediable_type, 'isCacheClearEnabled') and $model::isCacheClearEnabled())
            $model::forgetCache();
    }
}