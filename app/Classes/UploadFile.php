<?php
namespace App\Classes;

class UploadFile
{
    private $file;
    private $type;
    private $size;
    private $folder;
    public $allowed_image = ['image/jpeg', 'image/jpg', 'image/png'];
    public $allowed_video = ['video/mp4'];

    function __construct($file, Array $type, $folder = '', $size)
    {
        $this->file = $file;
        $this->type = $type;
        $this->size = $size;
        $this->folder = 'uploads/' . $folder;
    }

    function upload()
    {
        if ($this->checkType() && $this->checkSize())
        {
            $name = $this->generateName();
            $result = $this->file->move($this->folder, $name);
            $result->type = $this->type;
            return $result;
        }
        else
        {
            return false;
        }
    }

    function generateName()
    {
        $ext = $this->file->getClientOriginalExtension();
        $name = time() . rand(1000, 9999) . '.' . $ext;
        return $name;
    }

    function checkType()
    {
        $type = $this->file->getMimeType();
        if (in_array($type, $this->allowed_image))
        {
            $this->type = 1;
            return true;
        }
        elseif (in_array($type, $this->allowed_video))
        {
            $this->type = 2;
            return true;
        }
        else
        {
            return false;
        }
    }

    function checkSize()
    {
        //size in KB
        $size = $this->file->getSize() / 1000;

        if ($size > $this->size)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}