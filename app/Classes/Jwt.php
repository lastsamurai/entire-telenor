<?php
/**
 * Created by PhpStorm.
 * User: Faezeh
 * Date: 2/4/2019
 * Time: 10:25 AM
 */

namespace App\Classes;


use Jenssegers\Agent\Agent;

class Jwt
{
    public function set($uuid){
        $agent = new Agent();

        $header = [
            'type' => 'JWT'
        ];

        $payload = [
            'platform' => $agent->platform(),
            'platformVersion' => $agent->version($agent->platform()),
            'browser' => $agent->browser(),
            'browserVersion' => $agent->version($agent->browser()),
            'languages' => $agent->languages(),
            'isMobile' => $agent->isMobile(),
            'uuid' => $uuid,
        ];

        $data = base64_encode(json_encode($header)) . "." . base64_encode(json_encode($payload));
        $signature = encrypt($data);
        $jwt = serialize($data . "." . $signature);

        return $jwt;

    }
}