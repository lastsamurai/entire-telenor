<?php

namespace App\Classes;

use Request;
use App\UserToken;

class Token
{
    public function generate($user_id, $device_id, $guest = false)
    {
        $done = false;
        $ip_address = Request::ip();
        $old_tokens = [];

        $user_token = UserToken::firstOrNew(['user_id' => $user_id, 'device_id' => $device_id]);
        do{
            $token = str_random(128);

            if(in_array($token, $old_tokens))
                continue;

            if ($user_token->exists)
            {
                $user_token->token = $token;
                $user_token->last_ip = $ip_address;
            }
            else
            {

                $user_token->token = $token;
                $user_token->ip = $ip_address;
                $user_token->last_ip = $ip_address;
            }

            if ($guest)
            {
                $user_token->user_type = UserToken::GUEST_USER_TYPE;
            }
            else
            {
                if ($user_token->user_type)
                {
                    if ($user_token->user_type == UserToken::GUEST_USER_TYPE)
                        $user_token->user_type = UserToken::MIGRATED_USER_TYPE;
                }
                else
                    $user_token->user_type = UserToken::PURCHASED_USER_TYPE;
            }

            if ($user_token->save())
                $done = true;

        }
        while(!$done);

        return $token;
    }
}