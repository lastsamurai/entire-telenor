<?php

namespace App\Providers;

use App\Classes\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator as Validation;

class ValidatorServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Validation::resolver(function ($translator, $data, $rules, $messages) {
            return new Validator($translator, $data, $rules, $messages);
        });

        Validation::replacer('media', function($message, $attribute, $rule, $parameters)
        {
            return trans('validation.media', ['attribute' => trans("validation.attributes.file"), 'values' => trans("validation.attributes.{$parameters[0]}")]);
        });
    }
}
