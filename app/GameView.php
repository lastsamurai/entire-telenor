<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class GameView extends Pivot
{
    protected $table = 'game_views';
    protected $fillable = ['game_id', 'user_id'];
    protected $hidden = ['updated_at'];
    protected $casts = ['id' => 'string', 'user_id' => 'string', 'game_id' => 'string'];

    public function getUpdatedAtColumn()
    {
        return 'updated_at';
    }

    public function game()
    {
        return $this->belongsTo(Game::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
