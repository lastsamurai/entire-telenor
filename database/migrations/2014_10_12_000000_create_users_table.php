<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('phone', 25);
            $table->string('msisdn', 30);

            $table->string('image_file_name')->nullable();
            $table->integer('image_file_size')->nullable();
            $table->string('image_content_type')->nullable();
            $table->timestamp('image_updated_at')->nullable();


            $table->dateTime('unsubscribed_sms_at')->nullable();
            $table->string('verification_code',5)->nullable();
            $table->timestamp('verification_expire')->nullable();
            $table->boolean('active');
            $table->boolean('notification_status');
            $table->tinyInteger('mobile_service_provider');
            $table->ipAddress('ip');
            $table->rememberToken();
            $table->integer('deleter_id')->unsigned()->nullable();
            $table->ipAddress('deleter_ip')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('users');
    }
}
