<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Games extends Migration
{
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('link');
            $table->longText('description')->nullable();
            $table->string('supported_browsers')->nullable();
            $table->boolean('editor_choice');
            $table->integer('bookmarks')->unsigned();
            $table->integer('likes')->unsigned();
            $table->integer('views')->unsigned();
            $table->boolean('published');

            $table->integer('creator_id')->unsigned()->nullable();
            $table->integer('updater_id')->unsigned()->nullable();
            $table->integer('deleter_id')->unsigned()->nullable();
            $table->ipAddress('creator_ip')->nullable();
            $table->ipAddress('updater_ip')->nullable();
            $table->ipAddress('deleter_ip')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }


    public function down()
    {
        Schema::drop('games');
    }
}
