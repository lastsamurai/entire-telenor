<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Media extends Migration
{
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->increments('id');

            $table->string('file_file_name')->nullable();
            $table->integer('file_file_size')->nullable();
            $table->string('file_content_type')->nullable();
            $table->timestamp('file_updated_at')->nullable();

            $table->string('thumbnail_file_name')->nullable();
            $table->integer('thumbnail_file_size')->nullable();
            $table->string('thumbnail_content_type')->nullable();
            $table->timestamp('thumbnail_updated_at')->nullable();

            $table->tinyInteger('type');

            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->integer('duration')->nullable();

            $table->integer('creator_id')->unsigned()->nullable();
            $table->integer('updater_id')->unsigned()->nullable();
            $table->integer('deleter_id')->unsigned()->nullable();
            $table->ipAddress('creator_ip')->nullable();
            $table->ipAddress('updater_ip')->nullable();
            $table->ipAddress('deleter_ip')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('media');
    }
}
