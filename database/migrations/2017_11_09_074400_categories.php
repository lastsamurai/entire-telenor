<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Categories extends Migration
{
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->tinyInteger('type')->after('description');
            $table->string('hex')->nullable()->after('type');
            $table->integer('views')->after('hex');
            $table->integer('bookmarks')->after('views');

            $table->integer('games')->unsigned()->after('views');

            $table->integer('creator_id')->unsigned()->nullable()->after('parent_id');
            $table->integer('updater_id')->unsigned()->nullable()->after('creator_id');
            $table->integer('deleter_id')->unsigned()->nullable()->after('updater_id');

            $table->ipAddress('creator_ip')->nullable()->after('deleter_id');
            $table->ipAddress('updater_ip')->nullable()->after('creator_ip');
            $table->ipAddress('deleter_ip')->nullable()->after('updater_ip');
        });
    }
}
