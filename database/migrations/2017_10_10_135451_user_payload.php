<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserPayload extends Migration
{
    public function up()
    {
        Schema::create('user_payload', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('text');
            $table->string('device_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('user_payload');
    }
}
