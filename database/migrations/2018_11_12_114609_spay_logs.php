<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SpayLogs extends Migration
{
    public function up()
    {
        Schema::create('spay_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('msisdn', 25)->index();
            $table->string('type', 75);
            $table->text('data');
            $table->boolean('status')->nullable();
            $table->ipAddress('ip')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('spay_logs');
    }
}