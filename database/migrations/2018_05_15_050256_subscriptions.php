<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Subscription;

class Subscriptions extends Migration
{
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('price');
            $table->string('unit', 35);
            $table->tinyInteger('type');

            $table->integer('creator_id')->unsigned()->nullable();
            $table->integer('updater_id')->unsigned()->nullable();
            $table->integer('deleter_id')->unsigned()->nullable();
            $table->ipAddress('creator_ip')->nullable();
            $table->ipAddress('updater_ip')->nullable();
            $table->ipAddress('deleter_ip')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        $units = collect(config('settings.units'));

        $subscription = new Subscription;
        $subscription->price = 10;
        $subscription->unit = $units->where('id', 1)->first()['id'];
        $subscription->type = Subscription::DAILY_TYPE;
        $subscription->save();

        $subscription = new Subscription;
        $subscription->price = 20;
        $subscription->unit = $units->where('id', 1)->first()['id'];
        $subscription->type = Subscription::WEEKLY_TYPE;
        $subscription->save();

        $subscription = new Subscription;
        $subscription->price = 30;
        $subscription->unit = $units->where('id', 3)->first()['id'];
        $subscription->type = Subscription::MONTHLY_TYPE;
        $subscription->save();
    }

    public function down()
    {
        Schema::drop('subscriptions');
    }
}
