<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TigoMessages extends Migration
{
    public function up()
    {
        Schema::create('tigo_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('transaction_uuid');
            $table->string('product_id');
            $table->string('price_point_id');
            $table->string('text');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::drop('tigo_messages');
    }
}
