<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DotLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dot_logs',function (Blueprint $table){
           $table->increments('id');
           $table->string('msisdn',25);
           $table->string('type',75);
           $table->text('data');
           $table->boolean('status');
           $table->ipAddress('ip');

           $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
