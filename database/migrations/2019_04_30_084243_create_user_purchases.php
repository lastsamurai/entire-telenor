<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPurchases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_purchases',function (Blueprint $table){
            $table->increments('id');
            $table->string('msisdn');
            $table->string('amount');
            $table->string('transaction_id');
            $table->string('dot_trans_id');

            $table->ipAddress('creator_ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
