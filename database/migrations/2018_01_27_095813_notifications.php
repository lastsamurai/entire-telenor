<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notifications extends Migration
{
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255)->nullable();
            $table->string('message', 60)->nullable();
            $table->string('url', 100)->nullable();
            $table->string('image', 255)->nullable();
            $table->tinyInteger('type');
            $table->integer('notificable_id')->unsigned()->nullable();
            $table->tinyInteger('device_type');
            $table->integer('creator_id')->unsigned()->nullable();
            $table->ipAddress('creator_ip')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('notifications');
    }
}
