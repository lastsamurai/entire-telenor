<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Settings extends Migration
{
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('site_description', 200)->nullable();
            $table->string('site_keywords', 250)->nullable();

            $table->string('favicon_file_name')->nullable();
            $table->integer('favicon_file_size')->nullable();
            $table->string('favicon_content_type')->nullable();
            $table->timestamp('favicon_updated_at')->nullable();

            $table->string('image_file_name')->nullable();
            $table->integer('image_file_size')->nullable();
            $table->string('image_content_type')->nullable();
            $table->timestamp('image_updated_at')->nullable();

            $table->integer('updater_id')->unsigned()->nullable();
            $table->ipAddress('updater_ip')->nullable();

            $table->timestamps();
        });


        $settings = new \App\Setting;
        $settings->save();
    }

    public function down()
    {
        Schema::drop('settings');
    }
}
