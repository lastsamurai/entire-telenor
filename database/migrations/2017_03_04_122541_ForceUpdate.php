<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForceUpdate extends Migration
{
    public function up()
    {
        Schema::create('force_update', function(Blueprint $table){
            $table->increments('id');
            $table->string('latest');
            $table->string('minimum');
            $table->tinyInteger('device');
            $table->integer('updater_id')->unsigned()->nullable();
            $table->ipAddress('updater_ip')->nullable();
            $table->timestamps();
        });

        $force_update = new \App\ForceUpdate;
        $force_update->minimum = '1.0';
        $force_update->latest = '1.0';
        $force_update->device = \App\ForceUpdate::ANDROID_DEVICE;
        $force_update->updater_id = null;
        $force_update->updater_ip = null;
        $force_update->save();

        $force_update = new \App\ForceUpdate;
        $force_update->minimum = '1.0';
        $force_update->latest = '1.0';
        $force_update->device = \App\ForceUpdate::IOS_DEVICE;
        $force_update->updater_id = null;
        $force_update->updater_ip = null;
        $force_update->save();
    }

    public function down()
    {
        Schema::drop('force_update');
    }
}
