<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeviceToken extends Migration
{
    public function up()
    {
        Schema::create('device_token', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('token_id')->unsigned()->nullable();
            $table->foreign('token_id')->references('id')->on('user_token')->onDelete('cascade');
            $table->string('reg_id')->nullable();
            $table->tinyInteger('type');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('device_token');
    }
}
