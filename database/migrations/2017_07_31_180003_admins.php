<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Admins extends Migration
{
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('role_id')->unsigned()->nullable();

            $table->string('image_file_name')->nullable();
            $table->integer('image_file_size')->nullable();
            $table->string('image_content_type')->nullable();
            $table->timestamp('image_updated_at')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
        
        $admin = new \App\Admin;
        $admin->name = 'mostafa';
        $admin->email = 'mostafa.zeinivand@gmail.com';
        $admin->password = bcrypt('sDT_^&u9#Ar@jy[z');
        $admin->role_id = 1;
        $admin->save();

        $admin = new \App\Admin;
        $admin->name = 'ramin';
        $admin->email = 'rkhatibi@gmail.com';
        $admin->password = bcrypt('8P]Px}QNhd3V"`sY');
        $admin->role_id = 1;
        $admin->save();
    }

    public function down()
    {
        Schema::drop('admins');
    }
}