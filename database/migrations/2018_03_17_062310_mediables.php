<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mediables extends Migration
{
    public function up()
    {
        Schema::create('mediables', function (Blueprint $table) {
            $table->integer('media_id')->unsigned();
            $table->string('mediable_type',100);
            $table->integer('mediable_id')->unsigned();
            $table->string('field', 75);

            $table->integer('creator_id')->unsigned()->nullable();
            $table->integer('updater_id')->unsigned()->nullable();
            $table->integer('deleter_id')->unsigned()->nullable();
            $table->ipAddress('creator_ip')->nullable();
            $table->ipAddress('updater_ip')->nullable();
            $table->ipAddress('deleter_ip')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('media_id')->references('id')->on('media')->onDelete('cascade');
            $table->primary(['media_id', 'mediable_type', 'mediable_id', 'field']);
            $table->index(['mediable_id', 'mediable_type']);
        });
    }

    public function down()
    {
        Schema::drop('mediables');
    }
}
