<?php

use Faker\Factory as Faker;
use Faker\Generator;
use Carbon\Carbon;

$factory->define(App\User::class, function (Generator $faker) {
    $faker = Faker::create('fa_IR');

    $phone = $faker->unique()->phoneNumber;
    return [
        'name' => $faker->firstName,
        'phone' => $phone,
        'msisdn' => $phone,
        'active' => 1,
        'mobile_service_provider' => 1,
        'ip' => $faker->ipv4,
        'created_at' => Carbon::now(),
    ];
});
