<?php

return [
    // exclude routes from authorization
    'exclude' => ['dashboard', 'dashboard/admins/self', 'dashboard/tags/json'],

    'sections' => [
        '0' => [
            'controller' => 'UserController',
            'id' => 'users.name',
            'icon' => 'people',
            'access' => 'read',
            'has_publish' => false,
            'url' => 'dashboard.users.index',

            'read' => ['index'],
            'write' => [],
            'update' => ['deactivate', 'activate'],
            'delete' => [],
            'publish' => [],
        ],

        '1' => [
            'controller' => 'CategoryController',
            'id' => 'categories.label',
            'icon' => 'view_module',
            'access' => 'read',
            'has_publish' => false,
            'url' => [
                'prefix' => 'dashboard/categories',
                'group' => [
                    ['id' => 'categories.add', 'link' => 'dashboard.categories.add'],
                    ['id' => 'categories.list', 'link' => 'dashboard.categories.index']
                ]
            ],

            'read' => ['index'],
            'write' => ['store', 'add'],
            'update' => ['edit', 'update'],
            'delete' => ['destroy'],
            'publish' => [],
        ],

        '2' => [
            'controller' => 'TagController',
            'id' => 'tags.label',
            'icon' => 'label',
            'access' => 'read',
            'has_publish' => false,
            'url' => 'dashboard.tags.index',

            'read' => ['index'],
            'update' => ['edit', 'update'],
            'delete' => ['destroy'],
            'publish' => [],
        ],

        '3' => [
            'controller' => 'GameController',
            'id' => 'games.label',
            'icon' => 'videocam',
            'access' => 'read',
            'has_publish' => true,
            'url' => [
                'prefix' => 'dashboard/games',
                'group' => [
                    ['id' => 'games.add', 'link' => 'dashboard.games.add'],
                    ['id' => 'games.list', 'link' => 'dashboard.games.index']
                ]
            ],

            'read' => ['index', 'edit', 'update'],
            'write' => ['add', 'store', 'preview'],
            'update' => ['edit', 'update'],
            'delete' => ['destroy'],
            'publish' => ['publish', 'unPublish', 'push'],
        ],

        '5' => [
            'controller' => 'AdminController',
            'id' => 'admins.label',
            'icon' => 'star_rate',
            'access' => 'read',
            'has_publish' => false,
            'url' => [
                'prefix' => 'dashboard/admins',
                'group' => [
                    ['id' => 'admins.add', 'link' => 'dashboard.admins.add'],
                    ['id' => 'admins.list', 'link' => 'dashboard.admins.index']
                ]
            ],

            'read' => ['index'],
            'write' => ['add', 'store'],
            'update' => ['edit', 'update'],
            'delete' => ['destroy'],
            'publish' => [],
        ],

        '6' => [
            'controller' => 'RoleController',
            'id' => 'roles.label',
            'icon' => 'accessibility',
            'access' => 'read',
            'has_publish' => false,
            'url' => [
                'prefix' => 'dashboard/roles',
                'group' => [
                    ['id' => 'roles.add', 'link' => 'dashboard.roles.add'],
                    ['id' => 'roles.list', 'link' => 'dashboard.roles.index']
                ]
            ],

            'read' => ['index'],
            'write' => ['add', 'store'],
            'update' => ['edit', 'update'],
            'delete' => [],
            'publish' => [],
        ],

        '7' => [
            'controller' => 'MediaController',
            'id' => 'media.label',
            'icon' => 'perm_media',
            'access' => 'read',
            'has_publish' => false,
            'url' => [
                'prefix' => 'dashboard/media',
                'group' => [
                    ['id' => 'media.add', 'link' => 'dashboard.media.add'],
                    ['id' => 'media.list', 'link' => 'dashboard.media.index']
                ]
            ],

            'read' => ['index'],
            'write' => ['add', 'store'],
            'update' => ['edit', 'update'],
            'delete' => ['destroy'],
            'publish' => [],
        ],

        /*'8' => [
            'controller' => 'AdsController',
            'id' => 'ads.label',
            'icon' => 'volume_up',
            'access' => 'read',
            'has_publish' => true,
            'url' => [
                'prefix' => 'dashboard/ads',
                'group' => [
                    ['id' => 'ads.add', 'link' => 'dashboard.ads.add'],
                    ['id' => 'ads.list', 'link' => 'dashboard.ads.index']
                ]
            ],

            'read' => ['index'],
            'write' => ['add', 'store'],
            'update' => ['edit', 'update', 'sort'],
            'delete' => ['destroy'],
            'publish' => ['publish', 'unPublish'],
        ],*/

        '9' => [
            'controller' => 'PageController',
            'id' => 'pages.label',
            'icon' => 'insert_drive_file',
            'access' => 'read',
            'has_publish' => true,
            'url' => [
                'prefix' => 'dashboard/pages',
                'group' => [
                    ['id' => 'pages.add', 'link' => 'dashboard.pages.add'],
                    ['id' => 'pages.list', 'link' => 'dashboard.pages.index']
                ]
            ],

            'read' => ['index'],
            'write' => ['add', 'store'],
            'update' => ['edit', 'update'],
            'delete' => ['destroy'],
            'publish' => ['publish', 'unPublish'],
        ],

        '10' => [
            'controller' => 'ForceUpdateController',
            'id' => 'force-update.name',
            'icon' => 'system_update_alt',
            'url' => 'dashboard.force-update.index',
            'access' => 'read',
            'has_publish' => false,

            'read' => ['index'],
            'write' => [],
            'update' => ['update'],
            'delete' => [],
            'publish' => [],
        ],

        '11' => [
            'controller' => 'SubscriptionController',
            'id' => 'subscriptions.label',
            'icon' => 'settings',
            'access' => 'update',
            'has_publish' => false,
            'url' => 'dashboard.subscriptions.index',

            'read' => ['index'],
            'write' => [],
            'update' => ['update'],
            'delete' => [],
            'publish' => [],
        ],

        '12' => [
            'controller' => 'SettingsController',
            'id' => 'settings.label',
            'icon' => 'settings',
            'access' => 'update',
            'has_publish' => false,
            'url' => 'dashboard.settings.index',

            'read' => ['index'],
            'write' => [],
            'update' => ['update'],
            'delete' => [],
            'publish' => [],
        ]
    ],

    'dynamic' => [
        'toggle' => [
            '0' => [
                'slug' => 'GameCategory',
                'class' => 'post-category',
                'driver' => 'cache',
                'provider' => 'categories',
                'type_slug' => 'type',
                'type_value' => \App\Category::TYPE_GAME,

                'id' => 'id',
                'label' => 'name'
            ]
        ]
    ]
];
