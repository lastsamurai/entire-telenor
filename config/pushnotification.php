<?php

return [
    'gcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'My_ApiKey',
    ],
    'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AAAArKPBIxc:APA91bERaw-7in2NklCUnvFOtOt5CGfDZbj52-A_7ptidpgQyvgEmupg8JtjF8aQnwwqJEifENEsLa9flCHSfc0z0maQMmZaHKWfAFb-hxSJbgYV-iTgZ__VZTIDtVfCIS_gJb4k9PaM',
    ],
    'apn' => [
        'certificate' => config_path('ios-certificates/devPushcert.pem'),
        'passPhrase' => env('IOS_PASS_PHRASE_DEV'),
        /*'passFile' => __DIR__ . '/iosCertificates/yourKey.pem',*/
        'dry_run' => true
    ]
];