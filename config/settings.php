<?php

return [
    'salt' => env('SALT'),

    'ftp_status' => false,

    'browsers' => [
        ['id' => 1, 'slug' => 'chrome', 'label-ur' => 'کروم','label-en' => 'Chrome'],
        ['id' => 2, 'slug' => 'firefox', 'label-ur' => 'فایرفاکس','label-en' => 'Firefox'],
        ['id' => 3, 'slug' => 'opera', 'label-ur' => 'اوپرا','label-en' => 'Opera'],
        ['id' => 4, 'slug' => 'safari', 'label-ur' => 'سافاری', 'label-en' => 'Safari'],
        ['id' => 5, 'slug' => 'edge', 'label-ur' => 'ادج', 'label-en' => 'Edge'],
        ['id' => 6, 'slug' => 'internet explorer', 'label-ur' => 'اینترنت اکسبلورر', 'label-en' => 'Internet Explorer'],
        ['id' => 7, 'slug' => 'uc-browser', 'label-ur' => 'یو سی', 'label-en' => 'Uc browser'],
    ],


    'units' => [
        ['id' => 1, 'label' => 'USD (US$)'],
        ['id' => 2, 'label' => 'EUR (€)'],
        ['id' => 3, 'label' => 'GBP (£)'],
        ['id' => 4, 'label' => 'IRR (ریال)'],
        ['id' => 5, 'label' => 'TSH'],
    ],

    'guest_expiry_time' => 48, // hours
    'sms_expiry_time' => 15, // minutes
    'support_mail' => 'support@tigo.site',
    'download_app' => 'http://google.com',

    'explore' => [
        'category' => 4
    ],

    'post_trending_limit' => 10,
    'most_viewed_tags' => 10,

    'mci' => [
        'baseurl' => 'http://10.20.9.135:8600/samsson-sdp/',
        'service_key' => '89ee76eb198943c6a32de6cf71fb58d8',
        'service_name' => 'CTELVP',
        'short_code' => '98307212',
        'charging_code' => 'TELRENCTELDIGI4000',
        'unsub_charging_code' => 'TELUSUBCTELDIGIFOO',
        'sub_charging_code' => 'TELSUBCTELVP',
        'username' => 'HOOSHMANDTADBIR',
        'password' => 'Hooshamand@2017',
        'message' => 'Test',

        'price' => '400',
        'app_link' => 'http://google.com',

        'subscription' => [
            'daily' => '1'
        ]
    ],

    'mtn' => [
        'access_token' => 'e57230d1-68b5-3713-a440-02239c1fdbff',
        'sku' => '1354',
        'package_name' => 'com.ionicframework.tutian'
    ],

    'tigo' => [
        'service_id' => '84233001734056',
        'service_name' => 'Portal',
        'country_id' => '255',
        'mcc' => '640',
        'mnc' => '02',
        'large_account' => '15514',

        'products' => [
            ['id' => 4901, 'name' => 'daily', 'price' => '150', 'days' => 1],
            ['id' => 4904, 'name' => 'weekly', 'price' => '500', 'days' => 7],
            ['id' => 4905, 'name' => 'monthly', 'price' => '1500', 'days' => 30],
        ],

        'price_points' => [
            ['id' => '40217', 'shortcode' => '15514', 'name' => 'DOB-150TSH'],
            ['id' => '40218', 'shortcode' => '15514', 'name' => 'DOB-500TSH'],
            ['id' => '40219', 'shortcode' => '15514', 'name' => 'DOB-1500TSH'],
            ['id' => '40230', 'shortcode' => '15514', 'name' => 'MT FREE'],
            ['id' => '40216', 'shortcode' => '15514', 'name' => 'MO FREE'],
        ],

        'signup_message' => 'Mpendwa mteja, haujajiunga na Tigo Games. Jiunge leo na Ufurahie huduma zetu.Tembelea www.tigogames.com au Tuma neno Game, Game7, Game30 kwenda 15514 Ufurahie!'
    ],

    'phone_code' => [
        'mtn' => ['0901', '0902', '0903', '0930', '0933', '0935', '0936', '0937', '0938', '0939'],
        'mci' => ['0910', '0911', '0912', '0913', '0914', '0915', '0916', '0917', '0918', '0919', '0990']
    ],

    'developer_signup' => [
        'status' => true,
        'phone' => '09183436822'
    ],

    'languages'=>[
        'ur' => 'اردو',
        'en' => 'English',
    ]
];