$.fn.toJalaali = function(a, b, c) {
    if ("[object Date]" === Object.prototype.toString.call(a)) {
        c = a.getDate();
        b = a.getMonth() + 1;
        a = a.getFullYear();
    }
    return d2j(g2d(a, b, c));
};


(function( $ ){
    $.fn.toGregorian = function(a, b, c) {
        return d2g(j2d(a, b, c));
    };
})( jQuery );


function isValidJalaaliDate(a, b, c) {
    return a >= -61 && a <= 3177 && b >= 1 && b <= 12 && c >= 1 && c <= jalaaliMonthLength(a, b);
}

function isLeapJalaaliYear(a) {
    return 0 === jalCal(a).leap;
}

function jalaaliMonthLength(a, b) {
    if (b <= 6) return 31;
    if (b <= 11) return 30;
    if (isLeapJalaaliYear(a)) return 30;
    return 29;
}

function jalCal(a) {
    var b = [ -61, 9, 38, 199, 426, 686, 756, 818, 1111, 1181, 1210, 1635, 2060, 2097, 2192, 2262, 2324, 2394, 2456, 3178 ], c = b.length, d = a + 621, e = -14, f = b[0], g, h, i, j, k, l, m;
    if (a < f || a >= b[c - 1]) throw new Error("Invalid Jalaali year " + a);
    for (m = 1; m < c; m += 1) {
        g = b[m];
        h = g - f;
        if (a < g) break;
        e = e + 8 * div(h, 33) + div(mod(h, 33), 4);
        f = g;
    }
    l = a - f;
    e = e + 8 * div(l, 33) + div(mod(l, 33) + 3, 4);
    if (4 === mod(h, 33) && h - l === 4) e += 1;
    j = div(d, 4) - div(3 * (div(d, 100) + 1), 4) - 150;
    k = 20 + e - j;
    if (h - l < 6) l = l - h + 33 * div(h + 4, 33);
    i = mod(mod(l + 1, 33) - 1, 4);
    if (i === -1) i = 4;
    return {
        leap: i,
        gy: d,
        march: k
    };
}

function j2d(a, b, c) {
    var d = jalCal(a);
    return g2d(d.gy, 3, d.march) + 31 * (b - 1) - div(b, 7) * (b - 7) + c - 1;
}

function d2j(a) {
    var b = d2g(a).gy, c = b - 621, d = jalCal(c), e = g2d(b, 3, d.march), f, g, h;
    h = a - e;
    if (h >= 0) if (h <= 185) {
        g = 1 + div(h, 31);
        f = mod(h, 31) + 1;
        return {
            jy: c,
            jm: g,
            jd: f
        };
    } else h -= 186; else {
        c -= 1;
        h += 179;
        if (1 === d.leap) h += 1;
    }
    g = 7 + div(h, 30);
    f = mod(h, 30) + 1;
    return {
        jy: c,
        jm: g,
        jd: f
    };
}

function g2d(a, b, c) {
    var d = div(1461 * (a + div(b - 8, 6) + 100100), 4) + div(153 * mod(b + 9, 12) + 2, 5) + c - 34840408;
    d = d - div(3 * div(a + 100100 + div(b - 8, 6), 100), 4) + 752;
    return d;
}

function d2g(a) {
    var b, c, d, e, f;
    b = 4 * a + 139361631;
    b = b + 4 * div(3 * div(4 * a + 183187720, 146097), 4) - 3908;
    c = 5 * div(mod(b, 1461), 4) + 308;
    d = div(mod(c, 153), 5) + 1;
    e = mod(div(c, 153), 12) + 1;
    f = div(b, 1461) - 100100 + div(8 - e, 6);
    return {
        gy: f,
        gm: e,
        gd: d
    };
}

function div(a, b) {
    return ~~(a / b);
}

function mod(a, b) {
    return a - ~~(a / b) * b;
}