/*************** POPOVER ***********************************************************************************************/

$(document).ready(function(){
    var flag = $(".container").hasClass('has-popover');

    if(flag)
        $('[data-toggle="popover"]').popover({
            delay: {
                show: "400",
                hide: "100"
            },
        });
});

/*************** CHANGE MODAL ID ***************************************************************************************/

$(document).ready(function(){
    $("body").on('click', '.change-modal-id', function () {
        var id = $(this).data('id');
        var target = $(this).data('target');

        $(target).find(".modal-id").val(id);
    });
});

/*************** DATE TIME PICKER **************************************************************************************/

$(document).ready(function(){
    if($(".date-time-picker").length > 0)
    {
        $('.date-time-picker').datepicker({
            changeMonth: true,
            changeYear: true
        });
    }
});

/*************** colorpicker *******************************************************************************************/

$(document).ready(function () {
    color_picker(".hex-input");

    $(".gradient-input").each(function () {
        var id = $(this).attr('id');
        color_picker("#" + id);
    });

    $(".gradient-input").keyup(function () {
        var id = $(this).attr('id');
        var color = $(this).val();
        $("#" + id).ColorPickerSetColor(color);
    });
});

function color_picker(elem) {
    if ($.isFunction($.fn.ColorPicker))
    {
        var color = $(elem).val();
        $(elem).ColorPicker({
            color: color,
            onChange: function (hsb, hex, rgb) {
                $(elem).val('#' + hex);
            }
        });
    }
}

/*************** Number Converter *****************************************************************************************/

// persian numbers
(function( $ ){
    $.fn.PersianNumber = function(number) {
        var digits = [], r;
        do {
            r = number % 10;
            number = (number - r) / 10;
            digits.unshift(['&#', r + 1776, ';'].join(''));
        } while (number > 0);

        return digits.join('');
    };
})( jQuery );

// english numbers
(function( $ ){
    $.fn.EnglishNumber = function(str) {
        return Number( str.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function(d) {
            return d.charCodeAt(0) - 1632;
        }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function(d) {
            return d.charCodeAt(0) - 1776;
        }) );
    };
})( jQuery );

/*********** ALERT BEFORE CLOSE WINDOW *********************************************************************************/
$(document).ready(function() {
    // set flag
    $("input[type=text], input[type=checkbox], input[type=radio], input[type=email], input[type=file], textarea").change(function(e){
        changeCloseFlag();
    });

    // disable it on form submit
    $("form").on('submit',function(){
        window.onbeforeunload = null;
    });

    // check refresh and back button
    HandleBackFunctionality();
});

// show an alert when closing ...
function changeCloseFlag(){
    window.onbeforeunload = function (e) {
        var message = "اطلاعات شما هنوز ذخیره نشده است، مطمئنید که میخواهید خارج شوید؟";
        return message;
    }
}

function HandleBackFunctionality() {
    if(window.event)
    {
        if(window.event.clientX < 40 && window.event.clientY < 0)
        {
            changeCloseFlag();
        }
        else
        {
            changeCloseFlag();
        }
    }
    else if (event)
    {
        if(event.currentTarget.performance.navigation.type == 1)
        {
            changeCloseFlag();
        }
        if(event.currentTarget.performance.navigation.type == 2)
        {
            changeCloseFlag();
        }
    }
}

/*************** BASE64 IMAGE PREVIEW **********************************************************************************/

$(document).ready(function(){
    // remove add image
    $(".box").on('change', '.img-input', function(){
        var elem    = $(this).closest('.box').find('.img-input-label');
        var file    = $(this).closest('.box').find('input.img-input')[0].files[0];
        var reader  = new FileReader();

        if (file) {
            reader.readAsDataURL(file);

            // put base64 image
            reader.addEventListener("load", function () {
                elem.removeAttr('style');
                elem.css({
                    'background-image' : 'url('+ reader.result +')',
                    'background-size'  : 'cover'
                });
            }, false);
        }
    });
});

/*************** UPLOADER **********************************************************************************************/

$(document).ready(function () {
    $('body').on('change', '.inputfile', function(){
        $(this).closest('div').find('.uploader-label').text($(this).val());
    });
});

/*************** Settings **********************************************************************************************/

$(document).ready(function(){
    $(".edit-setting-row .edit-btn").click(function () {
        var row = $(this).closest('.edit-setting-row');

        $(this).hide();
        row.find('.no-edit-btn').show();
        row.find('.submit-area').show();
        row.find('input').prop('disabled', false);
        row.find('textarea').prop('disabled', false);

        row.find('input').first().focus();
    });

    $(".edit-setting-row .no-edit-btn").click(function () {
        var row = $(this).closest('.edit-setting-row');

        $(this).hide();
        row.find('.edit-btn').show();
        row.find('.submit-area').hide();
        row.find('input').prop('disabled', true);
        row.find('textarea').prop('disabled', true);
    });
});

/*************** tagsinput *********************************************************************************************/

$(document).ready(function () {
    var url = baseurl + 'tags/json';
    var suggestions = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,

        prefetch: {
            url: url,
            filter: function(list) {
                return $.map(list, function(suggestion) {
                    return { name: suggestion }; });
            }
        }
    });
    suggestions.initialize();

    $('.tagsinput').tagsinput({
        confirmKeys: [13, 44],
        typeaheadjs: [{
            minLength: 1,
            highlight: true
        },{
            minlength: 1,
            name: 'suggestion',
            displayKey: 'name',
            valueKey: 'name',
            source: suggestions.ttAdapter()
        }],
        freeInput: true
    });
});

/*************** text editor *******************************************************************************************/
var froala_url_prefix = 'http://79.175.165.38/v1.0/public';

$(document).ready(function () {
    $.extend($.FroalaEditor.POPUP_TEMPLATES, {
        'tooltipPlugin.popup': '[_BUTTONS_][_CUSTOMLAYER_]'
    });

    if ($('.publisher').length)
    {
        $.FroalaEditor.PLUGINS.tooltipPlugin = function (editor) {
            var selected = '';
            var html = '';

            function initPopup () {
                var popup_buttons = '';
                if (editor.opts.popupButtons.length > 1) {
                    popup_buttons += '<div class="fr-buttons">';
                    popup_buttons += editor.button.buildList(editor.opts.popupButtons);
                    popup_buttons += '</div>';
                }

                // load popup template.
                var template = $.FroalaEditor.POPUP_TEMPLATES.customPopup;
                if (typeof template === 'function') {
                    template = template.apply(editor);
                }

                var tooltipPopupLayer = '<div class="tooltip-layer"><input type="text" id="tooltip-text"></div>';

                var template = {
                    buttons: popup_buttons,
                    customLayer: tooltipPopupLayer
                };

                editor.popups.onShow('tooltipPlugin.popup', function () {
                    if (editor.format.is('tooltip'))
                    {
                        var title = $(this.selection.element()).attr('title');
                        $("#tooltip-text").val(title);
                    }

                    selected = this.selection.text();
                    html = this.html;
                });

                editor.popups.onHide('tooltipPlugin.popup', function () {
                    $("#tooltip-text").val('');
                    html = '';
                    selected = '';
                });

                // create popup.
                var $popup = editor.popups.create('tooltipPlugin.popup', template);

                return $popup;
            }

            function showPopup () {
                // Get the popup object defined above.
                var $popup = editor.popups.get('tooltipPlugin.popup');

                // If popup doesn't exist then create it.
                // To improve performance it is best to create the popup when it is first needed
                // and not when the editor is initialized.
                if (!$popup) $popup = initPopup();

                // Set the editor toolbar as the popup's container.
                editor.popups.setContainer('tooltipPlugin.popup', editor.$tb);

                // If the editor is not displayed when a toolbar button is pressed, then set BODY as the popup's container.
                // editor.popups.setContainer('tooltipPlugin.popup', $('body'));

                // Trigger refresh for the popup.
                editor.popups.refresh('tooltipPlugin.popup');

                // This custom popup is opened by pressing a button from the editor's toolbar.
                // Get the button's object in order to place the popup relative to it.
                var $btn = editor.$tb.find('.fr-command[data-cmd="tooltip"]');

                // Compute the popup's position.
                var left = $btn.offset().left + $btn.outerWidth() / 2;
                var top = $btn.offset().top + (editor.opts.toolbarBottom ? 10 : $btn.outerHeight() - 10);

                // Show the custom popup.
                // The button's outerHeight is required in case the popup needs to be displayed above it.
                editor.popups.show('tooltipPlugin.popup', left, top, $btn.outerHeight());
            }

            function hidePopup () {
                editor.popups.hide('tooltipPlugin.popup');
            }

            function insert() {
                var tooltip = $("#tooltip-text").val();
                if (tooltip != '' && editor.selection.text())
                {
                    editor.selection.restore();
                    editor.format.apply('tooltip', { title: tooltip });
                    editor.selection.save();
                }

                hidePopup();
            }

            function remove() {
                if (editor.selection.text())
                {
                    if (editor.format.is('tooltip'))
                    {
                        var elem = $(editor.selection.element());
                        elem.contents().unwrap();
                    }
                }

                hidePopup();
            }

            // methods visible outside the plugin.
            return {
                showPopup: showPopup,
                hidePopup: hidePopup,
                insert: insert,
                remove: remove
            };
        };

        $.FroalaEditor.DefineIcon('tooltipIcon', {
            NAME: 'commenting'
        });
        $.FroalaEditor.RegisterCommand('tooltip', {
            title: 'Tooltip',
            icon: 'tooltipIcon',
            undo: true,
            focus: false,
            plugin: 'tooltipPlugin',
            callback: function() {
                var editor = this;
                editor.selection.save();
                this.tooltipPlugin.showPopup();
            }
        });

        $.FroalaEditor.DefineIcon('insertTooltip', { NAME: 'plus-square-o' });
        $.FroalaEditor.RegisterCommand('insertTooltip', {
            title: 'Insert',
            undo: true,
            focus: true,
            callback: function () {
                this.tooltipPlugin.insert();
            }
        });

        $.FroalaEditor.DefineIcon('removeTooltip', { NAME: 'minus-square-o' });
        $.FroalaEditor.RegisterCommand('removeTooltip', {
            title: 'Remove',
            undo: false,
            focus: false,
            callback: function () {
                this.tooltipPlugin.remove();
            }
        });

        var htmlAllowedTags = $.FE.DEFAULTS.htmlAllowedTags;
        htmlAllowedTags.push('tooltip');
    }

    var options = {
        direction: 'rtl',
        height: 250,
        htmlExecuteScripts: false,
        pastePlain: true,
        language: 'fa',
        linkAlwaysNoFollow: false,
        htmlAllowedTags: htmlAllowedTags,
        inlineStyles: {
            'قرمز ضخیم': 'font-size: 20px; color: red;',
            'کوچک خاکستری': 'font-size: 14px; color: #ccc;'
        },
        paragraphStyles: {
            'fr-text-gray': 'خاکستری',
            'fr-text-bordered': 'بوردر دار'
        },

        charCounterCount: true,
        requestHeaders: {
            'X-CSRF-TOKEN': token
        },

        imageUpload: true,
        imageAllowedTypes: ['jpeg', 'jpg', 'png'],
        imageMaxSize: 1024 * 1024 * 5,
        imageUploadURL: froala_url_prefix + '/dashboard/uploader/image',
        imageUploadParam: 'image',

        videoUpload: true,
        videoAllowedTypes: ['mp4'],
        videoMaxSize: 1024 * 1024 * 50,
        videoUploadURL: froala_url_prefix + '/dashboard/uploader/video',
        videoUploadParam: 'video',

        requestWithCredentials: true,

        entities: '',
        popupButtons: ['insertTooltip', 'removeTooltip'],
        toolbarButtons: ['tooltip', 'customQuote', "bold", "italic", "underline", "|", "paragraphFormat", "formatOL", "formatUL", "quote", "|", "insertLink", "insertImage", "insertVideo", "-", "selectAll", "print", "html", "|", "undo", "redo"]
    };

    $('textarea.froala').froalaEditor(options);

    $('textarea.froala').on('froalaEditor.destroy', function (e, editor) {
        setTimeout(function () {
            $('textarea.froala').froalaEditor(options);
        }, 1);
    });

    $('textarea.froala').on('froalaEditor.contentChanged', function (e, editor) {
        changeCloseFlag();
    });
});

/*************** Sort **************************************************************************************************/
// ajax header
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': token
        }
    });
});


$(document).ready(function(){
    $("#sortable-container").sortable({
        animation: 300,
        handle: ".sortable-rows",
        onEnd: function (evt) {
            var length = $("#sortable-container .sortable-rows").length - 1;
            var url = $("#sortable-container").data('target');

            if (evt.oldIndex != evt.newIndex)
            {
                if (evt.newIndex != length)
                {
                    var row = $("#sortable-container .sortable-rows").eq(evt.newIndex).data('id');
                    var swap = $("#sortable-container .sortable-rows").eq(evt.newIndex + 1).data('id');
                    var type = 'moveBefore';
                }
                else
                {
                    var row = $("#sortable-container .sortable-rows").eq(evt.newIndex).data('id');
                    var swap = $("#sortable-container .sortable-rows").eq(evt.newIndex - 1).data('id');
                    var type = 'moveAfter';
                }

                $.ajax({
                    url: baseurl + url,
                    type: "POST",
                    data: ({model: row, swap: swap, type: type}),
                    dataType: 'json',
                    success: function (result) {
                        if (result.status)
                        {
                            alert('done');
                        }
                        else
                        {
                            alert(result.message);
                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        if (xmlHttpRequest.readyState == 0 || xmlHttpRequest.status == 0)
                            return;  // it's not really an error
                        else
                        {
                            alert(errorThrown + "-" + textStatus);
                        }
                    }
                });
            }
        }
    });
});


/*************** edit force update *************************************************************************************/

$(document).ready(function () {
    $("#force-update-page .edit-force-update").click(function () {
        var parent = $(this).closest('.row');
        var target = $("#edit-force-update");
        var name = parent.find('.name span').text();
        var minimum = parent.find('.minimum span').text();
        var latest = parent.find('.latest span').text();

        target.find('#myModalLabel').text(name);
        target.find('#minimum').val(minimum);
        target.find('#latest').val(latest);
    });
});

/*************** admins ************************************************************************************************/

$(document).ready(function () {
    $("#is-writer").change(function () {
        $(".is-writer-input input, .is-writer-input textarea").prop('disabled', function () {
            return ! $(this).prop('disabled');
        });

        $(".is-writer-input").toggle();
    });

    $("#toggle-admin-create").click(function () {
        $("#admin-create-section").slideToggle();
    });
});

/*************** preview ***********************************************************************************************/

$(document).ready(function () {
    setTimeout(function () {
        $(".searchable-select").width(0).height(0).show();
    }, 50);
});

function validate(form) {
    var validator = form.validate({
        showErrors: function (errorMap, errorList) {
            // do nothing
        }
    });

    var valid = form.valid();

    return valid;
}

$(document).ready(function () {
    $(".preview-btn").click(function () {
        var btn = $(this);
        var form = btn.closest('form');
        var action = form.attr('action');
        var method = form.attr('method');
        var _method = form.find('input[name="_method"]').val();
        var url = btn.data('url');
        var publishBtn = btn.data('btn');
        var left = ($(window).width()/2)-(360/2);
        var top   = ($(window).height()/2)-(640/2);

        if(validate(form))
        {
            window.open('', url, 'width=360,height=640,top=' + top + ',left=' + left);
            form.attr('target', url).attr('action', url).attr('method', 'post');
            form.find('input[name="_method"]').val('post');
            $(publishBtn).trigger('click');
            form.attr('target', '_self').attr('action', action).attr('method', method);
            form.find('input[name="_method"]').val(_method);
        }
        else
            $(publishBtn).trigger('click');
    });
});


$(document).ready(function () {
    $(".preview-btn-desktop").click(function () {
        var btn = $(this);
        var form = btn.closest('form');
        var action = form.attr('action');
        var method = form.attr('method');
        var _method = form.find('input[name="_method"]').val();
        var url = btn.data('url');
        var publishBtn = btn.data('btn');

        form.attr('target', '_blank').attr('action', url).attr('method', 'post');
        form.find('input[name="_method"]').val('post');
        $(publishBtn).trigger('click');
        form.attr('target', '_self').attr('action', action).attr('method', method);
        form.find('input[name="_method"]').val(_method);
    });
});

/*************** role management ***************************************************************************************/

$(document).ready(function () {
    $('.publish-role').change(function () {
        var row = $(this).closest('.role-row');

        if ($(this).is(':checked')) {
            row.find('.update-role').prop('checked', true);
        }
    });

    $('.update-role').change(function () {
        var row = $(this).closest('.role-row');

        if (!$(this).is(':checked') && row.find('.publish-role').is(':checked')) {
            $(this).prop('checked', true);
            alert('در صورتی که دسترسی انتشار فعال باشد، دسترسی بروز رسانی هم باید فعال باشد');
        }
    });
});

/****** custom select box **********************************************************************************************/

$(document).ready(function(){
    $("select.searchable-select").chosen({
        rtl: true,
        no_results_text: 'ننیجه ای یافت نشد',
        no_results_text: 'ننیجه ای یافت نشد',
        width: "100%"
    });
});

/****** ingredients ****************************************************************************************************/

$(document).ready(function(){
    var firstIngredientRow = $('#first-ingredient').clone(false);
    firstIngredientRow.find("textarea").val('');
    firstIngredientRow.find("input[type=text]").val('');
    firstIngredientRow.find("select").val('');
    firstIngredientRow.attr('id', '');
    firstIngredientRow.find('.remove-ingredient-row').show();
    firstIngredientRow.find('.chosen-container').remove();
    firstIngredientRow.find('select').show();


    $("#add-ingredient-row").click(function () {
        $("#ingredient-row-container").append(firstIngredientRow.clone(false));
        setTimeout(function () {
            $("#ingredient-row-container .ingredient-row select").chosen();
        }, 10);

        updateIngredientIndex();
    });

    $("body").on('click', '.remove-ingredient-row', function () {
        $(this).closest('.ingredient-row').remove();

        updateIngredientIndex();
    });

    function updateIngredientIndex() {
        var index = 1;
        $("#ingredient-row-container .ingredient-row").each(function () {
            $(this).find('.ingredient-name').attr('name', "ingredients[" + index + "][name]");
            $(this).find('.ingredient-value').attr('name', "ingredients[" + index + "][value]");
            $(this).find('.ingredient-unit').attr('name', "ingredients[" + index + "][unit]");
            $(this).find('.ingredients-input').attr('id', "ingredient-" + index);

            ingredientInit('#ingredient-' + index);

            index++;
        });
    }
});

/****** gallery ****************************************************************************************************/

$(document).ready(function(){
    $("body").on('click', '.remove-gallery-row', function () {
        $(this).closest('.gallery-row').remove();
    });

    function updateGalleryIndex() {
        var index = 1;
        $("#gallery-row-container .gallery-row").each(function () {
            index++;
        });
    }
});

/****** push notification **********************************************************************************************/

$(document).ready(function(){
    $(".push-item").click(function () {
        var link = $(this).data('link');
        var title = $(this).closest('.row').find('.post-title').text();

        $("#push-link").attr('href', link);
        $("#push-modal code").text(title);


        $("#push-modal").modal('show');
    });
});

/****** hex dropdown ***************************************************************************************************/

$(document).ready(function(){
    $(".hex-dropdown .dropdown-item").click(function (e) {
        e.stopPropagation();
        var hex = $(this).data('hex');
        $('.hex-input').val(hex);

        $(".hex-dropdown .dropdown-toggle .color-preview").text(hex).css('background', hex);
    });

    $('.hex-dropdown').on('hide.bs.dropdown', function () {
        if ($('.colorpicker').is(":visible"))
            return false;
    });
});