/**** login ***********************************************************************************************************/
// ajax header
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': token,
        }
    });
});

function PhoneRegex(phone) {
    var regex = /^(\+?0?92|0)([3]{1}[4]{1}[5-7]{1})([0-9]{7})$/;
    var str = phone;
    var m;

    if ((m = regex.exec(str)) !== null) {
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }
    }

    if (m == null)
        return false;
    return true;
}

$(document).ready(function () {
    $("#login-btn").click(function () {
        var btn = $("#login-btn");
        var phone = $.fn.EnglishNumber($("#modal-phone").val());
        var flag = true;

        $("#login-modal .success").hide();


        // phone validation
        phone = phone.trim();
        if (!PhoneRegex(phone)) {
            $("#login-modal .error").show().text(invalidPhoneMessage);
            flag = false;
        }
        else {
            $("#login-modal .error").hide().text('');
        }

        if (flag) {
            btn.prop("disabled", true);
            var jwt = getCookie('jwt');
            console.log('jwt: '.jwt);

            $.ajax({
                url: baseurl + "/users/signup",
                type: "POST",
                func: 'register',
                data: {msisdn: phone,jwt: jwt},
                dataType: 'json',
                success: function (result) {
                    console.log(result);
                    if (result.action == 'redirect') {
                        setCookie('jwt',result.jwt, 10);
                        location.reload();
                    }else if(result.action == 'error'){
                        $("#login-modal .error").show().text(result.message);
                    }
                    else if (result.action == 'message') {
                        $('#login-modal').hide();
                        $('#verification-modal').modal('show');
                        $('#verification-modal-phone').val(phone);
                        $("#login-modal .success").show().text(result.message);
                        btn.prop("disabled", false);
                    }
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    if (xmlHttpRequest.readyState == 0 || xmlHttpRequest.status == 0)
                        return;  // it's not really an error
                    else
                        alert(errorThrown + "-" + textStatus);

                    btn.prop("disabled", false);
                }
            });
        }
    });
});


$(document).ready(function () {
    $("#verification-btn").click(function () {
        var btn = $("#verification-btn");
        var phone = $("#verification-modal-phone").val();
        var code = $('#modal-code').val();
        console.log('start of verification');
        console.log(phone);
        var flag = true;

        $("#verification-modal .success").hide();


        // phone validation
        phone = phone.trim();
        if (!PhoneRegex(phone)) {
            $("#verification-modal .error").show().text('phone is invalid');
            flag = false;
        }
        else {
            $("#verification-modal .error").hide().text('');
        }

        if (flag) {
            btn.prop("disabled", true);
            var jwt = getCookie('jwt');
            $.ajax({
                url: baseurl + "/users/confirm-code",
                type: "POST",
                func: 'register',
                data: {msisdn: phone, code: code, jwt: jwt},
                dataType: 'json',
                success: function (result) {
                    if (result.action == 'redirect') {
                        location.reload();
                    }
                    else if (result.action == 'message') {
                        $("#verification-modal .error").show().text(result.message);
                        btn.prop("disabled", false);
                    }
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    if (xmlHttpRequest.readyState == 0 || xmlHttpRequest.status == 0)
                        return;  // it's not really an error
                    else
                        alert(errorThrown + "-" + textStatus);

                    btn.prop("disabled", false);
                }
            });
        }
    });
});

$(document).ready(function () {
    $("#logout-btn").click(function () {
        var btn = $("#logout-btn");
        var id = $("#logout_user_id").val();
        var flag = true;
        console.log(id);

        $("#logout-modal .success").hide();

        if (flag) {
            btn.prop("disabled", true);

            $.ajax({
                url: baseurl + "/users/unsub?id="+id,
                type: "GET",
                func: 'register',
                // data: ({msisdn: phone}),
                dataType: 'json',
                success: function (result) {
                    if (result.action == 'redirect') {
                        location.reload();
                    }
                    else if (result.action == 'message') {
                        $("#logout-modal .success").show().text(result.message);
                        btn.prop("disabled", false);
                    }
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    if (xmlHttpRequest.readyState == 0 || xmlHttpRequest.status == 0)
                        return;  // it's not really an error
                    else
                        alert(errorThrown + "-" + textStatus);

                    btn.prop("disabled", false);
                }
            });
        }
    });
});

/**** parallax *********************************************************************************************************/

$(document).ready(function () {
    var pageId = $('body').attr('id');
    console.log(pageId);
    if (pageId == 'landing-page') {
        var scrolled = $(window).scrollTop();
        var height = $('#title-section').height();
        var half = height / 2;

        if (scrolled < half)
            $("#site-header").addClass('transparent');
    }
});

$(document).ready(function () {
    $(window).scroll(function () {
        var scrolled = $(window).scrollTop();
        $('.parallax-item').each(function (index, element) {
            var height = $(this).height();
            var speed = $(this).attr('data-speed');
            speed = parseFloat(speed);

            if ($(this).hasClass('p-plus'))
                var operator = 1;
            else
                var operator = -1;

            var visible = isInViewport(this);
            if (visible) {
                // parallax items
                $(this).css('background-position', 'center bottom ' + (operator * parseInt(scrolled * speed)) + 'px');

                // handle top menu bg
                var half = height / 2;
                if (scrolled > half)
                    $("#site-header").removeClass('transparent');
                else
                    $("#site-header").addClass('transparent');

                // fade out text
                var opacity = (height - scrolled) / height;
                if (opacity < 1)
                    $("#title-section h4").css('opacity', opacity - 0.45);
                else
                    $("#title-section h4").css('opacity', 1);
            }
        });
    })
});

function isInViewport(node) {
    var rect = node.getBoundingClientRect();
    return (
        (rect.height > 0 || rect.width > 0) &&
        rect.bottom >= 0 &&
        rect.right >= 0 &&
        rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.left <= (window.innerWidth || document.documentElement.clientWidth)
    )
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    document.cookie = name+'=; Max-Age=-99999999;';
}

/**** slick ************************************************************************************************************/

$(document).ready(function () {
    $('.slick-carousel').slick({
        infinite: false,
        variableWidth: true,
        slidesToShow: 18,
        rtl: rtlDirection,
        nextArrow: '<button type="button" class="slick-next"><i class="material-icons">chevron_right</i></button>',
        prevArrow: '<button type="button" class="slick-prev"><i class="material-icons">chevron_left</i></button>',

        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 12
            }

        }, {

            breakpoint: 600,
            settings: {
                slidesToShow: 8
            }

        }, {

            breakpoint: 300,
            settings: {
                slidesToShow: 4
            }
        }]
    });
});

$(document).ready(function () {
    $('.slick-related-apps-carousel').slick({
        infinite: true,
        variableWidth: true,
        slidesToShow: 9,
        rtl: rtlDirection,
        nextArrow: '<button type="button" class="slick-next"><i class="material-icons">chevron_right</i></button>',
        prevArrow: '<button type="button" class="slick-prev"><i class="material-icons">chevron_left</i></button>',

        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 6
            }

        }, {

            breakpoint: 600,
            settings: {
                slidesToShow: 3
            }

        }, {

            breakpoint: 300,
            settings: {
                slidesToShow: 4
            }
        }]
    });
});

/*************** Number Converter *****************************************************************************************/

// persian numbers
    (function ($) {
        $.fn.PersianNumber = function (number) {
            var digits = [], r;
            do {
                r = number % 10;
                number = (number - r) / 10;
                digits.unshift(['&#', r + 1776, ';'].join(''));
            } while (number > 0);

            return digits.join('');
        };
    })(jQuery);

// english numbers
    (function ($) {
        $.fn.EnglishNumber = function (str) {
            return String(str.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function (d) {
                return d.charCodeAt(0) - 1632;
            }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function (d) {
                return d.charCodeAt(0) - 1776;
            }));
        };
    })(jQuery);

    /*************** Add to Fav *****************************************************************************************/
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': token
            }
        });

        $('#add-favorite').click(function () {
            console.log('add fav clicked');
            var user_id = $(this).data('user');
            var game_id = $(this).data('game');
            console.log('user id is: '+ user_id);
            if(!user_id){
                $('#login-modal').modal('show');
            }else{
                $.ajax({
                    type:'POST',
                    url: baseurl + '/apps/add-favorite',
                    data: ({
                        user_id: user_id,
                        game_id: game_id
                    }),
                    dataType: 'json',
                    success: function (result) {
                        console.log(result);
                        $('.favorite-icon').addClass('selected');
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log('failed');
                    }
                });
            }


        })
    })

    /********************************Show login modal******************************************/
    $(document).ready(function () {
        $('.show-login-modal').click(function (event) {
            var user_id = $(this).data('user');
            if(!user_id){
                event.preventDefault();
                $('#login-modal').modal('show');
            }
        })
    })

    /*******************************Change language********************************************/
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': token
            }
        });

        $('.change-language').change(function () {
            var lang = $('.change-language :selected').val();
            window.location.href = window.location.href.replace( /[\?#].*|$/, "?lang="+lang );
        })
        $('.change-xs-language').change(function () {
            var lang = $('.change-xs-language :selected').val();
            window.location.href = window.location.href.replace( /[\?#].*|$/, "?lang="+lang );
        })
    })

/***************************Disapear language selection*****************************************/

/*$(document).ready(function () {
    $('body').on('scroll', function (e){
        if ($('#landing-page').has($(e.target)).length){
           console.log('scroll down');
        }
    });
})*/


