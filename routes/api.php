<?php

// User
Route::group(['prefix' => 'user'], function () {
    // SignUp
    Route::group(['prefix' => 'signup'], function (){

        Route::post('get-phone', 'API\UserController@getPhone');

        // MTN
        Route::group(['prefix' => 'mtn'], function (){
            // APP
            Route::group(['prefix' => 'app'], function (){
                Route::post('/', 'API\UserMtnController@signUpApp');
            });
        });

        // MCI
        Route::group(['prefix' => 'mci'], function (){
            Route::post('verify', 'API\UserMciController@verify');

            // Notification
            Route::group(['prefix' => 'notifications'], function (){
                Route::get('delivery', 'API\UserMciController@delivery');
                Route::get('income', 'API\UserMciController@incomeMessage');
                Route::get('notify', 'API\UserMciController@notification');
            });
        });
    });

    // Verify
    Route::group(['prefix' => 'verify'], function (){
        // MTN
        Route::group(['prefix' => 'mtn'], function (){
            // APP
            Route::group(['prefix' => 'app'], function (){
                Route::post('code', 'API\UserController@mtnVerifyCode');
            });

            // WEB
            Route::group(['prefix' => 'web'], function (){
                Route::post('payload', 'API\UserMtnController@mtnVerifyPayload');
            });
        });
    });

    // Subscription
    Route::group(['prefix' => 'subscription'], function (){
        // MTN
        Route::group(['prefix' => 'mtn'], function (){
            // WEB
            Route::group(['prefix' => 'web'], function (){
                Route::post('check', 'API\UserMtnController@mtnCheckSubscription');
            });
        });

        // Tigo
        Route::group(['prefix' => 'tigo'], function (){
            Route::post('opt-in', 'API\UserTigoController@optIn');
            Route::post('opt-out', 'API\UserTigoController@optOut');
            Route::post('incoming-message', 'API\UserTigoController@incomingMessage');
            Route::get('test', 'API\UserTigoController@test');
        });

        //GET api/user/subscription/cg-redirect?
        Route::get('cg-redirect','UserController@CGRedirect')->middleware('VerifyDotSignature');
    });

    Route::get('setting', 'API\UserController@setting');
});

// Settings
Route::group(['prefix' => 'settings'], function () {
    Route::get('force-update/{device}/{version}', 'API\SettingController@forceUpdate')->where('device', '[1-2]');
    Route::get('general-light', 'API\SettingController@GeneralLight');
});


Route::group(['middleware'=> 'api.token'], function(){
    // Users
    Route::group(['prefix' => 'user'], function () {
        // Self
        Route::group(['prefix' => 'self'], function (){
            Route::get('profile', 'API\UserController@profile');
            Route::post('update', 'API\UserController@update');
            Route::delete('remove-image', 'API\UserController@removeImage');

            // UnSubscribe
            Route::group(['prefix' => 'unsubscribe'], function (){
                Route::get('mtn', 'API\UserController@unSubscribeMTN')->name('web-unsubscribe');
            });
        });

        // Logout
        Route::get('logout', 'API\UserController@logout');

        // Notification
        Route::group(['prefix' => 'notification'], function (){
            Route::get('list', 'API\NotificationController@index');
            Route::post('send-token', 'API\NotificationController@sendToken');

            // Status
            Route::group(['prefix' => 'status'], function (){
                Route::put('set', 'API\UserController@setNotificationStatus');
            });
        });
    });

    // Home
    Route::group(['prefix' => 'home'], function () {
        Route::get('/', 'API\HomeController@index');
    });

    // Explore
    Route::group(['prefix' => 'explore'], function () {
        Route::get('/', 'API\ExploreController@index');
    });

    // Categories
    Route::group(['prefix' => 'categories'], function () {
        Route::get('tree', 'API\CategoryController@tree');
        Route::get('root', 'API\CategoryController@root');
        Route::get('bookmarked', 'API\CategoryController@bookmarked');
    });


    // Posts
    Route::group(['prefix' => 'posts'], function () {
        Route::get('list', 'API\PostController@index');
        Route::get('details/{id}', 'API\PostController@details');
        Route::get('bookmarked', 'API\PostController@bookmarked');
        Route::post('increment', 'API\PostController@increment');
        Route::get('related/{id}', 'API\PostController@related');
        Route::get('tag/{name}', 'API\PostController@byTag');
        Route::get('category/{id}', 'API\PostController@byCategory');
    });

    // Bookmarks
    Route::group(['prefix' => 'bookmarks'], function () {
        Route::post('/add', 'API\BookmarkController@store');
        Route::post('/destroy', 'API\BookmarkController@destroy');
    });

    // Likes
    Route::group(['prefix' => 'likes'], function () {
        Route::post('/add', 'API\LikeController@store');
        Route::post('/destroy', 'API\LikeController@destroy');
    });

    // News
    Route::group(['prefix' => 'news'], function () {
        Route::get('list', 'API\NewsController@index');
        Route::post('increment', 'API\NewsController@increment');
    });

    // Search
    Route::group(['prefix' => 'search'], function () {
        Route::get('posts', 'API\SearchController@posts');
    });

    // Settings
    Route::group(['prefix' => 'settings'], function () {
        Route::get('general', 'API\SettingController@general');
    });
});