<?php

use Illuminate\Routing\Route as IlluminateRoute;
use App\Classes\CaseInsensitiveUriValidator;
use Illuminate\Routing\Matching\UriValidator;

$validators = IlluminateRoute::getValidators();
$validators[] = new CaseInsensitiveUriValidator;
IlluminateRoute::$validators = array_filter($validators, function ($validator) {
    return get_class($validator) != UriValidator::class;
});

// Auth
Route::group(['prefix' => 'auth'], function () {
    Route::get('/login', 'Auth\LoginController@showLoginForm');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/logout', 'Auth\LoginController@logout');
    Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
    Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
});


Route::group(['middleware' => 'admin'], function () {
    // Home
    Route::get('/', 'HomeController@index')->name('dashboard.index');

    // Tags
    Route::get('/tags/json', 'TagController@json');

    // Categories
    Route::group(['prefix' => 'categories'], function () {
        Route::get('/', 'CategoryController@index')->name('dashboard.categories.index');
        Route::get('/add', 'CategoryController@add')->name('dashboard.categories.add');
        Route::post('/store', 'CategoryController@store')->name('dashboard.categories.store');
        Route::get('/edit/{id}', 'CategoryController@edit')->name('dashboard.categories.edit');
        Route::put('/update', 'CategoryController@update')->name('dashboard.categories.update');
        Route::post('/delete', 'CategoryController@destroy')->name('dashboard.categories.destroy');
    });

    // Tags
    Route::group(['prefix' => 'tags'], function () {
        Route::get('/', 'TagController@index')->name('dashboard.tags.index');
        Route::get('/edit/{id}', 'TagController@edit')->name('dashboard.tags.edit');
        Route::put('/update', 'TagController@update')->name('dashboard.tags.update');
        Route::post('/delete', 'TagController@destroy')->name('dashboard.tags.destroy');
    });

    // Games
    Route::group(['prefix' => 'games'], function () {
        Route::get('/', 'GameController@index')->name('dashboard.games.index');
        Route::get('/add', 'GameController@add')->name('dashboard.games.add');
        Route::post('/store', 'GameController@store')->name('dashboard.games.store');
        Route::get('/edit/{id}', 'GameController@edit')->name('dashboard.games.edit');
        Route::put('/update', 'GameController@update')->name('dashboard.games.update');
        Route::post('/delete', 'GameController@destroy')->name('dashboard.games.destroy');

        Route::get('/push/{id}', 'GameController@push')->name('dashboard.games.push');
        Route::get('/publish/{id}', 'GameController@publish')->name('dashboard.games.publish');
        Route::get('/unpublish/{id}', 'GameController@unPublish')->name('dashboard.games.unpublish');
    });

    // Audios
    Route::group(['prefix' => 'podcasts'], function () {
        Route::get('/', 'PodcastController@index')->name('dashboard.podcasts.index');
        Route::get('/add', 'PodcastController@add')->name('dashboard.podcasts.add');
        Route::post('/store', 'PodcastController@store')->name('dashboard.podcasts.store');
        Route::get('/edit/{id}', 'PodcastController@edit')->name('dashboard.podcasts.edit');
        Route::put('/update', 'PodcastController@update')->name('dashboard.podcasts.update');
        Route::post('/delete', 'PodcastController@destroy')->name('dashboard.podcasts.destroy');

        Route::get('/push/{id}', 'PodcastController@push')->name('dashboard.podcasts.push');
        Route::get('/publish/{id}', 'PodcastController@publish')->name('dashboard.podcasts.publish');
        Route::get('/unpublish/{id}', 'PodcastController@unPublish')->name('dashboard.podcasts.unpublish');
    });

    // Users
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', 'UserController@index')->name('dashboard.users.index');
        Route::get('/search', 'UserController@search')->name('dashboard.users.index.search');
        Route::post('/activate', 'UserController@activate')->name('dashboard.users.update.activate');
        Route::post('/deactivate', 'UserController@deactivate')->name('dashboard.users.update.deactivate');
    });

    // Admins
    Route::group(['prefix' => 'admins'], function () {
        Route::get('/', 'AdminController@index')->name('dashboard.admins.index');
        Route::get('/add', 'AdminController@add')->name('dashboard.admins.add');
        Route::post('/store', 'AdminController@store')->name('dashboard.admins.store');
        Route::get('/edit/{id}', 'AdminController@edit')->name('dashboard.admins.edit');
        Route::put('/update', 'AdminController@update')->name('dashboard.admins.update');

        Route::group(['prefix' => 'self'], function () {
            Route::get('edit', 'AdminController@selfEdit')->name('dashboard.admins.self.edit');
            Route::put('update', 'AdminController@selfUpdate')->name('dashboard.admins.self.update');
        });
    });

    // Roles
    Route::group(['prefix' => 'roles'], function () {
        Route::get('/', 'RoleController@index')->name('dashboard.roles.index');
        Route::get('/add', 'RoleController@add')->name('dashboard.roles.add');
        Route::post('/add', 'RoleController@store')->name('dashboard.roles.store');
        Route::get('/edit/{id}', 'RoleController@edit')->name('dashboard.roles.edit');
        Route::put('/update', 'RoleController@update')->name('dashboard.roles.update');
    });

    // Media
    Route::group(['prefix' => 'media'], function () {
        Route::get('/', 'MediaController@index')->name('dashboard.media.index');
        Route::get('/add', 'MediaController@add')->name('dashboard.media.add');
        Route::post('/store', 'MediaController@store')->name('dashboard.media.store');
        Route::get('/edit/{id}', 'MediaController@edit')->name('dashboard.media.edit');
        Route::put('/update', 'MediaController@update')->name('dashboard.media.update');
        Route::post('/destroy', 'MediaController@destroy')->name('dashboard.media.destroy');
    });

    // Ads
    Route::group(['prefix' => 'ads'], function () {
        Route::post('/sort', 'AdsController@sort')->name('dashboard.ads.sort');

        Route::get('/', 'AdsController@index')->name('dashboard.ads.index');
        Route::get('/add', 'AdsController@add')->name('dashboard.ads.add');
        Route::post('/store', 'AdsController@store')->name('dashboard.ads.store');
        Route::get('/edit/{id}', 'AdsController@edit')->name('dashboard.ads.edit');
        Route::put('/update', 'AdsController@update')->name('dashboard.ads.update');
        Route::post('/destroy', 'AdsController@destroy')->name('dashboard.ads.destroy');

        Route::get('/publish/{id}', 'AdsController@publish')->name('dashboard.ads.publish');
        Route::get('/unpublish/{id}', 'AdsController@unPublish')->name('dashboard.ads.unpublish');
    });

    // Force Update
    Route::group(['prefix' => 'force-update'], function () {
        Route::get('/', 'ForceUpdateController@index')->name('dashboard.force-update.index');
        Route::put('/update', 'ForceUpdateController@update')->name('dashboard.force-update.update');
    });

    // Pages
    Route::group(['prefix' => 'pages'], function () {
        Route::get('/', 'PageController@index')->name('dashboard.pages.index');
        Route::get('/add', 'PageController@add')->name('dashboard.pages.add');
        Route::post('/store', 'PageController@store')->name('dashboard.pages.store');
        Route::get('/edit/{id}', 'PageController@edit')->name('dashboard.pages.edit');
        Route::put('/update', 'PageController@update')->name('dashboard.pages.update');
        Route::post('/delete', 'PageController@destroy')->name('dashboard.pages.destroy');
    });

    // Subscriptions
    Route::group(['prefix' => 'subscriptions'], function () {
        Route::get('/', 'SubscriptionController@index')->name('dashboard.subscriptions.index');
        Route::put('/update', 'SubscriptionController@update')->name('dashboard.subscriptions.update');
    });

    // Settings
    Route::group(['prefix' => 'settings'], function () {
        Route::get('/', 'SettingsController@index')->name('dashboard.settings.index');
        Route::put('/update', 'SettingsController@update')->name('dashboard.settings.update');
    });
});