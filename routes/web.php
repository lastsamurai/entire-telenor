<?php

use Illuminate\Routing\Route as IlluminateRoute;
use App\Classes\CaseInsensitiveUriValidator;
use Illuminate\Routing\Matching\UriValidator;

$validators = IlluminateRoute::getValidators();
$validators[] = new CaseInsensitiveUriValidator;
IlluminateRoute::$validators = array_filter($validators, function ($validator)
{
    return get_class($validator) != UriValidator::class;
});


/***** frontend ********************************************************************************************************/

// Home
Route::get('/', 'HomeController@index')->name('home');

// Users
Route::group(['prefix' => 'users'], function () {
        Route::post('signup', 'UserController@signUp');
        Route::post('confirm-code', 'UserController@confirmOtp');
        Route::get('unsub', 'UserController@unsubscribe');
        
        Route::get('enter-number','UserController@getPhoneNumber');
        Route::get('confirm-pin','UserController@sendPin');
        Route::post('confirm-pin','UserController@checkPin');
});

// Pages
Route::group(['prefix' => 'pages'], function () {
    Route::get('{slug}', 'PageController@index')->name('pages.detail');
});

// Apps
Route::group(['prefix' => 'apps'], function () {
    Route::get('/details/{id}/{slug?}', 'AppController@details');
    Route::get('/play/{id}', 'AppController@play');
    Route::post('/add-favorite', 'AppController@addFavorite');
});

// Dev
Route::group(['prefix' => 'dev'], function () {
    Route::get('headers', 'DevController@headers');
});

Route::group(['prefix'=>'setting'],function(){
   Route::post('/set-lang','SettingController@setLang');
});
